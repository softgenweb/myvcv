<?php 
   session_start();
   $yr = date('y');
   
   // define('WP_USE_THEMES', true);
   include('../Admin/configuration.php');
   require('../wp-config.php');
   date_default_timezone_set("Asia/Calcutta");
   get_header();



   /*==========Get User data===========*/
	global $conn;
	
   if($_REQUEST['page']=="preview"){
     $aliasname = $_SESSION['username'];
   }else{
    echo  $aliasname = explode('=',$_SERVER['REQUEST_URI'])[1];
    // echo  $aliasname = basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
   }
	
   
	// page_status
   $page = '';//($_REQUEST['page']=="preview")?'':" AND `page_status`=2";

	$sql = mysqli_query($conn, "SELECT * FROM `users` WHERE `username` LIKE '".$aliasname."' $page");

	$result = mysqli_fetch_array($sql);


	$video = mysqli_fetch_array(mysqli_query($conn, "SELECT `video_name` FROM `jobseeker_video` WHERE `user_id`='".$result['id']."' AND `status`=1"));

	$get_skill = mysqli_query($conn, "SELECT * FROM `jobseeker_skills` WHERE `user_id`='".$result['id']."' AND `status`=1 ORDER BY `percent` DESC");
   $check_skill = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `jobseeker_skills` WHERE `user_id`='".$result['id']."' AND `status`=1 ORDER BY `percent` DESC"));

	$get_exp = mysqli_query($conn, "SELECT * FROM `jobseeker_experience` WHERE `user_id`='".$result['id']."' AND `status`=1 ORDER BY `year` DESC");
	$get_exp1 = mysqli_query($conn, "SELECT * FROM `jobseeker_experience` WHERE `user_id`='".$result['id']."' AND `status`=1 ORDER BY `year` DESC");
   $check_exp = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `jobseeker_experience` WHERE `user_id`='".$result['id']."' AND `status`=1 ORDER BY `year` DESC"));


	$get_edu = mysqli_query($conn, "SELECT * FROM `jobseeker_education` WHERE `user_id`='".$result['id']."' AND `status`=1 ORDER BY `year` DESC");

	$get_portfolio = mysqli_query($conn, "SELECT * FROM `jobseeker_portfolio` WHERE `user_id`='".$result['id']."' AND `status`=1 ORDER BY `id` ASC");
   $check_portfolio = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `jobseeker_portfolio` WHERE `user_id`='".$result['id']."' AND `status`=1 ORDER BY `id` ASC"));

	$get_interest = mysqli_fetch_array(mysqli_query($conn, "SELECT `name` FROM `jobseeker_interest` WHERE `user_id`='".$result['id']."' AND `status`=1 "));
	$interest = str_replace(',', '<br>', $get_interest['name']);

   ?>
   <style type="text/css">
      #top-header{
         top: 0px !important;
      }
       #main-header{
           top: 30px !important;
       }
   </style>
               <div id="page-container">
                  <div id="et-main-area">
                     <div id="main-content">
                        <article id="post-208" class="post-208 page type-page status-publish hentry">
                           <div class="entry-content">
                              <div id="et-boc" class="et-boc">
                                 <div class="et_builder_inner_content et_pb_gutters3">
<!-- ============if page is unpublished and shown at public user end====== -->
         <?php if($result['page_status']==1 && $_REQUEST['page']!='preview'){ ?>
                                    <div class="et_pb_section et_pb_section_1 et_pb_equal_columns et_pb_with_background et_pb_section_parallax et_section_specialty">
                                       <div class="et_pb_row">
                                          <div class="et_pb_column et_pb_column_1_2 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough et_pb_column_single">
                                             <div class="et_pb_module et_pb_image et_pb_image_0">
                                                <span class="et_pb_image_wrap "><img src="<?php echo $result['image']!=''?'../Admin/media/user_profile/'.$result['image']:'../Admin/images/img_avatar.png'; ?>" alt="&nbsp;"></span>
                                             </div>
                                              <?php if($result['thumbprint']!=''){ ?>
                                             <div class="et_pb_module et_pb_image et_pb_image_1">
                                                <span class="et_pb_image_wrap "><img src="../Admin/media/user_profile/<?php echo $result['thumbprint']; ?>" alt="&nbsp;" style="width: 100px;"></span>
                                             </div>
                                             <?php } if($result['signature']!=''){ ?>
                                             <div class="et_pb_module et_pb_image et_pb_image_2">
                                                <span class="et_pb_image_wrap "><img src="../Admin/media/user_profile/<?php echo $result['signature']; ?>" alt="&nbsp;" style="width: 100px;"></span>
                                             </div>
                                          <?php } ?>
                                          </div>
                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_1_2 et_pb_column_2   et_pb_specialty_column  et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_row_inner et_pb_row_inner_0">
                                                <div class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_0   et-last-child">
                                                   <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">
                                                      <div class="et_pb_text_inner">
                                                         <!-- <h6><?php echo $result['title_me']; ?></h6> -->
                                                         <h1><?php echo $result['name']; ?></h1>
                                                         <h2>We will be right back</h2>
                                                         <p></p>
                                                      </div>
                                                   </div>
                                                   <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                             </div>
                                             <!-- .et_pb_row_inner -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>

<?php }else{ ?>

<!-- ====================================================================== -->
                                    <div class="et_pb_section et_pb_section_0 et_section_regular et_pb_section_first" data-fix-page-container="on" style="padding-top: 94px;">
                                       <div class="et_pb_row et_pb_row_0">
                                          <?php if($video['video_name']!=''){ ?>
                                          <div class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                             <div class="et_pb_module et_pb_video et_pb_video_0">
                                                <div class="et_pb_video_box">
                                                   <video src="../Admin/media/user_profile/<?php echo $result['id']; ?>/videos/<?php echo $video['video_name']; ?>" controls="">
                                                     
                                                   </video>
                                                </div>
                                             </div>
                                          </div>
                                       <?php } ?>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>

                                    <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_1 et_pb_equal_columns et_pb_with_background et_pb_section_parallax et_section_specialty">
                                       <div class="et_pb_row">
                                          <div class="et_pb_column et_pb_column_1_2 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough et_pb_column_single">
                                             <div class="et_pb_module et_pb_image et_pb_image_0">
                                                <span class="et_pb_image_wrap "><img src="<?php echo $result['image']!=''?'../Admin/media/user_profile/'.$result['image']:'../Admin/images/img_avatar.png'; ?>" alt="&nbsp;"></span>
                                             </div>
                                              <?php if($result['thumbprint']!=''){ ?>
                                             <div class="et_pb_module et_pb_image et_pb_image_1">
                                                <span class="et_pb_image_wrap "><img src="../Admin/media/user_profile/<?php echo $result['thumbprint']; ?>" alt="&nbsp;" style="width: 100px;"></span>
                                             </div>
                                             <?php } if($result['signature']!=''){ ?>
                                             <div class="et_pb_module et_pb_image et_pb_image_2">
                                                <span class="et_pb_image_wrap "><img src="../Admin/media/user_profile/<?php echo $result['signature']; ?>" alt="&nbsp;" style="width: 100px;"></span>
                                             </div>
                                          <?php } ?>
                                          </div>
                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_1_2 et_pb_column_2   et_pb_specialty_column  et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_row_inner et_pb_row_inner_0">
                                                <div class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_0   et-last-child">
                                                   <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">
                                                      <div class="et_pb_text_inner">
                                                         <h6><?php echo $result['title_me']; ?></h6>
                                                         <h1><?php echo $result['name']; ?></h1>
                                                         <!-- <h1>Chhadva</h1> -->
                                                         <p><?php echo $result['about_me']; ?></p>
                                                      </div>
                                                   </div>
                                                   <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                             </div>
                                             <!-- .et_pb_row_inner -->
                                             <div class="et_pb_row_inner et_pb_row_inner_1 et_pb_row_1-4_1-4">
                                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_1  ">
                                                   <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper  et_pb_module ">
                                                      <a class="et_pb_button et_pb_custom_button_icon et_pb_button_0 et_pb_bg_layout_light" href="#contact" data-icon="$">Get In Touch</a>
                                                   </div>
                                                   <?php if($result['skills_title']!=''){ ?>
                                                   <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_left">
                                                      <div class="et_pb_text_inner">
                                                         <h6><?php echo $result['skills_title']; ?></h6>
                                                         <p><strong><span style="font-size: small;"><?php echo $result['skills_description']; ?></span></strong></p>
                                                         <p>&nbsp;
                                                         </p>
                                                      </div>
                                                   </div>
                                                <?php } ?>
                                                   <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <?php if($check_skill>0){ ?>
                                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_2  ">
                                                   <ul class="et_pb_with_border et_pb_module et_pb_counters et_pb_counters_0 et-waypoint et_pb_bg_layout_light">
                                                   	<?php while ($skill = mysqli_fetch_array($get_skill)) {
                                                   	 ?>
                                                      <li class="et_pb_counter_0 et_hover_enabled">
                                                         <span class="et_pb_counter_title"><?php echo $skill['skill_name']; ?></span>
                                                         <span class="et_pb_counter_container">
                                                         <span class="et_pb_counter_amount" style="width: <?php echo $skill['percent']; ?>%;" data-width="<?php echo $skill['percent']; ?>%"><span class="et_pb_counter_amount_number"></span></span>
                                                         <span class="et_pb_counter_amount overlay" style="width: <?php echo $skill['percent']; ?>%;" data-width="<?php echo $skill['percent']; ?>%"><span class="et_pb_counter_amount_number"></span></span>
                                                         </span>
                                                      </li> 	<?php	} ?>
                                                   
                                                   </ul>
                                                   <!-- .et_pb_counters -->
                                                </div>
                                             <?php } ?>
                                                <!-- .et_pb_column -->
                                             </div>
                                             <!-- .et_pb_row_inner -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>

                                                                        <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_9 et_pb_with_background et_section_regular">
                                       <div class="et_pb_row et_pb_row_11">
                                          <div class="et_pb_column et_pb_column_4_4 et_pb_column_29    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                             <div class="et_pb_module et_pb_text et_pb_text_30 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <blockquote>
                                                      <h1 style="text-align: center;"><?php echo $result['quote1']; ?></h1>
                                                   </blockquote>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular">
                                       <div class="et_pb_row et_pb_row_1">
                                          <div class="et_pb_column et_pb_column_4_4 et_pb_column_3    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                             <div class="et_pb_module et_pb_divider_0 et_pb_space et_pb_divider_hidden">
                                                <div class="et_pb_divider_internal"></div>
                                             </div>
                                             <div class="et_pb_module et_pb_divider_1 et_pb_space et_pb_divider_hidden">
                                                <div class="et_pb_divider_internal"></div>
                                             </div>
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>

                                    <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_specialty">
                                       <?php if($result['experience_title']!=''){ ?>
                                       <div class="et_pb_row">
                                          <div class="et_pb_column et_pb_column_1_2 et_pb_column_4    et_pb_css_mix_blend_mode_passthrough et_pb_column_single">
                                             <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <h6>My Experience</h6>
                                                   <h2><?php echo $result['experience_title']; ?></h2>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                             <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <p><span style="font-size: 14px;"><?php echo $result['experience_description']; ?></p>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                             <?php if($check_exp>0){ ?>
                                             <div class="et_pb_module et_pb_slider et_pb_slider_0 et_pb_slider_fullwidth_off et_pb_bg_layout_dark">
                                                <div class="et_pb_slides">
                                                   <div class="et_pb_slide et_pb_slide_0 et_pb_bg_layout_dark et_pb_media_alignment_center et-pb-active-slide">
                                                      <div class="et_pb_container clearfix" style="height: 224px;">
                                                         <div class="et_pb_slider_container_inner">
                                                            <div class="et_pb_slide_description">
                                                               <div class="et_pb_slide_content">
                                                                  <ul class="et_post_gallery clearfix">
                                                             <?php while ($expr =mysqli_fetch_assoc($get_exp)) {
                                                   							 ?>
                                                                     <li class="et_gallery_item et_pb_gallery_image">
                                                                        <a href="../Admin/media/user_profile/<?php echo $expr['logo']; ?>" title="&nbsp;">
                                                                        <span class="et_portfolio_image">
                                                                        <img width="226" height="100" src="../Admin/media/user_profile/<?php echo $expr['logo']; ?>" class="attachment-et-pb-portfolio-image size-et-pb-portfolio-image" alt="">
                                                                        <span class="et_overlay"></span>
                                                                        </span>
                                                                        </a>
                                                                     </li>
                                                                 <?php } ?>
                                                                  </ul>
                                                               </div>
                                                            </div>
                                                            <!-- .et_pb_slide_description -->
                                                         </div>
                                                      </div>
                                                      <!-- .et_pb_container -->
                                                   </div>
                                                   <!-- .et_pb_slide -->
                                                   <div class="et_pb_slide et_pb_slide_1 et_pb_bg_layout_dark et_pb_media_alignment_center">
                                                      <div class="et_pb_container clearfix" style="height: 224px;">
                                                         <div class="et_pb_slider_container_inner">
                                                            <!-- <div class="et_pb_slide_description">
                                                               <div class="et_pb_slide_content">
                                                                  <ul class="et_post_gallery clearfix">
                                                                     <li class="et_gallery_item et_pb_gallery_image">
                                                                        <a href="https://www.myvcv.in/wp-content/uploads/2019/05/logo_07.png" title="logo_07">
                                                                        <span class="et_portfolio_image">
                                                                        <img width="226" height="100" src="https://www.myvcv.in/wp-content/uploads/2019/05/logo_07.png" class="attachment-et-pb-portfolio-image size-et-pb-portfolio-image" alt="">
                                                                        <span class="et_overlay"></span>
                                                                        </span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="et_gallery_item et_pb_gallery_image">
                                                                        <a href="https://www.myvcv.in/wp-content/uploads/2019/05/logo_06.png" title="logo_06">
                                                                        <span class="et_portfolio_image">
                                                                        <img width="226" height="100" src="https://www.myvcv.in/wp-content/uploads/2019/05/logo_06.png" class="attachment-et-pb-portfolio-image size-et-pb-portfolio-image" alt="">
                                                                        <span class="et_overlay"></span>
                                                                        </span>
                                                                        </a>
                                                                     </li>
                                                                     <li class="et_gallery_item et_pb_gallery_image">
                                                                        <a href="https://www.myvcv.in/wp-content/uploads/2019/05/logo_01.png" title="logo_01">
                                                                        <span class="et_portfolio_image">
                                                                        <img width="226" height="100" src="https://www.myvcv.in/wp-content/uploads/2019/05/logo_01.png" class="attachment-et-pb-portfolio-image size-et-pb-portfolio-image" alt="">
                                                                        <span class="et_overlay"></span>
                                                                        </span>
                                                                        </a>
                                                                     </li>
                                                                  </ul>
                                                               </div>
                                                            </div> -->
                                                            <!-- .et_pb_slide_description -->
                                                         </div>
                                                      </div>
                                                      <!-- .et_pb_container -->
                                                   </div>
                                                   <!-- .et_pb_slide -->
                                                </div>
                                                <!-- .et_pb_slides -->
                                                <!-- <div class="et-pb-slider-arrows"><a class="et-pb-arrow-prev" href="#" style="color: inherit;"><span>Previous</span></a><a class="et-pb-arrow-next" href="#" style="color: inherit;"><span>Next</span></a></div> -->
                                                <div class="et-pb-controllers"><a href="#" class="et-pb-active-control">1</a><a href="#">2</a></div>
                                             </div>
                                          <?php } ?>
                                             <!-- .et_pb_slider -->
                                          </div>
                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_1_2 et_pb_column_5   et_pb_specialty_column  et_pb_css_mix_blend_mode_passthrough">
                                          	<?php while($expr1 = mysqli_fetch_array($get_exp1)) { ?>
                                             <div class="et_pb_with_border et_pb_row_inner et_pb_row_inner_2 et_pb_gutters2 et_pb_row_1-6_1-6_1-6">
                                                <div class="et_pb_column et_pb_column_1_6 et_pb_column_inner et_pb_column_inner_3  ">
                                                   <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                      <div class="et_pb_text_inner">
                                                         <h3><?php echo $expr1['company_name']; ?></h3>
                                                      </div>
                                                   </div>
                                                   <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div class="et_pb_column et_pb_column_1_6 et_pb_column_inner et_pb_column_inner_4  ">
                                                   <div class="et_pb_module et_pb_text et_pb_text_5 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                      <div class="et_pb_text_inner">
                                                         <h4><?php echo $expr1['job_title']; ?></h4>
                                                         <p><?php echo $expr1['description']; ?></p>
                                                      </div>
                                                   </div>
                                                   <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div class="et_pb_column et_pb_column_1_6 et_pb_column_inner et_pb_column_inner_5  ">
                                                   <div class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                      <div class="et_pb_text_inner">
                                                         <p><?php echo $expr1['year']; ?></p>
                                                      </div>
                                                   </div>
                                                   <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                             </div>
                                         <?php } ?>
                                          
                                             <!-- .et_pb_row_inner -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                    <?php } ?>
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_4 et_pb_with_background et_section_regular">
                                       <div class="et_pb_row et_pb_row_2">
                                          <div class="et_pb_column et_pb_column_4_4 et_pb_column_6    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                             <div class="et_pb_module et_pb_divider_2 et_pb_space et_pb_divider_hidden">
                                                <div class="et_pb_divider_internal"></div>
                                             </div>
                                             <div class="et_pb_module et_pb_divider_3 et_pb_space et_pb_divider_hidden">
                                                <div class="et_pb_divider_internal"></div>
                                             </div>
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_5 et_section_regular">
                                       <div class="et_pb_row et_pb_row_3">
                                          <div class="et_pb_column et_pb_column_4_4 et_pb_column_7    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                             <div class="et_pb_module et_pb_text et_pb_text_16 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <h2>Awards &amp; Education</h2>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                       <?php while ($edu = mysqli_fetch_array($get_edu)) {
                                       	?>
                                       <div class="et_pb_row et_pb_row_4 et_pb_row_1-2_1-4_1-4">
                                          <div class="et_pb_column et_pb_column_1_2 et_pb_column_8    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_text et_pb_text_17 et_pb_bg_layout_light  et_pb_text_align_right">
                                                <div class="et_pb_text_inner">
                                                   <h3><?php echo $edu['institute_name']; ?></h3>
                                                   <h3><?php echo $edu['location']; ?></h3>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                          </div>
                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_1_4 et_pb_column_9    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_text et_pb_text_18 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <h4><?php echo $edu['title']; ?></h4>
                                                   <p><?php echo $edu['description']; ?></p>
                                                   <p>&nbsp;</p>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                          </div>
                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_1_4 et_pb_column_10    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_text et_pb_text_19 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <p><?php echo $edu['year']; ?></p>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                   <?php } ?>
                                       <!-- .et_pb_row -->
                                      
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <div id="my-work" class="et_pb_section et_pb_section_6 
                                    et_pb_with_background et_section_regular">
                                       <div class="et_pb_row et_pb_row_8">
                                          <div class="et_pb_column et_pb_column_1_2 et_pb_column_20    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_text et_pb_text_29 et_pb_bg_layout_dark  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <h2>Documents</h2>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                          </div>
                                            <style type="text/css">
                                       	.img_width{
                                       		width: 80px !important;
                                       		height: 80px !important;
                                       	}
                                       </style>
                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_1_2 et_pb_column_21    et_pb_css_mix_blend_mode_passthrough et_pb_column_empty">
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                         <?php $passport = explode('.', $result['passport']);
                                          $passport_path = ($passport[1]=='pdf')?'../Admin/images/pdf-icon.png':('../Admin/media/user_profile/'.$result['passport']);
                                           ?>
                                       <!-- .et_pb_row -->
                                     
                                       <div class="et_pb_row et_pb_row_9 et_pb_row_6col">
                                          <div class="et_pb_column et_pb_column_1_6 et_pb_column_22    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_image et_pb_image_3">
                                                <span class="et_pb_image_wrap "><img class="img_width" width="80" height="80"  src="<?php echo $passport_path; ?>" alt=""></span>
                                             </div>
                                          </div>
                                            <?php $police = explode('.', $result['police_form']);
                                          $police_path = ($police[1]=='pdf')?'../Admin/images/pdf-icon.png':('../Admin/media/user_profile/'.$result['police_form']);
                                           ?>
                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_1_6 et_pb_column_23    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_image et_pb_image_4">
                                                <span class="et_pb_image_wrap "><img class="img_width" width="80" height="80"  src="<?php echo $police_path; ?>" alt=""></span>
                                             </div>
                                          </div>
                                          <!-- .et_pb_column -->
                                          <?php $ref = explode('.', $result['reference_letter']);
                                          $ref_path = ($ref[1]=='pdf')?'../Admin/images/pdf-icon.png':('../Admin/media/user_profile/'.$result['reference_letter']);
                                           ?>
                                          <div class="et_pb_column et_pb_column_1_6 et_pb_column_24    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_image et_pb_image_5">
                                                <span class="et_pb_image_wrap "><img class="img_width" width="80" height="80" src="<?php echo $ref_path; ?>" alt=""></span>
                                             </div>
                                          </div>
                                          <!-- .et_pb_column -->
                                          <?php $aadhar = explode('.', $result['aadhar']);
                                          $aadhar_path = ($aadhar[1]=='pdf')?'../Admin/images/pdf-icon.png':('../Admin/media/user_profile/'.$result['aadhar']);
                                           ?>
                                          <div class="et_pb_column et_pb_column_1_6 et_pb_column_25    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_image et_pb_image_6">
                                                <span class="et_pb_image_wrap "><img class="img_width" src="<?php echo $aadhar_path; ?>" alt=""></span>
                                             </div>
                                          </div>
                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_1_6 et_pb_column_26    et_pb_css_mix_blend_mode_passthrough">
                                          	   <?php $pancard = explode('.', $result['pancard']);
                                          $pancard_path = ($pancard[1]=='pdf')?'../Admin/images/pdf-icon.png':('../Admin/media/user_profile/'.$result['pancard']);
                                           ?>
                                             <div class="et_pb_module et_pb_image et_pb_image_7">
                                                <span class="et_pb_image_wrap "><img class="img_width" src="<?php echo $pancard_path; ?>" alt=""></span>
                                             </div>
                                          </div>
                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_1_6 et_pb_column_27    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_image et_pb_image_8">
                                                <span class="et_pb_image_wrap "><img class="img_width" src="https://www.myvcv.in/wp-content/uploads/2019/05/logo_02.png" alt=""></span>
                                             </div>
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_7 et_pb_with_background et_section_regular">
                                       <div class="et_pb_row et_pb_row_10">
                                          <div class="et_pb_column et_pb_column_4_4 et_pb_column_28    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                             <div class="et_pb_module et_pb_divider_4 et_pb_space et_pb_divider_hidden">
                                                <div class="et_pb_divider_internal"></div>
                                             </div>
                                             <div class="et_pb_module et_pb_divider_5 et_pb_space et_pb_divider_hidden">
                                                <div class="et_pb_divider_internal"></div>
                                             </div>
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <div id="portfolio" class="et_pb_section et_pb_section_8 et_pb_fullwidth_section et_section_regular">
                                       <?php if($check_portfolio>0){ ?>
                                       <div class="et_pb_module et_pb_fullwidth_portfolio et_pb_fullwidth_portfolio_0 et_pb_bg_layout_light et_pb_fullwidth_portfolio_carousel" data-auto-rotate="off" data-auto-rotate-speed="7000">
                                          <h2 class="et_pb_portfolio_title">My Portfolio</h2>
                                          <div class="et_pb_portfolio_items clearfix columns-4" data-portfolio-columns="" style="height: 100.938px;">
                                             <div class="et-pb-slider-arrows" style="display: none;"><a class="et-pb-arrow-prev" href="#"><span>Previous</span></a><a class="et-pb-arrow-next" href="#"><span>Next</span></a></div>
                                             <div class="et_pb_carousel_group active">
                                             	<?php while ($portfolio = mysqli_fetch_array($get_portfolio)) {
                                             	 ?>
                                                <div id="post-232" class="et_pb_portfolio_item et_pb_grid_item  post-232 project type-project status-publish has-post-thumbnail hentry position_1 first_in_row on_last_row" style="height: 100.938px; width: 25%; max-width: 25%;">
                                                   <div class="et_pb_portfolio_image landscape" style="">
                                                      <img src="../Admin/media/user_profile/<?php echo $portfolio['portfolio_img']; ?>" alt="uix">
                                                      <div class="meta">
                                                         <a href="javascript:;">
                                                            <span class="et_overlay"></span>																			
                                                            <h3 class="et_pb_module_header"><?php echo $portfolio['portfolio_title']; ?></h3>
                                                            <p class="post-meta"><?php echo $portfolio['portfolio_description']; ?></p>
                                                         </a>
                                                      </div>
                                                   </div>
                                                </div>
                                                  <?php } ?>
                                             </div>
                                          </div>
                                          <!-- .et_pb_portfolio_items -->
                                       </div>
                                    <?php } ?>
                                       <!-- .et_pb_fullwidth_portfolio -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_9 et_pb_with_background et_section_regular">
                                       <div class="et_pb_row et_pb_row_11">
                                          <div class="et_pb_column et_pb_column_4_4 et_pb_column_29    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                             <div class="et_pb_module et_pb_text et_pb_text_30 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <blockquote>
                                                      <h1 style="text-align: center;"><?php echo $result['quote2']; ?></h1>
                                                   </blockquote>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_10 et_section_regular">
                                       <div class="et_pb_row et_pb_row_12">
                                          <div class="et_pb_column et_pb_column_4_4 et_pb_column_30    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                             <div class="et_pb_module et_pb_toggle et_pb_toggle_0 et_pb_toggle_item  et_pb_toggle_close">
                                                <h5 class="et_pb_toggle_title">Other Interests</h5>
                                                <div class="et_pb_toggle_content clearfix">
                                                   <p><?php echo $interest; ?></p>
                                                </div>
                                                <!-- .et_pb_toggle_content -->
                                             </div>
                                             <!-- .et_pb_toggle -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <div class="et_pb_section et_pb_section_11 et_pb_with_background et_section_regular">
                                       <div class="et_pb_row et_pb_row_13">
                                          <div class="et_pb_column et_pb_column_4_4 et_pb_column_31    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                             <div class="et_pb_module et_pb_text et_pb_text_31 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <blockquote>
                                                      <h1 style="text-align: center;"><?php echo $result['quote3']; ?></h1>
                                                   </blockquote>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->
                                    <div id="contact" class="et_pb_section et_pb_section_12 et_section_regular">
                                       <div class="et_pb_row et_pb_row_14 et_pb_equal_columns">
                                          <div class="et_pb_column et_pb_column_3_5 et_pb_column_32    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_text et_pb_text_32 et_pb_bg_layout_dark  et_pb_text_align_center">
                                                <div class="et_pb_text_inner">
                                                   <h6>Get In Touch</h6>
                                                   <h2>Ready to Chat?</h2>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                             <ul class="et_pb_module et_pb_social_media_follow et_pb_social_media_follow_0 clearfix et_pb_bg_layout_light  et_pb_text_align_center">
                                             	<?php if($result['facebook']){ ?>
                                                <li class="et_pb_social_media_follow_network_0 et_pb_social_icon et_pb_social_network_link  et-social-facebook et_pb_social_media_follow_network_0"><a target="_blank" href="<?php echo $result['facebook']; ?>" class="icon et_pb_with_border" title="Follow on Facebook" target="_blank"><span class="et_pb_social_media_follow_network_name" aria-hidden="true">Follow</span></a></li>
                                            <?php }
                                             if($result['twitter']){ ?>
                                                <li class="et_pb_social_media_follow_network_1 et_pb_social_icon et_pb_social_network_link  et-social-twitter et_pb_social_media_follow_network_1"><a target="_blank" href="<?php echo $result['twitter']; ?>" class="icon et_pb_with_border" title="Follow on Twitter" target="_blank"><span class="et_pb_social_media_follow_network_name" aria-hidden="true">Follow</span></a></li>
                                                <?php }
                                             if($result['instagram']){ ?>
                                                <li class="et_pb_social_media_follow_network_2 et_pb_social_icon et_pb_social_network_link  et-social-instagram et_pb_social_media_follow_network_2"><a target="_blank" href="<?php echo $result['instagram']; ?>" class="icon et_pb_with_border" title="Follow on LinkedIn" target="_blank"><span class="et_pb_social_media_follow_network_name" aria-hidden="true">Follow</span></a></li>
                                                <?php }
                                             if($result['linkedin']){ ?>
                                                 <li class="et_pb_social_media_follow_network_2 et_pb_social_icon et_pb_social_network_link  et-social-linkedin et_pb_social_media_follow_network_2"><a target="_blank" href="<?php echo $result['linkedin']; ?>" class="icon et_pb_with_border" title="Follow on LinkedIn" target="_blank"><span class="et_pb_social_media_follow_network_name" aria-hidden="true">Follow</span></a></li>
                                            <?php } ?>
                                             </ul>
                                             <!-- .et_pb_counters -->
                                             <div class="et_pb_module et_pb_video et_pb_video_1">
                                                <div class="et_pb_video_box">
                                                   <video src="../Admin/media/user_profile/<?php echo $result['id']; ?>/videos/<?php echo $video['video_name']; ?>" controls="">
                                                      
                                                   </video>
                                                </div>
                                             </div>
                                          </div>



                                          <!-- .et_pb_column -->
                                          <div class="et_pb_column et_pb_column_2_5 et_pb_column_33    et_pb_css_mix_blend_mode_passthrough">
                                             <div class="et_pb_module et_pb_text et_pb_text_33 et_pb_bg_layout_light  et_pb_text_align_left">
                                                <div class="et_pb_text_inner">
                                                   <h3>(m) <?php echo $result['mobile']; ?><br><?php echo $result['email']; ?></h3>
                                                   <ul></ul>
                                                </div>
                                             </div>
                                             <!-- .et_pb_text -->
                                             <div id="et_pb_contact_form_0" class="et_pb_with_border et_pb_module et_pb_contact_form_0 et_pb_contact_form_container clearfix" data-form_unique_num="0">
                                                <div class="et-pb-contact-message"></div>
                                                <div class="et_pb_contact">
                                                   <form class="et_pb_contact_form clearfix" method="post" action="https://www.myvcv.in/sample-page-2/">
                                                      <p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_last" data-id="name" data-type="input">
                                                         <label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Name</label>
                                                         <input type="text" id="et_pb_contact_name_0" class="input" value="" name="et_pb_contact_name_0" data-required_mark="required" data-field_type="input" data-original_id="name" placeholder="Name">
                                                      </p>
                                                      <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_last" data-id="email" data-type="email">
                                                         <label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Email Address</label>
                                                         <input type="text" id="et_pb_contact_email_0" class="input" value="" name="et_pb_contact_email_0" data-required_mark="required" data-field_type="email" data-original_id="email" placeholder="Email Address">
                                                      </p>
                                                      <p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_last" data-id="message" data-type="text">
                                                         <label for="et_pb_contact_message_0" class="et_pb_contact_form_label">Message</label>
                                                         <textarea name="et_pb_contact_message_0" id="et_pb_contact_message_0" class="et_pb_contact_message input" data-required_mark="required" data-field_type="text" data-original_id="message" placeholder="Message"></textarea>
                                                      </p>
                                                      <input type="hidden" value="et_contact_proccess" name="et_pb_contactform_submit_0">
                                                      <input type="text" value="" name="et_pb_contactform_validate_0" class="et_pb_contactform_validate_field">
                                                      <div class="et_contact_bottom_container">
                                                         <button type="submit" class="et_pb_contact_submit et_pb_button et_pb_custom_button_icon" data-icon="$">Send Message</button>
                                                      </div>
                                                      <input type="hidden" id="_wpnonce-et-pb-contact-form-submitted-0" name="_wpnonce-et-pb-contact-form-submitted-0" value="29eb19ec39"><input type="hidden" name="_wp_http_referer" value="/sample-page-2/">
                                                   </form>
                                                </div>
                                                <!-- .et_pb_contact -->
                                             </div>
                                             <!-- .et_pb_contact_form_container -->
                                          </div>
                                          <!-- .et_pb_column -->
                                       </div>
                                       <!-- .et_pb_row -->
                                    </div>
                                    <!-- .et_pb_section -->			
                                 <?php } ?>
                                 </div>
                              </div>
                           </div>
                           <!-- .entry-content -->
                        </article>
                        <!-- .et_pb_post -->
                     </div>
                     <!-- #main-content -->
                     <span class="et_pb_scroll_top et-pb-icon"></span>
                  </div>
                  <!-- #et-main-area -->
               </div>
               <!-- #page-container -->
               <script type="text/javascript"></script>
               <link rel="stylesheet" id="mediaelement-css" href="https://www.myvcv.in/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css?ver=4.2.6-78496d1" type="text/css" media="all">
               <link rel="stylesheet" id="wp-mediaelement-css" href="https://www.myvcv.in/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=5.2.2" type="text/css" media="all">
             
               <!-- <script type="text/javascript" src="https://www.myvcv.in/wp-content/themes/Divi/js/custom.min.js?ver=3.23.3"></script> -->
               <!-- <script type="text/javascript" src="https://www.myvcv.in/wp-content/themes/Divi/core/admin/js/common.js?ver=3.23.3"></script> -->
               <!-- <script type="text/javascript" src="https://www.myvcv.in/wp-includes/js/wp-embed.min.js?ver=5.2.2"></script> -->
               <script type="text/javascript">
                  var mejsL10n = {"language":"en","strings":{"mejs.install-flash":"You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/","mejs.fullscreen-off":"Turn off Fullscreen","mejs.fullscreen-on":"Go Fullscreen","mejs.download-video":"Download Video","mejs.fullscreen":"Fullscreen","mejs.time-jump-forward":["Jump forward 1 second","Jump forward %1 seconds"],"mejs.loop":"Toggle Loop","mejs.play":"Play","mejs.pause":"Pause","mejs.close":"Close","mejs.time-slider":"Time Slider","mejs.time-help-text":"Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.","mejs.time-skip-back":["Skip back 1 second","Skip back %1 seconds"],"mejs.captions-subtitles":"Captions\/Subtitles","mejs.captions-chapters":"Chapters","mejs.none":"None","mejs.mute-toggle":"Mute Toggle","mejs.volume-help-text":"Use Up\/Down Arrow keys to increase or decrease volume.","mejs.unmute":"Unmute","mejs.mute":"Mute","mejs.volume-slider":"Volume Slider","mejs.video-player":"Video Player","mejs.audio-player":"Audio Player","mejs.ad-skip":"Skip ad","mejs.ad-skip-info":["Skip in 1 second","Skip in %1 seconds"],"mejs.source-chooser":"Source Chooser","mejs.stop":"Stop","mejs.speed-rate":"Speed Rate","mejs.live-broadcast":"Live Broadcast","mejs.afrikaans":"Afrikaans","mejs.albanian":"Albanian","mejs.arabic":"Arabic","mejs.belarusian":"Belarusian","mejs.bulgarian":"Bulgarian","mejs.catalan":"Catalan","mejs.chinese":"Chinese","mejs.chinese-simplified":"Chinese (Simplified)","mejs.chinese-traditional":"Chinese (Traditional)","mejs.croatian":"Croatian","mejs.czech":"Czech","mejs.danish":"Danish","mejs.dutch":"Dutch","mejs.english":"English","mejs.estonian":"Estonian","mejs.filipino":"Filipino","mejs.finnish":"Finnish","mejs.french":"French","mejs.galician":"Galician","mejs.german":"German","mejs.greek":"Greek","mejs.haitian-creole":"Haitian Creole","mejs.hebrew":"Hebrew","mejs.hindi":"Hindi","mejs.hungarian":"Hungarian","mejs.icelandic":"Icelandic","mejs.indonesian":"Indonesian","mejs.irish":"Irish","mejs.italian":"Italian","mejs.japanese":"Japanese","mejs.korean":"Korean","mejs.latvian":"Latvian","mejs.lithuanian":"Lithuanian","mejs.macedonian":"Macedonian","mejs.malay":"Malay","mejs.maltese":"Maltese","mejs.norwegian":"Norwegian","mejs.persian":"Persian","mejs.polish":"Polish","mejs.portuguese":"Portuguese","mejs.romanian":"Romanian","mejs.russian":"Russian","mejs.serbian":"Serbian","mejs.slovak":"Slovak","mejs.slovenian":"Slovenian","mejs.spanish":"Spanish","mejs.swahili":"Swahili","mejs.swedish":"Swedish","mejs.tagalog":"Tagalog","mejs.thai":"Thai","mejs.turkish":"Turkish","mejs.ukrainian":"Ukrainian","mejs.vietnamese":"Vietnamese","mejs.welsh":"Welsh","mejs.yiddish":"Yiddish"}};
               </script>
               <script type="text/javascript" src="https://www.myvcv.in/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=4.2.6-78496d1"></script>
               <script type="text/javascript" src="https://www.myvcv.in/wp-includes/js/mediaelement/mediaelement-migrate.min.js?ver=5.2.2"></script>
               <script type="text/javascript">
                  /* <![CDATA[ */
                  var _wpmejsSettings = {"pluginPath":"\/wp-includes\/js\/mediaelement\/","classPrefix":"mejs-","stretching":"responsive"};
                  /* ]]> */
               </script>
               <script type="text/javascript" src="https://www.myvcv.in/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=5.2.2"></script>
  <style type="text/css">
     

.et_pb_section_0.et_pb_section {
   padding-top: 0px;
   padding-right: 0px;
   padding-bottom: 24px;
   padding-left: 0px
}

.et_pb_row_11.et_pb_row {
   margin-top: 3px!important;
   margin-right: auto!important;
   margin-left: auto!important;
   padding-top: 16px;
   padding-right: 0px;
   padding-bottom: 0px;
   padding-left: 0px
}

.et_pb_image_5 {
   text-align: left;
   margin-left: 0
}

.et_pb_image_6 {
   text-align: left;
   margin-left: 0
}

.et_pb_image_7 {
   text-align: left;
   margin-left: 0
}

.et_pb_image_8 {
   text-align: left;
   margin-left: 0
}

.et_pb_section_7.et_pb_section {
   padding-top: 0px;
   padding-right: 0px;
   padding-bottom: 0px;
   padding-left: 0px;
   background-color: #1c1c1c!important
}

.et_pb_row_10 {
   background-image: linear-gradient(90deg, rgba(12, 206, 173, 0) 10%, #e02b20 90%)
}

.et_pb_divider_4 {
   height: 30px
}

.et_pb_divider_4:hover:before {
   border-top-width: px
}

.et_pb_divider_5 {
   height: 30px
}

.et_pb_divider_5:hover:before {
   border-top-width: px
}

.et_pb_fullwidth_portfolio_0 {
   padding-bottom: 0px
}

.et_pb_section_9.et_pb_section {
   padding-top: 0px;
   padding-bottom: 0px;
   margin-bottom: -20px;
   background-color: rgba(224, 124, 118, 0.43)!important
}

.et_pb_text_30 {
   font-family: 'Didact Gothic', Helvetica, Arial, Lucida, sans-serif;
   background-color: rgba(0, 0, 0, 0);
   padding-bottom: 0px!important
}

.et_pb_image_3 {
   text-align: left;
   margin-left: 0
}

.et_pb_text_30 h1 {
   font-family: 'Bad Script', handwriting;
   font-weight: 700;
   letter-spacing: 1px
}

.et_pb_section_10.et_pb_section {
   padding-top: 2px;
   padding-bottom: 1px
}

.et_pb_toggle_0.et_pb_toggle h5,
.et_pb_toggle_0.et_pb_toggle h1.et_pb_toggle_title,
.et_pb_toggle_0.et_pb_toggle h2.et_pb_toggle_title,
.et_pb_toggle_0.et_pb_toggle h3.et_pb_toggle_title,
.et_pb_toggle_0.et_pb_toggle h4.et_pb_toggle_title,
.et_pb_toggle_0.et_pb_toggle h6.et_pb_toggle_title {
   font-family: 'Didact Gothic', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-variant: small-caps;
   font-size: 20px;
   letter-spacing: 3px;
   line-height: 1.4em;
   text-shadow: 0.08em 0.08em 0.08em rgba(0, 0, 0, 0.4)
}

.et_pb_toggle_0.et_pb_toggle.et_pb_toggle_close h5,
.et_pb_toggle_0.et_pb_toggle.et_pb_toggle_close h1.et_pb_toggle_title,
.et_pb_toggle_0.et_pb_toggle.et_pb_toggle_close h2.et_pb_toggle_title,
.et_pb_toggle_0.et_pb_toggle.et_pb_toggle_close h3.et_pb_toggle_title,
.et_pb_toggle_0.et_pb_toggle.et_pb_toggle_close h4.et_pb_toggle_title,
.et_pb_toggle_0.et_pb_toggle.et_pb_toggle_close h6.et_pb_toggle_title {
   font-family: 'Didact Gothic', Helvetica, Arial, Lucida, sans-serif;
   text-transform: uppercase;
   font-size: 20px;
   letter-spacing: 3px;
   text-shadow: 0.08em 0.08em 0.08em rgba(0, 0, 0, 0.4)
}

.et_pb_toggle_0.et_pb_toggle {
   text-shadow: 0.08em 0.08em 0.08em rgba(0, 0, 0, 0.4)
}

.et_pb_section_11.et_pb_section {
   padding-top: 0px;
   padding-bottom: 0px;
   margin-bottom: -20px;
   background-color: rgba(224, 124, 118, 0.43)!important
}

.et_pb_row_13.et_pb_row {
   margin-top: 3px!important;
   margin-right: auto!important;
   margin-left: auto!important;
   padding-top: 16px;
   padding-right: 0px;
   padding-bottom: 0px;
   padding-left: 0px
}

.et_pb_text_31 {
   font-family: 'Didact Gothic', Helvetica, Arial, Lucida, sans-serif;
   background-color: rgba(0, 0, 0, 0);
   padding-bottom: 0px!important
}

.et_pb_text_31 h1 {
   font-family: 'Bad Script', handwriting;
   font-weight: 700;
   letter-spacing: 1px
}

.et_pb_section_12.et_pb_section {
   padding-top: 0px;
   padding-bottom: 0px
}

.et_pb_row_14.et_pb_row {
   margin-top: 20px!important;
   margin-right: auto!important;
   margin-left: auto!important;
   padding-top: 0;
   padding-bottom: 0
}

.et_pb_row_14 {
   width: 100%;
   max-width: 100%
}

.et_pb_image_4 {
   text-align: left;
   margin-left: 0
}

.et_pb_row_9.et_pb_row {
   padding-top: 0;
   padding-right: 0px;
   padding-bottom: 25.2625px;
   padding-left: 0px
}

.et_pb_text_32 h2 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 56px;
   line-height: 1.4em
}

.et_pb_text_24 p {
   line-height: 1.8em
}

.et_pb_text_18 h4 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 14px;
   line-height: 1.8em
}

.et_pb_text_19 p {
   line-height: 1.8em
}

.et_pb_text_19 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_row_0.et_pb_row {
   padding-top: 3px;
   padding-right: 0px;
   padding-bottom: 0px;
   padding-left: 0px
}

.et_pb_text_21.et_pb_text {
   color: #999999!important
}

.et_pb_text_21 p {
   line-height: 1.8em
}

.et_pb_text_21 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_21 h4 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 14px;
   line-height: 1.8em
}

.et_pb_text_22 p {
   line-height: 1.8em
}

.et_pb_text_22 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_23 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_24.et_pb_text {
   color: #999999!important
}

.et_pb_text_24 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_29 h6 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   color: #acacac!important;
   letter-spacing: 1px;
   line-height: 1.8em
}

.et_pb_text_24 h4 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 14px;
   line-height: 1.8em
}

.et_pb_text_25 p {
   line-height: 1.8em
}

.et_pb_text_25 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_26 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_27.et_pb_text {
   color: #999999!important
}

.et_pb_text_27 p {
   line-height: 1.8em
}

.et_pb_text_27 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_27 h4 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 14px;
   line-height: 1.8em
}

.et_pb_text_28 p {
   line-height: 1.8em
}

.et_pb_text_28 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_section_6.et_pb_section {
   padding-top: 25px;
   padding-right: 0px;
   padding-bottom: 0px;
   padding-left: 0px;
   background-color: #1c1c1c!important
}

.et_pb_row_8.et_pb_row {
   padding-top: 0;
   padding-right: 0px;
   padding-bottom: 5px;
   padding-left: 0px
}

.et_pb_text_29 h2 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 36px;
   line-height: 1.4em
}

.et_pb_column_32 {
   background-color: #e02b20;
   padding-top: 8vw;
   padding-right: 8vw;
   padding-bottom: 8vw;
   padding-left: 8vw
}

.et_pb_text_32 h6 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   color: #ffffff!important;
   letter-spacing: 1px;
   line-height: 1.8em
}

.et_pb_text_18 p {
   line-height: 1.8em
}

.et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button {
   text-shadow: 0em 0.1em 0.1em rgba(0, 0, 0, 0.4)
}

.et_pb_contact_field_2.et_pb_contact_field .input:hover,
.et_pb_contact_field_2.et_pb_contact_field .input:hover::placeholder {
   color:
}

.et_pb_contact_field_2.et_pb_contact_field .input:hover::-webkit-input-placeholder {
   color:
}

.et_pb_contact_field_2.et_pb_contact_field .input:hover::-moz-placeholder {
   color:
}

.et_pb_contact_field_2.et_pb_contact_field .input:hover::-ms-input-placeholder {
   color:
}

.et_pb_contact_field_2.et_pb_contact_field .input:focus:hover,
.et_pb_contact_field_2.et_pb_contact_field .input:focus:hover::placeholder {
   color:
}

.et_pb_contact_field_2.et_pb_contact_field .input:focus:hover::-webkit-input-placeholder {
   color:
}

.et_pb_contact_field_2.et_pb_contact_field .input:focus:hover::-moz-placeholder {
   color:
}

.et_pb_contact_field_2.et_pb_contact_field .input:focus:hover::-ms-input-placeholder {
   color:
}

.et_pb_contact_form_0.et_pb_contact_form_container .input,
.et_pb_contact_form_0.et_pb_contact_form_container .input::placeholder,
.et_pb_contact_form_0.et_pb_contact_form_container .input[type=checkbox]+label,
.et_pb_contact_form_0.et_pb_contact_form_container .input[type=radio]+label {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   line-height: 1.8em
}

.et_pb_contact_form_0.et_pb_contact_form_container .input::-webkit-input-placeholder {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   line-height: 1.8em
}

.et_pb_contact_form_0.et_pb_contact_form_container .input::-moz-placeholder {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   line-height: 1.8em
}

.et_pb_contact_form_0.et_pb_contact_form_container .input:-ms-input-placeholder {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   line-height: 1.8em
}

.et_pb_contact_form_0.et_pb_contact_form_container .input,
.et_pb_contact_form_0.et_pb_contact_form_container .input[type="checkbox"]+label i,
.et_pb_contact_form_0.et_pb_contact_form_container .input[type="radio"]+label i {
   border-width: 1px;
   border-color: #dbdbdb
}

.et_pb_contact_field_2.et_pb_contact_field .input:hover,
.et_pb_contact_field_2.et_pb_contact_field .input+label:hover i {
   background-color:
}

body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button {
   color: #ffffff!important;
   border-width: 10px!important;
   border-color: rgba(0, 0, 0, 0);
   border-radius: 4px;
   font-size: 14px;
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif!important;
   font-weight: 700!important;
   background-color: #e02b20
}

body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after {
   margin-left: .3em;
   left: auto;
   margin-left: .3em;
   opacity: 1;
   color:
}

body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after {
   line-height: inherit;
   font-size: inherit!important;
   margin-left: -1em;
   left: auto
}

.et_pb_contact_form_0 .input,
.et_pb_contact_form_0 .input[type="checkbox"]+label i,
.et_pb_contact_form_0 .input[type="radio"]+label i {
   background-color: #ffffff
}

.et_pb_contact_form_0 .input:hover,
.et_pb_contact_form_0 .input[type="checkbox"]:hover+label i,
.et_pb_contact_form_0 .input[type="radio"]:hover+label i {
   background-color:
}

.et_pb_contact_form_0 .input:focus:hover {
   background-color:
}

.et_pb_contact_form_0 .input:hover,
.et_pb_contact_form_0 .input:hover::placeholder {
   color:
}

.et_pb_contact_form_0 .input:hover::-webkit-input-placeholder {
   color:
}

.et_pb_contact_form_0 .input:hover::-moz-placeholder {
   color:
}

.et_pb_contact_form_0 .input:hover::-ms-input-placeholder {
   color:
}

.et_pb_contact_form_0 .input:focus:hover,
.et_pb_contact_form_0 .input:focus:hover::placeholder {
   color:
}

.et_pb_contact_form_0 .input:focus:hover::-webkit-input-placeholder {
   color:
}

.et_pb_contact_form_0 .input:focus:hover::-moz-placeholder {
   color:
}

.et_pb_contact_field_2.et_pb_contact_field .input:focus:hover,
.et_pb_contact_field_2.et_pb_contact_field .input:focus+label:hover i {
   background-color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:focus:hover::-ms-input-placeholder {
   color:
}

.et_pb_text_32 {
   margin-bottom: 50px!important
}

.et_pb_contact_field_0.et_pb_contact_field .input:hover::-webkit-input-placeholder {
   color:
}

ul.et_pb_social_media_follow_0 a.icon {
   border-radius: 0px 0px 0px 0px
}

.et_pb_social_media_follow_0 .et_pb_social_icon a {
   box-shadow: 0px 15px 60px -15px #1c1c1c
}

.et_pb_video_1 {
   border-radius: 15px 15px 15px 15px;
   overflow: hidden
}

.et_pb_video_1 .et_pb_video_overlay_hover:hover {
   background-color: rgba(0, 0, 0, .6)
}

.et_pb_column_33 {
   padding-top: 8vw;
   padding-right: 10vw;
   padding-bottom: 8vw
}

.et_pb_text_33 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif
}

.et_pb_text_33 ul li {
   line-height: 2em
}

.et_pb_text_33 ul {
   font-size: 16px;
   line-height: 2em
}

.et_pb_text_33 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_contact_field_0.et_pb_contact_field .input:hover,
.et_pb_contact_field_0.et_pb_contact_field .input+label:hover i {
   background-color:
}

.et_pb_contact_field_0.et_pb_contact_field .input:focus:hover,
.et_pb_contact_field_0.et_pb_contact_field .input:focus+label:hover i {
   background-color:
}

.et_pb_contact_field_0.et_pb_contact_field .input:hover,
.et_pb_contact_field_0.et_pb_contact_field .input:hover::placeholder {
   color:
}

.et_pb_contact_field_0.et_pb_contact_field .input:hover::-moz-placeholder {
   color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:focus:hover::-moz-placeholder {
   color:
}

.et_pb_contact_field_0.et_pb_contact_field .input:hover::-ms-input-placeholder {
   color:
}

.et_pb_contact_field_0.et_pb_contact_field .input:focus:hover,
.et_pb_contact_field_0.et_pb_contact_field .input:focus:hover::placeholder {
   color:
}

.et_pb_contact_field_0.et_pb_contact_field .input:focus:hover::-webkit-input-placeholder {
   color:
}

.et_pb_contact_field_0.et_pb_contact_field .input:focus:hover::-moz-placeholder {
   color:
}

.et_pb_contact_field_0.et_pb_contact_field .input:focus:hover::-ms-input-placeholder {
   color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:hover,
.et_pb_contact_field_1.et_pb_contact_field .input+label:hover i {
   background-color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:focus:hover,
.et_pb_contact_field_1.et_pb_contact_field .input:focus+label:hover i {
   background-color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:hover,
.et_pb_contact_field_1.et_pb_contact_field .input:hover::placeholder {
   color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:hover::-webkit-input-placeholder {
   color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:hover::-moz-placeholder {
   color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:hover::-ms-input-placeholder {
   color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:focus:hover,
.et_pb_contact_field_1.et_pb_contact_field .input:focus:hover::placeholder {
   color:
}

.et_pb_contact_field_1.et_pb_contact_field .input:focus:hover::-webkit-input-placeholder {
   color:
}

.et_pb_text_18 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_20 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_18.et_pb_text {
   color: #999999!important
}

.et_pb_counters .et_pb_counter_2:hover .et_pb_counter_amount {
   background-color:
}

.et_pb_counters .et_pb_counter_1 .et_pb_counter_container {
   background-image: none
}

.et_pb_counter_1 .et_pb_counter_container {
   background-color: #dddddd
}

.et_pb_counter_1 .et_pb_counter_container:hover {
   background-color:
}

.et_pb_counter_1 .et_pb_counter_amount {
   background-color: #0ccead
}

.et_pb_counter_1 .et_pb_counter_amount.overlay {
   color: #0ccead
}

.et_pb_counters .et_pb_counter_1:hover .et_pb_counter_amount {
   background-color:
}

.et_pb_counters .et_pb_counter_1:hover .et_pb_counter_amount.overlay {
   color:
}

.et_pb_counters .et_pb_counter_2 .et_pb_counter_container {
   background-image: none
}

.et_pb_counter_2 .et_pb_counter_container {
   background-color: #dddddd
}

.et_pb_counter_2 .et_pb_counter_container:hover {
   background-color:
}

.et_pb_counter_2 .et_pb_counter_amount {
   background-color: #0ccead
}

.et_pb_counter_2 .et_pb_counter_amount.overlay {
   color: #0ccead
}

.et_pb_counters .et_pb_counter_2:hover .et_pb_counter_amount.overlay {
   color:
}

.et_pb_counters .et_pb_counter_0:hover .et_pb_counter_amount {
   background-color:
}

.et_pb_counters_0.et_pb_counters .et_pb_counter_title {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 12px;
   letter-spacing: 1px;
   line-height: 1.8em
}

.et_pb_counters_0.et_pb_counters .et_pb_counter_amount_number {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   line-height: 1.4em
}

.et_pb_counters_0 .et_pb_counter_container {
   border-width: 6px;
   border-color: #f4f4f4;
   box-shadow: 20px 8px 120px 0px rgba(0, 0, 0, 0.16)
}

.et_pb_counters_0.et_pb_counters {
   min-height: 243px;
   margin-top: 55px!important;
   margin-right: -130px!important
}

.et_pb_counters_0.et_pb_counters .et_pb_counter_amount {
   padding-top: 5px;
   padding-right: 72px
}

.et_pb_section_2.et_pb_section {
   padding-top: 0px;
   padding-right: 0px;
   padding-bottom: 0px;
   padding-left: 0px;
   background-color: #1c1c1c!important
}

.et_pb_row_1 {
   background-image: linear-gradient(90deg, rgba(12, 206, 173, 0) 10%, #e02b20 90%)
}

.et_pb_divider_0 {
   height: 30px
}

.et_pb_divider_0:hover:before {
   border-top-width: px
}

.et_pb_text_17 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_divider_1 {
   height: 30px
}

.et_pb_divider_1:hover:before {
   border-top-width: px
}

.et_pb_counters .et_pb_counter_0:hover .et_pb_counter_amount.overlay {
   color:
}

.et_pb_counter_0 .et_pb_counter_amount.overlay {
   color: #0ccead
}

.et_pb_text_2 h2 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 36px;
   color: #e02b20!important;
   line-height: 1.4em
}

.et_pb_text_0 h6 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   color: #e02b20!important;
   letter-spacing: 1px;
   line-height: 1.8em
}

.et_pb_video_0 {
   padding-top: 0px;
   margin-top: -30px!important;
   margin-bottom: 0px!important;
   transform: scaleX(0.9) scaleY(0.9)
}

.et_pb_video_0 .et_pb_video_overlay_hover:hover {
   background-color: rgba(0, 0, 0, .6)
}

.et_pb_section_1.et_pb_section {
   padding-top: 0px;
   padding-bottom: 0px;
   background-color: #ffffff!important
}

.et_pb_section_1>.et_pb_row {
   width: 100%;
   max-width: 100%
}

.et_pb_column_1 {
   background-image: linear-gradient(90deg, #1c1c1c 60%, #ffffff 60%);
   background-color: #176d0f;
   padding-top: 3vw;
   padding-bottom: 0vw;
   padding-left: 10vw
}

.et_pb_image_0 {
   padding-top: 0px;
   padding-bottom: 0px;
   text-align: left;
   margin-left: 0
}

.et_pb_image_1 {
   width: 13%;
   transform: translateX(2px) translateY(-93px);
   text-align: left;
   margin-left: 0
}

.et_pb_image_2 {
   background-color: rgba(255, 255, 255, 0.45);
   transform: translateX(-19px) translateY(-62px);
   text-align: left;
   margin-left: 0
}

.et_pb_column_2 {
   padding-top: 12vw;
   padding-right: 10vw;
   padding-bottom: 12vw
}

.et_pb_text_0 p {
   line-height: 1.8em
}

.et_pb_text_0 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em;
   max-width: 600px
}

.et_pb_text_0 h1 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 90px;
   line-height: 1.2em
}

.et_pb_row_inner_1.et_pb_row_inner {
   margin-right: 0px!important
}

.et_pb_counter_0 .et_pb_counter_amount {
   background-color: #0ccead
}

.et_pb_row_inner_1 {
   max-width: 80%
}

.et_pb_column .et_pb_row_inner_1 {
   padding-top: 0;
   padding-right: 0px;
   padding-bottom: 0;
   padding-left: 0px
}

.et_pb_button_0_wrapper .et_pb_button_0,
.et_pb_button_0_wrapper .et_pb_button_0:hover {
   padding-top: 15px!important;
   padding-right: 40px!important;
   padding-bottom: 15px!important;
   padding-left: 40px!important
}

body #page-container .et_pb_button_0 {
   color: #ffffff!important;
   border-width: 0px!important;
   border-color: rgba(0, 0, 0, 0);
   border-radius: 0px;
   font-size: 14px;
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif!important;
   font-weight: 700!important;
   background-color: #e02b20
}

body #page-container .et_pb_button_0:hover:after {
   margin-left: .3em;
   left: auto;
   margin-left: .3em;
   opacity: 1;
   color:
}

body #page-container .et_pb_button_0:after {
   line-height: inherit;
   font-size: inherit!important;
   margin-left: -1em;
   left: auto
}

.et_pb_button_0,
.et_pb_button_0:after {
   transition: all 300ms ease 0ms
}

.et_pb_text_1 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_1 h6 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   color: #000000!important;
   letter-spacing: 1px;
   line-height: 1.8em
}

.et_pb_text_1 {
   padding-top: 27px!important;
   margin-top: 30px!important
}

.et_pb_counters .et_pb_counter_0 .et_pb_counter_container {
   background-image: none
}

.et_pb_counter_0 .et_pb_counter_container {
   background-color: #dddddd
}

.et_pb_counter_0 .et_pb_counter_container:hover {
   background-color:
}

.et_pb_section_3.et_pb_section {
   padding-top: 21px;
   padding-right: 0px;
   padding-bottom: 0px;
   padding-left: 0px;
   background-color: #1c1c1c!important
}

.et_pb_contact_form_0 .input:focus:hover::-ms-input-placeholder {
   color:
}

.et_pb_text_2 h6 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   letter-spacing: 1px;
   line-height: 1.8em
}

.et_pb_text_11.et_pb_text {
   color: #999999!important
}

.et_pb_row_inner_5 {
   border-radius: 3px 3px 3px 3px;
   overflow: hidden;
   border-width: 2px
}

.et_pb_text_12 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_12 p {
   line-height: 1.8em
}

.et_pb_text_11 h4 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 14px;
   line-height: 1.8em
}

.et_pb_text_11 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_11 p {
   line-height: 1.8em
}

.et_pb_text_10 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_column .et_pb_row_inner_5 {
   padding-top: 2px;
   padding-right: 5px;
   padding-bottom: 2px;
   padding-left: 5px
}

.et_pb_column .et_pb_row_inner_4 {
   padding-top: 2px;
   padding-right: 5px;
   padding-bottom: 2px;
   padding-left: 5px
}

.et_pb_row_inner_4.et_pb_row_inner {
   margin-top: 1px!important;
   margin-bottom: 1px!important
}

.et_pb_row_inner_4 {
   border-radius: 3px 3px 3px 3px;
   overflow: hidden;
   border-width: 2px
}

.et_pb_text_9 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_9 p {
   line-height: 1.8em
}

.et_pb_text_8 h4 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 14px;
   line-height: 1.8em
}

.et_pb_row_inner_5.et_pb_row_inner {
   margin-bottom: 1px!important
}

.et_pb_text_13 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_3.et_pb_text {
   color: #999999!important
}

.et_pb_divider_2 {
   height: 30px
}

.et_pb_text_16 h2 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 36px;
   line-height: 1.4em
}

.et_pb_row_3.et_pb_row {
   padding-top: 0;
   padding-right: 0px;
   padding-bottom: 1px;
   padding-left: 0px
}

.et_pb_section_5.et_pb_section {
   padding-top: 0px;
   padding-right: 0px;
   padding-bottom: 0px;
   padding-left: 0px
}

.et_pb_divider_3:hover:before {
   border-top-width: px
}

.et_pb_divider_3 {
   height: 30px
}

.et_pb_divider_2:hover:before {
   border-top-width: px
}

.et_pb_row_2 {
   background-image: linear-gradient(90deg, rgba(12, 206, 173, 0) 10%, #e02b20 90%)
}

.et_pb_text_14.et_pb_text {
   color: #999999!important
}

.et_pb_section_4.et_pb_section {
   padding-top: 0px;
   padding-right: 0px;
   padding-bottom: 0px;
   padding-left: 0px;
   background-color: #ffffff!important
}

.et_pb_text_15 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_15 p {
   line-height: 1.8em
}

.et_pb_text_14 h4 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 14px;
   line-height: 1.8em
}

.et_pb_text_14 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_14 p {
   line-height: 1.8em
}

.et_pb_text_8 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_8 p {
   line-height: 1.8em
}

.et_pb_text_8.et_pb_text {
   color: #999999!important
}

.et_pb_text_5 p {
   line-height: 1.8em
}

.et_pb_text_3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif
}

.et_pb_slide_0 {
   background-color: rgba(0, 0, 0, 0)
}

.et_pb_slide_1 {
   background-color: rgba(0, 0, 0, 0)
}

.et_pb_text_7 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_column .et_pb_row_inner_2 {
   padding-top: 2px;
   padding-right: 5px;
   padding-bottom: 2px;
   padding-left: 5px
}

.et_pb_text_4 h1 {
   color: #ffffff!important
}

.et_pb_text_4 h3 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   font-size: 16px;
   color: #ffffff!important;
   line-height: 1.8em
}

.et_pb_text_4 h6 {
   color: #ffffff!important
}

.et_pb_text_5.et_pb_text {
   color: #999999!important
}

.et_pb_row_inner_2 {
   border-radius: 3px 3px 3px 3px;
   overflow: hidden;
   border-width: 2px
}

.et_pb_text_5 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_text_5 h4 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-weight: 700;
   text-transform: uppercase;
   font-size: 14px;
   line-height: 1.8em
}

.et_pb_text_6 p {
   line-height: 1.8em
}

.et_pb_text_6 {
   font-family: 'Muli', Helvetica, Arial, Lucida, sans-serif;
   font-size: 16px;
   line-height: 1.8em
}

.et_pb_row_inner_3 {
   border-radius: 3px 3px 3px 3px;
   overflow: hidden;
   border-width: 2px
}

.et_pb_row_inner_3.et_pb_row_inner {
   margin-top: 1px!important
}

.et_pb_column .et_pb_row_inner_3 {
   padding-top: 2px;
   padding-right: 5px;
   padding-bottom: 2px;
   padding-left: 5px
}

.et_pb_social_media_follow_network_2 a.icon {
   background-color: #1c1c1c!important
}

.et_pb_slider .et_pb_slide_0 {
   background-color: rgba(0, 0, 0, 0)
}

.et_pb_social_media_follow_network_0 a.icon {
   background-color: #1c1c1c!important
}

.et_pb_social_media_follow li.et_pb_social_media_follow_network_0 a {
   padding-top: 30px;
   padding-right: 15px;
   padding-bottom: 30px;
   padding-left: 15px;
   width: auto;
   height: auto
}

.et_pb_social_media_follow_network_0 {
   margin-right: -4px!important;
   margin-left: -4px!important
}

.et_pb_counters .et_pb_counter_2 {
   margin-bottom: 40px
}

.et_pb_social_media_follow li.et_pb_social_media_follow_network_1 a {
   padding-top: 30px;
   padding-right: 15px;
   padding-bottom: 30px;
   padding-left: 15px;
   width: auto;
   height: auto
}

.et_pb_social_media_follow_network_1 {
   margin-right: -4px!important;
   margin-left: -4px!important
}

.et_pb_social_media_follow_network_3 a.icon {
   background-color: #1c1c1c!important
}

.et_pb_social_media_follow li.et_pb_social_media_follow_network_2 a {
   padding-top: 30px;
   padding-right: 15px;
   padding-bottom: 30px;
   padding-left: 15px;
   width: auto;
   height: auto
}

.et_pb_social_media_follow_network_2 {
   margin-right: -4px!important;
   margin-left: -4px!important
}

.et_pb_counters .et_pb_counter_0 {
   margin-bottom: 40px
}

.et_pb_social_media_follow li.et_pb_social_media_follow_network_3 a {
   padding-top: 30px;
   padding-right: 15px;
   padding-bottom: 30px;
   padding-left: 15px;
   width: auto;
   height: auto
}

.et_pb_social_media_follow_network_3 {
   margin-right: -4px!important;
   margin-left: -4px!important
}

.et_pb_slider .et_pb_slide_1 {
   background-color: rgba(0, 0, 0, 0)
}

.et_pb_counters .et_pb_counter_1 {
   margin-bottom: 40px
}

.et_pb_social_media_follow_network_1 a.icon {
   background-color: #1c1c1c!important
}

.et_pb_row_2.et_pb_row {
   margin-left: auto!important;
   margin-right: 0px!important;
   padding-top: 0;
   padding-right: 0px;
   padding-bottom: 0;
   padding-left: 0px
}

.et_pb_row_10.et_pb_row {
   margin-left: auto!important;
   margin-right: 0px!important;
   padding-top: 0;
   padding-right: 0px;
   padding-bottom: 0;
   padding-left: 0px
}

.et_pb_row_1.et_pb_row {
   margin-left: auto!important;
   margin-right: 0px!important;
   padding-top: 0;
   padding-right: 0px;
   padding-bottom: 0;
   padding-left: 0px
}

@media only screen and (max-width:980px) {
   .et_pb_video_0 {
      transform: scaleX(1.28) scaleY(1.28)
   }
   .et_pb_image_3 {
      text-align: center;
      margin-left: auto;
      margin-right: auto
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:before {
      display: none
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after {
      line-height: inherit;
      font-size: inherit!important;
      margin-left: -1em;
      left: auto;
      display: inline-block;
      opacity: 0;
      content: attr(data-icon);
      font-family: "ETmodules"!important
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover {
      padding-left: 0.7em;
      padding-right: 2em
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button {
      padding-left: 1em;
      padding-right: 1em
   }
   .et_pb_row>.et_pb_column.et_pb_column_33 {
      padding-left: 10vw
   }
   .et_pb_row>.et_pb_column.et_pb_column_32 {
      padding-right: 10vw
   }
   .et_pb_text_31 h1 {
      letter-spacing: 2px
   }
   .et_pb_text_30 h1 {
      letter-spacing: 2px
   }
   .et_pb_image_8 {
      text-align: center;
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_7 {
      text-align: center;
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_6 {
      text-align: center;
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_5 {
      text-align: center;
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_4 {
      text-align: center;
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_text_29 h2 {
      font-size: 24px
   }
   .et_pb_section_1.et_pb_section {
      margin-bottom: 100px
   }
   .et_pb_text_16 h2 {
      font-size: 24px
   }
   .et_pb_text_2 h2 {
      font-size: 24px
   }
   .et_pb_counters_0.et_pb_counters {
      margin-top: 61px!important;
      margin-right: 6px!important
   }
   body #page-container .et_pb_button_0:hover:after {
      margin-left: .3em;
      left: auto;
      margin-left: .3em;
      opacity: 1
   }
   body #page-container .et_pb_button_0:before {
      display: none
   }
   body #page-container .et_pb_button_0:after {
      line-height: inherit;
      font-size: inherit!important;
      margin-left: -1em;
      left: auto;
      display: inline-block;
      opacity: 0;
      content: attr(data-icon);
      font-family: "ETmodules"!important
   }
   body #page-container .et_pb_button_0:hover {
      padding-left: 0.7em;
      padding-right: 2em
   }
   body #page-container .et_pb_button_0 {
      padding-left: 1em;
      padding-right: 1em
   }
   .et_pb_text_0 h1 {
      font-size: 56px
   }
   .et_pb_row>.et_pb_column.et_pb_column_2 {
      padding-top: 0vw;
      padding-right: 10vw;
      padding-bottom: 0vw;
      padding-left: 10vw
   }
   .et_pb_image_2 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_1 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_0 {
      padding-top: 0px;
      padding-bottom: 0px;
      text-align: center;
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_row>.et_pb_column.et_pb_column_1 {
      padding-top: 6vw;
      padding-left: 13vw
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after {
      margin-left: .3em;
      left: auto;
      margin-left: .3em;
      opacity: 1
   }
}

@media only screen and (max-width:767px) {
   .et_pb_row_0.et_pb_row {
      padding-top: 30px!important
   }
   .et_pb_text_29 h2 {
      font-size: 18px
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:before {
      display: none
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:after {
      line-height: inherit;
      font-size: inherit!important;
      margin-left: -1em;
      left: auto;
      display: inline-block;
      opacity: 0;
      content: attr(data-icon);
      font-family: "ETmodules"!important
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover {
      padding-left: 0.7em;
      padding-right: 2em
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button {
      padding-left: 1em;
      padding-right: 1em
   }
   .et_pb_text_32 h2 {
      font-size: 36px
   }
   .et_pb_text_31 h1 {
      font-size: 20px;
      line-height: 1.5em;
      letter-spacing: 0px
   }
   .et_pb_text_30 h1 {
      font-size: 20px;
      line-height: 1.5em;
      letter-spacing: 0px
   }
   .et_pb_image_8 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_7 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_6 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_5 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_4 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_3 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_text_16 h2 {
      font-size: 18px
   }
   .et_pb_video_0 {
      padding-top: 28px;
      transform: scaleX(1.28) scaleY(1.28)
   }
   .et_pb_text_2 h2 {
      font-size: 18px
   }
   .et_pb_counters_0.et_pb_counters {
      margin-right: 0px!important
   }
   body #page-container .et_pb_button_0:hover:after {
      margin-left: .3em;
      left: auto;
      margin-left: .3em;
      opacity: 1
   }
   body #page-container .et_pb_button_0:before {
      display: none
   }
   body #page-container .et_pb_button_0:after {
      line-height: inherit;
      font-size: inherit!important;
      margin-left: -1em;
      left: auto;
      display: inline-block;
      opacity: 0;
      content: attr(data-icon);
      font-family: "ETmodules"!important
   }
   body #page-container .et_pb_button_0:hover {
      padding-left: 0.7em;
      padding-right: 2em
   }
   body #page-container .et_pb_button_0 {
      padding-left: 1em;
      padding-right: 1em
   }
   .et_pb_text_0 h1 {
      font-size: 32px
   }
   .et_pb_row>.et_pb_column.et_pb_column_2 {
      padding-bottom: 0vw
   }
   .et_pb_image_2 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_1 {
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_image_0 {
      padding-top: 0px;
      margin-left: auto;
      margin-right: auto
   }
   .et_pb_row>.et_pb_column.et_pb_column_1 {
      padding-bottom: 0vw;
      padding-left: 8vw
   }
   body #page-container .et_pb_contact_form_0.et_pb_contact_form_container.et_pb_module .et_pb_button:hover:after {
      margin-left: .3em;
      left: auto;
      margin-left: .3em;
      opacity: 1
   }
}


  </style>             
            <!-- </body> -->
            <span class="gr__tooltip"><span class="gr__tooltip-content"></span><i class="gr__tooltip-logo"></i><span class="gr__triangle"></span></span>
         <!-- </html> -->
<!-- #main-content -->
<?php get_footer(); ?>

