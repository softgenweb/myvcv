-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 13, 2019 at 12:52 PM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_realcrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `confic`
--

CREATE TABLE IF NOT EXISTS `confic` (
`id` int(11) NOT NULL,
  `confic_type_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `control` varchar(200) NOT NULL DEFAULT 'text'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `confic`
--

INSERT INTO `confic` (`id`, `confic_type_id`, `title`, `value`, `description`, `control`) VALUES
(1, 1, 'paging', '100', 'Page should be #5, 10, 20, 50, 100, 1000', 'select'),
(3, 1, 'default_mail', 'narai1987@gmail.com', 'Default mail to communicate with user', 'text'),
(4, 1, 'max_count', '100', '', ''),
(5, 2, 'language', '1', '', ''),
(6, 1, 'service_tax', '8', '', ''),
(10, 1, 'join_point', '100', '', ''),
(11, 1, 'rating_point', '10', '', ''),
(12, 1, 'get_point_on_purchase_product_per_1000_USD', '100', '', ''),
(13, 1, 'login_point', '5', '', ''),
(14, 1, 'price_per_100_point', '2', '', ''),
(15, 1, 'admin_gift_point', '50', '', ''),
(16, 1, 'minimum_used_point', '500', '', ''),
(17, 1, 'point_per_beverage', '5', '', ''),
(18, 1, 'point_per_eqipment', '5', '', ''),
(19, 1, 'point_per_food', '5', '', ''),
(20, 1, 'point_per_cabin', '10', '', ''),
(21, 1, 'trip_low_price_range', '5000', '', 'text'),
(22, 1, 'trip_high_price_range', '25000', '', 'text');

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
`id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `tmp_type` int(11) NOT NULL,
  `default_temp` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`id`, `name`, `tmp_type`, `default_temp`, `status`) VALUES
(1, 'jet_tmp', 1, 0, 1),
(2, 'api', 0, 0, 1),
(7, 'pms', 1, 1, 1),
(8, 'realcrm', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  `gender` char(100) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state_id` char(255) NOT NULL,
  `city_id` char(255) NOT NULL,
  `department_id` varchar(255) NOT NULL,
  `post_id` char(255) NOT NULL COMMENT 'HRM Post ID',
  `branch_id` varchar(255) NOT NULL,
  `location_id` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `utype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `postalcode` int(11) NOT NULL,
  `salary` varchar(255) NOT NULL,
  `zone` varchar(255) NOT NULL,
  `reporting` varchar(255) NOT NULL,
  `gross_salary` varchar(255) NOT NULL,
  `basic_salary` varchar(255) NOT NULL,
  `ta` varchar(255) NOT NULL,
  `hra` varchar(255) NOT NULL,
  `special` varchar(255) NOT NULL,
  `medical` varchar(255) NOT NULL,
  `incentive` varchar(255) NOT NULL,
  `others` varchar(255) NOT NULL,
  `pf_detuction` varchar(255) NOT NULL,
  `professional_tax` varchar(255) NOT NULL,
  `total_month_deduction` varchar(255) NOT NULL,
  `month_salary_pay` varchar(255) NOT NULL,
  `paid_leave` varchar(255) NOT NULL,
  `joining_date` varchar(255) NOT NULL,
  `date_time` datetime NOT NULL,
  `blood_group` varchar(255) NOT NULL,
  `pancard_no` varchar(255) NOT NULL,
  `aadhar_no` varchar(255) NOT NULL,
  `marital` varchar(255) NOT NULL,
  `hobbies` varchar(255) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `pancard` varchar(255) NOT NULL,
  `aadhar` varchar(255) NOT NULL,
  `present_address` varchar(255) NOT NULL,
  `present_pincode` varchar(255) NOT NULL,
  `present_state` varchar(255) NOT NULL,
  `present_city` varchar(255) NOT NULL,
  `permanent_address` varchar(255) NOT NULL,
  `permanent_pincode` varchar(255) NOT NULL,
  `permanent_state` varchar(255) NOT NULL,
  `permanent_city` varchar(255) NOT NULL,
  `issue_apl_status` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='HRM Registration , Employee Record' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `mobile`, `gender`, `dob`, `email`, `country`, `state_id`, `city_id`, `department_id`, `post_id`, `branch_id`, `location_id`, `status`, `utype`, `address`, `image`, `postalcode`, `salary`, `zone`, `reporting`, `gross_salary`, `basic_salary`, `ta`, `hra`, `special`, `medical`, `incentive`, `others`, `pf_detuction`, `professional_tax`, `total_month_deduction`, `month_salary_pay`, `paid_leave`, `joining_date`, `date_time`, `blood_group`, `pancard_no`, `aadhar_no`, `marital`, `hobbies`, `qualification`, `nationality`, `pancard`, `aadhar`, `present_address`, `present_pincode`, `present_state`, `present_city`, `permanent_address`, `permanent_pincode`, `permanent_state`, `permanent_city`, `issue_apl_status`) VALUES
(1, 'baba', 'admin', 'Baba', '8382936646', 'Male', '12/12/1995', 'nagesh.k@softgentechnologies.com', '1', '1', '1', '1', '1', '1', '1', 1, 'Report', '', '1/1profileimgMascot1.png', 0, '', '3', '', '45000', '18000', '1000', '6750', '6750', '1250', '', '', '2160', '0', '2160', '42840', '1', '18/02/2017', '2017-02-09 16:42:44', '', '', '', 'Married', '', '', 'Indian', '1/1pancardlogo1.png', '1/1aadharadvertisement_Website_2016.pdf', '', '', '4', '2', '', '', '4', '2', '1'),
(2, 'moin', '4545', 'Naaz', '8382936646', '1', '01/01/1992', 'hm@gmail.com', '1', '1', '1', '', '2', '', '', 1, 'Administrator', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '18/08/2018', '2018-08-18 11:25:31', '', '', '', 'Married', '', '', 'Indian', '', '', '', '', '', '', '', '', '', '', '0'),
(3, 'hmcity_report', 'admin', 'HMCITY Report', '05224301189', '1', '01/01/1992', 'hmcity7374@gmail.com', '1', '1', '1', '', '2', '', '', 0, 'Report', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '30/08/2018', '2018-08-30 17:00:31', '', '', '', 'Married', '', '', 'Indian', '', '', '', '', '', '', '', '', '', '', '0'),
(4, 'super@hmcity', 'sgt@admin', 'Super Admin', '8299431080', 'Male', '25/09/1995', 'gaurav.s@softgentechnologies.com', '1', '1', '1', '1', 'top', '1', '1', 1, 'Administrator', '', '1/1profileimgMascot1.png', 0, '', '3', '', '45000', '18000', '1000', '6750', '6750', '1250', '', '', '2160', '0', '2160', '42840', '1', '18/02/2017', '2017-02-09 16:42:44', '', '', '', 'Married', '', '', 'Indian', '1/1pancardlogo1.png', '1/1aadharadvertisement_Website_2016.pdf', '', '', '4', '2', '', '', '4', '2', '1'),
(5, 'sharib', 'sharib@12345', 'sharib khan', '9044569699', '1', '', 'sharib@123gmail.com', '1', '1', '10', '', '2', '', '', 0, 'Employee', 'khurram nagar', '', 276305, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '21/11/2018', '2018-11-21 15:11:06', '', '', '', 'Unmarried', '', 'Graduate', 'Indian', '', '', '', '', '', '', '', '', '', '', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `confic`
--
ALTER TABLE `confic`
 ADD PRIMARY KEY (`id`,`confic_type_id`);

--
-- Indexes for table `templates`
--
ALTER TABLE `templates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `confic`
--
ALTER TABLE `confic`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `templates`
--
ALTER TABLE `templates`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
