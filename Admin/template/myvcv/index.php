<!-- <!DOCTYPE html>
<html>
   <head> -->
<?php
   header("Pragma: no-cache");
   header("Cache-Control: no-cache");
   header("Expires: 0");
   /*=====================*/
   session_start();
   $yr = date('y');
   
   define('WP_USE_THEMES', true);
      // include('../configuration.php');
   require('../wp-config.php');
   get_header();
   /*====================*/
    date_default_timezone_set("Asia/Kolkata");
   ?>

      <base href="/myvcv/Admin/" >
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Admin - MyVCV</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" href="<?php echo $tmp;?>/bootstrap/css/bootstrap.min.css">
  

      <!-- Font Awesome -->
       <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
      <link rel="stylesheet" href="<?php echo $tmp;?>/bootstrap/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- DataTables -->
      <!-- <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/datatables/dataTables.bootstrap.css"> -->
      
      <!-- <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/datepicker/datepicker3.css">     -->
      <!-- fullCalendar 2.2.5-->
      <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/WorldClock/css/jClocksGMT.css">    <!-- fullCalendar 2.2.5-->

    <!-- <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/fullcalendar/fullcalendar.min.css"> -->
    <!-- <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/fullcalendar/fullcalendar.print.css" media="print"> -->
      <!-- Theme style -->
      <link rel="stylesheet" href="<?php echo $tmp;?>/dist/css/AdminLTE.min.css">
      <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/ionslider/ion.rangeSlider.css">
      <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/ionslider/ion.rangeSlider.skinNice.css">
      <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
      <link rel="stylesheet" href="<?php echo $tmp;?>/dist/css/skins/_all-skins.min.css">
      <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/datatables/dataTables.bootstrap.css">
      <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/iCheck/all.css">
      <!-- Theme style -->
          <link rel="stylesheet" href="<?php echo $tmp;?>/style.css">
 
      <!-------------------------------Start Popup-------------------------------->
      <!-- <link rel="stylesheet" type="text/css" href="assets/popup/css/reveal.css" media="all" /> -->
      <!-- <script type="text/javascript" src="assets/popup/js/jquery-1.4.1.min.js"></script> -->
      <!-- <script type="text/javascript" src="assets/popup/js/jquery.reveal.js"></script> -->
      <!-------------------------------End Popup-------------------------------->
      <script type="text/javascript">
         var jq = $.noConflict();
      </script>
      <script src="assets/javascript/script.js"></script>
      <!-- <link href="assets/rating/rating.css" rel="stylesheet" type="text/css"> -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <!-- <script src="<?php echo $tmp;?>/plugins/datepicker/bootstrap-datepicker.js"></script> -->

      <style type="text/css">
         .overall-rating{font-size: 14px;margin-top: 5px;color: #8e8d8d;}
         #main-header, #top-header{
         display: none;
         }
      </style>
      <script type="text/javascript">
         function paging(str) {
                    //  alert(str);
                    try {
                      document.getElementById("task").value=document.getElementById("defftask").value;
                    }
                    catch(e) {  
                    }
                    document.getElementById("page").value = str;
                    document.getElementById("tpages").value = document.getElementById("filter").value;
                    document.filterForm.submit();
                  }
         
                  function paging1(str) { 
          //alert(str);
          document.getElementById("task").value = (document.getElementById("task").value)=="delete"?'':(document.getElementById("task").value);
          document.getElementById("page").value = 1;
          document.getElementById("tpages").value = str;
          //alert(document.getElementById("tpages").value);
          document.filterForm.submit();
         }
      </script>  
      <style>

         .pagination {
         border: 1px solid #ccc;
         border-radius: 4px;
         display: inline-block;
         margin: 10px;
         padding: 10px;
         }
         .page {
         border: 1px solid #ccc;
         border-radius: 4px;
         padding: 3px;
         cursor: pointer;
         }
         .page.active {
         background: #30a5ff none repeat scroll 0 0;
         border: medium none;
         color: #fff !important;
         }
         td, th {
         padding: 5px;
         }
         
         .navbar-default {
         background-color: #fff !important;
         border-color: #e7e7e7;
         }
         a img, .letter_img, .certificate_img{
         height: 50px ;
         }
         ul.dropdown-menu.profile{
         min-width: 280px !important;
         }
         .navbar-right .dropdown-menu {
         right: 50px;
         left: auto;
         }
         .user-header{
         background-color: #cd5c5c;
         }
         h3,small{
         color: #fff;
         }
         #overlay {
  position: fixed;
  display: none;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  z-index: 9999;
  /*cursor: pointer;*/
  color: #fff !important;
}
.overlay{
  display: none;
  position: fixed;
  top: 40%;
  left: 50%;
  z-index: 9999;
  font-size: 50px;
  color: #fff;
  z-index: 99999;
}
#page-container{
  padding-top:0px !important;
}
#main-footer{
  position: relative;
/*  top: 20px;*/
  z-index: 9999;
}
      </style>
   </head>
   <body class="hold-transition skin-blue sidebar-mini">
     <div id="overlay" ></div>
      <div class="overlay" style="">
          <i class="fa fa-refresh fa-spin" style=""></i>
      </div>
      <div class="wrapper">
         <div class="main-header"> <a href="#" class="hidden-lg hidden-md hidden-sm sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            </a>
         </div>
         <?php //include_once('includes/header.php');?>
         <!-- Left side column. contains the logo and sidebar -->
         <?php include_once('includes/sidebar.php');?>
         <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!--  <section class="content-header">
               <h1>
                 Dashboard
                <small>Control panel</small>
               </h1>
               <ol class="breadcrumb">
                 <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                 <li class="active">Dashboard</li>
               </ol>
               </section>-->
            <!-- Main content -->
               <?php include_once('includes/nav_bar.php'); ?>
            <section class="content">
               <?php include_once('controller.php'); ?>
            </section>
            <!-- /.content -->
         </div>
       </div>
         <!-- /.content-wrapper -->
         <div class="clearfix"></div>
         <?php include_once('includes/footer.php');
           get_footer(); 
            ?>
         <!-- <div class="control-sidebar-bg"></div> -->
<!--       </div>
   </body>
</html> -->

