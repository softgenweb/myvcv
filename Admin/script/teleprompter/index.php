<br>
<div class="col-sm-12">


	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" />
	<link rel="stylesheet" href="css/style.css?v=0.6.1">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!-- =================Tale prompter========================= -->
<style type="text/css">
.overlay .top {
	position: fixed;
	top: 125px;
	height: 75px;
	width: 100%;
	background-color: #000;
	background-color: rgba(0,0,0,0.75);
	z-index: 10;
}
.marker {
	position: fixed;
	left: 0;
	top: 100px;
	color: #4297d7;
	font-size: 40px;
	display: none;
	z-index: 100;
}
article .teleprompter {
	padding: 70px 5px 300px 30px !important;
	 resize: both;
    overflow: auto;
}
</style>
	<article>
		<div class="overlay">
			<div class="top"></div>
			<!-- <div class="bottom"></div> -->
		</div>
		<div class="teleprompter" id="teleprompter" contenteditable style="font-size: 31px;">
		
		</div>
		<i class="icon-play marker"></i>
	</article>
		<header>
		<h1><i class="icon icon-time"></i> <span class="clock">00:00</span></h1>
		<nav style="opacity: 1 !important;">
			<div class="buttons">
				<a style="display: none;" class="button small icon-tablet remote" href="#" onclick="return false;"></a>
				<a style="display: none;" class="button small icon-undo reset" href="#" onclick="return false;"></a>
				<a style="display: none;" class="button small icon-text-width flipx" href="#" onclick="return false;"></a>
				<a style="display: none;" class="button small icon-text-height flipy" href="#" onclick="return false;"></a>
				<a class="button icon-play play active" id="start" href="#" onclick="return false;"></a>
			</div>
			<div class="sliders">
				<label class="font_size_label">Font Size <span>(31)</span>:&nbsp; <div class="font_size slider"></div></label><br>
				<label class="speed_label">Speed <span>(12)</span>:&nbsp; <div class="speed slider"></div></label>
			</div>
			<div class="colors">
				<label>
				<input id="text-color-picker" class="color {hash:true,caps:true,valueElement:'text-color'}"><input type="hidden" id="text-color" value="#ffffff"> Text Color</label>
				<label><input id="background-color-picker" class="color {hash:true,caps:true,valueElement:'background-color'}"> Background </label>
				<input type="hidden" id="background-color" value="#141414">
			</div>
		</nav>
	</header>
	<!-- ============================================= -->

	<script src="https://promptr.tv/remote/socket.io/socket.io.js"></script>
	<script src="js/plugins.js?v=1.1.0"></script>
	<script src="js/script.js?v=1.1.0"></script>

</div>
