<?php session_start();
include('../class/dbaccess.php');
$db = new DbAccess();
//no_img.png
  if($db->user_image($_SESSION['adminid'])!=''){
    $img_path = 'media/user_profile/'.$db->user_image($_SESSION['adminid']);
  }
  elseif($db->user_image($_SESSION['adminid'])=='' && $_SESSION['gender']=='Male'){
    $img_path = 'images/img_avatar.png';
  }
  elseif($db->user_image($_SESSION['adminid'])=='' && $_SESSION['gender']=='Female'){
    $img_path = 'images/img_avatar2.png';      
  }
  elseif($db->user_image($_SESSION['adminid'])=='' && $_SESSION['gender']==''){
    $img_path = 'images/img_avatar.png';      
  }

?>
<aside class="main-sidebar" >

<!-- <aside class="main-sidebar" style="background-image:url('images/sidebar-5.jpg') !important;"> -->
   <div class="background_layer"></div>
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
      <div class="user-panel" style="padding-bottom: 95%;">
         <div class="pull-left info">
            <a href="" data-toggle="modal" data-target="#update_img">
            <img src="<?php echo $img_path; ?>" class="img-circle" alt="User Image" style="width: 135px;height: 140px;"></a>
         </div>
         <div class="clearfix"></div>
         <div class=" user info" >
          <h3 style="margin:unset;"><?php echo $_SESSION['admin_name'];?></h3>
          <small><?php echo $_SESSION['username']; ?></small>
         </div>
       <!--   <div class="pull-left info" >
            <h3 style="margin:unset;"><?php echo $_SESSION['admin_name']; ?></h3>
          
            <a href="index.php?control=user&task=changepassword" title="Change Password"><i class="fa fa-circle text-success"></i><b>Change Password</b></a><br />
         </div> -->
      </div>
      <?php if($_SESSION['utype']=="Administrator"){ ?>
      <ul class="sidebar-menu">
         <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
            <a href="#">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Dashboard</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='user' && $_REQUEST['task']!='feedback'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=user&task=personal_detail">
            <i class="fa fa-pencil-square" aria-hidden="true"></i>
            <span>Admin Profile</span>
            </a>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='jobseeker'){ ?>active <?php } ?>">
            <a href="javascript:;"><i class="fa fa-suitcase" aria-hidden="true"></i> Job Seeker
              <i class="fa fa-angle-left pull-right"></i>
            </a>
              <ul class="treeview-menu">
                 <li <?php if($_REQUEST['control']=='jobseeker' && ($_REQUEST['task']=='paid_list' || $_REQUEST['task']=='unpaid_list')){ ?>class="active"<?php } ?>><a href="index.php?control=jobseeker&task=paid_list"><i class="fa fa-users" aria-hidden="true"></i> Jobseeker List</a></li>
                 <!-- <li <?php if($_REQUEST['control']=='jobseeker' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=jobseeker&task=show"><i class="fa fa-comments" aria-hidden="true"></i> Jobseeker Messages</a></li> -->
                 <li <?php if($_REQUEST['control']=='jobseeker' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=jobseeker&task=show"><i class="fa fa-rss" aria-hidden="true"></i> Jobseeker Feedbacks</a></li>
                 <li <?php if($_REQUEST['control']=='jobseeker' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=jobseeker&task=show"><i class="fa fa-bell" aria-hidden="true"></i> Jobseeker Notifications</a></li>
                 <li <?php if($_REQUEST['control']=='jobseeker' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=jobseeker&task=show"><i class="fa fa-trash" aria-hidden="true"></i> Deleted Jobseeker list</a></li>
              </ul>  
         </li>

         <li class="treeview <?php if($_REQUEST['control']=='support'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=support&task=show">
            <i class="fa fa-ticket" aria-hidden="true"></i>
            <span>Online Support</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='master' && $_REQUEST['task']=='promocode_show'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=master&task=promocode_show">
            <i class="fa fa-pencil-square" aria-hidden="true"></i>
            <span>Promo Code</span>
            </a>
         </li>
        <li class="treeview <?php if($_REQUEST['control']=='payment'){ ?>active <?php } ?>">
            <a href="javascript:;"><i class="fa fa-picture-o" aria-hidden="true"></i> Payment History
              <i class="fa fa-angle-left pull-right"></i>
            </a>
              <ul class="treeview-menu">
                 <li <?php if($_REQUEST['control']=='jobseeker' && $_REQUEST['task']=='list'){ ?>class="active"<?php } ?>><a href="index.php?control=jobseeker&task=list"><i class="fa fa-picture-o" aria-hidden="true"></i> Notifications</a></li>
                 <li <?php if($_REQUEST['control']=='jobseeker' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=jobseeker&task=show"><i class="fa fa-picture-o" aria-hidden="true"></i> Total Payments Received</a></li>
                 <li <?php if($_REQUEST['control']=='jobseeker' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=jobseeker&task=show"><i class="fa fa-picture-o" aria-hidden="true"></i> Total Commissions Paid</a></li>
              </ul>  
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='recruiter'){ ?>active <?php } ?>">
            <a href="javascript:;"><i class="fa fa-picture-o" aria-hidden="true"></i> Recruiter
              <i class="fa fa-angle-left pull-right"></i>
            </a>
              <ul class="treeview-menu">
                 <li <?php if($_REQUEST['control']=='recruiter' && $_REQUEST['task']=='addnew'){ ?>class="active"<?php } ?>><a href="index.php?control=recruiter&task=addnew"><i class="fa fa-picture-o" aria-hidden="true"></i> Create a New Recruiter</a></li>
                 <li <?php if($_REQUEST['control']=='recruiter' && $_REQUEST['task']=='recruiter_list'){ ?>class="active"<?php } ?>><a href="index.php?control=recruiter&task=recruiter_list"><i class="fa fa-picture-o" aria-hidden="true"></i> Recruiter List</a></li>
                 <!-- <li <?php if($_REQUEST['control']=='recruiter' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=recruiter&task=show"><i class="fa fa-picture-o" aria-hidden="true"></i> Recruiter Messages</a></li> -->
                  <li <?php if($_REQUEST['control']=='recruiter' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=recruiter&task=show"><i class="fa fa-picture-o" aria-hidden="true"></i>Recruiter Feedbacks</a></li>
                 <li <?php if($_REQUEST['control']=='recruiter' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=recruiter&task=show"><i class="fa fa-picture-o" aria-hidden="true"></i> Recruiter Notifications</a></li>
                 <li <?php if($_REQUEST['control']=='recruiter' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=recruiter&task=show"><i class="fa fa-picture-o" aria-hidden="true"></i> Deleted recruiter list</a></li>
                 <li <?php if($_REQUEST['control']=='recruiter' && $_REQUEST['task']=='show'){ ?>class="active"<?php } ?>><a href="index.php?control=recruiter&task=show"><i class="fa fa-picture-o" aria-hidden="true"></i> Commissions Management</a></li>
              </ul>  
         </li>



      </ul>
    <?php }elseif($_SESSION['utype']=="job_seeker"){ ?>
      <ul class="sidebar-menu">
         <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
            <a href="#">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Dashboard</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='user' && $_REQUEST['task']!='feedback'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=user&task=personal_detail">
            <i class="fa fa-pencil-square" aria-hidden="true"></i>
            <span>Update Profile</span>
            </a>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='media'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=media&task=show">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
            <span>Media</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='support'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=support&task=show">
            <i class="fa fa-ticket" aria-hidden="true"></i>
            <span>Online Support</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='user' && $_REQUEST['task']=='feedback'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=user&task=feedback">
            <i class="fa fa-pencil-square" aria-hidden="true"></i>
            <span>Feedback</span>
            </a>
         </li>
         
      </ul>      
    <?php }elseif($_SESSION['utype']=="recruiter"){ ?>

      <ul class="sidebar-menu">
         <li class="treeview <?php if($_REQUEST['control']==''){ ?>active active-menu<?php } ?>">
            <a href="#">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Dashboard</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='user' && $_REQUEST['task']!='feedback'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=user&task=personal_detail">
            <i class="fa fa-pencil-square" aria-hidden="true"></i>
            <span>Update Profile</span>
            </a>
         </li>
         
         <li class="treeview <?php if($_REQUEST['control']=='media'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=media&task=show">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
            <span>Media</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='support'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=support&task=show">
            <i class="fa fa-ticket" aria-hidden="true"></i>
            <span>Online Support</span>
            </a>
         </li>
         <li class="treeview <?php if($_REQUEST['control']=='user' && $_REQUEST['task']=='feedback'){ ?>active active-menu<?php } ?>">
            <a href="index.php?control=user&task=feedback">
            <i class="fa fa-pencil-square" aria-hidden="true"></i>
            <span>Feedback</span>
            </a>
         </li>
         
      </ul>   
      
    <?php } ?>
   </section>
   <!-- /.sidebar -->
</aside>

<!-- ============Profile Image Model====== -->
<!--           <div class="example-modal" id="update_img">
            <div class="modal modal-danger">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal Danger</h4>
                  </div>
                  <div class="modal-body">
                    <p>One fine body&hellip;</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-outline">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
<div id="update_img" class="modal fade modal-danger" role="dialog">
  <div class="modal-dialog">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       <center> <h4 class="modal-title">Upload Profile Photo</h4></center>
      </div>
      <div class="modal-body">
        <p>
          <form name="form" method="post" enctype="multipart/form-data">
             <div class="col-md-4 col-sm-4 col-xs-12">
                     <label>Uplod Photo</label>
                     
                     <!-- <b style="color:red">*</b> --> 
                  </div>

                  <div class="col-md-4 col-sm-4 col-xs-12" >
                     <input type="file" class="form-control-file" value="<?php echo $result['image']; ?>" id="image" name="image" class="form-control" autocomplete="off" >
                     <a target="_blank" href="" id="image_a"><img id="image_img" src="" alt="&nbsp;" height="100"  /></a>
                     <!-- <input type="hidden" name="image" value="<?php echo $result['image']; ?>"> -->
                  </div>
         
          <div class="clearfix"></div>
        </p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
      <button type="submit" name="save" class="btn btn-outline">Save changes</button>
      <input type="hidden" name="control" value="user"/>
      <input type="hidden" name="old_control" value="<?php echo $_REQUEST['control']; ?>"/>
      <input type="hidden" name="task" value="profile_pic_update"/>
      <input type="hidden" name="old_task" value="<?php echo $_REQUEST['task']; ?>"/>
      <input type="hidden" name="id" id="idd" value="<?php echo $_SESSION['adminid']; ?>"  />
       </form>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
   function image(input) {
   if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
         $('#image_img').attr('src', e.target.result);
         $('#image_a').attr('href', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
   }
}
$("#image").change(function(){
   image(this);
});
</script>