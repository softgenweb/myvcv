<?php
include '../class/dbaccess.php';
$db = new DbAccess();
$msg_from = ($_SESSION['utype']=="Administrator")?"  AND `read_status`=1":"  AND `chat_status`=1";
 $new_msg = mysqli_num_rows(mysqli_query($conn,"SELECT `id` FROM `support_chat` WHERE `to_user`='".$_SESSION['adminid']."' $msg_from")); 


 $all_tickets = mysqli_fetch_array(mysqli_query($conn,"SELECT `id` FROM `support_chat` WHERE  `to_user`='".$_SESSION['adminid']."' AND `chat_status`=1")); 

/*==Personal==*/
$Personal = mysqli_query($conn, "SELECT `id`,`name`, `mobile`, `facebook`, `twitter`, `instagram`, `linkedin`, `languages`, `gender`, `country`, `state_id`, `city_id`, `address`, `father_husband_name`, `alt_no`, `profession`, `title_me`, `about_me`, `postalcode`, `marital`,  `nationality`, `goal`, `quote1`, `quote2`, `quote3`,pancard,aadhar,police_form,reference_letter,thumbprint,signature,face FROM `users` WHERE `id`='".$_SESSION['adminid']."'");
$TASK1 =  $db->taskPercent($Personal); 
/*==Video==*/
$Video = mysqli_query($conn, "SELECT `id`,`video_name` FROM `jobseeker_video` WHERE `user_id`='".$_SESSION['adminid']."' AND `status`=1 GROUP BY `user_id`");
$TASK2 =  $db->taskPercent($Video); 
/*==Experience==*/
$exp1 = mysqli_query($conn, "SELECT `id`,`experience_title`,`experience_description`,(SELECT `company_name` FROM `jobseeker_experience` WHERE `user_id`='".$_SESSION['adminid']."' GROUP BY `user_id`) as `company` FROM `users` WHERE `id`='".$_SESSION['adminid']."'");
$TASK3 =  $db->taskPercent($exp1);
/*==Skill==*/
$skill1 = mysqli_query($conn, "SELECT `id`, `skills_title`, `skills_description`,(SELECT `name` FROM `jobseeker_interest` WHERE `user_id`='".$_SESSION['adminid']."' GROUP BY `user_id`) as `interests` FROM `users` WHERE `id`='".$_SESSION['adminid']."'");
$skill2 = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `jobseeker_skills` WHERE `user_id`='".$_SESSION['adminid']."' ORDER BY `id` ASC LIMIT 0,5 "));
$TASK4_1 = 12.5*$skill2;
$TASK4 = number_format( ($db->taskPercent($skill1))/2.67 + $TASK4_1);
/*==Education==*/
$edu1 = mysqli_query($conn, "SELECT `id`, `education_title`, `education_description` FROM `users` WHERE `id`='".$_SESSION['adminid']."'");
$edu2 = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `jobseeker_education` WHERE `user_id`='".$_SESSION['adminid']."' ORDER BY `id` ASC LIMIT 0,2 "));
$TASK5_1 = 25*$edu2;
$TASK5 =( ($db->taskPercent($edu1))/2 + $TASK5_1);
/*==Portfolio==*/
$Portfolio = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `jobseeker_portfolio` WHERE `user_id`='".$_SESSION['adminid']."' ORDER BY `id` ASC LIMIT 0,4 "));
$TASK6 = 25*$Portfolio;


 ?>


<nav class="navbar navbar-default navbar-fixed">
     <div class="container-fluid">
        <div class="navbar-header">
           <a class="navbar-brand" href="https://www.myvcv.asia" style="padding-top: 0;"><img src="../wp-content/uploads/2019/09/myvcv_logo.png"></a> 
           <!-- ================For Mobile View================ -->
        <div class="btn-group small-screen pull-right">
         
          <a href="javascript:;" type="button" class="dropdown-toggle" data-toggle="dropdown">
           <?php echo $_SESSION['username']; ?>
            
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="index.php?control=user&task=changepassword">Change Password</a></li>
            <li><a href="logout.php">Logout</a></li>
          </ul>
        </div>
           <!-- =============================================== -->
        </div>
        <div class="collapse navbar-collapse">
           <ul class="nav navbar-nav navbar-left">
           </ul>

           <ul class="nav navbar-nav navbar-right">
              <li class="dropdown messages-menu">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                 <i class="fa fa-envelope-o"></i>
                 <span class="label label-success info_batch"><?php echo $new_msg; ?></span>
                 </a>
                 <ul class="dropdown-menu">
                    <li class="header">You have <?php echo $new_msg; ?> messages</li>
                    <li>
                       <!-- inner menu: contains the actual data -->
                       <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                          <ul class="menu listitems" style="width: 100%; height: 200px;">
                            <?php
                            if($_SESSION['utype']!="Administrator"){
                             $sql = mysqli_query($conn, "SELECT * FROM `support_ticket` WHERE `created_by`='".$_SESSION['adminid']."' ORDER BY `date_modify` DESC"); 
                           }else{
                              $sql = mysqli_query($conn, "SELECT * FROM `support_ticket` WHERE 1 ORDER BY `date_modify` DESC"); 
                           }
                             // $sql = mysqli_query($conn,"SELECT * FROM `support_chat` WHERE  `to_user`='".$_SESSION['adminid']."' AND `chat_status`=1"); 
                             $i=0;
                            while($ticket = mysqli_fetch_array($sql)){
                              $i++;
                              // global $conn;
                            
                              // $last_msg = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM `support_chat` WHERE  `to_user`='".$_SESSION['adminid']."' AND `ticket_id`='".$ticket['id']."' ORDER BY `id` DESC LIMIT 1")); 
                              $last_msg = (strlen($db->last_msg($ticket['id']))>5)? implode(' ', array_slice(explode(' ', $db->last_msg($ticket['id'])), 0, 3))."..." :$db->last_msg($ticket['id']);
                                $userimg = mysqli_fetch_array(mysqli_query($conn, "SELECT `image` FROM `users` WHERE `id`='".$ticket['to_user']."'"));

                                $t_n_m = mysqli_num_rows(mysqli_query($conn,"SELECT `id` FROM `support_chat` WHERE `to_user`='".$_SESSION['adminid']."' AND `ticket_id`='".$ticket['id']."' $msg_from")); 
                              // $ticket = mysqli_fetch_array(mysqli_query($conn,"SELECT * FROM `support_ticket` WHERE `to_user`='".$ticket['to_user']."'"));
                                $bg = ($t_n_m>0)?"background-color:#cacaca;font-weight:600;":"";
                             
                            ?>
                             <li style="<?php echo $bg; ?>">
                                <!-- start message -->
                                <a href="index.php?control=support&task=chat_box&ticket=<?php echo $ticket['id']; ?>">
                                   <div class="pull-left">
                                      <img src="<?php echo $img = $userimg['image']?'media/user_profile/'.$userimg['image']:'images/img_avatar.png'; ?>" class="img-circle" alt="&nbsp;">
                                   </div>
                                   <h4>
                                     <?php echo $ticket['ticket_no']."(".$ticket['title'].")"; ?>
                                      <!-- <small><i class="fa fa-clock-o"></i> 5 mins</small> -->
                                   </h4>
                                   <p><?php echo $last_msg." (".$t_n_m.")"; ?></p>
                                </a>
                             </li>
                           <?php } ?>

                          </ul>
                          <div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 69px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 131.148px;"></div>
                          <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                       </div>
                    </li>
                    <li class="footer"><a href="index.php?control=support&task=show">See All Messages</a></li>
                 </ul>
              </li>
              <li class="dropdown tasks-menu">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                 <i class="fa fa-flag-o"></i>
                 <span class="label label-danger info_batch"><span class="remain_task"></span></span>
                 </a>
                 <ul class="dropdown-menu">
                    <li class="header">You have <span class="remain_task"></span> tasks</li>
                    <li class="body">
                       <!-- inner menu: contains the actual data -->
                       <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                          <ul class="menu task_menu" style="overflow: auto; width: 100%; height: 200px;">
                            <?php if($TASK1 < 100){ ?>
                             <li>
                                <!-- Task item -->
                                <a href="index.php?control=user&task=personal_detail">
                                   <h3>
                                      Personal Details
                                      <small class="pull-right"><?php echo $TASK1; ?>%</small>
                                   </h3>
                                   <div class="progress xs">
                                      <div class="progress-bar progress-bar-aqua" style="width: <?php echo $TASK1; ?>%" role="progressbar" aria-valuenow="<?php echo $db->taskPercent($Personal); ?>" aria-valuemin="0" aria-valuemax="100">
                                         <span class="sr-only"><?php echo $TASK1; ?>% Complete</span>
                                      </div>
                                   </div>
                                </a>
                             </li>
                             <!-- end task item -->
                           <?php } if($TASK2 < 100){ ?>
                             <li>
                                <!-- Task item -->
                                <a href="index.php?control=user&task=video_detail">
                                   <h3>
                                      Upload a Video
                                      <small class="pull-right"><?php echo $TASK2; ?>%</small>
                                   </h3>
                                   <div class="progress xs">
                                      <div class="progress-bar progress-bar-green" style="width: <?php echo $TASK2; ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                         <span class="sr-only"><?php echo $TASK2; ?>% Complete</span>
                                      </div>
                                   </div>
                                </a>
                             </li>
                          <?php } if($TASK3 < 100){ ?>
                             <!-- end task item -->
                             <li>
                                <!-- Task item -->
                                <a href="index.php?control=user&task=my_experience">
                                   <h3>
                                      My Experience
                                      <small class="pull-right"><?php echo $TASK3; ?>%</small>
                                   </h3>
                                   <div class="progress xs">
                                      <div class="progress-bar progress-bar-red" style="width: <?php echo $TASK3; ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                         <span class="sr-only"><?php echo $TASK3; ?>% Complete</span>
                                      </div>
                                   </div>
                                </a>
                             </li>
                             <!-- end task item -->
                               <?php } if($TASK4 < 100){ ?>
                             <li>
                                <!-- Task item -->
                                <a href="index.php?control=user&task=my_skill">
                                   <h3>
                                      My Skills
                                      <small class="pull-right"><?php echo $TASK4; ?>%</small>
                                   </h3>
                                   <div class="progress xs">
                                      <div class="progress-bar progress-bar-yellow" style="width: <?php echo $TASK4; ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                         <span class="sr-only"><?php echo $TASK4; ?>% Complete</span>
                                      </div>
                                   </div>
                                </a>
                             </li>
                               <?php } if($TASK5 < 100){ ?>
                             <li>
                                <!-- Task item -->
                                <a href="index.php?control=user&task=my_education">
                                   <h3>
                                     My Education & Awards
                                      <small class="pull-right"><?php echo $TASK5; ?>%</small>
                                   </h3>
                                   <div class="progress xs">
                                      <div class="progress-bar progress-bar-yellow" style="width: <?php echo $TASK5; ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                         <span class="sr-only"><?php echo $TASK5; ?>% Complete</span>
                                      </div>
                                   </div>
                                </a>
                             </li>
                               <?php } if($TASK6 < 100){ ?>
                             <li>
                                <!-- Task item -->
                                <a href="index.php?control=user&task=my_portfolio">
                                   <h3>
                                      My Portfolio
                                      <small class="pull-right"><?php echo $TASK6; ?>%</small>
                                   </h3>
                                   <div class="progress xs">
                                      <div class="progress-bar progress-bar-yellow" style="width: <?php echo $TASK6; ?>%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                         <span class="sr-only"><?php echo $TASK6; ?>% Complete</span>
                                      </div>
                                   </div>
                                </a>
                             </li>
                           <?php } ?>
                             <!-- end task item -->
                          </ul>
                          <div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                          <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                       </div>
                    </li>
                    <li class="footer">
                       <!-- <a href="#">View all tasks</a> -->
                    </li>
                 </ul>
              </li>
              <li class="dropdown user user-menu">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                 <img src="<?php
                  if($_SESSION['user_image']!=''){
                    $img_path = 'media/user_profile/'.$_SESSION['user_image'];
                  }
                  elseif($_SESSION['user_image']=='' && $_SESSION['gender']=='Male'){
                    $img_path = 'images/img_avatar.png';
                  }
                  elseif($_SESSION['user_image']=='' && $_SESSION['gender']=='Female'){
                    $img_path = 'images/img_avatar2.png';      
                  }
                    elseif($_SESSION['user_image']=='' && $_SESSION['gender']==''){
                      $img_path = 'images/img_avatar.png';      
                    }

                  echo $img_path; ?>" class="user-image" alt="User Image">
                 <span class="hidden-xs"><?php  echo $_SESSION['admin_name'];?></span>
                 </a>
                 <ul class="dropdown-menu profile">
                    <!-- User image -->
                    <li class="user-header">
                       <img src="<?php echo $img_path; ?>" class="img-circle" alt="User Image">
                       <p>
                          <a href="#" style="color: #fff;font-size: 30px;"><?php echo $_SESSION['admin_name']; ?></a><br>
                           <a href="#" style="color: #fff;"><small><?php echo $_SESSION['username']; ?></small></a>
                          <!-- <small>Member since Nov. 2012</small> -->
                       </p>
                    </li>
                    <!-- Menu Body -->
                    <!-- <li class="user-body">
                       <div class="col-xs-4 text-center">
                         <a href="#">Followers</a>
                       </div>
                       <div class="col-xs-4 text-center">
                         <a href="#">Sales</a>
                       </div>
                       <div class="col-xs-4 text-center">
                         <a href="#">Friends</a>
                       </div>
                       </li> -->
                    <!-- Menu Footer-->
                    <li class="user-footer">
                       <div class="pull-left">
                          <a href="index.php?control=user&task=changepassword" class="btn btn-default btn-flat">Change Password</a>
                       </div>
                       <div class="pull-right">
                          <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                       </div>
                    </li>
                 </ul>
              </li>
              <!-- <li>   
                 <a href="logout.php">
                   <p>Welcome <?php  echo $_SESSION['admin_name'];?> (Log out)</p>
                 </a> </li>
                 
                 <li class="separator hidden-lg"></li> -->
           </ul>
        </div>
     </div>
  </nav>

  <script type="text/javascript">
    $(document).ready(function(){
    n = $('.task_menu li').length;
    $('.remain_task').text(n);
    console.log(n);
    });
/*    $(function() {
  $(".listitems li").sort(sort_li).appendTo('.listitems');
  function sort_li(a, b) {
    return ($(b).data('position')) > ($(a).data('position')) ? 1 : -1;
  }
});*/
  </script>