<?php
	
	require_once('createthumb.php');
	if(file_exists('textconfig/config.php')) {
		require_once('textconfig/config.php');
	}
	else {
		require_once('../textconfig/config.php');
	}
	if(file_exists('configuration.php')){
		require_once('configuration.php');
	}
	else {
		require_once('../configuration.php');	
	}
	class DbAccess extends  CreateThumb {
		public $result;
		public $query;
		public $num_row;
		public $tmpPath;
		public $taxonomy_cont;
		public function DbAccess(){
			
			
			if($_SESSION['username']){
			$query = $tmpid?"SELECT * FROM templates WHERE id = '".$tmpid."' AND tmp_type = 0 AND  status = '1'":"SELECT * FROM templates WHERE status = '1' AND tmp_type = 0 AND default_temp='1'";
			$this->Query($query);
			$result = $this->fetchArray();
			$this->tmpPath = "template/".$result[0]['name'];
			}else {
			$query = "SELECT * FROM templates WHERE status = '1' AND tmp_type = 1 AND default_temp='1'";
			$this->Query($query);
			$result = $this->fetchArray();
			$this->tmpPath = "template/".$result[0]['name'];	
				
			}
		}
			function getTemplate($tmpid) {
					if($_SESSION['username']){
			$query = $tmpid?"SELECT * FROM templates WHERE id = '".$tmpid."' AND tmp_type = 0 AND  status = '1'":"SELECT * FROM templates WHERE status = '1' AND tmp_type = 0 AND default_temp='1'";
			$this->Query($query);
			return $this->fetchArray();
					}
			else {
		$query = $tmpid?"SELECT * FROM templates WHERE id = '".$tmpid."' AND tmp_type = 1 AND  status = '1'":"SELECT * FROM templates WHERE status = '1' AND tmp_type = 1 AND default_temp='1'";
			$this->Query($query);
			return $this->fetchArray();	
				
			}
			
		}
		

		function getMenuTop() {
			/*$query = "SELECT * FROM menus WHERE status = '1'";
			$this->Query($query);*/
			return $this->fetchArray();
			
		}
	
		
		function Query($str) {
			$this->query = $str;
			return true;	
		}
		
		function Execute() {
			global $conn;
			$this->result = mysqli_query($conn, $this->query) or die(mysqli_error($conn));
			return $this->result;
		}
		
		function isExecute() {
			global $conn;
			return  mysqli_query($conn, $this->query);
		}
		
		function fetchArray() {
			// global $conn;
			$this->Execute();
			$this->NumRow();
			for($i=0; $i<$this->num_row; $i++){
				$fetch_result[$i] = mysqli_fetch_array($this->result);
			}
			return $fetch_result;
		}
		
		function fetchObject() {
			// global $conn;
			$this->Execute();
			$this->NumRow();
			for($i=0; $i<$this->num_row; $i++){
				$fetch_result[$i] = mysqli_fetch_object($this->result);
			}
			return $fetch_result;
		}
		
		function numRow() {
			// global $conn;
			$this->num_row = mysqli_num_rows($this->result);	
			return $this->num_row;
		}

		
		function rowCount(){
			global $conn;
			$this->result = mysqli_query($conn, $this->query) or die(mysqli_error($conn));
			$this->num_row = mysqli_num_rows($this->result);	
			return $this->num_row;				
		}
		
	
		
		function mailsend($to,$from=NULL,$subject=NULL,$message=NULL) {
			
			$from = $from?$from:EMAILFROM; //$fromMail;//
			$subject = $subject?$subject:SUBJECTMAIL; 
			$message = $message?$message:"<a href=".$this->token.">Click here to varify your account</a>";
			//////////////////////////////////////////////////
			
			 
			 
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: '.SUBJECTMAIL.' '.$from;
			
			// Additional headers
			//$headers .= 'To: '. $from. "\r\n";
			//$headers .= 'From: '.$subject.' <'.$to.'>' . "\r\n";
			 
			$ok = @mail($to,$subject, $message,  $headers); 
			if ($ok) { 
				return true;
			} else { 
				return false;
			} 	
		}
		
		function uploadFile($dest,$file,$type = NULL) {
			if($type) {
				if($file['type']==$type) {					
					$des = $dest."/".$_REQUEST['id'].$file['name'];
					if(move_uploaded_file($file['tmp_name'],$des)) {
						return $des;
					}
					else {
						return NULL;
					}
				}
				else {
					return NULL;
				}
			}
			else {
				 $des = $dest."/".$_REQUEST['id'].$file['name'];
					if(move_uploaded_file($file['tmp_name'],$des)) {
						return $des;
					}
					else {
						return NULL;
					}
			}
		}
		//Language code start here
		function  taxolist() {
		
			return $arr;
		}
		function language() {
			/*$q_show = "SELECT id,content FROM languages  WHERE deff = '1' and status = '1'";			
			$this->Query($q_show);*/
			return $this->fetchArray();
		}
		function langAll() {
			/*$q_show = "SELECT id,content FROM languages  WHERE status = '1'";			
			$this->Query($q_show);*/
			return $this->fetchArray();
		}
		function siteLanguage() {
		
			  return $arrData;	
		}
		
		function rollback() {
			echo mysqli_query($conn, "rollback");	
		}
		function commit() {
			echo mysqli_query($conn, "commit");	
		}
		function defaultPageData() {
			$this->Query("SELECT * FROM confic WHERE title = 'paging'");
			$data = $this->fetchArray();
			return $data[0]['value']?$data[0]['value']:5;
		}
		
		function moneyFormatIndia($nums) {
		      $negative_value_check =  substr($nums,0,1);
			if($negative_value_check=='-'){
				$ve = '-';
				$num = substr($nums,1);
			}
		     else{
				 $ve = '';
				$num = $nums; 
			 }
			
			$explrestunits = "" ;
    if(strlen($num)>3) {
        $lastthree = substr($num, strlen($num)-3, strlen($num));
        $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
        $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
        $expunit = str_split($restunits, 2);
        for($i=0; $i<sizeof($expunit); $i++) {
            // creates each of the 2's group and adds a comma to the end
            if($i==0) {
                $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
            } else {
                $explrestunits .= $expunit[$i].",";
            }
        }
        $thecash = $explrestunits.$lastthree;
    } else {
        $thecash = $num;
    }
    return $ve.$thecash; // writes the final format where $currency is the currency symbol.
     }
	 
	 
	 		function country_name($id) {
			$sql = "SELECT `country_name` FROM `country` WHERE `id`='".$id."'";
			$this->Query($sql);
			$country = $this->fetchArray();
			return $country[0]['country_name'];
		}
		
		
			function state_name($id) {
			$sql = "SELECT `state_name` FROM `state` WHERE `id`='".$id."'";
			$this->Query($sql);
			$state = $this->fetchArray();
			return $state[0]['state_name'];
		}
		
			function city_name($id) {
			$sql = "SELECT `city_name` FROM `city` WHERE `id`='".$id."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['city_name'];
		}
		// echo $this->city_name('5');
	function check_exp($uid){
			$sql = "SELECT `user_id` FROM `jobseeker_experience` WHERE `user_id`='".$uid."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['user_id'];		
	}
	function check_testi($uid){
			$sql = "SELECT `user_id` FROM `recruiter_testimonials` WHERE `user_id`='".$uid."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['user_id'];		
	}
	function check_skill($uid){
			$sql = "SELECT `user_id` FROM `jobseeker_skills` WHERE `user_id`='".$uid."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['user_id'];		
	}
	function check_edu($uid){
			$sql = "SELECT `user_id` FROM `jobseeker_education` WHERE `user_id`='".$uid."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['user_id'];		
	}
	function check_portfolio($uid){
			$sql = "SELECT `user_id` FROM `jobseeker_portfolio` WHERE `user_id`='".$uid."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['user_id'];		
	}	
	function user_image($uid){
			$sql = "SELECT `image` FROM `users` WHERE `id`='".$uid."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['image'];		
	}	
	function user_name($uid){
			$sql = "SELECT `username` FROM `users` WHERE `id`='".$uid."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['username'];		
	}	
	function user_table($uid,$column){
			$sql = "SELECT `".$column."` AS `cols` FROM `users` WHERE `id`='".$uid."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['cols'];		
	}	
	function new_msg($ticket_id,$from_user,$column){
		$sql = "SELECT count(`id`) AS `msg` FROM `support_chat` WHERE `ticket_id`='".$ticket_id."' AND `from_user`='".$from_user."' AND `".$column."`= 1 ";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['msg'];
	}
	function get_video_name($user_id,$id){
			$sql = "SELECT `video_name`  FROM `jobseeker_video` WHERE `id`='".$id."' AND `user_id`='".$user_id."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['video_name'];
	}
	function get_image_name($user_id,$id){
			$sql = "SELECT `image`  FROM `user_profile_image` WHERE `id`='".$id."' AND `user_id`='".$user_id."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['image'];
	}
	function feedback_rate($fid,$uid){
			$sql = "SELECT `rating` FROM `feedback_rating` WHERE `user_id`='".$uid."' AND `feedback_id`='".$fid."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['rating']?$city[0]['rating']:0;
	}

	function taskPercent($result){
		  while($row = mysqli_fetch_row($result)){
		    $empty_count = 0;
		    $count = count($row);
		    for($i = 0; $i < $count; $i++)
		        if($row[$i] === '' || $row[$i] === NULL)
		            $empty_count++;
		    $total +=  ((int)(100*(1-$empty_count/($count-1))));
		}
		return $total;
	}

	function lastPayment($user_id,$column){
			$sql = "SELECT `".$column."` AS `data` FROM `online_payment` WHERE `user_id`='".$user_id."' ORDER BY `id` DESC LIMIT 1";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['data'];
	}

	function userDetail($user_id,$column){
			$sql = "SELECT `".$column."` AS `data` FROM `users` WHERE `id`='".$user_id."'";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['data']?$city[0]['data']:"N/A";
	}

	function last_msg($ticket){
			$sql = "SELECT `msg` FROM `support_chat` WHERE `ticket_id`='".$ticket."' ORDER BY `id` DESC LIMIT 1";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['msg'];
	}

	function total_user($recruiter_id,$status){
			$pay_status = $status?" AND `payment_status`='".$status."' ":'';
			$sql = "SELECT `id` FROM `users` WHERE `recruiter_id`='".$recruiter_id."' AND `utype`='job_seeker' $pay_status";
			$this->Query($sql);
			$this->Execute();
			$num = $this->numRow();
			return $num?$num:0;
	}

	function payment_detail($recruiter_id, $column, $table){
			$sql = "SELECT SUM(`".$column."`) AS `info` FROM `".$table."` WHERE `user_id`='".$recruiter_id."' ";
			$this->Query($sql);
			$city = $this->fetchArray();
			return $city[0]['info']?$city[0]['info']:0;
	}

	function comm_ids($recruiter_id){
		$sql = "SELECT `commision_id` FROM `recruiter_ledger` WHERE `user_id`='".$recruiter_id."' ";
		$this->Query($sql);
		$city = $this->fetchArray();
		$val = array_column($city, 'commision_id');
		return implode(',', $val);
	}

}

?>