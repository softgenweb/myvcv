<?php

require_once('dbaccess.php');

require_once('textconfig/config.php');		

if(file_exists('configuration.php')){		

	require_once('configuration.php');

}
class RecruiterClass extends DbAccess {
	public $view='';
	public $name='recruiter';		


function recruiter_list(){

		$query = "SELECT * FROM `users` WHERE `utype`='recruiter' ORDER BY `date_time` DESC";
		$_SESSION['recruiters'] = $query;
		$this->Query($query);
		$results = $this->fetchArray();		

		require_once("views/".$this->name."/".$this->task.".php"); 
	}

function addnew(){
		$id = $_REQUEST['id'];
		if($id){
			$query = "SELECT * FROM `users` WHERE `utype`='recruiter' AND `id`='".$id."'";
			// $_SESSION['recruiters'] = $query;
			$this->Query($query);
			$results = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php"); 
		}else{
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
	}

function save(){
		global $conn;
		$id = $_REQUEST['id'];	

		$username = $_REQUEST['username'];		$title = $_REQUEST['title'];
		$name = $_REQUEST['name'];				$gender = $_REQUEST['gender'];
		$mobile = $_REQUEST['mobile'];			$email = $_REQUEST['email'];
		$password = $_REQUEST['password'];		$dob = $_REQUEST['dob'];
		$commision = $_REQUEST['commision'];		//$dob = $_REQUEST['dob'];

		if(!$id){
			$insert = "INSERT INTO `users`(`title`, `username`, `name`, `gender`, `mobile`, `email`, `password`, `dob`, `commision`, `utype`, `date_time`) VALUES ('".$title."', '".$username."', '".$name."', '".$gender."', '".$mobile."', '".$email."', '".$password."', '".$dob."', '".$commision."', 'recruiter', '".date('Y-m-d H:i:s')."')";
			$this->Query($insert);
			$this->Execute();

			$user_id = mysqli_insert_id($conn);

			$insert1 = "INSERT INTO `bank_detail`(`user_id`, `date_created`, `date_modify`) VALUES ('".$user_id."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";
			$this->Query($insert1);
			$this->Execute();

			$insert2 = "INSERT INTO `comapny_info`(`user_id`, `date_created`, `date_modify`) VALUES ('".$user_id."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";
			$this->Query($insert2);
			$this->Execute();

            $_SESSION['alertmessage'] = ADDNEWRECORD;
            $_SESSION['errorclass'] = SUCCESSCLASS;
		}else{
			$update = "UPDATE `users` SET `title`='".$title."',`name`='".$name."',`gender`='".$gender."',`mobile`='".$mobile."',`password`='".$password."',`dob`='".$dob."',`commision`='".$commision."' WHERE `id`='".$id."' AND `utype`='recruiter'";
			$this->Query($update);
			$this->Execute();

			$_SESSION['alertmessage'] = UPDATERECORD;
            $_SESSION['errorclass'] = SUCCESSCLASS;
		}
		header("location:index.php?control=recruiter&task=recruiter_list");

	}

function status(){
		$query="UPDATE `users` SET `status`=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=recruiter&task=recruiter_list");
	}

function commission_list(){
		$id = $_REQUEST['id'];
		$query = "SELECT * FROM `recruiter_commission` WHERE `user_id`='".$id."' AND `status`=1 ORDER BY `id` DESC";
		$this->Query($query);
		$results = $this->fetchArray();	

		require_once("views/".$this->name."/".$this->task.".php"); 			
	}

function pay_commission(){
		$user_id = $_REQUEST['user_id'];			$commission_amt = $_REQUEST['commission_amt'];
		$paid_amount = $_REQUEST['paid_amount'];	$remain_amount = $_REQUEST['remain_amount'];
		$pay_mode = $_REQUEST['pay_mode'];			$bank_name = $_REQUEST['bank_name'];
		$txn_no = $_REQUEST['txn_no'];			$paid_on = $_REQUEST['paid_on'];

		$commision_id = implode(',' ,$_REQUEST['comm_ids']);

		$status = $remain_amount>1 ? "2" : "3";
		// exit;

		$insert = "INSERT INTO `recruiter_ledger`(`user_id`, `commision_id`, `commission_amt`, `paid_amount`, `remain_amount`, `pay_mode`, `bank_name`, `txn_no`, `paid_on`, `status`, `date_created`, `date_modify`) VALUES ('".$user_id."', '".$commision_id."', '".$commission_amt."', '".$paid_amount."', '".$remain_amount."', '".$pay_mode."', '".$bank_name."', '".$txn_no."', '".$paid_on."', '".$status."', '".date('Y-m-d H:i')."', '".date('Y-m-d H:i')."')";
		$this->Query($insert);
		$this->Execute();
        $_SESSION['alertmessage'] = ADDNEWRECORD;
        $_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=recruiter&task=commission_list&id=".$user_id);
	}

function payment_ledger(){
		$id = $_REQUEST['id'];

		$query1 = "SELECT * FROM `recruiter_ledger` WHERE `user_id`='".$id."' AND `status`!=1 ORDER BY `paid_on` DESC";
		$this->Query($query1);
		$uresults = $this->fetchArray();	
		require_once("views/".$this->name."/".$this->task.".php"); 			
	}

function user_list(){
		$id = $_REQUEST['id'];
		$query = "SELECT * FROM `users` WHERE `recruiter_id`='".$id."' AND `utype`='job_seeker' ORDER BY `id` DESC";
		$this->Query($query);
		$results = $this->fetchArray();	
		require_once("views/".$this->name."/".$this->task.".php"); 			
	}

/*===================Update Job Seeker Profile===============*/	

	function edit_details(){
		$id = $_REQUEST['user_id'];		$editid = $_REQUEST['editid'];
		$task = $_REQUEST['pg'];

		$name = $_REQUEST['name'];		$gender = $_REQUEST['gender'];
		$dob = $_REQUEST['dob'];		$joining_date = $_REQUEST['date_time'];
		$recruiter_id = $_REQUEST['recruiter_id'];		$title = $_REQUEST['title'];
		$coupon = $_REQUEST['coupon'];

		if($id!='' && $editid==1){
		 	$Update = "UPDATE `users` SET `title`='".$title."', `name`='".$name."', `gender`='".$gender."', `dob`='".$dob."', `date_time`='".$joining_date."', `coupon`='".$coupon."' WHERE `id`='".$id."' AND `utype`='job_seeker'";

			$this->Query($Update);
			$this->Execute();
			$_SESSION['alertmessage'] = UPDATERECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			header("location:index.php?control=recruiter&task=user_list&id=".$recruiter_id);
						
		}else{

			$query = "SELECT * FROM `users` WHERE `utype`='job_seeker' AND `id`='".$id."'";
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/".$this->task.".php"); 			
		}
	}	

	function jobseeker_status(){
		$recruiter_id = $_REQUEST['recr_id'];
		$query="UPDATE `users` SET `status`=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."' AND `utype`='job_seeker'";	
		$this->Query($query);	
		$this->Execute();
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=recruiter&task=user_list&id=".$recruiter_id);	
	}
}
