<?php

require_once('dbaccess.php');

require_once('textconfig/config.php');		

if(file_exists('configuration.php')){		

	require_once('configuration.php');

}
class MasterClass extends DbAccess {
	public $view='';
	public $name='master';		


function promocode_show(){

		$query = "SELECT * FROM `coupon_code` WHERE 1 ORDER BY `id` DESC";

		$this->Query($query);
		$results = $this->fetchArray();		

		require_once("views/".$this->name."/".$this->task.".php"); 
	}

function promocode_addnew(){
		$id = $_REQUEST['id'];
		if($id){
			$query = "SELECT * FROM `coupon_code` WHERE 1 AND `id`='".$id."'";

			$this->Query($query);
			$results = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php"); 
		}else{
			require_once("views/".$this->name."/".$this->task.".php"); 
		}
	}

function promocode_save(){
		$id = $_REQUEST['id'];	

		$promocode = strtoupper($_REQUEST['promocode']);		$discount = $_REQUEST['discount'];
		$amount = $_REQUEST['amount'];
		$recruiter_id = implode(', ', $_REQUEST['recruiter_id']);			

		if(!$id){
			$insert = "INSERT INTO `coupon_code`(`promocode`, `discount`, `amount`, `recruiter_id`, `date_created`, `date_modify`) VALUES ('".$promocode."', '".$discount."', '".$amount."', '".$recruiter_id."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";
		// exit();
			$this->Query($insert);
			$this->Execute();

            $_SESSION['alertmessage'] = ADDNEWRECORD;
            $_SESSION['errorclass'] = SUCCESSCLASS;
		}else{
			$update = "UPDATE `coupon_code` SET `discount`='".$discount."',`amount`='".$amount."',`recruiter_id`='".$recruiter_id."',`date_modify`='".date('Y-m-d H:i:s')."' WHERE `id`='".$id."'";
			$this->Query($update);
			$this->Execute();

			$_SESSION['alertmessage'] = UPDATERECORD;
            $_SESSION['errorclass'] = SUCCESSCLASS;
		}
		header("location:index.php?control=master&task=promocode_show");

	}

function promocode_status(){
		$query="UPDATE `coupon_code` SET `status`=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	
		$this->Query($query);	
		$this->Execute();
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=master&task=promocode_show");
	}



function jobseeker_feedback(){

		$query = "SELECT * FROM `users` WHERE `utype`='job_seeker' ORDER BY `id` DESC";

		$this->Query($query);
		$results = $this->fetchArray();		

		require_once("views/".$this->name."/".$this->task.".php"); 
	}

function recruiter_feedback(){

		$query = "SELECT * FROM `users` WHERE `utype`='recruiter' ORDER BY `id` DESC";

		$this->Query($query);
		$results = $this->fetchArray();		

		require_once("views/".$this->name."/".$this->task.".php"); 
	}

}
