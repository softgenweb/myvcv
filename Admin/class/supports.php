<?php

require_once('dbaccess.php');

require_once('textconfig/config.php');		

if(file_exists('configuration.php')){		

	require_once('configuration.php');

}
class supportClass extends DbAccess {
	public $view='';
	public $name='support';		

	function show(){

		if($_SESSION['utype']=="Administrator"){
			$dateFrom  = $_REQUEST['dateFrom'];
			$dateTo  = date('Y-m-d', strtotime($_REQUEST['dateTo'] . ' +1 day'));
			$user_type = $_REQUEST['user_type']?(" AND `user_type` LIKE '".$_REQUEST['user_type']."'"):"";
			$dateUse = $_REQUEST['dateFrom']?(" AND `created_date` LIKE '".$_REQUEST['dateFrom']."%'"):(" AND `created_date` LIKE '".$_REQUEST['dateTo']."%'");


			$dateFromTo = ($_REQUEST['dateFrom'] && $_REQUEST['dateTo'])?(' AND `created_date` between "'.$dateFrom.'" and "'.$dateTo.'"'):$dateUse;

			$uquery ="SELECT * FROM `support_ticket` WHERE `to_user`='".$_SESSION['adminid']."' $dateFromTo $user_type AND `ticket_no`!='' ORDER BY `date_modify` DESC";
		}else{
			$uquery ="SELECT * FROM `support_ticket` WHERE `created_by`='".$_SESSION['adminid']."' ORDER BY `date_modify` DESC";
		}

		$this->Query($uquery);
		$results = $this->fetchArray();		
		require_once("views/".$this->name."/".$this->task.".php"); 

		}

		function addnew(){
			if($_REQUEST['id']) {
				$query_com ="SELECT * FROM support_ticket WHERE id = ".$_REQUEST['id'];
				$this->Query($query_com);

				$results = $this->fetchArray();
			    require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}
		
		}

		function save(){
			global $conn;
			$id = $_REQUEST['id'];
			$title = $_REQUEST['title'];
			$to_user = $_REQUEST['to_user'];
			$created_by = $_SESSION['adminid'];
			$user_type = $_SESSION['utype'];

			if(!$id) {
			 	$query_com ="INSERT INTO `support_ticket`(`title`, `to_user`, `created_by`, `user_type`, `created_date`, `date_modify`) VALUES ('".$title."', '".$to_user."', '".$created_by."', '".$user_type."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."')";
				$this->Query($query_com);
				$this->Execute();

				$last_id = mysqli_insert_id($conn);
			// exit;

				$ticket_no = $_SESSION['fyear']."00".$last_id;

				$update = "UPDATE `support_ticket` SET `ticket_no`='".$ticket_no."' WHERE `id`='".$last_id."'";
				$this->Query($update);
				$this->Execute();

				header("location:index.php?control=support&task=chat_box&ticket=".$last_id);
			    // require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				 require_once("views/".$this->name."/".$this->task.".php"); 
			}			
		}

	function chat_box(){
		if($_REQUEST['ticket']) {
			 $query_com ="SELECT * FROM `support_ticket` WHERE `id` = ".$_REQUEST['ticket'];
			$this->Query($query_com);
			$results = $this->fetchArray();
		    require_once("views/".$this->name."/".$this->task.".php"); 
		}
		else {
			 require_once("views/".$this->name."/".$this->task.".php"); 
		}
	}
			
	function send_msg(){
		global $conn;
		$ticket_id = $_REQUEST['ticket_id'];
		// $send_msg = $_REQUEST['send_msg'];
		$msg = mysqli_real_escape_string($conn, $_REQUEST['message']);
		$to_user = $_REQUEST['to_user'];
		$from_user = $_SESSION['adminid'];


	$query = "INSERT INTO `support_chat`(`ticket_id`, `msg`, `from_user`, `to_user`, `created_by`, `date_created`) VALUES ('".$ticket_id."', '".$msg."', '".$from_user."', '".$to_user."', '".$from_user."', '".date('Y-m-d H:i:s')."')";
	$this->Query($query);
	$this->Execute();

	$this->Query("UPDATE `support_ticket` SET `date_modify`='".date('Y-m-d H:i:s')."' WHERE `id`='".$ticket_id."'");
	$this->Execute();

	$last_id = mysqli_insert_id($conn);
	
	if($_FILES['file_upload']['name']){
		if (!is_dir('media/msg_attchmet/'.$last_id)) {
				// dir doesn't exist, make it
				mkdir('media/msg_attchmet/'.$last_id,0777,true);
			}
			$folder = "media/msg_attchmet/".$last_id.'/';


			$file_upload = $_FILES['file_upload']['tmp_name'];						
			$short_file_upload = $_FILES['file_upload']['name'];						
			move_uploaded_file($file_upload , $folder.$last_id.$short_file_upload);

		$update = "UPDATE `support_chat` SET `file`='".($short_file_upload?$last_id.'/'.$last_id.$short_file_upload:'')."' WHERE `id`='".$last_id."'";
		$this->Query($update);
		$this->Execute();
		}

	header('location:index.php?control=support&task=chat_box&ticket='.$ticket_id);

		}

}
