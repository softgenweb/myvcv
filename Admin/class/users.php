<?php

require_once('dbaccess.php');

require_once('textconfig/config.php');		

if(file_exists('configuration.php')){		

	require_once('configuration.php');

}
class UserClass extends DbAccess {
	public $view='';
	public $name='user';		

	function show(){	

		$uquery ="select * from users where 1";

		$this->Query($uquery);

		$uresults = $this->fetchArray();	

		$tdata=count($uresults);

		/* Paging start here */

		$page   = intval($_REQUEST['page']);

			$_REQUEST['tpages'] = $tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default

			$adjacents  = intval($_REQUEST['adjacents']);

			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 

			$tdata = floor($tdata);

			if($page<=0)  $page  = 1;

			if($adjacents<=0) $tdata?($adjacents = 4):0;

			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	

			/* Paging end here */	

			$query ="select * from users where 1 LIMIT ".(($page-1)*$tpages).",".$tpages;

			$this->Query($query);

			$results = $this->fetchArray();		



			require_once("views/".$this->name."/".$this->task.".php"); 

		}

		function personal_detail()
		{   $id = $_REQUEST['id'];
		if($id)
		{
			$title = $_REQUEST['title'];				$name = $_REQUEST['name'];
			$languages = implode(',', $_REQUEST['languages']);				//$name = $_REQUEST['name'];
			$gender = $_REQUEST['gender'];				$father_husband_name = $_REQUEST['father_husband_name'];
			$dob = $_REQUEST['dob'];				$marital = $_REQUEST['marital'];
			$mobile = $_REQUEST['mobile'];			//	$alt_no = $_REQUEST['alt_no'];
			$email = $_REQUEST['email'];				$profession = $_REQUEST['profession'];
			$country = $_REQUEST['country'];				$state_id = $_REQUEST['state_id'];
			$city_id = $_REQUEST['city_id'];				$address = $_REQUEST['address'];
			$postalcode = $_REQUEST['postalcode'];				$nationality = $_REQUEST['nationality'];
			$title_me = $_REQUEST['title_me'];
			$about_me = $_REQUEST['about_me'];
			// $profile_color = $_REQUEST['profile_color'];
		
			$facebook = $_REQUEST['facebook'];				$twitter = $_REQUEST['twitter'];	
			$instagram = $_REQUEST['instagram'];				$linkedin = $_REQUEST['linkedin'];				
			$goal = $_REQUEST['goal'];				$quote1 = $_REQUEST['quote1'];		
			$quote2 = $_REQUEST['quote2'];					$quote3 = $_REQUEST['quote3'];				




			$update_profile = "UPDATE `users` SET `title`='".$title."', `name`='".$name."', `mobile`='".$mobile."', `facebook`='".$facebook."', `twitter`='".$twitter."', `instagram`='".$instagram."', `linkedin`='".$linkedin."', `languages`='".$languages."', `gender`='".$gender."', `country`='".$country."', `state_id`='".$state_id."', `city_id`='".$city_id."', `address`='".$address."', `father_husband_name`='".$father_husband_name."', `profession`='".$profession."', `title_me`='".$title_me."', `about_me`='".$about_me."', `postalcode`='".$postalcode."', `marital`='".$marital."',  `nationality`='".$nationality."', `goal`='".$goal."', `quote1`='".$quote1."', `quote2`='".$quote2."', `quote3`='".$quote3."', `date_time`='".date('Y-m-d H:i:s')."' WHERE `id`='".$id."'";
				// exit;
			$this->Query($update_profile);	
			$this->Execute();


			/*========================Document Upload=================*/
			if (!is_dir('media/user_profile/'.$id)) {
				// dir doesn't exist, make it
				mkdir('media/user_profile/'.$id,0777,true);
			}
			$folder = "media/user_profile/".$id.'/';
			$passport_old = $_REQUEST['passport'];				$aadhar_old = $_REQUEST['aadhar'];
			$pancard_old = $_REQUEST['pancard'];				$police_form_old = $_REQUEST['police_form'];
			$thumbprint_old = $_REQUEST['thumbprint'];	
			$face_old = $_REQUEST['face'];	
			$reference_letter_old = $_REQUEST['reference_letter'];	
			$signature_old = $_REQUEST['signature'];	

			$pancard = $_FILES['pancard']['tmp_name'];						
			$short_pancard = $_FILES['pancard']['name'];						
			move_uploaded_file($pancard , $folder.$id.'pancard_'.$short_pancard);


			$aadhar = $_FILES['aadhar']['tmp_name'];						
			$short_aadhar = $_FILES['aadhar']['name'];						
			move_uploaded_file($aadhar , $folder.$id.'aadhar_'.$short_aadhar);


			$passport = $_FILES['passport']['tmp_name'];						
			$short_passport = $_FILES['passport']['name'];						
			move_uploaded_file($passport , $folder.$id.'passport_'.$short_passport);


			$police_form = $_FILES['police_form']['tmp_name'];						
			$short_police_form = $_FILES['police_form']['name'];						
			move_uploaded_file($police_form , $folder.$id.'police_form_'.$short_police_form);


			$reference_letter = $_FILES['reference_letter']['tmp_name'];						
			$short_reference_letter = $_FILES['reference_letter']['name'];						
			move_uploaded_file($reference_letter , $folder.$id.'reference_letter_'.$short_reference_letter);

			$thumbprint = $_FILES['thumbprint']['tmp_name'];						
			$short_thumbprint = $_FILES['thumbprint']['name'];						
			move_uploaded_file($thumbprint , $folder.$id.'thumbprint_'.$short_thumbprint);

			$face = $_FILES['face']['tmp_name'];						
			$short_face = $_FILES['face']['name'];						
			move_uploaded_file($face , $folder.$id.'face_'.$short_face);

			$signature = $_FILES['signature']['tmp_name'];						
			$short_signature = $_FILES['signature']['name'];						
			move_uploaded_file($signature , $folder.$id.'signature_'.$short_signature);

			$upload_atchmt = ("UPDATE `users` SET `pancard`='".($short_pancard?$id.'/'.$id.'pancard_'.$short_pancard:$pancard_old)."', `aadhar`='".($short_aadhar?$id.'/'.$id.'aadhar_'.$short_aadhar:$aadhar_old)."', `passport`='".($short_passport?$id.'/'.$id.'passport_'.$short_passport:$passport_old)."', `police_form`='".($short_police_form?$id.'/'.$id.'police_form_'.$short_police_form:$police_form_old)."', `reference_letter`='".($short_reference_letter?$id.'/'.$id.'reference_letter_'.$short_reference_letter:$reference_letter_old)."', `thumbprint`='".($short_thumbprint?$id.'/'.$id.'thumbprint_'.$short_thumbprint:$thumbprint_old)."', `face`='".($short_face?$id.'/'.$id.'face_'.$short_face:$face_old)."', `signature`='".($short_signature?$id.'/'.$id.'signature_'.$short_signature:$signature_old)."' WHERE `id`='".$id."'"); 

			$this->Query($upload_atchmt);
			$this->Execute();
			/*=========================================================*/

			$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
			$this->Query($query_com);
			$results = $this->fetchArray();
			$_SESSION['gender'] = $gender; 
			$_SESSION['alertmessage'] = UPDATERECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			// require_once("views/".$this->name."/".$this->task.".php"); 
			header("location:index.php?control=user&task=personal_detail");
		}
		else {
			$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
			$this->Query($query_com);
			$results = $this->fetchArray();
					/*  $_SESSION['alertmessage'] = DUPLICATE; 
					$_SESSION['errorclass'] = ERRORCLASS;*/
					require_once("views/".$this->name."/".$this->task.".php"); 
				}
			}

			function video_detail()
			{	$id = $_REQUEST['id'];
			if($id)
			{
				$update_profile = "update users set username = '".$_REQUEST['username']."',name = '".$_REQUEST['name']."',mobile = '".$_REQUEST['mobile']."',email = '".$_REQUEST['email']."' where id = '".$_REQUEST['id']."'";
				$this->Query($update_profile);	
				$this->Execute();
				
				$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
				$this->Query($query_com);
				$results = $this->fetchArray();
				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				$query_com ="SELECT * FROM `jobseeker_video` WHERE `user_id`='".$_SESSION['adminid']."' AND `status`=1 ORDER BY `id` DESC LIMIT 1";
				$this->Query($query_com);
				$results = $this->fetchArray();
					/*  $_SESSION['alertmessage'] = DUPLICATE; 
					$_SESSION['errorclass'] = ERRORCLASS;*/
					require_once("views/".$this->name."/".$this->task.".php"); 
				}
			}

			function video_recorder(){
				
				require_once("views/".$this->name."/".$this->task.".php"); 
			}

			function my_experience()
			{		$id = $_REQUEST['id'];
				global $conn;
			if($id)
			{
				$experience_title = $_REQUEST['experience_title'];
				$experience_description = $_REQUEST['experience_description'];


				$company_name = $_REQUEST['company_name'];
				$job_title = $_REQUEST['job_title'];
				$year = $_REQUEST['year'];
				$description = $_REQUEST['description'];
				/*===================*/
				$status = $_REQUEST['check_status'];
				/*=================*/

				

				$update_profile = "UPDATE `users` SET `experience_title`='".$experience_title."', `experience_description`='".$experience_description."' WHERE `id` = '".$id."'";
				$this->Query($update_profile);	
				$this->Execute();

				/*============Add Experience=========*/
				if (!is_dir('media/user_profile/'.$id.'/experience/')) {
				// dir doesn't exist, make it
					mkdir('media/user_profile/'.$id.'/experience/',0777,true);
				}
				$folder = "media/user_profile/".$id.'/experience/';

				$this->Query("DELETE FROM `jobseeker_experience` WHERE `user_id`='".$id."'");
				$this->Execute();

				$this->Query("ALTER TABLE `jobseeker_experience` AUTO_INCREMENT = 1");
				$this->Execute();

				for ($i=0; $i < count($company_name); $i++) { 
					$logo_old = $_REQUEST['logo'][$i];
					$letter_old = $_REQUEST['letter'][$i];
					/*=================*/
					$stat = ($status[$i]=="1")?"0":"1";
					/*=================*/
					if($company_name[$i]!=''){
					$exp = "INSERT INTO `jobseeker_experience`(`user_id`, `company_name`, `job_title`, `year`, `description`, `status`, `date_created`) VALUES ('".$id."', '".$company_name[$i]."', '".$job_title[$i]."', '".$year[$i]."', '".$description[$i]."', '".$stat."', '".date('Y-m-d H:i:s')."')";

					$this->Query($exp);
					$this->Execute();
					$last_id = mysqli_insert_id($conn);


					/*======Upload logo=======*/

					$logo = $_FILES['logo']['tmp_name'][$i];						
					$short_logo = $_FILES['logo']['name'][$i];						
					move_uploaded_file($logo , $folder.$last_id.$short_logo);

					$letter = $_FILES['letter']['tmp_name'][$i];						
					$short_letter = $_FILES['letter']['name'][$i];						
					move_uploaded_file($letter , $folder.$last_id.$short_letter);

					$logo_upload = ("UPDATE `jobseeker_experience` SET `logo`='".($short_logo?$id.'/experience/'.$last_id.$short_logo:$logo_old)."',`recommendation_letter`='".($short_letter?$id.'/experience/'.$last_id.$short_letter:$letter_old)."' WHERE `id`='".$last_id."'");
					$this->Query($logo_upload);
					$this->Execute();
				}

				}
				// exit;
				$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
				$this->Query($query_com);
				$results = $this->fetchArray();
				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				// require_once("views/".$this->name."/".$this->task.".php"); 
				header("location:index.php?control=user&task=my_experience");
			}
			else {
				$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
				$this->Query($query_com);
				$results = $this->fetchArray();
					/*  $_SESSION['alertmessage'] = DUPLICATE; 
					$_SESSION['errorclass'] = ERRORCLASS;*/
					require_once("views/".$this->name."/".$this->task.".php"); 
				}
			}

			function my_skill()
			{ $id = $_REQUEST['id'];
				if($id)
				{
				$skills_title = $_REQUEST['skills_title'];
				$skills_description = $_REQUEST['skills_description'];


				$skill_name = $_REQUEST['skill_name'];
				$percent = $_REQUEST['percent'];
				// $year = $_REQUEST['year'];
				$description = $_REQUEST['description'];
				$status = $_REQUEST['check_status'];
				

				$update_profile = "UPDATE `users` SET `skills_title`='".$skills_title."', `skills_description`='".$skills_description."' WHERE `id` = '".$id."'";
				$this->Query($update_profile);	
				$this->Execute();

				/*============Add Skill=========*/
				$interest = implode(',', $_REQUEST['interest']);
				$this->Query("DELETE FROM `jobseeker_interest` WHERE `user_id`='".$id."'");
				$this->Execute();

				$this->Query("ALTER TABLE `jobseeker_interest` AUTO_INCREMENT = 1");
				$this->Execute();
				
				$this->Query("INSERT INTO `jobseeker_interest`(`user_id`, `name`, `status`, `date_created`) VALUES ('".$id."', '".$interest."', '1', '".date('Y-m-d H:i:s')."')");
				$this->Execute();

				$this->Query("DELETE FROM `jobseeker_skills` WHERE `user_id`='".$id."'");
				$this->Execute();
				
				$this->Query("ALTER TABLE `jobseeker_skills` AUTO_INCREMENT = 1");
				$this->Execute();

				for ($i=0; $i < count($skill_name); $i++) { 
					// $logo_old = $_REQUEST['logo'][$i];
					$stat = ($status[$i]=="1")?"0":"1";
					if($skill_name[$i]!=''){
					 $exp = "INSERT INTO `jobseeker_skills`(`user_id`, `skill_name`, `percent`, `description`, `status`, `date_created`) VALUES ('".$id."', '".$skill_name[$i]."', '".$percent[$i]."', '".$description[$i]."', '".$stat."', '".date('Y-m-d H:i:s')."')";
					

					$this->Query($exp);
					$this->Execute();
					}
				}

					$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
					$this->Query($query_com);
					$results = $this->fetchArray();
					$_SESSION['alertmessage'] = UPDATERECORD; 
					$_SESSION['errorclass'] = SUCCESSCLASS;
					// require_once("views/".$this->name."/".$this->task.".php"); 
					header("location:index.php?control=user&task=my_skill");
				}
				else {
					$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
					$this->Query($query_com);
					$results = $this->fetchArray();
					/*  $_SESSION['alertmessage'] = DUPLICATE; 
					$_SESSION['errorclass'] = ERRORCLASS;*/
					require_once("views/".$this->name."/".$this->task.".php"); 
				}
			}

			function my_education()
			{
				$id = $_REQUEST['id'];
				global $conn;
			if($id)
			{
				$education_title = $_REQUEST['education_title'];
				$education_description = $_REQUEST['education_description'];


				$institute_name = $_REQUEST['institute_name'];
				$location = $_REQUEST['location'];
				$title = $_REQUEST['title'];
				$year = $_REQUEST['year'];
				$description = $_REQUEST['description'];
				$status = $_REQUEST['check_status'];

				

				$update_profile = "UPDATE `users` SET `education_title`='".$education_title."', `education_description`='".$education_description."' WHERE `id` = '".$id."'";
				$this->Query($update_profile);	
				$this->Execute();

				/*============Add Experience=========*/
				if (!is_dir('media/user_profile/'.$id.'/certificate/')) {
				// dir doesn't exist, make it
					mkdir('media/user_profile/'.$id.'/certificate/',0777,true);
				}
				$folder = "media/user_profile/".$id.'/certificate/';

				$this->Query("DELETE FROM `jobseeker_education` WHERE `user_id`='".$id."'");
				$this->Execute();

				$this->Query("ALTER TABLE `jobseeker_education` AUTO_INCREMENT = 1");
				$this->Execute();

				for ($i=0; $i < count($institute_name); $i++) { 
					$certificate_old = $_REQUEST['certificate'][$i];
					$stat = ($status[$i]=="1")?"0":"1";
				if($institute_name[$i]!=''){
		$exp = "INSERT INTO `jobseeker_education`(`user_id`, `institute_name`, `location`, `title`, `year`, `description`, `status`, `date_created`) VALUES ('".$id."', '".$institute_name[$i]."', '".$location[$i]."', '".$title[$i]."', '".$year[$i]."', '".$description[$i]."', '".$stat."', '".date('Y-m-d H:i:s')."')";

		$this->Query($exp);
		$this->Execute();
		$last_id = mysqli_insert_id($conn);


		

		$certificate = $_FILES['certificate']['tmp_name'][$i];						
		$short_certificate = $_FILES['certificate']['name'][$i];						
		move_uploaded_file($certificate , $folder.$last_id.$short_certificate);

		$certificate_upload = ("UPDATE `jobseeker_education` SET `certificate`='".($short_certificate?$id.'/certificate/'.$last_id.$short_certificate:$certificate_old)."' WHERE `id`='".$last_id."'");
		$this->Query($certificate_upload);
						$this->Execute();

				}
				}
				// exit;
				$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
				$this->Query($query_com);
				$results = $this->fetchArray();
				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				// require_once("views/".$this->name."/".$this->task.".php"); 
				header("location:index.php?control=user&task=my_education");
			}
				else {
					$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
					$this->Query($query_com);
					$results = $this->fetchArray();
					/* $_SESSION['alertmessage'] = DUPLICATE; 
					$_SESSION['errorclass'] = ERRORCLASS;*/
					require_once("views/".$this->name."/".$this->task.".php"); 
				}
			}


function my_portfolio()
			{		global $conn;
				$id = $_REQUEST['id'];
			if($id)
			{
				
					$portfolio_title = $_REQUEST['portfolio_title'];
					$portfolio_description = $_REQUEST['portfolio_description'];
					$status = $_REQUEST['check_status'];


					if (!is_dir('media/user_profile/'.$id.'/portfolio/')) {
				// dir doesn't exist, make it
					mkdir('media/user_profile/'.$id.'/portfolio/',0777,true);
				}
				$folder = "media/user_profile/".$id.'/portfolio/';

				$this->Query("DELETE FROM `jobseeker_portfolio` WHERE `user_id`='".$id."'");
				$this->Execute();

				$this->Query("ALTER TABLE `jobseeker_portfolio` AUTO_INCREMENT = 1");
				$this->Execute();

				for ($i=0; $i < count($portfolio_title) ; $i++) { 
					$stat = ($status[$i]=="1")?"0":"1";
					if($portfolio_title[$i]!=''){
					$insert = ("INSERT INTO `jobseeker_portfolio`(`user_id`, `portfolio_title`, `portfolio_description`, `status`, `date_created`) VALUES ('".$id."', '".$portfolio_title[$i]."', '".$portfolio_description[$i]."', '".$stat."', '".date("Y-m-d H:i:s")."')");
					$this->Query($insert);
					$this->Execute();
					$last_id = mysqli_insert_id($conn);

 					$portfolio_image_old = $_REQUEST['old_portfolio'][$i];

					$portfolio_image = $_FILES['portfolio_image']['tmp_name'][$i];						
					$short_portfolio_image = $_FILES['portfolio_image']['name'][$i];						
					move_uploaded_file($portfolio_image , $folder.$last_id.$short_portfolio_image);

					$portfolio_image_upload = ("UPDATE `jobseeker_portfolio` SET `portfolio_img`='".($short_portfolio_image?$id.'/portfolio/'.$last_id.$short_portfolio_image:$portfolio_image_old)."' WHERE `id`='".$last_id."'");	
					$this->Query($portfolio_image_upload);
					$this->Execute();
					}
				}

				/*
				$this->Query($update_profile);	
				$this->Execute();*/
				
				$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
				$this->Query($query_com);
				$results = $this->fetchArray();
				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				require_once("views/".$this->name."/".$this->task.".php"); 
			}
			else {
				$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
				$this->Query($query_com);
				$results = $this->fetchArray();
					/*  $_SESSION['alertmessage'] = DUPLICATE; 
					$_SESSION['errorclass'] = ERRORCLASS;*/
					require_once("views/".$this->name."/".$this->task.".php"); 
				}
			}



function  online_support(){
		if((!empty($_REQUEST['id']))) {
				session_start();

				$query="update users set status=1 , gender='".$_REQUEST['gender']."'";			

				if(!empty($_FILES['certificate']['name'])) {
					if(move_uploaded_file($_FILES['logo']['tmp_name'],"images/admin/".$_FILES['logo']['name'])) {					
						$_SESSION['image'] = $_FILES['logo']['name'];
						$query .=",image='".$_FILES['logo']['name']."'"	;
					}

				}
				if(!empty($_REQUEST['fname'])){
				$query.=",fname='".$_REQUEST['fname']."'"	;
				$_SESSION['admin_name']=$_REQUEST['fname'];
				}		
				if(!empty($_REQUEST['lname'])){
				$query.=",lname='".$_REQUEST['lname']."'"	;
				}
				if(!empty($_REQUEST['mobile'])){
				$query.=",mobile='".$_REQUEST['mobile']."'"	;
				}
				$query.=" where id=".$_REQUEST['id'];
				$this->Query($query);
				$this->Execute();		 
			//header("location:index.php");
		}
		$query ="select * from users where id=".$_SESSION['adminid'];
		$this->Query($query);
		$results = $this->fetchArray();	foreach($results as $result){}
		require_once("views/".$this->name."/".$this->task.".php");	
	}




function  feedback(){
			
		$query = "SELECT * FROM `feedback_form` WHERE `status`=1 ORDER BY `id` ASC";

		$this->Query($query);
		$results = $this->fetchArray();		

		require_once("views/".$this->name."/".$this->task.".php"); 
	}











	function search(){	

		

		if(preg_match('|^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$|i',$_REQUEST['search'])) {

			$uquery="SELECT * from users  where email='".$_REQUEST['search']."'";

		}else if($_REQUEST['search']){

			$uquery="SELECT * from users where fname LIKE '%".$_REQUEST['search']."%'";

		}else{

			$uquery="SELECT * from users ";

		}	



		$this->Query($uquery);

		$uresults = $this->fetchArray();	

		$tdata=count($uresults);

		/* Paging start here */

		$page   = intval($_REQUEST['page']);

			$tpages = ($_REQUEST['tpages']) ? intval($_REQUEST['tpages']) : PERPAGE;//$tdata; // 20 by default

			$adjacents  = intval($_REQUEST['adjacents']);

			$tdata = ($tdata%$tpages)?(($tdata/$tpages)+1):round($tdata/$tpages);//$_GET['tpages'];// 

			$tdata = floor($tdata);

			if($page<=0)  $page  = 1;

			if($adjacents<=0) $tdata?($adjacents = 4):0;

			$reload = $_SERVER['PHP_SELF'] . "?control=".$_REQUEST['control']."&views=".$_REQUEST['view']."&task=".$_REQUEST['task']."&tmpid=".$_REQUEST['tmpid']."&tpages=" . $tpages . "&amp;adjacents=" . $adjacents;	

			/* Paging end here */



			if(preg_match('|^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$|i',$_REQUEST['search'])) {

				$query="SELECT * from users  where email='".$_REQUEST['search']."' LIMIT ".(($page-1)*$tpages).",".$tpages;

			}else if($_REQUEST['search']){

				$query="SELECT * from users where fname LIKE '%".$_REQUEST['search']."%' LIMIT ".(($page-1)*$tpages).",".$tpages;

			}else{

				$query="SELECT * from users LIMIT ".(($page-1)*$tpages).",".$tpages;

			}	

			

			$this->Query($query);

			$results = $this->fetchArray();	

			$this->task="show";

			require_once("views/".$this->name."/".$this->task.".php");	

		}

		

		

		function status(){

			$query="update users set status=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."'";	

			$this->Query($query);	

			$this->Execute();

			$this->task="show";

			$this->view ='show';

		//$this->show();	

			header("location:index.php?control=user");

		}

		

		

		function delete(){



			$query="DELETE FROM users WHERE id in (".$_REQUEST['id'].")";	

			$this->Query($query);

			$this->Execute();	

			$this->task="show";

			$this->view ='show';

		//$this->show();

			header("location:index.php?control=user");



		}
		function  changepassword(){
			require_once("views/".$this->name."/".$this->task.".php"); 
		}

		function  changepassword_save(){

			session_start();

		         //if(!empty($_REQUEST['id'])) {

			$query = "SELECT *  FROM users WHERE password='".$_REQUEST['oldpassword']."' and id='".$_REQUEST['id']."'";

			$this->Query($query);

			if($this->rowCount()) {
				if($_REQUEST['password']==$_REQUEST['cpassword']){
					$query1="update users set password = '".$_REQUEST['password']."' where id = '".$_REQUEST['id']."'";

					$this->Query($query1);

					$this->Execute();

					$_SESSION['alertmessage'] = 'Password Changed Successfully.'; 
					$_SESSION['errorclass'] = SUCCESSCLASS;

					echo "<script type='text/javascript'>";
					/*echo "alert('Password Changed Successfully.');";*/
					echo "window.location.href='index.php?control=user&task=changepassword'";
					echo "</script>";



				}else{

					$_SESSION['alertmessage'] = 'New Password & Confirm Password did not match.'; 
					$_SESSION['errorclass'] = ERRORCLASS;
					echo "<script type='text/javascript'>";
					/*echo "alert('New Password & Confirm Password did not match.');";*/
					echo "window.location.href='index.php?control=user&task=changepassword'";
					echo "</script>";  
				}
			} 
			else{
				
				$_SESSION['alertmessage'] = 'Old Password Not Match.'; 
				$_SESSION['errorclass'] = ERRORCLASS;

				echo "<script type='text/javascript'>";
				/*echo "alert('Old Password Not Match.');";*/
				echo "window.location.href='index.php?control=user&task=changepassword'";
				echo "</script>";  
			}
		}
		

/*===========Update profile image======*/
function profile_pic_update(){
	$id = $_REQUEST['id'];
	$control = $_REQUEST['old_control'];
	$task = $_REQUEST['old_task'];


if (!is_dir('media/user_profile/'.$id.'/profile_pic')) {
// dir doesn't exist, make it
mkdir('media/user_profile/'.$id.'/profile_pic',0777,true);
}
$folder = "media/user_profile/".$id.'/profile_pic/';

$image = $_FILES['image']['tmp_name'];						
$short_image = $_FILES['image']['name'];						
move_uploaded_file($image , $folder.$id.$short_image);


	$upload_atchmt = ("UPDATE `users` SET `image`='".($short_image?$id.'/profile_pic/'.$id.$short_image:'')."' WHERE `id`='".$id."'"); 	
	$this->Query($upload_atchmt);
	$this->Execute();

	$insert = "INSERT INTO `user_profile_image`(`user_id`, `image`, `date_time`) VALUES ('".$id."','".($short_image?$id.'/profile_pic/'.$id.$short_image:'')."','".date('Y-m-d H:i:s')."' )"; 	
	$this->Query($insert);
	$this->Execute();

	$_SESSION['alertmessage'] = 'Profile Photo Uploaded Successfully.'; 
	$_SESSION['errorclass'] = SUCCESSCLASS;
	$_SESSION['user_image'] = $id.'/profile_pic/'.$id.$short_image;
	header("location:index.php?control=".$control."&task=".$task);
}


function save_feedback(){

		$user_id = $_SESSION['adminid'];
		$feedback_id = $_REQUEST['feedback_id'];

		$this->Query("SELECT `id` FROM `feedback_rating` WHERE `user_id`='".$user_id."' ");
		$this->Execute();
		$chk = $this->numRow();
		// exit;
if(!$chk){
for ($i=0; $i <count($feedback_id) ; $i++) { 
		$rating = $_REQUEST['rating'.($i+1)];
			 	$insert = "INSERT INTO `feedback_rating`(`user_id`, `feedback_id`, `rating`, `date_create`, `date_modified`) VALUES ('".$user_id."', '".$feedback_id[$i]."', '".$rating."', '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."' )";

			$this->Query($insert);
			$this->Execute();
			}
			// exit;
		}else{

	for ($i=0; $i <count($feedback_id) ; $i++) { 
		$rating = $_REQUEST['rating'.($i+1)];
				$update = "UPDATE `feedback_rating` SET  `rating`='".$rating."', `date_modified`='".date('Y-m-d H:i:s')."' WHERE `feedback_id`='".$feedback_id[$i]."' AND `user_id`='".$user_id."'";

			$this->Query($update);
			$this->Execute();
			}
		}
	$_SESSION['alertmessage'] = 'Form Successfully Submitted..'; 
	$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=user&task=feedback");
	}

	function save_todo(){
		global $conn;
		$user_id = $_SESSION['adminid'];
		$id = $_REQUEST['id'];
		$schedule = $_REQUEST['schedule'];
		$color = $_REQUEST['bg_color'];
		$todo = mysqli_real_escape_string($conn, $_REQUEST['to_do']);

		if(!$id){
			$query = "INSERT INTO `todo_list` (`user_id`, `to_do`, `schedule`, `color`, `date_created`) VALUES ('".$user_id."', '".$todo."', '".$schedule."', '".$color."', '".date('Y-m-d H:i:s')."')";

		}else{

			$query = "UPDATE `todo_list` SET `to_do`='".$todo."', `schedule`='".$schedule."', `color`='".$color."' WHERE `id`='".$id."' AND `user_id`='".$user_id."'";
		}
		$this->Query($query);
		$this->Execute();
		header("location:index.php");
	}

	function delete_todo(){
		$id = $_REQUEST['id'];
		$user_id = $_SESSION['adminid'];

		$delete = "DELETE FROM `todo_list` WHERE `id`='".$id."' AND `user_id`='".$user_id."'";
		$this->Query($delete);
		$this->Execute();
		header("location:index.php");
	}

	function page_status(){
		$status = $_REQUEST['status'];
		$user_id = $_SESSION['adminid'];

		$status = "UPDATE `users` SET `page_status`='".$status."' WHERE `id`='".$user_id."'";
		$this->Query($status);
		$this->Execute();
		echo $page = ($_REQUEST['status']=='1'?' Un-Published':' Published');
		// exit;
		$_SESSION['alertmessage'] = 'Page Successfully'.$page; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php");
	}

	function recruiter_profile(){
		$id = $_REQUEST['id'];
		if($id){
			/*==Main Table==*/
			$name = $_REQUEST['name'];	
			$dob = $_REQUEST['dob'];			
			$mobile = $_REQUEST['mobile'];			//	$alt_no = $_REQUEST['alt_no'];
			$email = $_REQUEST['email'];				
			$facebook = $_REQUEST['facebook'];				$twitter = $_REQUEST['twitter'];	
			$instagram = $_REQUEST['instagram'];			$linkedin = $_REQUEST['linkedin'];

			$country = $_REQUEST['country'];				$state_id = $_REQUEST['state_id'];
			$city_id = $_REQUEST['city_id'];				$address = $_REQUEST['address'];
			$postalcode = $_REQUEST['postalcode'];

			$title_me = $_REQUEST['title_me'];
			$about_me = $_REQUEST['about_me'];
				
			$quote1 = $_REQUEST['quote1'];		
		

			
			/*==Bank Info==*/
			$bank_name = $_REQUEST['bank_name'];			$ac_no = $_REQUEST['ac_no'];
			$ifsc_code = $_REQUEST['ifsc_code'];			$ac_type = $_REQUEST['ac_type'];

			/*==Company/Director==*/
			$director_name = $_REQUEST['director_name'];	 $image = $_REQUEST['image'];
			$directors_words = $_REQUEST['directors_words']; $director_mobile = $_REQUEST['director_mobile'];
			$director_email = $_REQUEST['director_email']; 	$contact_pname = $_REQUEST['contact_pname'];
			$contact_pnum = $_REQUEST['contact_pnum']; 		$contact_pemail = $_REQUEST['contact_pemail'];
			$office_add = $_REQUEST['office_add']; 			$country_office = $_REQUEST['country_office'];
			$state_office = $_REQUEST['state_office']; 		$city_office = $_REQUEST['city_office'];
			$pin_office = $_REQUEST['pin_office']; 	


			$update_profile = "UPDATE `users` SET `name`='".$name."', `dob`='".$dob."', `mobile`='".$mobile."', `facebook`='".$facebook."', `twitter`='".$twitter."', `instagram`='".$instagram."', `linkedin`='".$linkedin."', `country`='".$country."', `state_id`='".$state_id."', `city_id`='".$city_id."', `address`='".$address."', `title_me`='".$title_me."', `about_me`='".$about_me."', `postalcode`='".$postalcode."', `quote1`='".$quote1."', `date_time`='".date('Y-m-d H:i:s')."' WHERE `id`='".$id."'";
			$this->Query($update_profile);	
			$this->Execute();



			$update_bank = "UPDATE `bank_detail` SET `bank_name`='".$bank_name."', `ifsc_code`='".$ifsc_code."', `ac_no`='".$ac_no."', `ac_type`='".$ac_type."', `date_modify`='".date('Y-m-d H:i:s')."' WHERE `user_id`='".$id."'";
			$this->Query($update_bank);	
			$this->Execute();

			/*========================Document Upload=================*/
			if (!is_dir('media/user_profile/'.$id)) {
				mkdir('media/user_profile/'.$id,0777,true);
			}
			$folder = "media/user_profile/".$id.'/';
			$image_old = $_REQUEST['image'];
			$image = $_FILES['image']['tmp_name'];						
			$short_image = $_FILES['image']['name'];						
			move_uploaded_file($image , $folder.$id.'director_'.$short_image);
			$direcot_img = ($short_image?$id.'/'.$id.'director_'.$short_image:$image_old);
			
			$update_company = "UPDATE `comapny_info` SET `director_name`='".$director_name."', `image`='".$direcot_img."', `directors_words`='".$directors_words."', `director_mobile`='".$director_mobile."', `director_email`='".$director_email."', `contact_pname`='".$contact_pname."', `contact_pnum`='".$contact_pnum."', `contact_pemail`='".$contact_pemail."', `office_add`='".$office_add."', `country_office`='".$country_office."', `state_office`='".$state_office."', `city_office`='".$city_office."', `pin_office`='".$pin_office."', `date_modify`='".date('Y-m-d H:i:s')."' WHERE `user_id`='".$id."'";
			$this->Query($update_company);	
			$this->Execute();	
			$_SESSION['alertmessage'] = UPDATERECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			header("location:index.php?control=user&task=recruiter_profile");

		}else{

			$query ="SELECT * FROM `users` WHERE `id` = ".$_SESSION['adminid'];
			$this->Query($query);
			$results = $this->fetchArray();
			$query1 ="SELECT * FROM `bank_detail` WHERE `user_id` = ".$_SESSION['adminid'];
			$this->Query($query1);
			$banks = $this->fetchArray();
			$query2 ="SELECT * FROM `comapny_info` WHERE `user_id` = ".$_SESSION['adminid'];
			$this->Query($query2);
			$companies = $this->fetchArray();
			require_once("views/".$this->name."/".$this->task.".php"); 			
		}

	}
	
	function testimonials()
			{		
			$id = $_REQUEST['id'];
			global $conn;
			if($id)
			{
				$person_name = $_REQUEST['person_name'];				
				$testimonial = $_REQUEST['testimonial'];
				/*===================*/
				$status = $_REQUEST['check_status'];
				/*=================*/


				/*============Add testimonials=========*/
				if (!is_dir('media/user_profile/'.$id.'/testimonials/')) {
				// dir doesn't exist, make it
					mkdir('media/user_profile/'.$id.'/testimonials/',0777,true);
				}
				$folder = "media/user_profile/".$id.'/testimonials/';

				$this->Query("DELETE FROM `recruiter_testimonials` WHERE `user_id`='".$id."'");
				$this->Execute();

				$this->Query("ALTER TABLE `recruiter_testimonials` AUTO_INCREMENT = 1");
				$this->Execute();

				for ($i=0; $i < count($person_name); $i++) { 
					$logo_old = $_REQUEST['logo'][$i];
					
					/*=================*/
					$stat = ($status[$i]=="1")?"0":"1";
					/*=================*/
					if($person_name[$i]!=''){
					$exp = "INSERT INTO `recruiter_testimonials`(`user_id`, `person_name`, `testimonial`, `status`, `date_created`) VALUES ('".$id."', '".$person_name[$i]."', '".$testimonial[$i]."', '".$stat."', '".date('Y-m-d H:i:s')."')";

					$this->Query($exp);
					$this->Execute();
					$last_id = mysqli_insert_id($conn);


					/*======Upload logo=======*/

					$logo = $_FILES['logo']['tmp_name'][$i];						
					$short_logo = $_FILES['logo']['name'][$i];						
					move_uploaded_file($logo , $folder.$last_id.$short_logo);

					
					$logo_upload = ("UPDATE `recruiter_testimonials` SET `logo`='".($short_logo?$id.'/testimonials/'.$last_id.$short_logo:$logo_old)."' WHERE `id`='".$last_id."'");
					$this->Query($logo_upload);
					$this->Execute();
				}

				}
				// exit;
				$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
				$this->Query($query_com);
				$results = $this->fetchArray();
				$_SESSION['alertmessage'] = UPDATERECORD; 
				$_SESSION['errorclass'] = SUCCESSCLASS;
				// require_once("views/".$this->name."/".$this->task.".php"); 
				header("location:index.php?control=user&task=testimonials");
			}
			else {
				$query_com ="SELECT * FROM users WHERE id = ".$_SESSION['adminid'];
				$this->Query($query_com);
				$results = $this->fetchArray();
					/*  $_SESSION['alertmessage'] = DUPLICATE; 
					$_SESSION['errorclass'] = ERRORCLASS;*/
					require_once("views/".$this->name."/".$this->task.".php"); 
				}
			}
}
?>