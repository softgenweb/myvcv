<?php

require_once('dbaccess.php');

require_once('textconfig/config.php');		

if(file_exists('configuration.php')){		

	require_once('configuration.php');

}
class MediaClass extends DbAccess {
	public $view='';
	public $name='media';		


function show(){

		$query = "SELECT * FROM `jobseeker_video` WHERE  `user_id`='".$_SESSION['adminid']."' ";

		$this->Query($query);
		$results = $this->fetchArray();		

		require_once("views/".$this->name."/".$this->task.".php"); 
	}


	function change_image(){
		$id = $_REQUEST['id'];
		$image =$_REQUEST['profile_image'];
		// profile_image
		$upload_atchmt = ("UPDATE `users` SET `image`='".$image."' WHERE `id`='".$id."'"); 

	$this->Query($upload_atchmt);
	$this->Execute();
	$_SESSION['alertmessage'] = 'Profile Photo Uploaded Successfully'; 
	$_SESSION['errorclass'] = SUCCESSCLASS;
	$_SESSION['user_image'] = $image;
	header("location:index.php?control=media&task=show");
	}

	function delete_video(){
		$vid = $_REQUEST['vid'];
		$user_id = $_SESSION['adminid'];


		$delete = "DELETE FROM `jobseeker_video` WHERE `id`='".$vid."' AND `user_id`='".$user_id."'";
		$video = $this->get_video_name($user_id,$vid);
		 $path = 'media/user_profile/'.$_SESSION['adminid'].'/videos/'.$video;
	
		$remove_video = unlink($path);
		if($remove_video){
				$this->Query($delete);
				$this->Execute();
			}
		$_SESSION['alertmessage'] = 'Video Successfully Deleted'; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=media&task=show");
	}
	function delete_image(){
		$id = $_REQUEST['id'];
		$user_id = $_SESSION['adminid'];


		$delete = "DELETE FROM `user_profile_image` WHERE `id`='".$id."' AND `user_id`='".$user_id."'";
		$img = $this->get_image_name($user_id,$id);

		$update_dp = ("UPDATE `users` SET `image`='' WHERE `image`='".trim($img)."'"); 
		 $path = 'media/user_profile/'.$img;
	// exit;
		 if($_SESSION['user_image'] == $img){
				$_SESSION['user_image'] = '';
			}
		$remove_img = unlink($path);
		if($remove_img){
				$this->Query($delete);
				$this->Execute();

				$this->Query($update_dp);
				$this->Execute();
			}
		$_SESSION['alertmessage'] = 'Image Successfully Deleted'; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		// $_SESSION['user_image'] = '';
		header("location:index.php?control=media&task=show");
	}

	function video_status(){
		$id = $_REQUEST['status'];
		$user_id = $_SESSION['adminid'];
		// $status = $_REQUEST['status'];

		$update = "UPDATE `jobseeker_video` SET `status`=1 WHERE `id`='".$id."' AND `user_id`='".$user_id."'";
			$this->Query($update);
				$this->Execute();
		$update1 = "UPDATE `jobseeker_video` SET `status`=0 WHERE `id`!='".$id."' AND `user_id`='".$user_id."'";
		$this->Query($update1);
		$this->Execute();

		$_SESSION['alertmessage'] = 'Video is Setted to Default'; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=media&task=show");
	}

	function upload_video(){
		global $conn;
		$user_id = $_SESSION['adminid'];
		$tempName = $_FILES['video_upload']['tmp_name'];	
		$fileName = $_FILES['video_upload']['name'];	
		
		$videoPath = 'media/user_profile/'.$_SESSION['adminid'].'/videos/';
		if(!is_dir($videoPath)){
		    mkdir('media/user_profile/'.$_SESSION['adminid'].'/videos/', 0777, true);
		}

    	$filePath = $videoPath . $fileName;
    
    // make sure that one can upload only allowed audio/video files
	  /*  $allowed = array('webm','wav','mp4','mkv','mp3','ogg');
	    $extension = pathinfo($filePath, PATHINFO_EXTENSION);
	    if (!$extension || empty($extension) || !in_array($extension, $allowed)) {
	        echo 'Invalid file extension: '.$extension;
	        return 0;
	    }*/						
		move_uploaded_file($tempName, $filePath);
							
		
	 $upload_atchmt = ("INSERT INTO `jobseeker_video`(`user_id`, `video_name`, `status`, `date_time`) VALUES ('".$_SESSION['adminid']."', '".$fileName."', '1', '".date('Y-m-d H:i:s')."')"); 	

	$this->Query($upload_atchmt);
	$this->Execute();

		$id = mysqli_insert_id($conn);

		$update1 = "UPDATE `jobseeker_video` SET `status`= 0 WHERE `id`!='".$id."' AND `user_id`='".$user_id."'";
	
		$this->Query($update1);
		$this->Execute();
	// exit;
		$_SESSION['alertmessage'] = 'Video is Setted to Default'; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=user&task=video_detail");
	}


	function no_def_video(){
		$user_id = $_SESSION['adminid'];

		if($user_id){
			$this->Query("UPDATE `jobseeker_video` SET `status`=0 WHERE `user_id`='".$user_id."'");
			$this->Execute();

		$_SESSION['alertmessage'] = 'No Video is Setted to Default'; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=media&task=show");
		}
	}

	function no_def_img(){
		$user_id = $_SESSION['adminid'];

		if($user_id){
			$this->Query("UPDATE `users` SET `image`= '' WHERE `id`='".$user_id."'");
			$this->Execute();

		$_SESSION['alertmessage'] = 'No profile Image is Setted to Default'; 
		$_SESSION['user_image'] = ''; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
		header("location:index.php?control=media&task=show");
		}
	}
}
