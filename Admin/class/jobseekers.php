<?php

require_once('dbaccess.php');

require_once('textconfig/config.php');		

if(file_exists('configuration.php')){		

	require_once('configuration.php');

}
class JobseekerClass extends DbAccess {
	public $view='';
	public $name='jobseeker';		


function paid_list(){

		$query = "SELECT * FROM `users` WHERE `utype`='job_seeker' AND `payment_status`='1' ORDER BY `id` DESC";
		$_SESSION['paid_users'] = $query;
		$this->Query($query);
		$results = $this->fetchArray();		

		require_once("views/".$this->name."/".$this->task.".php"); 
	}

function unpaid_list(){

		$query = "SELECT * FROM `users` WHERE `utype`='job_seeker' AND `payment_status`!='1' ORDER BY `id` DESC";
		$_SESSION['unpaid_users'] = $query;
		$this->Query($query);
		$results = $this->fetchArray();		

		require_once("views/".$this->name."/".$this->task.".php"); 
	}


function status(){
		$task = $_REQUEST['pg'];
		$query="UPDATE `users` SET `status`=".$_REQUEST['status']." WHERE id='".$_REQUEST['id']."' AND `utype`='job_seeker'";	
		$this->Query($query);	
		$this->Execute();
		$_SESSION['alertmessage'] = STATUS; 
		$_SESSION['errorclass'] = SUCCESSCLASS;
				
		header("location:index.php?control=jobseeker&task=".$task);
	}

/*===================Update Job Seeker Profile===============*/	

	function edit_details(){
		$id = $_REQUEST['user_id'];		$editid = $_REQUEST['editid'];
		$task = $_REQUEST['pg'];

		$name = $_REQUEST['name'];		$gender = $_REQUEST['gender'];
		$dob = $_REQUEST['dob'];		$joining_date = $_REQUEST['date_time'];
		$recruiter_id = $_REQUEST['recruiter_id'];		$title = $_REQUEST['title'];
		$coupon = $_REQUEST['coupon'];

		if($id!='' && $editid==1){
		 	$Update = "UPDATE `users` SET `title`='".$title."', `name`='".$name."', `gender`='".$gender."', `dob`='".$dob."', `date_time`='".$joining_date."', `recruiter_id`='".$recruiter_id."', `coupon`='".$coupon."' WHERE `id`='".$id."' AND `utype`='job_seeker'";

			$this->Query($Update);
			$this->Execute();
			$_SESSION['alertmessage'] = UPDATERECORD; 
			$_SESSION['errorclass'] = SUCCESSCLASS;
			header("location:index.php?control=jobseeker&task=".$task);			
		}else{

			$query = "SELECT * FROM `users` WHERE `utype`='job_seeker' AND `id`='".$id."'";
			$this->Query($query);
			$results = $this->fetchArray();		

			require_once("views/".$this->name."/".$this->task.".php"); 			
		}
	}


}
