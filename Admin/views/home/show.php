<?php
include('./configuration.php'); 

$daily_visitor = mysqli_num_rows(mysqli_query($conn,"SELECT (`system_ip`) FROM `visitors` WHERE `date_created` LIKE '".date('Y-m-d')."%' AND `user_id`='".$_SESSION['adminid']."'"));
$total_visitor = mysqli_num_rows(mysqli_query($conn,"SELECT (`system_ip`) FROM `visitors` WHERE `user_id`='".$_SESSION['adminid']."'"));

$new_msg = mysqli_num_rows(mysqli_query($conn,"SELECT `id` FROM `support_chat` WHERE  `to_user`='".$_SESSION['adminid']."' AND `chat_status`=1"));


$my_detail = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM `users` WHERE `id`='".$_SESSION['adminid']."'"));
 $page_status = $my_detail['page_status'];

?>

<div class="panel panel-default">
	<div class="col-md-6" style="padding:0px;">

	</div>
  <style type="text/css">
    .ui-datepicker-inline.ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all{
      width: 100% !important;
    }
    .bg-lime{
      color: #444 !important;
    }
    .fc-event-container a{
      color: #fff;
    }
    #calendar thead {
      background: transparent;
    }
    #move_list{
      max-height: 390px;
      scrollbar-width:thin;
    }
    .todo-list > li .tools {
      display: inline-block !important;
    }
    .fc-ltr .fc-basic-view .fc-day-number{
      cursor: pointer;
    }
    .pro_color{
      padding: unset;
      margin: unset;
      color: #000;
    }
    #color_msg{
      display: none;
      background: green;
      padding: 8px;
      color: white;
    }
    #move_list li{
      cursor: move;
    }
@media only screen and (min-width: 768px) {
  #myModal .modal-dialog{
    width: 720px !important;
  }
}
.text-center a{
  color: #fff !important;
}
  </style>
<!-- ====================================================== -->
<!-- Modal -->
<button id="tips_box_btn"  data-toggle="modal" data-target="#tips_box" style="display: none;"></button>
<?php if($my_detail['home_tips']=='1'){ ?>
<div id="tips_box" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h4 class="modal-title">Here are some Tips for you:</h4></center>
      </div>
      <div class="modal-body">
        <div class="text-center">
          <table class="table table-striped table-bordered">
            <tr><td>1.</td><td align="left">Change your Default Password for your security. Go to the Profile Tab on the top right side of the page. </td>
            </tr>
            <tr><td>2.</td><td align="left">Update your Biodata and Profile Details from "Update Profile Tab"</td>
            </tr>
            <tr><td>3.</td><td align="left">Update your Video CV </td>
            </tr>
            <tr><td>4.</td><td align="left">Click on the Display Image to update it</td>
            </tr>
            <tr><td>5.</td><td align="left">Click "View Profile" Button, to preview of your Profile Page</td>
            </tr>
            <tr><td>6.</td><td align="left">Click on Publish Button to Publish your Profile page - to make it visible for public.</td>
            </tr>
            <tr><td>7.</td><td align="left">After that you can share your profile url, with anyone you like..</td>
            </tr>
            <tr><td colspan="2"><hr></td></tr>
            <tr><td>I</td><td align="left">If you have any doubts, concerns or questions; send them across through "Online Support" Tab. </td>
            </tr>
            <tr><td>II</td><td align="left">We will appreciate your Feedback - Go to the "Feedback Page"</td>
            </tr>
          </table>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <div class="pull-left"><input type="checkbox" value="close" id="no_tips" name="no_tips"> No Tips </div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php } ?>
<!-- ====================================================== -->
<!-- ====================================================== -->
<!-- ====================================================== -->  
  <?php if(isset($_SESSION['alertmessage'])){?>
    <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
    </div>

    <?php 
    unset($_SESSION['alertmessage']);
    unset($_SESSION['errorclass']); 
  } ?>
  <?php 
  if($_SESSION['utype']=='Administrator') { ?> 
   <section class="content">

    <div class="row">
      <h3 class="box-title">Monetization</h3>
      <div class="col-lg-3 col-md-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-blue">
         <div class="inner">
          <h3><?php  echo 0;?></h3>
          <p>Payments</p>
        </div>
        <div class="icon">
          <i class="fa fa-file-archive-o"></i>
        </div>
        <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-md-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
       <div class="inner">
        <h3><?php echo 0;//$provider4= $provider4 ? $provider4 : 0; ?></h3>
        <p>Promo Codes</p>
      </div>
      <div class="icon">
        <i class="fa fa-exclamation"></i>
      </div>
      <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div><!-- ./col -->

<div class="col-lg-3 col-md-6 col-xs-12">
  <!-- small box -->
  <div class="small-box bg-orange">
   <div class="inner">
    <h3><?php echo 0; //$provider5=$provider5 ? $provider5 : 0; ?></h3>
    <p>Commissions</p>
  </div>
  <div class="icon">
    <i class="fa fa-inr"></i>
  </div>
  <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
</div>
</div><!-- ./col -->


</div><!-- /.row -->
    <div class="row">
      <h3 class="box-title">Job Seekers</h3>
      <div class="col-lg-3 col-md-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-blue">
         <div class="inner">
          <h3><?php  echo 0;?></h3>
          <p>List</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-md-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
       <div class="inner">
        <h3><?php echo 0;//$provider4= $provider4 ? $provider4 : 0; ?></h3>
        <p>Messages</p>
      </div>
      <div class="icon">
        <i class="fa fa-envelope"></i>
      </div>
      <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div><!-- ./col -->

<div class="col-lg-3 col-md-6 col-xs-12">
  <!-- small box -->
  <div class="small-box bg-orange">
   <div class="inner">
    <h3><?php echo 0; //$provider5=$provider5 ? $provider5 : 0; ?></h3>
    <p>Feedbacks</p>
  </div>
  <div class="icon">
    <i class="fa fa-reply"></i>
  </div>
  <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
</div>
</div><!-- ./col -->

<div class="col-lg-3 col-md-6 col-xs-12">
  <!-- small box -->
  <div class="small-box bg-orange">
   <div class="inner">
    <h3><?php echo 0;//$Advertisement=$advertise ? $advertise : 0; ?></h3>
    <p>Notifications</p>
  </div>
  <div class="icon">
    <i class="fa fa-bell"></i>
  </div>
  <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
</div>
</div><!-- ./col -->
</div><!-- /.row -->


<div class="row">
      <h3 class="box-title">Recruiters</h3>
      <div class="col-lg-3 col-md-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-blue">
         <div class="inner">
          <h3><?php  echo 0;?></h3>
          <p>List</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-md-6 col-xs-12">
      <!-- small box -->
      <div class="small-box bg-green">
       <div class="inner">
        <h3><?php echo 0;//$provider4= $provider4 ? $provider4 : 0; ?></h3>
        <p>Messages</p>
      </div>
      <div class="icon">
        <i class="fa fa-envelope"></i>
      </div>
      <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div><!-- ./col -->

<div class="col-lg-3 col-md-6 col-xs-12">
  <!-- small box -->
  <div class="small-box bg-red">
   <div class="inner">
    <h3><?php echo 0; //$provider5=$provider5 ? $provider5 : 0; ?></h3>
    <p>Feedbacks</p>
  </div>
  <div class="icon">
    <i class="fa fa-reply"></i>
  </div>
  <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
</div>
</div><!-- ./col -->

<div class="col-lg-3 col-md-6 col-xs-12">
  <!-- small box -->
  <div class="small-box bg-orange">
   <div class="inner">
    <h3><?php echo 0;//$Advertisement=$advertise ? $advertise : 0; ?></h3>
    <p>Notifications</p>
  </div>
  <div class="icon">
    <i class="fa fa-bell"></i>
  </div>
  <a href="" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
</div>
</div><!-- ./col -->

</div><!-- /.row -->








</section>
<?php }elseif($_SESSION['utype']=='job_seeker') { ?> 
	<section class="content">
		<div class="row"> <center><i class="fa fa-info-circle" data-toggle="modal" data-target="#tips_box" style="cursor: pointer;"></i></center>
      <div class="container" style="border:1px solid #cc5b5b;border-radius: 10px;margin:0 15px;padding:15px 0;width: 97%;">
        <div class="col-xs-12 col-md-3">
          <?php if($page_status=='2'){ ?>
            <a href="javascript:;"  data-toggle="modal" data-target="#share_box" id="share_btn" class="btn btn-primary  btn-block btn-flat">Share Profile <i class="fa fa-share-alt" aria-hidden="true"></i></a>
            <!-- <a onclick="prompt('Press Ctrl + C, then Enter to copy to clipboard','http://<?php echo strtolower($_SESSION['username']).".".$_SERVER['HTTP_HOST']; ?>')" class="btn btn-primary bulu btn-block btn-flat"><i class="fa fa-share-alt" aria-hidden="true"></i></a> -->
          <?php } ?></div>
     
      <div class="col-xs-12 col-md-6">
        <center>
          <?php if($page_status=='1'){ ?>
            <a href="index.php?control=user&task=page_status&status=2" class="btn btn-primary bulu btn-block btn-flat">Publish</a>
          <?php }else{ ?>
            <a href="index.php?control=user&task=page_status&status=1" class="btn bg-olive btn-flat  btn-block">Un-Publish</a>
          <?php } ?></center> 
          <!-- <button class="btn btn-primary bulu btn-block btn-flat">Publish</button> -->
        </div>
        
         <div class="col-xs-12 col-md-3 ">
          <!-- <a  target="_blank" class="btn btn-primary bulu pull-right" href="http://www.<?php echo strtolower($_SESSION['username']); ?>.myvcv.asia/">View Profile</a> -->
           <a  target="_blank" class="btn btn-primary bulu pull-right" href="../preview/?page=preview">View Profile</a>
         </div>


        <div class="clearfix"></div><br>

        <div class="col-xs-12 col-md-4" >
         <h3 class="pro_color"> Select Color for your profile.</h3>
        </div>
     
      <div class=" col-xs-12 col-md-4">
        
          <div class="btn-group-horizontal">
            <button id="red_color" type="button" onclick="select_color('',<?php echo $_SESSION['adminid']; ?>);" class="btn btn-danger" style="background-color: #cd5d5c;color: #fff;" <?php if($my_detail['profile_color']==''){echo "disabled";} ?>>Color 1</button>
            <button id="1073cb" type="button" onclick="select_color('1073cb',<?php echo $_SESSION['adminid']; ?>);" class="btn btn-default blue" style="background-color: #1073cb;color: #fff;" <?php if($my_detail['profile_color']=='#1073cb'){echo "disabled";} ?>>Color 2</button>
            <button id="0ccead" type="button" onclick="select_color('0ccead',<?php echo $_SESSION['adminid']; ?>);" class="btn btn-default green" style="background-color: #0ccead;color: #fff;" <?php if($my_detail['profile_color']=='#0ccead'){ echo "disabled";} ?>>Color 3</button>     
          </div>
       
      </div>
        
         <div class="col-xs-12 col-md-3">
           <span id="color_msg" style="display: none;">Profile Color Successfully Change...</span>
         </div>
         <div class="clearfix"></div>
        </div>
        <!-- <div class="clearfix"></div><br> --><br>
        <div class="col-lg-3 col-md-6 col-xs-12">
        <a href="index.php?control=user&task=personal_detail">
          <!-- small box -->
          <div class="small-box bg-red">
           <div class="inner">
            <h3><?php  echo 0;?></h3>
            <p>Update your Profile</p>
          </div>
          <div class="icon">
            <i class="fa fa-user"></i>
          </div>
          <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
        </div>
          </a>
      </div><!-- ./col -->


      <div class="col-lg-3 col-md-6 col-xs-12">
        <!-- small box -->
        <a href="index.php?control=user&task=video_detail">
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?php echo 0;//$buyer = $buyer5 ? $buyer5 : 0; ?></h3>
            <p>Update Your Video</p>
          </div>
          <div class="icon">
            <i class="fa fa-video-camera"></i>
            
          </div>
          <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
        </div>
      </a>
      </div>

      
      <div class="col-lg-3 col-md-6 col-xs-12">
        <!-- small box -->
        <a href="index.php?control=support&task=show" >
        <div class="small-box bg-orange">
         <div class="inner">
          <h3><?php echo $new_msg ? $new_msg : 0; ?></h3>
          <p>View your Messages</p>
        </div>
        <div class="icon">
          <i class="fa fa-envelope"></i>
        </div>
        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
      </div></a>
    </div><!-- ./col -->







    <div class="col-lg-3 col-md-6 col-xs-12">  
      <!-- small box -->
      <a href="index.php?control=home&task=visitors">
      <div class="small-box bg-red">
       <div class="inner">
        <h3><?php echo $total_visitor?$total_visitor:0; ?></h3>
        <p>Total Profile Visitors</p>
      </div>
      <div class="icon">
        <i class="fa fa-users"></i>
      </div>
      <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
    </div></a>
        </div> <!-- ./col -->

			
      <div class="clearfix"></div>
      <div class="col-lg-5 col-xs-12" >
       <div class="box box-primary " style="border:1px solid #cc5b5b;border-radius: 10px;">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">To Do List</h3>
          <div class="box-tools pull-right">

                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list " id="move_list">
                  	<?php $this->Query("SELECT * FROM `todo_list` WHERE `user_id`='".$_SESSION['adminid']."' ORDER BY `list_order` ASC"); 
                    $todos = $this->fetchArray();
                    $i=0;
                    foreach ($todos as $todo) {
                     $i++;
                     ?>
                     <li class="list_li <?php echo $todo['color']; echo ($todo['status']=='1')?'':' done'; ?>">
                      <input type="hidden" class="list_order" id="<?php echo $todo['id']; ?>" name="list_order[]" value="<?php echo $i; ?>">
                      <!-- <input type="hidden" class="list_id" name="list_id[]" value="<?php echo $todo['id']; ?>"> -->
                      <!-- <span class="list_order"><?php echo $i; ?></span> -->
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" id="done_todo<?php echo $todo['id']; ?>" onclick="done_todo(<?php echo $todo['id']; ?>)" value="" name="done_todo[]" <?php echo ($todo['status']=='1')?'':'checked'; ?>>
                      <!-- todo text -->
                      <div class="text"><?php echo $todo['to_do']; ?></div>
                      <!-- Emphasis label -->
                     &nbsp;
                      <!-- General tools such as edit or delete-->
                      <div class="tools pull-right">
                        <a href="" data-toggle="modal" data-target="#todo_popup<?php echo $i; ?>"><i class="fa fa-edit"></i></a>
                        <a onclick="return confirm('want to delete?');" href="index.php?control=user&task=delete_todo&id=<?php echo $todo['id'] ?>"><i class="fa fa-trash-o"></i></a>
                      </div>&nbsp;
                      <?php if($todo['schedule']){ ?>
                       <div class="label label-danger pull-right" style="margin: 0 7px;"><i class="fa fa-calendar"></i> <?php  
                      $todo_date = explode(' ', $todo['schedule']);
                      echo $todo_date[0]."<br>".$todo_date[1];
                       ?></div>
                     <?php } ?>&nbsp;
                      <!-- ===========Popup============= -->
                      <div id="todo_popup<?php echo $i; ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <center> <h4 class="modal-title">Edit TODO</h4></center>
                            </div>
                            <div class="modal-body">
                              <p>
                          <form name="form" method="post" enctype="multipart/form-data">

                           <div class="col-md-10 col-md-offset-1 col-xs-12">

                              <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group"> 
                               <label>Choose Color</label>
                             </div>
                              </div>
                             <div class="col-md-8 col-sm-8 col-xs-12" >
                              <div class="form-group">
                                <ul class="fc-color-picker" id="color-chooser">
                                  <li><a class="text-aqua select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-blue select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-light-blue select_color" href="javascript:;" style="color: #cd5c5c !important;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-teal select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-yellow select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-orange select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-green select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-lime select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-red select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-purple select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-fuchsia select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-muted select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-navy select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                </ul>
                              </div><!-- /btn-group -->
                           </div>
                          <div class="clearfix"></div>
                           
                          <div class="col-md-4 col-sm-4 col-xs-12">
                             <div class="form-group" >
                           <label>TO DO:</label>
                         </div>
                       </div>
                         <div class="col-md-8 col-sm-8 col-xs-12" >
                           <div class="form-group" >
                           <textarea class="form-control to_do <?php echo $todo['color']; ?>" name="to_do" id="to_do"><?php echo $todo['to_do']; ?></textarea>
                         </div>
                       </div>
                          <div class="clearfix"></div>
                           
                          <div class="col-md-4 col-sm-4 col-xs-12">
                             <div class="form-group" >
                           <label>Schedule :</label>
                         </div>
                       </div>

                       
                         <div class="col-md-8 col-sm-8 col-xs-12" >
                           <div class="form-group" >
                          <input type="text" value="<?php echo $todo['schedule']; ?>" class="form-control schedule" name="schedule" id="schedule" readonly="">
                        </div>
                      </div>
                       <div class="clearfix"></div>


                       </div>
                       <div class="clearfix"></div>
                     </p>
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                     <button type="submit" name="save" class="btn todo_submit <?php echo $todo['color']; ?>">Save</button>
                     <input type="hidden" name="control" value="user"/>
                     <input type="hidden" name="task" value="save_todo"/>
                     <input type="hidden" name="bg_color" class="todo_bg_color" id="bg_color" value="<?php echo $todo['color']; ?>"/>
                     <input type="hidden" name="id" id="idd" value="<?php echo $todo['id']; ?>"  />
                   </form>
                         </div>
                       </div>

                     </div>
                   </div>                      
                 </li>
               <?php } ?>


                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix no-border">
                  <!-- <a href="" class="btn btn-default  pull-left" data-toggle="modal" data-target="#todo_popup"><i class="fa fa-sort"></i> Short List</a> -->
                  <button class="btn btn-default  pull-right" data-toggle="modal" data-target="#todo_popup"><i class="fa fa-plus"></i> Add item</button>
                </div>
                <div id="todo_popup" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <center> <h4 class="modal-title">Add New</h4></center>
                      </div>
                      <div class="modal-body">
                        <p>
                          <form name="form" method="post" enctype="multipart/form-data">

                           <div class="col-md-10 col-md-offset-1 col-xs-12">

                              <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group"> 
                               <label>Choose Color</label>
                             </div>
                              </div>
                             <div class="col-md-8 col-sm-8 col-xs-12" >
                              <div class="form-group">
                                <ul class="fc-color-picker" id="color-chooser">
                                  <li><a class="text-aqua select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-blue select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-light-blue select_color" href="javascript:;" style="color: #cd5c5c !important;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-teal select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-yellow select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-orange select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-green select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-lime select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-red select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-purple select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-fuchsia select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-muted select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-navy select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                </ul>
                              </div><!-- /btn-group -->
                           </div>
                          <div class="clearfix"></div>
                           
                          <div class="col-md-4 col-sm-4 col-xs-12">
                             <div class="form-group" >
                           <label>TO DO:</label>
                         </div>
                       </div>
                         <div class="col-md-8 col-sm-8 col-xs-12" >
                           <div class="form-group" >
                           <textarea class="form-control to_do" name="to_do" id="to_do"></textarea>
                         </div>
                       </div>
                          <div class="clearfix"></div>
                           
                          <div class="col-md-4 col-sm-4 col-xs-12">
                             <div class="form-group" >
                           <label>Schedule :</label>
                         </div>
                       </div>

                       
                         <div class="col-md-8 col-sm-8 col-xs-12" >
                           <div class="form-group" >
                          <input type="text" class="form-control schedule" name="schedule" id="schedule" readonly="">
                        </div>
                      </div>
                       <div class="clearfix"></div>


                       </div>
                       <div class="clearfix"></div>
                     </p>
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                     <button type="submit" name="save" class="btn todo_submit">Save</button>
                     <input type="hidden" name="control" value="user"/>
                     <input type="hidden" name="task" value="save_todo"/>
                     <input type="hidden" name="bg_color" id="bg_color" class="todo_bg_color" value=""/>
                     <!-- <input type="hidden" name="id" id="idd" value="<?php echo $todo['id']; ?>"  /> -->
                   </form>
                 </div>
               </div>

             </div>
           </div>                 
         </div>
       </div>


       <!--Calendar -->
       <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
       <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
       <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/fullcalendar/fullcalendar.min.css">
       <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/fullcalendar/fullcalendar.print.css" media="print">
        <div class="col-lg-7 col-xs-12">
          <!-- =========Calendar====== -->
                        <div class="box box-primary">
                <div class="box-body no-padding">
                  <!-- THE CALENDAR -->
                  <div id="calendar" style="border: 1px solid #cd5c5c;border-radius: 10px;"></div>
                </div><!-- /.box-body -->
                <!-- -====================================== -->
                <!-- -====================================== -->
                <!-- -====================================== -->
            <div data-toggle="modal" data-target="#calender_popup_model" id="calender_popup"></div>
            <div id="calender_popup_model" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <center><h4 class="modal-title">To Do List</h4></center>
                  </div>
                    <div class="calender_todo modal-body" id="calender_todo">
                      <!-- ======================= -->
                    </div>
                   <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

                <!-- -====================================== -->
                <!-- -====================================== -->
                <!-- -====================================== -->
              </div><!-- /. box -->

       <div class="col-lg-3 col-xs-12">
         <div data-toggle="modal" data-target="#myModal" id="clock_india"></div>
      </div>
            <!-- ====================World Clocks================= -->
            <!-- ====================World Clocks================= -->


            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <center><h4 class="modal-title">World Clock</h4></center>
                  </div>
                  <div class="modal-body">
                    <div id="Delhi"></div>
                    <div id="Sheghai"></div>
                    <div id="Hongkong"></div>
                    <div id="Singapore"></div>
                    <div id="Dubai"></div>
                    <div id="Moscow"></div>
                    <div class="clearfix"></div>
                    <hr>
                    <div id="London"></div>
                    <div id="Paris"></div>
                    <div id="new_york"></div>
                    <div id="Washington"></div>
                    <div id="Sydney"></div>
            <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
            <!-- ====================World Clocks================= -->
            <!-- ====================World Clocks================= -->
            <!-- ====================World Clocks================= -->

        <!-- Modal -->
                <div id="share_box" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <center><h4 class="modal-title">Share Profile</h4></center>
                      </div>
                      <div class="modal-body">
                        <div class="text-center">
                          <?php $text = urlencode('Hey! Checkout My Video CV Profile'); ?>
                          <p id="profile_url" style="display: none;">http://www.<?php echo strtolower($_SESSION['username']).".myvcv.asia"; ?></p>
                          <!-- <a class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a> -->
                          <a onclick="copyToClipboard('#profile_url')" class="btn btn-social-icon btn-bitbucket"><i class="fa fa-clone"></i></a>
                          <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://<?php echo strtolower($_SESSION['username']).".".$_SERVER['HTTP_HOST']; ?>" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                          <!-- <a class="btn btn-social-icon btn-flickr"><i class="fa fa-flickr"></i></a> -->
                          <!-- <a class="btn btn-social-icon btn-foursquare"><i class="fa fa-foursquare"></i></a> -->
                          <!-- <a class="btn btn-social-icon btn-github"><i class="fa fa-github"></i></a> -->
                          <!-- <a target="_blank" href="https://www.google.com/bookmarks/mark?op=edit&bkmk=http://<?php echo strtolower($_SESSION['username']).".".$_SERVER['HTTP_HOST']; ?>&title=<?php echo $text; ?>" class="btn btn-social-icon btn-google"><i class="fa fa-google-plus"></i></a> -->
                          <!-- <a target="_blank" href="" class="btn btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></a> -->
                          <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=http://<?php echo strtolower($_SESSION['username']).".".$_SERVER['HTTP_HOST']; ?>&title=<?php echo $text; ?>" class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
                          <!-- <a class="btn btn-social-icon btn-tumblr"><i class="fa fa-tumblr"></i></a> -->
                          
                          <a target="_blank" href="http://twitter.com/share?text=<?php echo $text; ?>&url=http://<?php echo strtolower($_SESSION['username']).".".$_SERVER['HTTP_HOST']; ?>" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                          <a class="btn btn-social-icon btn-vk"><i class="fa fa-vk"></i></a>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>

                  </div>
                </div>
                <!-- ====================================================== -->
                <!-- ====================================================== -->

            </div><!-- /.row -->

  </section>
<?php }elseif($_SESSION['utype']=='recruiter') { ?> 
<!-- =====================recruiter dashboard here============= -->
<!-- =====================recruiter dashboard here============= -->
  <section class="content">
    <div class="row"> <center><i class="fa fa-info-circle" data-toggle="modal" data-target="#tips_box" style="cursor: pointer;"></i></center>
      <div class="container" style="border:1px solid #cc5b5b;border-radius: 10px;margin:0 15px;padding:15px 0;width: 97%;">
        <div class="col-xs-12 col-md-3">
          <?php if($page_status=='2'){ ?>
            <a href="javascript:;"  data-toggle="modal" data-target="#share_box" id="share_btn" class="btn btn-primary  btn-block btn-flat">Share Profile <i class="fa fa-share-alt" aria-hidden="true"></i></a>
          <?php } ?></div>
     
      <div class="col-xs-12 col-md-6">
        <center>
          <?php if($page_status=='1'){ ?>
            <a href="index.php?control=user&task=page_status&status=2" class="btn btn-primary bulu btn-block btn-flat">Publish</a>
          <?php }else{ ?>
            <a href="index.php?control=user&task=page_status&status=1" class="btn bg-olive btn-flat  btn-block">Un-Publish</a>
          <?php } ?></center> 
          <!-- <button class="btn btn-primary bulu btn-block btn-flat">Publish</button> -->
        </div>
        
         <div class="col-xs-12 col-md-3 ">
          <!-- <a  target="_blank" class="btn btn-primary bulu pull-right" href="http://www.<?php echo strtolower($_SESSION['username']); ?>.myvcv.asia/">View Profile</a> -->
           <a  target="_blank" class="btn btn-primary bulu pull-right" href="../preview/?page=preview">View Profile</a>
         </div>


        <div class="clearfix"></div><br>

        <div class="col-xs-12 col-md-4" >
         <h3 class="pro_color"> Select Color for your profile.</h3>
        </div>
     
      <div class=" col-xs-12 col-md-4">
        
          <div class="btn-group-horizontal">
            <button id="red_color" type="button" onclick="select_color('',<?php echo $_SESSION['adminid']; ?>);" class="btn btn-danger" style="background-color: #cd5d5c;color: #fff;" <?php if($my_detail['profile_color']==''){echo "disabled";} ?>>Color 1</button>
            <button id="1073cb" type="button" onclick="select_color('1073cb',<?php echo $_SESSION['adminid']; ?>);" class="btn btn-default blue" style="background-color: #1073cb;color: #fff;" <?php if($my_detail['profile_color']=='#1073cb'){echo "disabled";} ?>>Color 2</button>
            <button id="0ccead" type="button" onclick="select_color('0ccead',<?php echo $_SESSION['adminid']; ?>);" class="btn btn-default green" style="background-color: #0ccead;color: #fff;" <?php if($my_detail['profile_color']=='#0ccead'){ echo "disabled";} ?>>Color 3</button>     
          </div>
       
      </div>
        
         <div class="col-xs-12 col-md-3">
           <span id="color_msg" style="display: none;">Profile Color Successfully Change...</span>
         </div>
         <div class="clearfix"></div>
        </div>
        <!-- <div class="clearfix"></div><br> --><br>
        <div class="col-lg-3 col-md-6 col-xs-12">
        <a href="index.php?control=user&task=personal_detail">
          <!-- small box -->
          <div class="small-box bg-red">
           <div class="inner">
            <h3><?php  echo 0;?></h3>
            <p>Update your Profile</p>
          </div>
          <div class="icon">
            <i class="fa fa-user"></i>
          </div>
          <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
        </div>
          </a>
      </div><!-- ./col -->


      <div class="col-lg-3 col-md-6 col-xs-12">
        <!-- small box -->
        <a href="index.php?control=user&task=video_detail">
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?php echo 0;//$buyer = $buyer5 ? $buyer5 : 0; ?></h3>
            <p>Update Your Video</p>
          </div>
          <div class="icon">
            <i class="fa fa-video-camera"></i>
            
          </div>
          <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
        </div>
      </a>
      </div>

      
      <div class="col-lg-3 col-md-6 col-xs-12">
        <!-- small box -->
        <a href="index.php?control=support&task=show" >
        <div class="small-box bg-orange">
         <div class="inner">
          <h3><?php echo $new_msg ? $new_msg : 0; ?></h3>
          <p>View your Messages</p>
        </div>
        <div class="icon">
          <i class="fa fa-envelope"></i>
        </div>
        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
      </div></a>
    </div><!-- ./col -->







    <div class="col-lg-3 col-md-6 col-xs-12">  
      <!-- small box -->
      <a href="index.php?control=home&task=visitors">
      <div class="small-box bg-red">
       <div class="inner">
        <h3><?php echo $total_visitor?$total_visitor:0; ?></h3>
        <p>Total Profile Visitors</p>
      </div>
      <div class="icon">
        <i class="fa fa-users"></i>
      </div>
      <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
    </div></a>
        </div> <!-- ./col -->

      
      <div class="clearfix"></div>
      <div class="col-lg-5 col-xs-12" >
       <div class="box box-primary " style="border:1px solid #cc5b5b;border-radius: 10px;">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">To Do List</h3>
          <div class="box-tools pull-right">

                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list " id="move_list">
                    <?php $this->Query("SELECT * FROM `todo_list` WHERE `user_id`='".$_SESSION['adminid']."' ORDER BY `list_order` ASC"); 
                    $todos = $this->fetchArray();
                    $i=0;
                    foreach ($todos as $todo) {
                     $i++;
                     ?>
                     <li class="list_li <?php echo $todo['color']; echo ($todo['status']=='1')?'':' done'; ?>">
                      <input type="hidden" class="list_order" id="<?php echo $todo['id']; ?>" name="list_order[]" value="<?php echo $i; ?>">
                      <!-- <input type="hidden" class="list_id" name="list_id[]" value="<?php echo $todo['id']; ?>"> -->
                      <!-- <span class="list_order"><?php echo $i; ?></span> -->
                      <!-- drag handle -->
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <!-- checkbox -->
                      <input type="checkbox" id="done_todo<?php echo $todo['id']; ?>" onclick="done_todo(<?php echo $todo['id']; ?>)" value="" name="done_todo[]" <?php echo ($todo['status']=='1')?'':'checked'; ?>>
                      <!-- todo text -->
                      <div class="text"><?php echo $todo['to_do']; ?></div>
                      <!-- Emphasis label -->
                     &nbsp;
                      <!-- General tools such as edit or delete-->
                      <div class="tools pull-right">
                        <a href="" data-toggle="modal" data-target="#todo_popup<?php echo $i; ?>"><i class="fa fa-edit"></i></a>
                        <a onclick="return confirm('want to delete?');" href="index.php?control=user&task=delete_todo&id=<?php echo $todo['id'] ?>"><i class="fa fa-trash-o"></i></a>
                      </div>&nbsp;
                      <?php if($todo['schedule']){ ?>
                       <div class="label label-danger pull-right" style="margin: 0 7px;"><i class="fa fa-calendar"></i> <?php  
                      $todo_date = explode(' ', $todo['schedule']);
                      echo $todo_date[0]."<br>".$todo_date[1];
                       ?></div>
                     <?php } ?>&nbsp;
                      <!-- ===========Popup============= -->
                      <div id="todo_popup<?php echo $i; ?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <center> <h4 class="modal-title">Edit TODO</h4></center>
                            </div>
                            <div class="modal-body">
                              <p>
                          <form name="form" method="post" enctype="multipart/form-data">

                           <div class="col-md-10 col-md-offset-1 col-xs-12">

                              <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group"> 
                               <label>Choose Color</label>
                             </div>
                              </div>
                             <div class="col-md-8 col-sm-8 col-xs-12" >
                              <div class="form-group">
                                <ul class="fc-color-picker" id="color-chooser">
                                  <li><a class="text-aqua select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-blue select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-light-blue select_color" href="javascript:;" style="color: #cd5c5c !important;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-teal select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-yellow select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-orange select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-green select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-lime select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-red select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-purple select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-fuchsia select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-muted select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-navy select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                </ul>
                              </div><!-- /btn-group -->
                           </div>
                          <div class="clearfix"></div>
                           
                          <div class="col-md-4 col-sm-4 col-xs-12">
                             <div class="form-group" >
                           <label>TO DO:</label>
                         </div>
                       </div>
                         <div class="col-md-8 col-sm-8 col-xs-12" >
                           <div class="form-group" >
                           <textarea class="form-control to_do <?php echo $todo['color']; ?>" name="to_do" id="to_do"><?php echo $todo['to_do']; ?></textarea>
                         </div>
                       </div>
                          <div class="clearfix"></div>
                           
                          <div class="col-md-4 col-sm-4 col-xs-12">
                             <div class="form-group" >
                           <label>Schedule :</label>
                         </div>
                       </div>

                       
                         <div class="col-md-8 col-sm-8 col-xs-12" >
                           <div class="form-group" >
                          <input type="text" value="<?php echo $todo['schedule']; ?>" class="form-control schedule" name="schedule" id="schedule" readonly="">
                        </div>
                      </div>
                       <div class="clearfix"></div>


                       </div>
                       <div class="clearfix"></div>
                     </p>
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                     <button type="submit" name="save" class="btn todo_submit <?php echo $todo['color']; ?>">Save</button>
                     <input type="hidden" name="control" value="user"/>
                     <input type="hidden" name="task" value="save_todo"/>
                     <input type="hidden" name="bg_color" class="todo_bg_color" id="bg_color" value="<?php echo $todo['color']; ?>"/>
                     <input type="hidden" name="id" id="idd" value="<?php echo $todo['id']; ?>"  />
                   </form>
                         </div>
                       </div>

                     </div>
                   </div>                      
                 </li>
               <?php } ?>


                  </ul>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix no-border">
                  <!-- <a href="" class="btn btn-default  pull-left" data-toggle="modal" data-target="#todo_popup"><i class="fa fa-sort"></i> Short List</a> -->
                  <button class="btn btn-default  pull-right" data-toggle="modal" data-target="#todo_popup"><i class="fa fa-plus"></i> Add item</button>
                </div>
                <div id="todo_popup" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <center> <h4 class="modal-title">Add New</h4></center>
                      </div>
                      <div class="modal-body">
                        <p>
                          <form name="form" method="post" enctype="multipart/form-data">

                           <div class="col-md-10 col-md-offset-1 col-xs-12">

                              <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group"> 
                               <label>Choose Color</label>
                             </div>
                              </div>
                             <div class="col-md-8 col-sm-8 col-xs-12" >
                              <div class="form-group">
                                <ul class="fc-color-picker" id="color-chooser">
                                  <li><a class="text-aqua select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-blue select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-light-blue select_color" href="javascript:;" style="color: #cd5c5c !important;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-teal select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-yellow select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-orange select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-green select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-lime select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-red select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-purple select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-fuchsia select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-muted select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                  <li><a class="text-navy select_color" href="javascript:;"><i class="fa fa-square"></i></a></li>
                                </ul>
                              </div><!-- /btn-group -->
                           </div>
                          <div class="clearfix"></div>
                           
                          <div class="col-md-4 col-sm-4 col-xs-12">
                             <div class="form-group" >
                           <label>TO DO:</label>
                         </div>
                       </div>
                         <div class="col-md-8 col-sm-8 col-xs-12" >
                           <div class="form-group" >
                           <textarea class="form-control to_do" name="to_do" id="to_do"></textarea>
                         </div>
                       </div>
                          <div class="clearfix"></div>
                           
                          <div class="col-md-4 col-sm-4 col-xs-12">
                             <div class="form-group" >
                           <label>Schedule :</label>
                         </div>
                       </div>

                       
                         <div class="col-md-8 col-sm-8 col-xs-12" >
                           <div class="form-group" >
                          <input type="text" class="form-control schedule" name="schedule" id="schedule" readonly="">
                        </div>
                      </div>
                       <div class="clearfix"></div>


                       </div>
                       <div class="clearfix"></div>
                     </p>
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                     <button type="submit" name="save" class="btn todo_submit">Save</button>
                     <input type="hidden" name="control" value="user"/>
                     <input type="hidden" name="task" value="save_todo"/>
                     <input type="hidden" name="bg_color" id="bg_color" class="todo_bg_color" value=""/>
                     <!-- <input type="hidden" name="id" id="idd" value="<?php echo $todo['id']; ?>"  /> -->
                   </form>
                 </div>
               </div>

             </div>
           </div>                 
         </div>
       </div>


       <!--Calendar -->
       <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
       <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
       <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/fullcalendar/fullcalendar.min.css">
       <link rel="stylesheet" href="<?php echo $tmp;?>/plugins/fullcalendar/fullcalendar.print.css" media="print">
        <div class="col-lg-7 col-xs-12">
          <!-- =========Calendar====== -->
                        <div class="box box-primary">
                <div class="box-body no-padding">
                  <!-- THE CALENDAR -->
                  <div id="calendar" style="border: 1px solid #cd5c5c;border-radius: 10px;"></div>
                </div><!-- /.box-body -->
                <!-- -====================================== -->
                <!-- -====================================== -->
                <!-- -====================================== -->
            <div data-toggle="modal" data-target="#calender_popup_model" id="calender_popup"></div>
            <div id="calender_popup_model" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <center><h4 class="modal-title">To Do List</h4></center>
                  </div>
                    <div class="calender_todo modal-body" id="calender_todo">
                      <!-- ======================= -->
                    </div>
                   <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

                <!-- -====================================== -->
                <!-- -====================================== -->
                <!-- -====================================== -->
              </div><!-- /. box -->

       <div class="col-lg-3 col-xs-12">
         <div data-toggle="modal" data-target="#myModal" id="clock_india"></div>
      </div>
            <!-- ====================World Clocks================= -->
            <!-- ====================World Clocks================= -->


            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <center><h4 class="modal-title">World Clock</h4></center>
                  </div>
                  <div class="modal-body">
                    <div id="Delhi"></div>
                    <div id="Sheghai"></div>
                    <div id="Hongkong"></div>
                    <div id="Singapore"></div>
                    <div id="Dubai"></div>
                    <div id="Moscow"></div>
                    <div class="clearfix"></div>
                    <hr>
                    <div id="London"></div>
                    <div id="Paris"></div>
                    <div id="new_york"></div>
                    <div id="Washington"></div>
                    <div id="Sydney"></div>
            <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
            <!-- ====================World Clocks================= -->
            <!-- ====================World Clocks================= -->
            <!-- ====================World Clocks================= -->

        <!-- Modal -->
                <div id="share_box" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <center><h4 class="modal-title">Share Profile</h4></center>
                      </div>
                      <div class="modal-body">
                        <div class="text-center">
                          <?php $text = urlencode('Hey! Checkout My Video CV Profile'); ?>
                          <p id="profile_url" style="display: none;">http://www.<?php echo strtolower($_SESSION['username']).".myvcv.asia"; ?></p>
                          <!-- <a class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a> -->
                          <a onclick="copyToClipboard('#profile_url')" class="btn btn-social-icon btn-bitbucket"><i class="fa fa-clone"></i></a>
                          <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://<?php echo strtolower($_SESSION['username']).".".$_SERVER['HTTP_HOST']; ?>" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                
                          <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=http://<?php echo strtolower($_SESSION['username']).".".$_SERVER['HTTP_HOST']; ?>&title=<?php echo $text; ?>" class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
                         
                          <a target="_blank" href="http://twitter.com/share?text=<?php echo $text; ?>&url=http://<?php echo strtolower($_SESSION['username']).".".$_SERVER['HTTP_HOST']; ?>" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                          <a class="btn btn-social-icon btn-vk"><i class="fa fa-vk"></i></a>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>

                  </div>
                </div>
                <!-- ====================================================== -->
                <!-- ====================================================== -->

            </div><!-- /.row -->

  </section>
<!-- =====================recruiter dashboard here============= -->
<!-- =====================recruiter dashboard here============= -->

<?php } ?>
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?php echo $tmp;?>/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo $tmp;?>/plugins/WorldClock/js/jClocksGMT.js"></script>
<script src="<?php echo $tmp;?>/plugins/WorldClock/js/jquery.rotate.js"></script>
<script type="text/javascript">
  $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
  });
// $('.schedule').datepicker();

       $('.schedule').datetimepicker({
        yearOffset:0,
        lang:'ch',
        timepicker:true,
        format:'Y-m-d H:i',
        formatDate:'Y-m-d H:i',
        // maxDate:'+1970/01/01',
        scrollMonth : false
      });
$('.select_color').click(function(){
  color = $(this).attr('class');
  res = (color.split('text-'))[1].split(' ');

  console.log(res);
  // $('.bg_color').val('bg-'+res[0]);
  $('.todo_bg_color').val('bg-'+res[0]);
  $('.to_do').attr('class', 'form-control to_do').addClass(' bg-'+res[0]);

  $('.todo_submit').attr('class', 'btn todo_submit').addClass(' bg-'+res[0]);
  // $('.to_do').addClass('to_do bg-'+res[0]);
});

function done_todo(id){
  if($('#done_todo'+id).is(':checked')){
     $.ajax({url: "script/todo_done.php?id="+id+"&status=0", success: function(result){
     }
 });
   }else{
      $.ajax({url: "script/todo_done.php?id="+id+"&status=1", success: function(result){
     }
 });   
   }
}

/*============No tips=========*/
$('#no_tips').click(function(){
  if($(this).is(':checked')){
    var column = 'home_tips';
     $.ajax({url: "script/tips_page.php?column="+column+"&status=0", success: function(result){

     }
}) }
});
/*================*/
var date = new Date();
var d = date.getDate(),
  m = date.getMonth(),
  y = date.getFullYear();
       $('#calendar').fullCalendar({
         header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          buttonText: {
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
          },
           //Random default events
          events: [
          <?php $this->Query("SELECT * FROM `todo_list` WHERE `user_id`='".$_SESSION['adminid']."' AND `schedule`!='' AND `status`=1");
            $tasks = $this->fetchArray();
            foreach($tasks as $task){
           ?>
            {
              title: "<?php echo $task['to_do']; ?>",
              start: new Date('<?php echo $task['schedule']; ?>'),
              backgroundColor: "<?php echo explode('-', $task['color'])[1]; ?>",
              url: 'javascript:;', 

              borderColor: "<?php echo explode('-', $task['color'])[1]; ?>" 
            },
          <?php } ?>
           
          ],
       });


/*=============World Clock============*/
  $('#clock_india').jClocksGMT(
{
    title: 'New Delhi, India', 
    offset: '+5.5', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#Delhi').jClocksGMT(
{
    title: 'New Delhi, India', 
    offset: '+5.5', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#Sheghai').jClocksGMT(
{
    title: 'Shanghai, China', 
    offset: '+8', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#Hongkong').jClocksGMT(
{
    title: 'Hong Kong', 
    offset: '+8', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#Singapore').jClocksGMT(
{
    title: 'Singapore', 
    offset: '+8', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#Dubai').jClocksGMT(
{
    title: 'Dubai, UAE', 
    offset: '+4', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#Moscow').jClocksGMT(
{
    title: 'Moscow, Russia ', 
    offset: '+3', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#London').jClocksGMT(
{
    title: 'London, UK', 
    offset: '+1', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#Paris').jClocksGMT(
{
    title: 'Paris, France', 
    offset: '+2', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#new_york').jClocksGMT(
{
    title: 'New York, USA', 
    offset: '-4', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#Washington').jClocksGMT(
{
    title: 'Washington, USA', 
    offset: '-7', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});
  $('#Sydney').jClocksGMT(
{
    title: 'Sydney, Australia', 
    offset: '+10', 
    dst: true, 
    skin: 5, 
    // timeformat: 'HH:mm',
    date: true, 
    dateformat: 'YYYY/MM/DD', 
});

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
  alert('Link Copied');
}



function calender_popup_show(){
    $('.fc-day.fc-widget-content').click(function(){
    date = $(this).attr('data-date');
    $.ajax({url: "script/todo_popup.php?date="+date, success: function(result){

     }});

   })
$('.fc-ltr .fc-basic-view .fc-day-number').click(function(){
    date = $(this).attr('data-date');
    $.ajax({url: "script/todo_popup.php?date="+date, success: function(result){
      if(result.length > 5){
        $('#calender_todo').html(result);
       $('#calender_popup').click();
      } 
     }});
  }) 
}
/*==================================*/


$('.fc-button').click(function(){
  setTimeout(function(){
     calender_popup_show();
  },1000);
})

function select_color(color, user_id){
  console.log(color);
  $.ajax({url: "script/profile_color.php?color="+color+"&user_id="+user_id, success: function(result){
    if(result=="Sucess"){
        $("#color_msg").show();
    
        setTimeout(function(){
          $("#color_msg").hide();
          $('.btn-group-horizontal button').prop('disabled', false);
          $("#"+color).prop('disabled', true);
          if(color==''){
            $("#red_color").prop('disabled', true);
          }

        }, 2000);
      }
  }});
}

$(document).ready(function(){
  calender_popup_show();
 /*===============Delete Unused Videos===========*/
<?php if($_SESSION['utype']=="Administrator"){ ?>
  $.ajax({url: "script/delete_old_video.php", success: function(result){
    console.log(result);
  } });
<?php } ?>
});
</script>
