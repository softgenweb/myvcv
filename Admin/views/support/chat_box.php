<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>

<div class="row">
  <div class="col-xs-12">
    <div class="box">             
      <div class="box-header">
        <h3 class="box-title">Chat box</h3>
        <?php foreach($results as $result) { }  
        //$ticket_id = $result['id']?$result['id']:$_REQUEST['ticket'];
        ?>
             
      </div><!-- /.box-header -->
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
       <li><a href="index.php?control=support&task=show"><i class="fa fa-list" aria-hidden="true"></i> Ticket List</a></li>
        <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Chat Box</li>
      </ol>
      <?php if(isset($_SESSION['alertmessage'])){?>
       <div class="box-body">
        <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
        </div>
      </div>

      <?php   	unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
    } 
    ?>


    <div class="box-body">
     <div>
       <div class="divoverflow">

          <div class="col-md-6 col-md-offset-3">
              <!-- DIRECT CHAT PRIMARY -->
              <div class="box box-primary direct-chat direct-chat-primary">
                <!-- <div class="box-header with-border">
                  <h3 class="box-title">Direct Chat</h3>
                  <div class="box-tools pull-right">
                    <span data-toggle="tooltip" title="3 New Messages" class="badge bg-light-blue">3</span>
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div> --><!-- /.box-header -->
                <div class="box-body">
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages">
                    <!-- Message. Default to the left -->
                    <?php 
                  if($_SESSION['utype']!="Administrator"){
                    $update = "UPDATE `support_chat` SET `chat_status`='2' WHERE `ticket_id`='".$result['id']."'";
                  }else{
                    $update = "UPDATE `support_chat` SET `read_status`='2' WHERE `ticket_id`='".$result['id']."'";
                  }
                    $this->Query($update);
                    $this->Execute();

                   $this->Query("SELECT * FROM `support_chat` WHERE `ticket_id`='".$result['id']."' ORDER BY `id` ASC"); 

                   $msgs = $this->fetchArray(); 

                    $i = 0;
                    foreach($msgs as $msg){
                      $i++;
                      if($msg['id']!=""){
                      if($msg['from_user']==$_SESSION['adminid']){
                    ?>
                       <!-- ==================================================================== -->
                    <!-- Message to the right -->
                    <div class="direct-chat-msg right" id="msg_last<?php echo $i; ?>">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right"><?php echo $_SESSION['admin_name']; ?></span><br>
                        <span class="direct-chat-timestamp pull-right"><?php echo date('dS M g:i A', strtotime($msg['date_created'])); ?></span>
                      </div><!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="<?php echo $img = ($_SESSION['user_image']?'media/user_profile/'.$_SESSION['user_image']:'images/img_avatar.png'); ?>" alt="message user image"><!-- /.direct-chat-img -->
                      <div class="direct-chat-text" style="width: 150px;left: 55%;">
                     <?php echo $msg['msg']; ?>
                   </div>
                  <?php  if($msg['file']){   ?>
                       <img class="pull-right" src="media/msg_attchmet/<?php echo $msg['file']; ?>" width="100" height="100">
                     <?php } ?>
                      <!-- /.direct-chat-text -->
                    </div><!-- /.direct-chat-msg -->
                    <!-- ========================================================================= -->
                   
                  <?php }else{ ?>
                    <!-- ================================================================= -->
                    <div class="direct-chat-msg"  id="msg_last<?php echo $i; ?>">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-left"><?php echo $this->user_name($msg['from_user']); ?></span><br>
                        <span class="direct-chat-timestamp pull-left"><?php echo date('dS M g:i A', strtotime($msg['date_created'])); ?></span>
                      </div><!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="<?php echo $img = ($this->user_image($msg['from_user'])?'media/user_profile/'.($this->user_image($msg['from_user'])):'images/img_avatar.png'); ?>" alt="message user image"><!-- /.direct-chat-img -->
                      <div class="direct-chat-text" style="width: 150px;">
                       <?php echo $msg['msg'];  ?>
                   </div>
                  <?php  if($msg['file']){   ?>
                       <img class="pull-left" src="media/msg_attchmet/<?php echo $msg['file']; ?>" width="100" height="100">
                     <?php } ?><!-- /.direct-chat-text -->
                    </div><!-- /.direct-chat-msg -->
                    <!-- ==================================================================== -->
                  <?php }  }else{
                    echo "No any message...";
                  } } ?>
                  </div><!--/.direct-chat-messages-->

                </div><!-- /.box-body -->
                <div class="box-footer">
                  <form action="" method="post" enctype="multipart/form-data">
                    <div class="input-group">
                      <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                      <span class="input-group-btn">
                        <button type="submit" name="send" id="send" class="btn btn-primary btn-flat">Send</button>
                        <button type="button" id="attachment" class="btn btn-info btn-flat"><i class="fa fa-paperclip" aria-hidden="true"></i></button>
                        <input type="file" name="file_upload" id="file_upload" style="display: none;">
                        <!-- <input type="sub" name=""> -->
                        <input type="hidden" name="control" id="control" value="support">
                        <input type="hidden" name="task" id="task" value="send_msg">
                        <input type="hidden" name="ticket_id" id="ticket_id" value="<?php echo $result['id']; ?>">
                       <?php   if($_SESSION['utype']!="Administrator"){ ?>
                        <input type="hidden" name="to_user" id="to_user" value="<?php echo $result['to_user']; ?>">
                      <?php }else{ ?>
                          <input type="hidden" name="to_user" id="to_user" value="<?php echo $results[0]['created_by']; ?>">
                    <?php     } ?>
                      </span>
                    </div>
                  </form>
                </div><!-- /.box-footer-->
              </div><!--/.direct-chat -->
            </div>
</div>
</div><!-- table-responsive -->
</div><!-- /.box-body -->
</div><!-- /.box -->
</div><!-- /.col -->

<!--================ Second Table ================-->


</div><!-- /.row -->

<script>
$('.direct-chat-messages').scrollTop($('.direct-chat-messages').prop("scrollHeight"));
	/*============Auto hide alert box================*/
	$(".alert").delay(2000).slideUp(200, function() {
		$(this).alert('close');
	});
  $('#attachment').click(function(){
    $('#file_upload').click();
  });
</script>

