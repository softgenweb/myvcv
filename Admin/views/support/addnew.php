<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Create New Support</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=support&task=show"><i class="fa fa-list" aria-hidden="true"></i> Ticket List</a></li>
      <?php if($result!='') {?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Ticket</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Ticket</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){ ?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    
   unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">

               <div class="col-md-4" style="display: none;">
                  <div class="form-group center_text">
                     <label>Title :</label>
                  </div>
               </div>
               <div class="col-md-6" style="display: none;">
                  <div class="form-group">
                     <!-- <textarea name="title" id="title" class="form-control" required=""><?php echo $result['title']; ?></textarea> -->
                     <input type="text" value="1" id="to_user" name="to_user" class="form-control"  required="">         
                  </div>
               </div>
               <div class="clearfix"></div>

               <div class="col-md-4">
                  <div class="form-group center_text">
                     <label>Title :</label>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <textarea name="title" id="title" class="form-control" required=""><?php echo $result['title']; ?></textarea>
                     <!-- <input type="text" value="<?php echo $result['room_no']; ?>" id="room_no" name="room_no" class="form-control"  required="">          -->
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
               </div>
               <input type="hidden" name="control" value="support"/>
               <input type="hidden" name="edit" value="1"/>
               <input type="hidden" name="task" value="save"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>

<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
      $(this).alert('close');
   });
   
     
   function goBack() {
      window.history.back();
   }
</script>

