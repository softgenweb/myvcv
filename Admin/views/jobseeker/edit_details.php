<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Job Seeker</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=jobseeker&task=<?php echo $_REQUEST['pg']; ?>"><i class="fa fa-list" aria-hidden="true"></i> Job Seeker List</a></li>
      <?php if($result!='') {?>           
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Job Seeker</li>
      <?php }?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Job Seeker Aliasname:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" maxlength="15" value="<?php echo $result['username']; ?>" id="username" name="username" class="form-control" <?php echo $result['username']?"disabled":"required"; ?>>
                        <span class="msgValid" id="msgpUname"></span>
                  </div>
               </div>
               <div class="clearfix"></div>                            
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Title:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="title" id="title" class="form-control" autocomplete="off" required="">
                        <option value="">Select</option>
                        <option value="Mr" <?php if($result['title']=='Mr') {?> selected="selected" <?php } ?>>Mr</option>
                        <option value="Ms" <?php if($result['title']=='Ms') {?> selected="selected" <?php } ?>>Ms</option>
                        <option value="Mrs" <?php if($result['title']=='Mrs') {?> selected="selected" <?php } ?>>Mrs</option>
                     </select>
                     <span class="msgValid" id="msgtitle"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Name:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text"  value="<?php echo $result['name']; ?>" id="name" name="name" class="form-control" required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Gender:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="gender" id="gender" class="form-control" autocomplete="off" required="">
                        <option value="">Select</option>
                        <option value="Male" <?php if($result['gender']=='Male') {?> selected="selected" <?php } ?>>Male</option>
                        <option value="Female" <?php if($result['gender']=='Female') {?> selected="selected" <?php } ?>>Female</option>
                     </select>
                  </div>
               </div>
               <!-- <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Mobile: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                
                     <input type="text"  value="<?php echo $result['mobile']; ?>" pattern="[6789][0-9]{9}" maxlength="10" id="mobile" name="mobile" class="form-control" >
                     <span class="msgValid" id="msgmobile"></span>
                  </div>
               </div> -->
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Email-Id: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                
                     <input type="text"  value="<?php echo $result['email']; ?>" id="email" name="email" class="form-control" <?php echo $result['email']?"disabled":"required"; ?>>
                     <span class="msgValid" id="msgpEmail"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Date of Birth: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                              
                     <input type="text"  value="<?php echo $result['dob']; ?>" id="dob" name="dob"  class="form-control" readonly>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Recruiter: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                              
                      <select name="recruiter_id" id="recruiter_id" placeholder="Select Recruiters" class="select2 form-control" autocomplete="off" >
                        <option value="">Select</option>
                       <?php $recr_id = explode(',', $result['recruiter_id']); 
                       $sql = "SELECT * FROM `users` WHERE `utype`='recruiter' AND `status`=1 ;";
                       $this->Query($sql);
                       $recruiters = $this->fetchArray();
                       foreach ($recruiters as $recruiter){ ?>
                           <option value="<?php echo $recruiter['id']; ?>" <?php echo in_array($recruiter['id'], $recr_id)?'selected':''; ?>><?php echo $recruiter['name']." (".$recruiter['username'].")"; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Joining Date: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                              
                     <input type="text"  value="<?php echo $result['date_time']; ?>" id="doj" name="date_time"  class="form-control" readonly>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Coupon : 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                              
                    <select name="coupon" id="coupon" placeholder="Select Recruiters" class="select2 form-control" autocomplete="off">
                        <option value="">Select</option>
                       <?php $recr_id = explode(',', $result['coupon']); 
                       $sql = "SELECT * FROM `coupon_code` WHERE `status`=1 ;";
                       $this->Query($sql);
                       $recruiters = $this->fetchArray();
                       foreach ($recruiters as $recruiter){ ?>
                           <option value="<?php echo $recruiter['promocode']; ?>" <?php echo in_array($recruiter['promocode'], $recr_id)?'selected':''; ?>><?php echo $recruiter['promocode']; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" id="submit-btn" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
               </div>
               <input type="hidden" name="control" value="jobseeker"/>
               <input type="hidden" name="task" value="edit_details"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
               <input type="hidden" name="editid" id="editid" value="1"  />
               <input type="hidden" name="pg" id="pg" value="<?php echo $_REQUEST['pg']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
   $('#dob').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d-m-Y',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });
   $('#doj').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:true,
    format:'Y-m-d H:i:s',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });
   
   
   /*=============Gsingh Jquery==============*/
   function id_proof(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
               $('#id_proof_img').attr('src', e.target.result);
               $('#id_proof_a').attr('href', e.target.result);
           }
           reader.readAsDataURL(input.files[0]);
           }
       }
   $("#id_proof").change(function(){
       id_proof(this);
   });
   
   
   function photo(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
               $('#photo_img').attr('src', e.target.result);
               $('#photo_a').attr('href', e.target.result);
           }
           reader.readAsDataURL(input.files[0]);
           }
       }
   $("#photo").change(function(){
       photo(this);
   });
   function isNumber(evt) {
       evt = (evt) ? evt : window.event;
           var charCode = (evt.which) ? evt.which : event.keyCode;
       if (charCode != 46 && charCode > 31
       && (charCode < 48 || charCode > 57))
           return false;
   
       return true;
   }
   
   
   /*==========Commission==============*/
   
   function get_commission(){
   
       var amount = $('#amount').val();
       var percent = $('#comm_per').val();
       var getcomm =  parseInt((amount*percent)/100);
   
       $('#comm_amount').val(getcomm);
   }
   $('#comm_per').keyup(function(){
       get_commission();
   });
   
    /*============Auto hide alert box================*/
    $(".alert").delay(2000).slideUp(200, function() {
      $(this).alert('close');
    });
   
   
/*==Ckh Password==*/
   $('#c_password').keyup(function(){
      password = $('#password').val();
      c_password = $(this).val();
      if(username.length<1){
          $('#msgPass').text('');
      }else{
          if(password != c_password){
            txt = "Password Didn't match, Please try again!!!";
            $("#msgPass").text(txt).css('color','red');
            $("#submit-btn").prop('disabled',true);
          }else{
            txt = "Password matched, Go ahead!!!";
            $("#msgPass").text(txt).css('color','green');
            $("#submit-btn").prop('disabled',false);
          }
    }
   });


   $('#username').blur(function(){
      username = $(this).val();
      utype = "recruiter";
      if(username.length<1){

        $('#msgpUname').text('');

      }else{

        $.ajax({url: "script/checking.php?name="+username+"&utype="+utype, success: function(result){
          if(result>0){
            txt = "Aliasname Not Available";
              $('#msgpUname').text(txt).css('color','red');
              $("#submit-btn").prop('disabled',true);
          }else{
            txt = "Aliasname Available, Go ahead!!!";
              $('#msgpUname').text(txt).css('color','green');
              $("#submit-btn").prop('disabled',false);
          }

     }
});
}
   });
   $('#email').blur(function(){
      username = $(this).val();
      utype = "job_seeker";
      if(username.length<1){
        
        $('#msgpEmail').text('');

      }else{

        $.ajax({url: "script/checking.php?name="+username+"&utype="+utype, success: function(result){
          if(result>0){
            txt = "Email-Id Not Available";
              $('#msgpEmail').text(txt).css('color','red');
              $("#submit-btn").prop('disabled',true);
          }else{
            txt = "Email-Id Available, Go ahead!!!";
              $('#msgpEmail').text(txt).css('color','green');
              $("#submit-btn").prop('disabled',false);
          }

     }
});
}
   });
</script>

