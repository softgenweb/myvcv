<?php foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
	.tabs, .tabs .col-md-2, #select2-recruiter_id-container {
		padding: unset !important;
	}

.delete_btn{
position: absolute;
top: 0;
cursor: pointer;
color: #cd5c5c;
}
</style>

<div class="panel panel-default" >
	<div class="box-header">        
		<h3 class="box-title">Add Personal Details <i class="fa fa-info-circle" data-toggle="modal" data-target="#tips_box" style="cursor: pointer;"></i></h3>
		<!-- <a  target="_blank" class="btn btn-primary bulu pull-right" href="http://www.<?php echo strtolower($_SESSION['username']); ?>.myvcv.asia/">View Profile</a> -->
		<a  target="_blank" class="btn btn-primary bulu pull-right" href="../preview/?page=preview">View Profile</a>
	</div> 


	<?php if(isset($_SESSION['alertmessage'])){?>
	<div class="box-body">
		<div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
		</div>
	</div>

	<?php 
	unset($_SESSION['alertmessage']);
	unset($_SESSION['errorclass']); 

}	
$img_path = 'media/user_profile/';
$noimg = 'images/no_img.png';
if($_REQUEST['id']==''){   ?>

<?php } ?>
<button id="tips_box_btn"  data-toggle="modal" data-target="#tips_box" style="display: none;"></button>
<?php if($result['profile_tips']=='1'){ ?>
<div id="tips_box" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h4 class="modal-title">Here are some To Dos for you:</h4></center>
      </div>
      <div class="modal-body">
        <div class="text-left">
			<p>You can upload your Biodata and other personal details here, and also press other tabs as follow:</p>
			<p><label>Video CV</label> --> Upload or reccord your New Video CV here</p>
			<p><label>My Experience</label> --> Upload your work history and experience details</p>
			<p><label>My Skills</label> --> Upload your skills and other intersts</p>
			<p><label>My Awards</label>/Education --> Upload your academic history and courses and cretificates details here</p>
			<p><label>My Portfolio</label> --> Upload images of your portfoilio of your past creations</p>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <div class="pull-left"><input type="checkbox" value="close" id="no_tips" name="no_tips"> No Tips </div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div class="panel-body">
	<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
		<div class="box-body">				

			<fieldset>

         <!-- ==================================Tabs on Mobile ==================================== -->
    <div class="col-md-12 tabs small-screen" style="margin-bottom: 15px;">
            <ul class="list-group">
            <a data-toggle="collapse" href="#collapse1"><li class="list-group-item" >Update Profile<i class="fa fa-caret-down pull-right"></i></li></a>
          </ul>
        <div id="collapse1" class="panel-collapse collapse">
          <ul class="list-group">
            <a href="index.php?control=user&task=personal_detail"><li class="list-group-item">Personal Details</li></a>
            <a href="index.php?control=user&task=video_detail"><li class="list-group-item"> Video CV</li></a>
            <a href="index.php?control=user&task=my_experience"><li class="list-group-item"> My Experience</li></a>
            <a href="index.php?control=user&task=my_skill"><li class="list-group-item"> My Skills</li></a>
            <a href="index.php?control=user&task=my_education"><li class="list-group-item"> My Awards / Education</li></a>
            <a href="index.php?control=user&task=my_portfolio"><li class="list-group-item"> My Portfolio</li></a>
          </ul>
        </div>
      
    </div>
    <!-- ====================================================================================== -->
            	<div class="col-md-12 tabs large-screen">
<?php include 'script/updateprofile_header.php'; ?>
            	</div>
            	<div class="col-sm-10 col-md-offset-1 second_header ons" >
            		</div>
            		<div class="clearfix"></div>
            		<div class="form-group oneline">
            			<div class="col-md-2 col-sm-2 col-xs-12">
            				<label>Title</label> 
            			</div>
            			<div class="col-md-4 col-sm-4 col-xs-12">
            				<select name="title" id="title" class="form-control" autocomplete="off">
            					<option value="">Select</option>
            					<option value="Mr" <?php if($result['title']=='Mr') {?> selected="selected" <?php } ?>>Mr</option>
            					<option value="Ms" <?php if($result['title']=='Ms') {?> selected="selected" <?php } ?>>Ms</option>
            					<option value="Mrs" <?php if($result['title']=='Mrs') {?> selected="selected" <?php } ?>>Mrs</option>

            				</select>
            				<span class="msgValid" id="msgtitle"></span> 
            			</div>

            			<div class="col-md-2 col-sm-2 col-xs-12">
            				<label>Full Name</label> 
            			</div>

            			<div class="col-md-4 col-sm-4 col-xs-12">
            				<input type="text"  value="<?php echo $result['name']; ?>" id="name" name="name" class="form-control" autocomplete="off">
            			</div>
            		</div> 
            		<div class="form-group oneline">
            			<div class="col-md-2 col-sm-2 col-xs-12">
            				<label>Gender</label> 
            			</div>
            			<div class="col-md-4 col-sm-4 col-xs-12">
            				<select name="gender" id="gender" class="form-control" autocomplete="off">
            					<option value="">Select</option>
            					<option value="Male" <?php if($result['gender']=='Male') {?> selected="selected" <?php } ?>>Male</option>
            					<option value="Female" <?php if($result['gender']=='Female') {?> selected="selected" <?php } ?>>Female </option>
            					<option value="Other" <?php if($result['gender']=='Other') {?> selected="selected" <?php } ?>>Other </option>
            					

            				</select>
            				<span class="msgValid" id="msggender"></span> 
            			</div>

            			<div class="col-md-2 col-sm-2 col-xs-12">
            				<label>Father/Husband Name</label> 
            			</div>

            			<div class="col-md-4 col-sm-4 col-xs-12">
            				<input type="text"  value="<?php echo $result['father_husband_name']; ?>" id="father_husband_name" name="father_husband_name" class="form-control" autocomplete="off">
            				<span class="msgValid" id="msgfather_husband_name"></span>
            			</div>
            		</div> 
            		<div class="form-group oneline">
            			<div class="col-md-2 col-sm-2 col-xs-12">
            				<label>Date Of Birth</label> 
            			</div>
            			<div class="col-md-4 col-sm-4 col-xs-12">
            				<input type="text" placeholder="DD/MM/YYYY" value="<?php echo $result['dob']; ?>" id="dob" name="dob" class="form-control" autocomplete="off"  disabled="">
            				<span class="msgValid" id="msgdob"></span> 
            			</div>

            			<div class="col-md-2 col-sm-2 col-xs-12">
            				<label>Marital Status</label> 
            			</div>

						<div class="col-md-4 col-sm-4 col-xs-12">
            				<select name="marital" id="marital" class="form-control" autocomplete="off">
            					<option value="">Select</option>
            					<option value="Single" <?php if($result['marital']=='Single') {?> selected="selected" <?php } ?>>Single </option>
            					<option value="Married" <?php if($result['marital']=='Married') {?> selected="selected" <?php } ?>>Married</option>
            					
            					<option value="Separated" <?php if($result['marital']=='Separated') {?> selected="selected" <?php } ?>>Separated </option>
            					

            				</select>
            				<span class="msgValid" id="msgmarital"></span> 
            			</div>
            		</div>
            		<div class="form-group oneline">
            			<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Nationality</label>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="text"  value="<?php echo $result['nationality']; ?>" id="nationality" name="nationality" class="form-control" autocomplete="off" >
							<span class="msgValid" id="msgnationality"></span>
						</div>

					<div class="col-md-2 col-sm-2 col-xs-12">
						<label>Languages Known</label> 
							<!-- <b style="color:red">*</b> --> 
						</div>
            		
                        <?php $lang = explode(',', $result['languages']); ?>
					<div class="col-md-4 col-sm-4 col-xs-12">
							  <select name="languages[]" class="form-control languages" multiple="multiple" data-placeholder="Select all languages u know" style="width: 100%;">
			                      <option value="English" <?php echo in_array('English', $lang)?'selected':''; ?>>English</option>
			                      <option value="Hindi" <?php echo in_array('Hindi', $lang)?'selected':''; ?>>Hindi</option>
			                      <option value="French" <?php echo in_array('French', $lang)?'selected':''; ?>>French</option>
			                      <option value="Gujrati" <?php echo in_array('Gujrati', $lang)?'selected':''; ?>>Gujrati</option>
			                      <option value="Italian" <?php echo in_array('Italian', $lang)?'selected':''; ?>>Italian</option>
			                      <option value="Tamil" <?php echo in_array('Tamil', $lang)?'selected':''; ?>>Tamil</option>
			                      <option value="German" <?php echo in_array('German', $lang)?'selected':''; ?>>German</option>
                   			 </select>
						</div>
            		</div> 
					


					<div class="clearfix"></div><br>
					<label><strong>Contact Details :</strong></label>
					<span data-toggle="tooltip" data-placement="right" title="Keep your contact details uptodate"><i class="fa fa-info-circle" aria-hidden="true"></i></span>

					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Mobile Number</label>
							<!-- <b style="color:red">*</b> --> 
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['mobile']; ?>" maxlength="10" id="mobile" name="mobile" pattern="[0-9]+" class="form-control" autocomplete="off" >
							<span class="msgValid" id="msgmobile"></span>
						</div>

						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Email</label>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['email']; ?>"  id="email" name="email" class="form-control" autocomplete="off" disabled="">
							<span class="msgValid" id="msgmobile"></span>
						</div>
					</div> 
					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Facebook</label>
							
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['facebook']; ?>" id="facebook" name="facebook" class="form-control" autocomplete="off" >
							<span class="msgValid" id="msgfacebook"></span>
						</div>

						<div class="col-md-2 col-sm-2 col-xs-12">
            				<label>Twitter</label> 
            			</div>
							

					<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['twitter']; ?>" id="twitter" name="twitter" class="form-control" autocomplete="off" >
							<span class="msgValid" id="msgtwitter"></span>
						</div>
					</div>  

					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Instagram</label>
							
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['instagram']; ?>" id="instagram" name="instagram" class="form-control" autocomplete="off"  >
							<span class="msgValid" id="msginstagram"></span>
						</div>

						<div class="col-md-2 col-sm-2 col-xs-12">
            				<label>Linkedin</label> 
            			</div>
							

					<div class="col-md-4 col-sm-4 col-xs-12">
							<input type="text"  value="<?php echo $result['linkedin']; ?>" id="linkedin" name="linkedin" class="form-control" autocomplete="off">
							<span class="msgValid" id="msglinkedin"></span>
						</div>
					</div>  

					<div class="clearfix"></div><br>
					<label><strong>Residential Address :</strong></label>
					<span data-toggle="tooltip" data-placement="right" title="Keep your residential address uptodate"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Country</label>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="country" id="country" onchange="state_change(this.value);" class="form-control" autocomplete="off">
								<option value="">Select</option>
								<?php $this->Query("SELECT * FROM `country` WHERE 1"); 
										$contrys = $this->fetchArray();
								foreach($contrys as $contry) {?>
								<option value="<?php echo $contry['id']; ?>" <?php if($result['country']==$contry['id']) {?> selected="selected" <?php } ?>><?php echo $contry['country_name']; ?></option>
								<?php }?>
							</select>
						</div>


						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>State</label>
							<!-- <b style="color:red">*</b> --> 
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="state_id" id="state_id" onchange="city_change(this.value);" class="form-control" autocomplete="off" >
								<option value="">Select</option>
								<?php $this->Query("SELECT * FROM `state` WHERE `country_id`='".$result['country']."'"); 
								$states = $this->fetchArray();
								foreach($states as $state) {
									 ?>
								<option value="<?php echo $state['id']; ?>" <?php if($result['state_id']==$state['id']) {?> selected="selected" <?php } ?>><?php echo $state['state_name']; ?></option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>City</label>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12">
							<select name="city_id" id="city_id" class="form-control" autocomplete="off"  >
								<option value="">Select</option>
								<?php $this->Query("SELECT * FROM `city` WHERE `state_id`='".$result['state_id']."' AND `country_id`='".$result['country']."'"); 
								$citys = $this->fetchArray();
								foreach($citys as $city) {?>
								<option value="<?php echo $city['id']; ?>" <?php if($result['city_id']==$city['id']) {?> selected="selected" <?php } ?>><?php echo $city['city_name']; ?></option>
								<?php }?>
							</select>
							<span class="msgValid" id="msgvehicle_no"></span>
						</div>


						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Address</label>
							<!-- <b style="color:red">*</b> --> 
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<textarea name="address" id="address" class="form-control" cols="3" style="height: 70px;"><?php echo $result['address']; ?></textarea>
							<span class="msgValid" id="msgres_address"></span> 
						</div>

						<div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
							<label>Pin Code</label>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
							<input type="text"  value="<?php echo $result['postalcode']; ?>" id="postalcode" name="postalcode" class="form-control" autocomplete="off">
							<span class="msgValid" id="msgres_pincode"></span>
						</div>
					</div>

					<div class="clearfix"></div><br>
							<label><strong>Documents :</strong></label>
							<span data-toggle="tooltip" data-placement="right" title="Here you upload all your ID proof and biometeric information"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
					
					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Thumb Print</label>
							 <span data-toggle="tooltip" data-placement="right" title="Store your thumb print, using a biometeric device available with a registered recruitment agency"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="file" class="form-control-file" value="<?php echo $result['thumbprint']; ?>" id="thumbprint" name="thumbprint" class="form-control" autocomplete="off" >
							<!-- =============1================= -->
							<div style="position: relative;">
							<!-- ============================== -->
								<a target="<?php echo $result['thumbprint']?'_blank':''; ?>" href="<?php echo $result['thumbprint']?('media/user_profile/'.$result['thumbprint']):'javascript:;';?>" id="thumbprint_a">
									<img id="thumbprint_img" src="<?php echo $result['thumbprint']?$img_path.$result['thumbprint']:$noimg; ?>" alt="&nbsp;" height="50"  /></a>
								<!-- =========2========change id as per name (delete_thumb) -->
								<i class="fa fa-times-circle fa-lg delete_btn" id="delete_thumb" onclick="delete_images('thumb','<?php echo $result['thumbprint']; ?>')"  style="display: <?php echo $result['thumbprint']?'block':'none'; ?>;"></i>
								<!-- ============================== -->
							<input type="hidden" name="thumbprint" id="old_thumb" value="<?php echo $result['thumbprint']; ?>">
							<!-- ==========3==================== -->
							</div>
							<!-- ============================== -->
						</div>


						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Face Recognition</label>
							<span data-toggle="tooltip" data-placement="right" title="Store your face scan Using a biometeric device available with a registered recruitment agency"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="file" class="form-control-file" value="<?php echo $result['face']; ?>" id="face" name="face" class="form-control" autocomplete="off" >
						<div style="position: relative;">
							<a target="<?php echo $result['face']?'_blank':''; ?>" href="<?php echo $result['face']?('media/user_profile/'.$result['face']):'javascript:;';?>" id="face_a">
								<img id="face_img" src="<?php echo $result['face']?$img_path.$result['face']:$noimg; ?>" alt="&nbsp;" height="50"  /></a>
							
							<i class="fa fa-times-circle fa-lg delete_btn" id="delete_face" onclick="delete_images('face','<?php echo $result['face']; ?>')"  style="display: <?php echo $result['face']?'block':'none'; ?>;"></i>

							<input type="hidden" name="face" id="old_face" value="<?php echo $result['face']; ?>">
						</div>
						</div>
					</div>


					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Signature</label>
							<span data-toggle="tooltip" data-placement="right" title="You can store your signature"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="file" class="form-control-file" value="<?php echo $result['signature']; ?>" id="signature" name="signature" class="form-control" autocomplete="off" >
							<div style="position: relative;">
							<a target="<?php echo $result['signature']?'_blank':''; ?>" href="<?php echo $result['signature']?('media/user_profile/'.$result['signature']):'javascript:;';?>" id="signature_a">
								<img id="signature_img" src="<?php echo $result['signature']?$img_path.$result['signature']:$noimg; ?>" alt="&nbsp;" height="50"  /></a>
								

							<i class="fa fa-times-circle fa-lg delete_btn" id="delete_sign" onclick="delete_images('sign','<?php echo $result['signature']; ?>')"  style="display: <?php echo $result['signature']?'block':'none'; ?>;"></i>

							<input type="hidden" name="signature" id="old_sign" value="<?php echo $result['signature']; ?>">
							</div>
						</div>


						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Passport</label>
							<span data-toggle="tooltip" data-placement="right" title="Upload only valid documents"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="file" class="form-control-file" value="<?php echo $result['passport']; ?>" id="passport" name="passport" class="form-control" autocomplete="off" >
							<div style="position: relative;">
							<a target="<?php echo $result['passport']?'_blank':''; ?>" href="<?php echo $result['passport']?('media/user_profile/'.$result['passport']):'javascript:;';?>" id="passport_a">
								<img id="passport_img" src="<?php echo $result['passport']?$img_path.$result['passport']:$noimg; ?>" alt="&nbsp;" height="50"  /></a>

							<i class="fa fa-times-circle fa-lg delete_btn" id="delete_passport" onclick="delete_images('passport','<?php echo $result['passport']; ?>')"  style="display: <?php echo $result['passport']?'block':'none'; ?>;"></i>

							<input type="hidden" name="passport" id="old_passport" value="<?php echo $result['passport']; ?>">
							</div>
						</div>
					</div>



					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Aadhar Card</label>
							<span data-toggle="tooltip" data-placement="right" title="Upload only valid documents "><i class="fa fa-info-circle" aria-hidden="true"></i></span>
							
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="file" class="form-control-file" value="<?php echo $result['aadhar']; ?>" id="aadhar_card" name="aadhar" class="form-control" >
							<div style="position: relative;">
							<a target="<?php echo $result['aadhar']?'_blank':''; ?>" href="<?php echo $result['aadhar']?('media/user_profile/'.$result['aadhar']):'javascript:;';?>" id="aadhar_card_a">
								<img id="aadhar_card_img" src="<?php echo $result['aadhar']?$img_path.$result['aadhar']:$noimg; ?>" alt="&nbsp;" height="50"  /></a>

							<i class="fa fa-times-circle fa-lg delete_btn" id="delete_aadhar" onclick="delete_images('aadhar','<?php echo $result['aadhar']; ?>')"  style="display: <?php echo $result['aadhar']?'block':'none'; ?>;"></i>

							<input type="hidden" name="aadhar" id="old_aadhar" value="<?php echo $result['aadhar']; ?>">
							</div>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Pan Card</label>
							<span data-toggle="tooltip" data-placement="right" title="Upload only valid documents"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="file" class="form-control-file" value="<?php echo $result['pancard']; ?>" id="pan_card" name="pancard" class="form-control"  >

							<div style="position: relative;">
							<a target="<?php echo $result['pancard']?'_blank':''; ?>" href="<?php echo $result['pancard']?('media/user_profile/'.$result['pancard']):'javascript:;';?>" id="pan_card_a">
								<img id="pan_card_img" src="<?php echo $result['pancard']?$img_path.$result['pancard']:$noimg; ?>" alt="&nbsp;" height="50"  /></a>

							<i class="fa fa-times-circle fa-lg delete_btn" id="delete_pancard" onclick="delete_images('pancard','<?php echo $result['pancard']; ?>')"  style="display: <?php echo $result['pancard']?'block':'none'; ?>;"></i>

							<input type="hidden" name="pancard" id="old_pancard" value="<?php echo $result['pancard']; ?>">
						</div>
						</div>

					</div>

					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Police Verification</label>
							<span data-toggle="tooltip" data-placement="right" title="Character certificate stamped by the local police station"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
							
						</div>

						<?php 
						$ext = explode('.',$result['police_form']);
						$police_form = ($ext[1]=='pdf'?'images/pdf-icon.png':'media/user_profile/'.$result['police_form']);
						 ?>
						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="file" class="form-control-file" value="<?php echo $result['police_form']; ?>" id="police_form" name="police_form" class="form-control" >
							<div style="position: relative;">
							<a target="<?php echo $result['police_form']?'_blank':''; ?>" href="<?php echo $result['police_form']?('media/user_profile/'.$result['police_form']):'javascript:;';?>" id="police_form_a">
								<img id="police_form_img" src="<?php echo $result['police_form']?$police_form:$noimg; ?>" alt="&nbsp;" height="50"  /></a>

							<i class="fa fa-times-circle fa-lg delete_btn" id="delete_police_form" onclick="delete_images('policeform','<?php echo $result['police_form']; ?>')"  style="display: <?php echo $result['police_form']?'block':'none'; ?>;"></i>

							<input type="hidden" name="police_form" id="old_police_form" value="<?php echo $result['police_form']; ?>">
							</div>
						</div>
						<?php 
						$ext = explode('.',$result['reference_letter']);
						$letter = ($ext[1]=='pdf'?'images/pdf-icon.png':'media/user_profile/'.$result['reference_letter']);
						 ?>
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Refernce Letters</label>
							<span data-toggle="tooltip" data-placement="right" title="Reference letters from past employer or mentor "><i class="fa fa-info-circle" aria-hidden="true"></i></span>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="file" class="form-control-file" value="<?php echo $result['reference_letter']; ?>" id="reference_letter" name="reference_letter" class="form-control"  >
							<div style="position: relative;">
							<a target="<?php echo $result['reference_letter']?'_blank':''; ?>" href="<?php echo $result['reference_letter']?('media/user_profile/'.$result['reference_letter']):'javascript:;';?>" id="reference_letter_a">
								<img id="reference_letter_img" src="<?php echo $result['reference_letter']?$letter:$noimg; ?>" alt="&nbsp;" height="50"  /></a>

							<i class="fa fa-times-circle fa-lg delete_btn" id="delete_letter" onclick="delete_images('letter','<?php echo $result['reference_letter']; ?>')"  style="display: <?php echo $result['reference_letter']?'block':'none'; ?>;"></i>

							<input type="hidden" name="reference_letter" id="old_letter" value="<?php echo $result['reference_letter']; ?>">
							</div>
						</div>

					</div>


					<div class="clearfix"></div>
					<label><strong>Something More About You :</strong></label>
					<span data-toggle="tooltip" data-placement="right" title="Express yourself, share what is interesting and unique about you"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Title Me</label>
							<span data-toggle="tooltip" data-placement="right" title="Share an alias name which describes you best"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="text"  value="<?php echo $result['title_me']; ?>" id="title_me" name="title_me" class="form-control" autocomplete="off">
						</div>
					</div>

					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>About me</label>
							<span data-toggle="tooltip" data-placement="right" title="Share something about your personality and the way you think, your attitude"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-10 col-sm-10 col-xs-12">
							<textarea class="form-control" style="height: 139px;" name="about_me" maxlength="500" id="about_me" cols="8"><?php echo $result['about_me']; ?></textarea>
						</div>
					</div>
					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>My Goal</label>
							<span data-toggle="tooltip" data-placement="right" title="What is ultimate goal / dream in life?"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12" >
							<input type="text"  value="<?php echo $result['goal']; ?>" id="goal" name="goal" class="form-control" autocomplete="off">
						</div>
					</div>


					<div class="clearfix"></div>
					<label><strong>My Favorite Quotations / Sayings:</strong></label>
					<span data-toggle="tooltip" data-placement="right" title="Favourite line, proverb or saying that best describes your attitude in life / has inspired you"><i class="fa fa-info-circle" aria-hidden="true"></i></span>

					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Quote 1 </label>
							
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12" >
							<input type="text"  value="<?php echo $result['quote1']; ?>" id="quote1" name="quote1" class="form-control" autocomplete="off">
						</div>
					</div>
					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Quote 2 </label>
							
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12" >
							<input type="text"  value="<?php echo $result['quote2']; ?>" id="quote2" name="quote2" class="form-control" autocomplete="off">
						</div>
					</div>	

					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Quote 3 </label>
							
						</div>
						<div class="col-md-10 col-sm-10 col-xs-12" >
							<input type="text"  value="<?php echo $result['quote3']; ?>" id="quote3" name="quote3" class="form-control" autocomplete="off">
						</div>
					</div>


				</div>
			</fieldset>
			<!--=================Session Value=====================--> 

			<!-- ====================================================== -->

			<br>
			<br>

			
			<div class="row" style="text-align: center;">

				<input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">
				<!-- <a style="position:relative; right:34px;" href="<?php if($result['id']!=''){ echo 'index.php?control=user_profile&task=step3&id='.$result["id"]; }else{ echo 'javascript:;';} ?>" class="btn btn-primary redu" onclick="reload();" ><?php if($result['id']!=''){ echo 'Next'; }else{ echo 'Cancel';} ?></a> -->
				<!-- <a style="position:relative; right:34px;" href="javascript:;" class="btn btn-primary redu" onclick="reload();" >Cancel</a>  -->
			</div>
			<input type="hidden" name="control" value="user"/>
			<input type="hidden" name="task" value="personal_detail"/>
			<input type="hidden" id="profile_color" name="profile_color" value="<?php echo $result['profile_color']; ?>"/>
			<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

			<!--account details ends here-->
		</div>
	</form>
</div>
</div>



<?php } ?>
</section>

<script>
	function goBack() {
		window.history.back();
	}
</script>
<link rel="stylesheet" href="template/myvcv/plugins/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script type="text/javascript" src="template/myvcv/plugins/select2/select2.full.min.js"></script>
<script>
	$('#dob').datetimepicker({
		yearOffset:0,
		lang:'ch',
		timepicker:false,
		format:'d/m/Y',
		formatDate:'Y/m/d',
		scrollMonth : false,
	//minDate:'-1970/01/02', // yesterday is minimum date
	maxDate:'+1970/01/01' // and tommorow is maximum date calendar
});
	
	$('.languages').select2();
</script> 




<script type="text/javascript">
	/*===============================Gaurav Js================*/

/*=========================================*/

/*$("#plot_id").change(function(){
	var str = $(this).val();
	var pid = $('#project').val();
	var bid = $('#block_id').val();
	$.ajax({url: "script/user_profile/project_plot_id.php?project="+pid+"&block="+bid+"&plot="+str, success: function(result){
		$("#project_plot_id").val(result);
	}});
});
*/
$(document).ready(function(){
  // $('#tips_box_btn').click();
});
/*=================Show Image=================*/

function readpassport(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#passport_img').attr('src', e.target.result);
			$('#passport_a').attr('href', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#passport").change(function(){
	readpassport(this);
});


function readthumbprint(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#thumbprint_img').attr('src', e.target.result);
			$('#thumbprint_a').attr('href', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#thumbprint").change(function(){
	readthumbprint(this);
});


function readface(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#face_img').attr('src', e.target.result);
			$('#face_a').attr('href', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#face").change(function(){
	readface(this);
});


function readsignature(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#signature_img').attr('src', e.target.result);
			$('#signature_a').attr('href', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#signature").change(function(){
	readsignature(this);
});



function readaadhar_card(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#aadhar_card_img').attr('src', e.target.result);
			$('#aadhar_card_a').attr('href', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#aadhar_card").change(function(){
	readaadhar_card(this);
});

function readpan_card(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#pan_card_img').attr('src', e.target.result);
			$('#pan_card_a').attr('href', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#pan_card").change(function(){
	readpan_card(this);
});


function readpolice_form(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#police_form_img').attr('src', e.target.result);
			$('#police_form_a').attr('href', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#police_form").change(function(){
	readpolice_form(this);
});


function readreference_letter(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			if(e.target.result.split(":")[1].split(';')[0] == 'application/pdf'){
			$('#reference_letter_img').attr('src', 'images/pdf-icon.png');;
			$('#reference_letter_a').attr('href', 'images/pdf-icon.png');
			}else{
			$('#reference_letter_img').attr('src', e.target.result);
			$('#reference_letter_a').attr('href', e.target.result);
			}
		
		}
		reader.readAsDataURL(input.files[0]);
	}
}
$("#reference_letter").change(function(){
	readreference_letter(this);
});


/*============Auto hide alert box================*/
$(".alert").delay(2000).slideUp(200, function() {
	$(this).alert('close');
});

function delete_images(str,val){

var r = confirm("Are you sure?");
if (r == true) {
  $.ajax({url: "script/delete/delete_imgs.php?image_type="+str+"&img_name="+val, success: function(result){

  	if(result == 'thumb_deleted'){
  		$('#old_thumb').val('');
  		 $("#thumbprint_img").attr('src','');
  		 $("#thumbprint_a").attr('href','');
  		 $('#delete_thumb').hide();
  		 
  		 alert('Document Succesfully Removed');
  	}


  	if(result == 'face_deleted'){
  		 $("#old_face").val('');
  		 $("#face_img").attr('src','');
  		 $("#face_a").attr('href','');
  		 $("#delete_face").hide();
  		
  		 alert('Document Succesfully Removed');
  	}

  	if(result == 'sign_deleted'){
  		$('#old_sign').val('');
  		 $("#signature_img").attr('src','');
  		 $("#signature_a").attr('href','');
  		 $('#delete_sign').hide();
  		 
  		 alert('Document Succesfully Removed');
  	}

  	if(result == 'passport_deleted'){
  		 $('#old_passport').val('');
  		 $("#passport_img").attr('src','');
  		 $("#passport_a").attr('href','');
  		 $('#delete_passport').hide();
  		
  		 alert('Document Succesfully Removed');
  	}

  	if(result == 'aadhar_deleted'){
  		 $('#old_aadhar').val('');
  		 $("#aadhar_card_img").attr('src','');
  		 $("#aadhar_card_a").attr('href','');
  		 $('#delete_aadhar').hide();
  		
  		 alert('Document Succesfully Removed');
  	}

  	if(result == 'pancard_deleted'){
  		$('#old_pancard').val('');
  		 $("#pan_card_img").attr('src','');
  		 $("#pan_card_a").attr('href','');
  		 $('#delete_pancard').hide();
  		 
  		 alert('Document Succesfully Removed');
  	}

  	if(result == 'police_form_deleted'){
  		 $('#old_police_form').val('');
  		 $("#police_form_img").attr('src','');
  		 $("#police_form_a").attr('href','');
  		 $('#delete_police_form').hide();
  		
  		 alert('Document Succesfully Removed');
  	}


  	if(result == 'letter_deleted'){
  		 $('#old_letter').val('');
  		 $("#reference_letter_img").attr('src','');
  		 $("#reference_letter_a").attr('href','');
  		 $('#delete_letter').hide();
  		
  		 alert('Document Succesfully Removed');
  	}

        
    }});
} 
return 0;
}

function select_color(val){
	$('#profile_color').val(val);
}

$('#no_tips').click(function(){
  if($(this).is(':checked')){
    var column = 'profile_tips';
     $.ajax({url: "script/tips_page.php?column="+column+"&status=0", success: function(result){

     }
}) }
});
</script>
