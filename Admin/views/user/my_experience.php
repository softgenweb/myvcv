<?php foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
   .tabs, .tabs .col-md-2 {
   padding: unset !important;
   }
 
   .letter_text{
   	width: 80px !important;
   }
   /*.fix_width{
   width: 20% !important;
   } */
   input.file_width{
   width: 80px !important;
   }
   .delete_btn{
position: absolute;
top: 0;
cursor: pointer;
/*color: #cd5c5c;*/
right: 0;
}
</style>
<!-- <script>
   function preventBack(){window.history.forward();}
   setTimeout("preventBack()", 0);
   window.onunload=function(){null};
   </script> -->
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Your Experience Timeline</h3>
      <a  target="_blank" class="btn btn-primary bulu pull-right" href="../preview/?page=preview">View Profile</a>
   </div>
   <!-- <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Plot Booking List</a></li>
      <?php if($result!='') {?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Profile </li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Your Experience </li>
      <?php } ?>
   </ol> -->
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php 
      unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']); 
      
      		//header( "refresh:1;url=index.php?control=plot_booking&task=show" );
      }	if($_REQUEST['id']==''){   ?>
   <!-- <div class="col-md-10 col-md-offset-1"> <em style="color: red;">** Step-1 and Step-2 is Mandatory, Please Complete form till Step-4.</em> -->
   <!-- </div>  -->
   <?php } ?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
         <div class="box-body">
            <fieldset>

               
         <!-- ==================================Tabs on Mobile ==================================== -->
    <div class="col-md-12 tabs small-screen" style="margin-bottom: 15px;">
            <ul class="list-group">
            <a data-toggle="collapse" href="#collapse1"><li class="list-group-item" >Update Profile<i class="fa fa-caret-down pull-right"></i></li></a>
          </ul>
        <div id="collapse1" class="panel-collapse collapse">
          <ul class="list-group">
            <a href="index.php?control=user&task=personal_detail"><li class="list-group-item">Personal Details</li></a>
            <a href="index.php?control=user&task=video_detail"><li class="list-group-item"> Video CV</li></a>
            <a href="index.php?control=user&task=my_experience"><li class="list-group-item"> My Experience</li></a>
            <a href="index.php?control=user&task=my_skill"><li class="list-group-item"> My Skills</li></a>
            <a href="index.php?control=user&task=my_education"><li class="list-group-item"> My Awards / Education</li></a>
            <a href="index.php?control=user&task=my_portfolio"><li class="list-group-item"> My Portfolio</li></a>
          </ul>
        </div>
      
    </div>
    <!-- ====================================================================================== -->
               <div class="col-md-12 tabs large-screen">
<?php include 'script/updateprofile_header.php'; ?>
               </div>
               <div class="col-sm-12 second_header ons" >
                  <label><strong>My Experience :</strong></label>
                  <span data-toggle="tooltip" data-placement="right" title="you can list all the work experiences you had in the past in a timeline. 
                  what ever detail you add here will be presented in a very dynamic and appealing way"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                  <div class="form-group oneline">
                     <div class="col-md-2 col-sm-2 col-xs-12">
                        <label>Title </label>
                     </div>
                     <div class="col-md-4 col-sm-4 col-xs-12" >
                        <input type="text"  value="<?php echo $result['experience_title']; ?>" id="experience_title" name="experience_title" class="form-control" autocomplete="off">
                     </div>
                  </div>
                  <div class="form-group oneline">
                     <div class="col-md-2 col-sm-2 col-xs-12">
                        <label>Description</label>
                        <!-- <b style="color:red">*</b> --> 
                     </div>
                     <div class="col-md-10 col-sm-10 col-xs-12">
                        <textarea class="form-control" style="height: 139px;" name="experience_description" maxlength="5000" id="experience_description" cols="8"><?php echo $result['experience_description']; ?></textarea>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <br>
                  <label><strong>Experience Timeline:</strong></label>
                  <div class="form-group oneline">
                     <!-- <div class="col-md-2 col-sm-2 col-xs-12">
                        <label>Country</label>
                        </div> -->
                     <!-- <b style="color:red">*</b> --> 
                     <!-- ====================JS Boxes=============== -->
                     <table class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th width="50">Sl No</th>
                              <th >Company Name</th>
                              <th>Job Designation</th>
                              <th>Worked Year</th>
                              <th>Description</th>
                              <th  width="100">Logo</th>
                              <th>Recommendation Letter</th>
                              <th width="100">Action</th>
                           </tr>
                        </thead>
                        <tbody id="add_items">
                           <?php if($this->check_exp($_SESSION['adminid'])!=''){
                              $this->Query("SELECT * FROM `jobseeker_experience` WHERE `user_id`='".$_SESSION['adminid']."' ORDER BY `year` DESC");
                              $exps = $this->fetchArray(); 
                              $j = 0;
                              foreach($exps as $exp){
                              	$j++;
						$ext = explode('.',$exp['recommendation_letter']);
						$name = explode('.',explode('/',$exp['recommendation_letter'])[2])[0];
						$letter = ($ext[1]=='pdf'?'images/pdf-icon.png':'media/user_profile/'.$exp['recommendation_letter']);
                              	           ?>
                           <tr>
                              <td><strong class="slno"><?php echo $j; ?></strong></td>
                              <td><input type="text" value="<?php echo $exp['company_name']; ?>"   name="company_name[]" id="company_name<?php echo $J; ?>" class="form-control company_name" required=""></td>
                              <td><input type="text" value="<?php echo $exp['job_title']; ?>" name="job_title[]" id="job_title<?php echo $j; ?>" class="form-control job_title" required=""></td>
                              <td>
                                 <select class="form-control year" name="year[]" id="year<?php echo $j; ?>" required="">
                                    <option value="" style="width: 80px;">Select</option>
                                    <?php  foreach (range(date('Y'), date('Y')-60) as $year) { ?>
                                    <option value="<?php echo $year; ?>" <?php echo $year==$exp['year']?'selected':''; ?>><?php echo $year; ?></option>
                                    <?php }  ?> 
                                 </select>
                                 <!-- <input type="text" value="" name="year[]" id="year1" class="form-control " required=""></td> -->
                              <td><textarea class="form-control description" name="description[]" id="description<?php echo $j; ?>"><?php echo $exp['description']; ?></textarea>   </td>
                              <!-- <td><input type="text" value="" name="disc[]" id="disc1" class="form-control " >   </td> -->
                              <td width="100"><input type="file" value="" name="logo[]" onchange="logo_upload(this,<?php echo $j; ?>)" id="logo<?php echo $j; ?>" class="file_width logo" > 
                                 <div style="position: relative;">
                                 <img id="logo_img<?php echo $j; ?>" class="logo_img" src="media/user_profile/<?php echo $exp['logo']; ?>" alt='&nbsp;' width="100"> 
                                 <!-- =========2========change id as per name (delete_thumb) -->
                        <i class="fa fa-times-circle fa-lg delete_btn" id="delete_logo<?php echo $j; ?>" onclick="delete_images('experience_logo','<?php echo $exp['logo']; ?>','<?php echo $j; ?>')"  style="display: <?php echo $exp['logo']?'block':'none'; ?>;"></i>
                        <!-- ============================== -->
                                 <input type="hidden" name="logo[]" id="old_logo<?php echo $j; ?>" value="<?php echo $exp['logo']; ?>"> 
                              </div>
                              </td>
                              <td width="100"><input type="file" value="" onchange="pdf_upload(this,<?php echo $j; ?>)" name="letter[]"  id="letter<?php echo $j; ?>" class="file_width letter" > 
                              <div style="position: relative;">
                                 <img class="letter_img" id="letter_img<?php echo $j; ?>" src="<?php echo $letter; ?>" alt='&nbsp;' height="50">
                                 <span class="letter_text" id="letter_text<?php echo $j; ?>"><?php echo $name; ?></span> 
                                  <!-- =========2========change id as per name (delete_thumb) -->
                        <i class="fa fa-times-circle fa-lg delete_btn" id="delete_letter<?php echo $j; ?>" onclick="delete_images('experience_letter','<?php echo $exp['recommendation_letter']; ?>','<?php echo $j; ?>')"  style="display: <?php echo $exp['recommendation_letter']?'block':'none'; ?>;"></i>
                        <!-- ============================== -->
                                 <input type="hidden" name="letter[]" id="old_letter<?php echo $j; ?>" value="<?php echo $exp['recommendation_letter']; ?>">
                                 </div> 
                              </td>
                              <!-- ===================== -->
                              <td class="action_btn"><a href="javascript:;" class="remove" id="remove<?php echo $j; ?>"><i class="fa fa-minus-circle fa-2x"></i></a>&nbsp;&nbsp;
                                 <strong><input type="checkbox" onclick="status_check(<?php echo $j; ?>);" class="minimal-red" id="status<?php echo $j; ?>" name="status[]" value="1" <?php echo $exp['status']=='0'?'checked':''; ?>> Hide
                                    <input type="hidden" name="check_status[]"  value="<?php echo $exp['status']=='0'?'1':'0'; ?>" id="check_status<?php echo $j; ?>">
                                 </strong>
                              </td>
                              <!-- ========================= -->
                           </tr>
                           <?php } }else{ ?>
                           <tr>
                              <td><strong class="slno">1</strong></td>
                              <td><input type="text" value=""   name="company_name[]" id="company_name1" class="form-control company_name" ></td>
                              <td><input type="text" value="" name="job_title[]" id="job_title1" class="form-control job_title" ></td>
                              <td>
                                 <select class="form-control year" name="year[]" id="year1" >
                                    <option value="">Select</option>
                                    <?php  foreach (range(date('Y'), date('Y')-60) as $year) { ?>
                                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                    <?php }  ?> 
                                 </select>
                                 <!-- <input type="text" value="" name="year[]" id="year1" class="form-control " required=""></td> -->
                              <td><textarea class="form-control description" name="description[]" id="description1"></textarea>   </td>
                              <!-- <td><input type="text" value="" name="disc[]" id="disc1" class="form-control " >   </td> -->
                              <td width="100"><input type="file" value="0" onchange="logo_upload(this,1)"  name="logo[]" id="logo1" class="file_width logo" >
                                 <img id="logo_img1" src="logo_img" alt='&nbsp;' width="100"> 
                              </td>
                              <td width="100"><input type="file" value="0" onchange="pdf_upload(this,1)" name="letter[]" id="letter1" class="file_width letter" >
                                 <img class="letter_img" id="letter_img<?php echo $j; ?>" src="media/user_profile/<?php echo $exp['letter']; ?>" alt='&nbsp;'  height="50">
                                 <span class="letter_text" id="letter_text<?php echo $j; ?>"></span>    
                              </td>
                              <!-- ================== -->
                              <td class="action_btn"><a href="javascript:;" class="remove" id="remove1" style="display: none;"><i class="fa fa-minus-circle fa-2x"></i></a>&nbsp;&nbsp;
                                 <strong><input type="checkbox"  onclick="status_check(1);" class="minimal-red" id="status1" name="status[]" value="1"> Hide
                           <input type="hidden" name="check_status[]" value="0" id="check_status1">
                                 </strong>
                              </td>
                              <!-- =================== -->
                           </tr>
                           <?php  } ?>
                        </tbody>
                        <tfoot>
                           <tr valign="middle" align="center" >
                              <th colspan="3"></th>
                              <th><a href="javascript:;" class="add_more"><i class="fa fa-plus-circle fa-2x" style="cursor: pointer;"> </i></a></th>
                              <th colspan="3"></th>
                           </tr>
                        </tfoot>
                     </table>
                     <!-- ============================================ -->
                  </div>
                  <div class="clearfix"></div>
               </div>
            </fieldset>
            <!--=================Session Value=====================--> 
            <!-- ====================================================== -->
            <br>
            <br>
            <div class="row" style="text-align: center;">
               <input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">
            </div>
            <input type="hidden" name="control" value="user"/>
            <input type="hidden" name="task" value="my_experience"/>
            <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            <!--account details ends here-->
         </div>
      </form>
   </div>
</div>
</section>
<script>
   function goBack() {
   	window.history.back();
   }
</script>
<script type="text/javascript" src="template/myvcv/plugins/iCheck/icheck.min.js"></script>
<script>

   
   
   $('.add_more').click(function(){
   	n = $('tbody#add_items tr').length +1;
   // alert(n);
   if(n>50){
   show_error();
   // alert("You Can not add Itmes more than 50");
   return 0;
   }else{
   var html_box = '<tr><td><strong class="slno">'+n+'</strong><td><input type="text" value="" name="company_name[]" id="company_name'+n+'" class="form-control company_name" required=""></td><td><input type="text" value="" name="job_title[]" id="job_title'+n+'" class="form-control job_title" required=""></td><td><select class="form-control year" name="year[]" id="year'+n+'" required=""><option value="">Select</option><?php  foreach (range(date('Y'), date('Y')-60) as $year) { ?><option value="<?php echo $year; ?>"><?php echo $year; ?></option> <?php }  ?> </select></td><td><textarea class="form-control description" name="description[]" id="description'+n+'"></textarea> </td><td width="100"><input type="file" value="0" name="logo[]" onchange="logo_upload(this,'+n+')" id="logo'+n+'" class="file_width logo" > <img id="logo_img'+n+'" class="logo_img"  alt="&nbsp;" width="100"> </td><td width="100"><input type="file" onchange="pdf_upload(this,'+n+')" value="0" name="letter[]" id="letter'+n+'" class="file_width letter" ><img class="letter_img" id="letter_img'+n+'" src="" alt="&nbsp;"  height="50"><span class="letter_text" id="letter_text'+n+'"></span>  </td><td  class="action_btn"><a href="javascript:;" class="remove" id="remove'+n+'"><i class="fa fa-minus-circle fa-2x"></i></a>&nbsp;&nbsp; <strong><input type="checkbox"  class="minimal-red" id="status'+n+'" onclick="status_check('+n+');" name="status[]" value="1"> Hide<input type="hidden" name="check_status[]" value="0" id="check_status'+n+'"></strong></td></tr>';
   $('#add_items').append(html_box);
   $('#total_item').val(n);
   
   
   
   } 
   
   
   $('.remove').click(function(){
   $(this).closest('tr').remove();
   
   $('#total_item').val(($('tbody#add_items tr').length));
   $('td strong.slno').text(function (i) { return i + 1; });
   
   $('td input.company_name').each(function(b){
   $(this).attr('id','company_name'+parseInt(b+1));
   });
   $('td input.job_title').each(function(c){
   $(this).attr('id','job_title'+parseInt(c+1));
   });
   $('td input.year').each(function(d){
   $(this).attr('id','year'+parseInt(d+1));
   });
   $('td textarea.description').each(function(e){
   $(this).attr('id','description'+parseInt(e+1));
   });
   
   $('td input.logo').each(function(f){
   $(this).attr('id','logo'+parseInt(f+1));
   $(this).attr('onchange','logo_upload(this,'+parseInt(f+1)+')');
   });
   $('td img.logo_img').each(function(f){
   $(this).attr('id','logo_img'+parseInt(f+1));
   });
   
   $('td input.letter').each(function(f){
   $(this).attr('id','letter'+parseInt(f+1));
    $(this).attr('onchange','pdf_upload(this,'+parseInt(f+1)+')');
   });
   $('td img.letter_img').each(function(f){
   $(this).attr('id','letter_img'+parseInt(f+1));
   });
   $('td span.letter_text').each(function(f){
   $(this).attr('id','letter_text'+parseInt(f+1));
   });
   
   
  /* if((($('tbody#add_items tr').length)-1) < 1){
   $('.remove').hide();
   }*/
   
   });
   
   });
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
   	$(this).alert('close');
   });
   
   
   
   function logo_upload(input,id) {
   
   	img = '#logo_img'+id;
   // link = '#portfolio_a'+id;
   if (input.files && input.files[0]) {
   var reader = new FileReader();
   reader.onload = function (e) {
   $(img).attr('src', e.target.result);
   // $(link).attr('href', e.target.result);
   }
   reader.readAsDataURL(input.files[0]);
   }
   }
   
   function pdf_upload(input,id) {
   
	letter = '#letter'+id;
	img = '#letter_img'+id;
	text = '#letter_text'+id; 
	value = $(letter).val();
	name = value.replace(/\\/g, '/').split("/")[2].split('.')[0];
   if (input.files && input.files[0]) {
   var reader = new FileReader();
   reader.onload = function (e) {
   		if(e.target.result.split(":")[1].split(';')[0] == 'application/pdf'){
   $(img).attr('src', 'images/pdf-icon.png');
		}else if(e.target.result.split(":")[1].split('/')[0] == 'image'){
 	$(img).attr('src', e.target.result);
		}
		$(text).html(name);
   }
   reader.readAsDataURL(input.files[0]);
   }
   }
   
   
   
   <?php if($this->check_exp($_SESSION['adminid'])!=''){ ?>
   
   $('.remove').click(function(){
   
   
   $(this).closest('tr').remove();
   
   // $('#total_item').val(($('tbody#add_items tr').length));
   
   $('td strong.slno').text(function (i) { return i + 1; });
   
   $('td input.company_name').each(function(b){
   $(this).attr('id','company_name'+parseInt(b+1));
   });
   $('td input.job_title').each(function(c){
   $(this).attr('id','job_title'+parseInt(c+1));
   });
   $('td input.year').each(function(d){
   $(this).attr('id','year'+parseInt(d+1));
   });
   $('td textarea.description').each(function(e){
   $(this).attr('id','description'+parseInt(e+1));
   });
   
   $('td input.logo').each(function(f){
   $(this).attr('id','logo'+parseInt(f+1));
   $(this).attr('onchange','logo_upload(this,'+parseInt(f+1)+')');
   });
   $('td img.logo_img').each(function(f){
   $(this).attr('id','logo_img'+parseInt(f+1));
   });
   $('td input.letter').each(function(f){
   $(this).attr('id','letter'+parseInt(f+1));
   $(this).attr('onchange','pdf_upload(this,'+parseInt(f+1)+')');
   });
   $('td img.letter_img').each(function(f){
   $(this).attr('id','letter_img'+parseInt(f+1));
   });
   $('td span.letter_text').each(function(f){
   $(this).attr('id','letter_text'+parseInt(f+1));
   });
   
  /* if((($('tbody#add_items tr').length)-1) < 1){
   $('.remove').hide();
   }*/
   });
   <?php } ?>
  /*    $('input[type="checkbox"].minimal-red').iCheck({
   checkboxClass: 'icheckbox_minimal-red'
            // radioClass: 'iradio_minimal-red'
        });
*/

function status_check(id){
   check_status = '#check_status'+id;
   console.log(check_status);
  
    if($('#status'+id).is(":checked")){
       console.log('#status'+id);
      $(check_status).val('1');
    }else{
       $(check_status).val('0');
    }
}
function delete_images(str,val,id){
//str = thumb, 
var r = confirm("Are you sure?");
if (r == true) {
  $.ajax({url: "script/delete/delete_imgs.php?image_type="+str+"&img_name="+val+"&id="+id, success: function(result){

   if(result == 'logo_deleted'){
       $("#logo_img"+id).attr('src','');
       $("#old_logo"+id).val('');
       $('#delete_logo'+id).hide();
       alert('Logo Succesfully Removed');
   }
   if(result == 'letter_deleted'){
       $("#letter_img"+id).attr('src','');
       $("#letter_text"+id).html('');
       $("#old_letter"+id).val('');
       $('#delete_letter'+id).hide();
       alert('Letter Succesfully Removed');
   }

    }});
} 
return 0;
}
</script>

