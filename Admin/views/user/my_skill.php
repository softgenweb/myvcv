<?php foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
   .tabs, .tabs .col-md-2 {
   padding: unset !important;
   }

   /*.fix_width{
   width: 20% !important;
   } */
</style>
<!-- <script>
   function preventBack(){window.history.forward();}
   setTimeout("preventBack()", 0);
   window.onunload=function(){null};
   </script> -->
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Your Skills</h3>
      <a  target="_blank" class="btn btn-primary bulu pull-right" href="../preview/?page=preview">View Profile</a>
   </div>
 <!--   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href=""><i class="fa fa-list" aria-hidden="true"></i> Edit Profile </a></li>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add/Edit Your Skills </li>
   
   </ol> -->
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php 
      unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']); 
      
      		//header( "refresh:1;url=index.php?control=plot_booking&task=show" );
      }	if($_REQUEST['id']==''){   ?>
   <!-- <div class="col-md-10 col-md-offset-1"> <em style="color: red;">** Step-1 and Step-2 is Mandatory, Please Complete form till Step-4.</em> -->
   <!-- </div>  -->
   <?php } ?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
         <div class="box-body">
            <fieldset>
               
         <!-- ==================================Tabs on Mobile ==================================== -->
    <div class="col-md-12 tabs small-screen" style="margin-bottom: 15px;">
            <ul class="list-group">
            <a data-toggle="collapse" href="#collapse1"><li class="list-group-item" >Update Profile<i class="fa fa-caret-down pull-right"></i></li></a>
          </ul>
        <div id="collapse1" class="panel-collapse collapse">
          <ul class="list-group">
            <a href="index.php?control=user&task=personal_detail"><li class="list-group-item">Personal Details</li></a>
            <a href="index.php?control=user&task=video_detail"><li class="list-group-item"> Video CV</li></a>
            <a href="index.php?control=user&task=my_experience"><li class="list-group-item"> My Experience</li></a>
            <a href="index.php?control=user&task=my_skill"><li class="list-group-item"> My Skills</li></a>
            <a href="index.php?control=user&task=my_education"><li class="list-group-item"> My Awards / Education</li></a>
            <a href="index.php?control=user&task=my_portfolio"><li class="list-group-item"> My Portfolio</li></a>
          </ul>
        </div>
      
    </div>
    <!-- ====================================================================================== -->


               <div class="col-md-12 tabs large-screen">
  <?php include 'script/updateprofile_header.php'; ?>
               </div>
               <div class="col-sm-12 second_header ons" >
                  <label><strong>My Skills :</strong></label>
                  <span data-toggle="tooltip" data-placement="right" title="you describe different skills and talents you have. it can be as many as 5 unique skills.
                  Add a job title thats best descibes your skill and give brief description of your level of expertise"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                  <div class="form-group oneline">
                     <div class="col-md-2 col-sm-2 col-xs-12">
                        <label>Title </label>
                     </div>
                     <div class="col-md-4 col-sm-4 col-xs-12" >
                        <input type="text"  value="<?php echo $result['skills_title']; ?>" id="skills_title" name="skills_title" class="form-control" autocomplete="off">
                     </div>
                  </div>
                  <div class="form-group oneline">
                     <div class="col-md-2 col-sm-2 col-xs-12">
                        <label>Description</label>
                        <!-- <b style="color:red">*</b> --> 
                     </div>
                     <div class="col-md-10 col-sm-10 col-xs-12">
                        <textarea class="form-control" style="height: 139px;" name="skills_description" maxlength="500" id="skills_description" cols="8"><?php echo $result['skills_description']; ?></textarea>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <br>
                  <label><strong>My Top 5 Skills:</strong></label>
                  <span data-toggle="tooltip" data-placement="right" title="add the 5 top skills you want to highlight and the percentage is the level of expertize with 100% means complete mastery. 
                  the info you add here will be displayed as a eye catching graphic illustration"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                  <div class="form-group oneline">
                     <!-- <div class="col-md-2 col-sm-2 col-xs-12">
                        <label>Country</label>
                        </div> -->
                     <!-- <b style="color:red">*</b> --> 
                     <!-- ====================JS Boxes=============== -->
                     <table class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th width="50">Sl No</th>
                              <th >Skill Name</th>
                              <th>Percent (%)</th>
                              <th>Description</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody id="add_items">
                           <?php if($this->check_skill($_SESSION['adminid'])!=''){
                             $this->Query("SELECT * FROM `jobseeker_skills` WHERE `user_id`='".$_SESSION['adminid']."' ORDER BY `id` ASC");
                              $skills = $this->fetchArray(); 
                              $j = 0;
                              foreach($skills as $skill){
                              	$j++;           ?>              
                           <tr>
                              <td><strong class="slno"><?php echo $j; ?></strong></td>
                              <td><input type="text" value="<?php echo $skill['skill_name']; ?>"   name="skill_name[]" id="skill_name1" class="form-control skill_name" required=""></td>
                              <td><input type="text" maxlength="3" pattern="[0-9]+" value="<?php echo $skill['percent']; ?>" onkeyup="check_val(this.value,<?php echo $j; ?>)" onkeypress="return isNumber(event)"  name="percent[]" id="percent<?php echo $j; ?>" class="form-control percent" required=""></td>
                              <td><textarea class="form-control description" name="description[]" id="description<?php echo $j; ?>"><?php echo $skill['description']; ?></textarea>   </td>
                              <!-- <td><input type="text" value="" name="disc[]" id="disc1" class="form-control " >   </td> -->
                              <td class="action_btn"><a href="javascript:;" class="remove" id="remove<?php echo $j; ?>"><i class="fa fa-minus-circle fa-2x"></i></a>&nbsp;&nbsp;
                                 <strong><input type="checkbox" onclick="status_check(<?php echo $j; ?>);"  id="status<?php echo $j; ?>" name="status[]" value="1" <?php echo $skill['status']=='0'?'checked':''; ?>> Hide
                                    <input type="hidden" name="check_status[]"   value="<?php echo $skill['status']=='0'?'1':'0'; ?>" id="check_status<?php echo $j; ?>">
                                 </strong>
                              </td>
                           </tr>
                           <?php } }else{ ?>  
                           <tr>
                              <td><strong class="slno">1</strong></td>
                              <td><input type="text" value=""   name="skill_name[]" id="skill_name1" class="form-control skill_name" required=""></td>
                              <td><input type="text" value="" onkeyup="check_val(this.value,1)" onkeypress="return isNumber(event)"  name="percent[]" id="percent1" class="form-control percent" required=""></td>
                              <td><textarea class="form-control description" name="description[]" id="description1"></textarea>   </td>
                              <!-- <td><input type="text" value="" name="disc[]" id="disc1" class="form-control " >   </td> -->
                              <td class="action_btn"><a href="javascript:;" class="remove" id="remove1" style="display: none;"><i class="fa fa-minus-circle fa-2x"></i></a>&nbsp;&nbsp;
                                 <strong><input type="checkbox"  onclick="status_check(1);" class="minimal-red" id="status1" name="status[]" value="1"> Hide
                           <input type="hidden" name="check_status[]" value="0" id="check_status1">
                                 </strong>
                              </td>
                           </tr>
                           <?php } ?>
                        </tbody>
                        <tfoot>
                           <tr valign="middle" align="center" >
                              <th colspan="2"></th>
                              <th>
                                 <center><a href="javascript:;" class="add_more"><i class="fa fa-plus-circle fa-2x" style="cursor: pointer;"> </i></a></center>
                              </th>
                              <th colspan="3"></th>
                           </tr>
                           <tr>
                              <td></td>
                              <td><strong> My Interest:</strong></td>
                             
                              <td colspan="3">
                                 <div class="form-group col-sm-12">
                                    <div class="col-md-3">
                                       <?php 
                                       $this->Query("SELECT * FROM `jobseeker_interest` WHERE `user_id`='".$_SESSION['adminid']."'");
                                          $ress = $this->fetchArray();

                                          $interest = explode(',', $ress[0]['name']); ?>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Singing" <?php echo in_array("Singing",$interest)?"checked":""; ?>> <strong> Singing</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Dancing" <?php echo in_array("Dancing",$interest)?"checked":""; ?>> <strong> Dancing</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Drawing" <?php echo in_array("Drawing",$interest)?"checked":""; ?>> <strong> Drawing</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Painting" <?php echo in_array("Painting",$interest)?"checked":""; ?>> <strong> Painting</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Creative Writing" <?php echo in_array("Creative Writing",$interest)?"checked":""; ?>> <strong> Creative Writing</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Task Management" <?php echo in_array("Task Management",$interest)?"checked":""; ?>> <strong> Task Management</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Leadership" <?php echo in_array("Leadership",$interest)?"checked":""; ?>> <strong> Leadership</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Social Work" <?php echo in_array("Social Work",$interest)?"checked":""; ?>> <strong> Social Work</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Community Service" <?php echo in_array("Community Service",$interest)?"checked":""; ?>> <strong> Community Service</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Farming" <?php echo in_array("Farming",$interest)?"checked":""; ?>> <strong> Farming</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Cycling" <?php echo in_array("Cycling",$interest)?"checked":""; ?>> <strong> Cycling</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Musical Instrument" <?php echo in_array("Musical Instrument",$interest)?"checked":""; ?>> <strong> Musical Instrument</strong></label><br>
                                    </div>
                                    <div class="col-md-3">
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Learning" <?php echo in_array("Learning",$interest)?"checked":""; ?>> <strong> Learning</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Reading" <?php echo in_array("Reading",$interest)?"checked":""; ?>> <strong> Reading</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Traveling" <?php echo in_array("Traveling",$interest)?"checked":""; ?>> <strong> Traveling</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Fitness" <?php echo in_array("Fitness",$interest)?"checked":""; ?>> <strong> Fitness</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Nutrition" <?php echo in_array("Nutrition",$interest)?"checked":""; ?>> <strong> Nutrition</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Design & Decor" <?php echo in_array("Design & Decor",$interest)?"checked":""; ?>> <strong> Design & Decor</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Training" <?php echo in_array("Training",$interest)?"checked":""; ?>> <strong> Training</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Sports" <?php echo in_array("Sports",$interest)?"checked":""; ?>> <strong> Sports</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Adventure" <?php echo in_array("Adventure",$interest)?"checked":""; ?>> <strong> Adventure</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Healing" <?php echo in_array("Healing",$interest)?"checked":""; ?>> <strong> Healing</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Partying" <?php echo in_array("Partying",$interest)?"checked":""; ?>> <strong> Partying</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Driving" <?php echo in_array("Driving",$interest)?"checked":""; ?>> <strong> Driving</strong></label><br>
                                    </div>
                                    <div class="col-md-3">
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Talking on Phone" <?php echo in_array("Talking on Phone",$interest)?"checked":""; ?>> <strong> Talking on Phone</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Social Networking" <?php echo in_array("Social Networking",$interest)?"checked":""; ?>> <strong> Social Networking</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Internet Surfing" <?php echo in_array("Internet Surfing",$interest)?"checked":""; ?>> <strong> Internet Surfing</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Meeting New People" <?php echo in_array("Meeting New People",$interest)?"checked":""; ?>> <strong> Meeting New People</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Learning New Skill" <?php echo in_array("Learning New Skill",$interest)?"checked":""; ?>> <strong> Learning New Skill</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Swimming" <?php echo in_array("Swimming",$interest)?"checked":""; ?>> <strong> Swimming</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Photography" <?php echo in_array("Photography",$interest)?"checked":""; ?>> <strong> Photography</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Cooking" <?php echo in_array("Cooking",$interest)?"checked":""; ?>> <strong> Cooking</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Acting" <?php echo in_array("Acting",$interest)?"checked":""; ?>> <strong> Acting</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Fashion" <?php echo in_array("Fashion",$interest)?"checked":""; ?>> <strong> Fashion</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Racing" <?php echo in_array("Racing",$interest)?"checked":""; ?>> <strong> Racing</strong></label><br>
                                    </div>
                                    <div class="col-md-3">
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Competition" <?php echo in_array("Competition",$interest)?"checked":""; ?>> <strong> Competition</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Home Improvement" <?php echo in_array("Home Improvement",$interest)?"checked":""; ?>> <strong> Home Improvement</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Landscaping / Gardening" <?php echo in_array("Landscaping / Gardening",$interest)?"checked":""; ?>> <strong> Landscaping / Gardening</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Arts & Crafts" <?php echo in_array("Arts & Crafts",$interest)?"checked":""; ?>> <strong> Arts & Crafts</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Research & Development" <?php echo in_array("Research & Development",$interest)?"checked":""; ?>> <strong> Research & Development</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Science & Technology" <?php echo in_array("Science & Technology",$interest)?"checked":""; ?>> <strong> Science & Technology</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Accounting" <?php echo in_array("Accounting",$interest)?"checked":""; ?>> <strong> Accounting</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Watching T.V." <?php echo in_array("Watching T.V.",$interest)?"checked":""; ?>> <strong> Watching T.V.</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Planning" <?php echo in_array("Planning",$interest)?"checked":""; ?>> <strong> Planning</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Eating" <?php echo in_array("Eating",$interest)?"checked":""; ?>> <strong> Eating</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Sleeping" <?php echo in_array("Sleeping",$interest)?"checked":""; ?>> <strong> Sleeping</strong></label><br>
                                       <label><input type="checkbox" class="minimal-red" name="interest[]" value="Personal Care" <?php echo in_array("Personal Care",$interest)?"checked":""; ?>> <strong> Personal Care</strong></label><br>
                                    </div>
                                 </div>
                              </td>
                              <td></td>
                           </tr>
                        </tfoot>
                     </table>
                     <!-- ============================================ -->
                  </div>
                  <div class="clearfix"></div>
               </div>
            </fieldset>
            <!--=================Session Value=====================--> 
            <!-- ====================================================== -->
            <br>
            <br>
       
            <div class="row" style="text-align: center;">
               <input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">
            </div>
            <input type="hidden" name="control" value="user"/>
            <input type="hidden" name="task" value="my_skill"/>
            <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            <!--account details ends here-->
         </div>
      </form>
   </div>
</div>
</section>
<script>
   function goBack() {
   	window.history.back();
   }
</script>
<link rel="stylesheet" href="template/myvcv/plugins/select2/select2.min.css">
<script type="text/javascript" src="template/myvcv/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="template/myvcv/plugins/select2/select2.full.min.js"></script>
<script>
   $('.add_more').click(function(){
   	n = $('tbody#add_items tr').length +1;
     // alert(n);
     if(n>5){
     // show_error();
     alert("You Can not add Itmes more than 5");  
     $('.add_more').hide();
     return 0;
     
   }else{
   var html_box = '<tr><td><strong class="slno">'+n+'</strong></td><td><input type="text" name="skill_name[]" id="skill_name'+n+'" class="form-control skill_name" required=""></td><td><input type="text" value="" onkeyup="check_val(this.value,'+n+')" onkeypress="return isNumber(event)"  name="percent[]" id="percent'+n+'" class="form-control percent" required=""></td><td><textarea class="form-control description" name="description[]" id="description'+n+'"></textarea> </td><td  class="action_btn"><a href="javascript:;" class="remove" id="remove'+n+'"><i class="fa fa-minus-circle fa-2x"></i></a>&nbsp;&nbsp; <strong><input type="checkbox"  class="minimal-red" id="status'+n+'" onclick="status_check('+n+');" name="status[]" value="1"> Hide<input type="hidden" name="check_status[]" value="0" id="check_status'+n+'"></strong></td></tr>';
   $('#add_items').append(html_box);
   $('#total_item').val(n);
   
   } 
   
   
   $('.remove').click(function(){
   $(this).closest('tr').remove();
   $('.add_more').show();
   $('#total_item').val(($('tbody#add_items tr').length));
   $('td strong.slno').text(function (i) { return i + 1; });
   
   $('td input.skill_name').each(function(b){
   	$(this).attr('id','skill_name'+parseInt(b+1));
   });
   $('td input.percent').each(function(c){
   	$(this).attr('id','percent'+parseInt(c+1));
   });
   $('td textarea.description').each(function(e){
   	$(this).attr('id','description'+parseInt(e+1));
   });
   
   /*if((($('tbody#add_items tr').length)-1) < 1){
   	$('.remove').hide();
   }*/
   });
   
   });
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
   	$(this).alert('close');
   });
   
   
   <?php if($this->check_skill($_SESSION['adminid'])!=''){ ?>
   	$('.remove').click(function(){
   		$(this).closest('tr').remove();
   
   		$('#total_item').val(($('tbody#add_items tr').length));
   		$('td strong.slno').text(function (i) { return i + 1; });
   
   		$('td input.skill_name').each(function(b){
   			$(this).attr('id','skill_name'+parseInt(b+1));
   		});
   		$('td input.percent').each(function(c){
   			$(this).attr('id','percent'+parseInt(c+1));
   		});
   		$('td textarea.description').each(function(e){
   			$(this).attr('id','description'+parseInt(e+1));
   		});
   
   		/*if((($('tbody#add_items tr').length)-1) < 1){
   			$('.remove').hide();
   		}*/
   	});
   	<?php } ?>
   	function isNumber(evt) {
   // alert(res);
   
   evt = (evt) ? evt : window.event;
   var charCode = (evt.which) ? evt.which : evt.keyCode;
   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
   	return false;
   }
   return true;
   }
   
   function check_val(val,id){
   ids = '#percent'+id;
   // alert(ids);
   if(val > 100){
   	alert("Percent not more than 100%");
   	$(ids).val(0);
   	return 0;
   }
   }
   
   $('input[type="checkbox"].minimal-red').iCheck({
   checkboxClass: 'icheckbox_minimal-red'
            // radioClass: 'iradio_minimal-red'
        });


   function status_check(id){
   check_status = '#check_status'+id;
   console.log(check_status);
  
    if($('#status'+id).is(":checked")){
       console.log('#status'+id);
      $(check_status).val('1');
    }else{
       $(check_status).val('0');
    }
}
    
</script>

