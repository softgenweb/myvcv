<?php 
foreach($results as $result) { }   
foreach($banks as $bank) { }   
foreach($companies as $company) { }   
   ?>
<style type="text/css">
   .tabs, .tabs .col-md-2, #select2-recruiter_id-container {
   padding: unset !important;
   }
   .delete_btn{
   position: absolute;
   top: 0;
   cursor: pointer;
   color: #cd5c5c;
   }
</style>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Personal Details <i class="fa fa-info-circle" data-toggle="modal" data-target="#tips_box" style="cursor: pointer;"></i></h3>
      <!-- <a  target="_blank" class="btn btn-primary bulu pull-right" href="http://www.<?php echo strtolower($_SESSION['username']); ?>.myvcv.asia/">View Profile</a> -->
      <a  target="_blank" class="btn btn-primary bulu pull-right" href="../preview/?page=preview">View Profile</a>
   </div>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php 
      unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']); 
      
      }	
      $img_path = 'media/user_profile/';
      $noimg = 'images/no_img.png';
      if($_REQUEST['id']==''){   ?>
   <?php } ?>
   <button id="tips_box_btn"  data-toggle="modal" data-target="#tips_box" style="display: none;"></button>
   <?php if($result['profile_tips']=='1'){ ?>
   <div id="tips_box" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <center>
                  <h4 class="modal-title">Here are some To Dos for you:</h4>
               </center>
            </div>
            <div class="modal-body">
               <div class="text-left">
                  <p>You can upload your Biodata and other personal details here, and also press other tabs as follow:</p>
                  <p><label>Video CV</label> --> Upload or reccord your New Video CV here</p>
                  <p><label>My Experience</label> --> Upload your work history and experience details</p>
                  <p><label>Testimonials</label> --> Upload your testimonials</p>
                  <p><label>Clients / Companies</label> --> Upload your  Clients / Companies hiring people from you</p>
                  <p><label>FAQ</label> --> Question / Answer rating</p>
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
               <div class="pull-left"><input type="checkbox" value="close" id="no_tips" name="no_tips"> No Tips </div>
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
   </div>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
         <div class="box-body">
            <fieldset>
               <!-- ==================================Tabs on Mobile ==================================== -->
               <div class="col-md-12 tabs small-screen" style="margin-bottom: 15px;">
                  <ul class="list-group">
                     <a data-toggle="collapse" href="#collapse1">
                        <li class="list-group-item" >Update Profile<i class="fa fa-caret-down pull-right"></i></li>
                     </a>
                  </ul>
                  <div id="collapse1" class="panel-collapse collapse">
                     <ul class="list-group">
                        <a href="index.php?control=user&task=recruiter_profile">
                           <li class="list-group-item">Personal Details</li>
                        </a>
                       <a href="index.php?control=user&task=video_detail">
                           <li class="list-group-item"> Video CV</li>
                        </a>
                       <a href="index.php?control=user&task=testimonials">
                       <li class="list-group-item"> Testimonials</li>
                       </a>
          				<a href="javascript:;">
                        <li class="list-group-item"> Clients / Companies</li>
                        </a>
           				<a href="javascript:;">
                        <li class="list-group-item"> FAQ</li>
                        </a>                       
                     </ul>
                  </div>
               </div>
               <!-- ====================================================================================== -->
               <div class="col-md-12 tabs large-screen">
<?php include 'script/updateprofile_header.php'; ?>
               </div>
               <div class="col-sm-10 col-md-offset-1 second_header ons" >
               </div>
               <div class="clearfix"></div>

               <div class="form-group oneline">
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <label>Company Name</label> 
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <input type="text"  value="<?php echo $result['name']; ?>" id="name" name="name" class="form-control" autocomplete="off">
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <label>User ID/Alias Name</label> 
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <input type="text"  value="<?php echo $result['username']; ?>" id="username" name="username" class="form-control" disabled="">
                  </div>
               </div>
               <div class="form-group oneline">
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <label>Coupon Code</label> 
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <input type="text"  value="<?php echo $result['father_husband_name']; ?>" id="coupon" name="coupon" class="form-control" disabled="">
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <label>Company Start Date</label> 
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <input type="text" placeholder="DD/MM/YYYY" value="<?php echo $result['dob']; ?>" id="dob" name="dob" class="form-control" autocomplete="off"  >
                  </div>
               </div>
               <div class="clearfix"></div>
               <br>
               <label><strong>Bank Details :</strong></label>
               <span data-toggle="tooltip" data-placement="right" title="Keep your contact details uptodate"><i class="fa fa-info-circle"></i></span>
               <div class="form-group oneline">
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <label>Bank Name</label> 
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <input type="text" value="<?php echo $bank['bank_name']; ?>"  id="bank_name" name="bank_name" class="form-control" autocomplete="off" >
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <label>Account Number</label>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <input type="text"  value="<?php echo $bank['ac_no']; ?>" id="ac_no" name="ac_no" class="form-control" autocomplete="off" >
                  </div>
               </div>
               <div class="form-group oneline">
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <label>IFSC Code</label>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                     <input type="text"  value="<?php echo $bank['ifsc_code']; ?>"  id="ifsc_code" name="ifsc_code" class="form-control" autocomplete="off" >
                     <span class="msgValid" id="msgmobile"></span>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                     <label>Account Type</label>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12">
                  	<select name="ac_type" id="ac_type" class="form-control" >
                  		<option value="">Select</option>
                  		<option value="Saving" <?php echo $bank['ac_type']=="Saving"?"selected":""; ?> >Saving</option>
                  		<option value="Current" <?php echo $bank['ac_type']=="Current"?"selected":""; ?> >Current</option>
                  	</select>
                  </div>
               </div>
			</div>
		<div class="clearfix"></div>
		<br>
		<label><strong>Contact Details :</strong></label>
		<span data-toggle="tooltip" data-placement="right" title="Keep your contact details uptodate"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Primary Contact Number</label> 
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $result['mobile']; ?>" maxlength="10" id="mobile" name="mobile" pattern="[0-9]+" class="form-control" autocomplete="off" >
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Registered email address</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $result['email']; ?>" id="email" name="email" class="form-control" autocomplete="off" >
		   </div>
		</div>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Facebook</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $result['facebook']; ?>" id="facebook" name="facebook" class="form-control" autocomplete="off" >
		      <span class="msgValid" id="msgfacebook"></span>
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Twitter</label> 
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $result['twitter']; ?>" id="twitter" name="twitter" class="form-control" autocomplete="off" >
		   </div>
		</div>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Instagram</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $result['instagram']; ?>" id="instagram" name="instagram" class="form-control" autocomplete="off"  >
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Linkedin</label> 
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $result['linkedin']; ?>" id="linkedin" name="linkedin" class="form-control" autocomplete="off">
		   </div>
		</div>
		<div class="clearfix"></div>
		<br>
		<label><strong>Director :</strong></label>
		<span data-toggle="tooltip" data-placement="right" title="Keep your contact details uptodate"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Name</label> 
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $company['director_name']; ?>"  id="director_name" name="director_name" class="form-control" autocomplete="off" >
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Photo</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		     <input type="file" class="form-control-file" value="<?php echo $company['image']; ?>" id="dirimage" name="image" >
		      <div style="position: relative;">
		         <a target="<?php echo $company['image']?'_blank':''; ?>" href="<?php echo $company['image']?('media/user_profile/'.$company['image']):'javascript:;';?>" id="image_a">
		         <img id="dirimage_img" src="<?php echo $company['image']?$img_path.$company['image']:$noimg; ?>" alt="&nbsp;" height="50"  /></a>
		         <!-- <i class="fa fa-times-circle fa-lg delete_btn" id="delete_dirimage" onclick="delete_images('image','<?php echo $company['image']; ?>')"  style="display: <?php echo $company['image']?'block':'none'; ?>;"></i> -->
		         <input type="hidden" name="image" id="old_image" value="<?php echo $company['image']; ?>">
		      </div>
		   </div>
		</div>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Words of Director</label>
		   </div>
		   <div class="col-md-10 col-sm-10 col-xs-12">
		      <input type="text"  value="<?php echo $company['directors_words']; ?>" id="directors_words" name="directors_words" class="form-control" autocomplete="off" >
		   </div>
		</div>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Contact number</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $company['director_mobile']; ?>" id="director_mobile" name="director_mobile" class="form-control" autocomplete="off"  >
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Email</label> 
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $company['director_email']; ?>" id="director_email" name="director_email" class="form-control" autocomplete="off">
		   </div>
		</div>
		<div class="clearfix"></div>
		<br>
		<label><strong>Contact Person :</strong></label>
		<span data-toggle="tooltip" data-placement="right" title="Keep your contact details uptodate"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Name</label> 
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $company['contact_pname']; ?>" id="contact_pname" name="contact_pname" class="form-control" autocomplete="off" >
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Contact number</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $company['contact_pnum']; ?>" id="contact_pnum" name="contact_pnum" class="form-control" autocomplete="off" >
		   </div>
		</div>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Email</label> 
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <input type="text"  value="<?php echo $company['contact_pemail']; ?>" id="contact_pemail" name="contact_pemail" class="form-control" autocomplete="off">
		   </div>
		</div>
		<div class="clearfix"></div>
		<br>
		<label><strong>Office Address 1 :</strong></label>
		<span data-toggle="tooltip" data-placement="right" title="Keep your residential address uptodate"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Country</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <select name="country" id="country" onchange="state_change(this.value);" class="form-control" autocomplete="off">
		         <option value="">Select</option>
		         <?php $this->Query("SELECT * FROM `country` WHERE 1"); 
		            $contrys = $this->fetchArray();
		            foreach($contrys as $contry) {?>
		         <option value="<?php echo $contry['id']; ?>" <?php if($result['country']==$contry['id']) { ?> selected="selected" <?php } ?>><?php echo $contry['country_name']; ?></option>
		         <?php }?>
		      </select>
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>State</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <select name="state_id" id="state_id" onchange="city_change(this.value);" class="form-control" autocomplete="off" >
		         <option value="">Select</option>
		         <?php $this->Query("SELECT * FROM `state` WHERE `country_id`='".$result['country']."'"); 
		            $states = $this->fetchArray();
		            foreach($states as $state) {
		            	 ?>
		         <option value="<?php echo $state['id']; ?>" <?php if($result['state_id']==$state['id']) {?> selected="selected" <?php } ?>><?php echo $state['state_name']; ?></option>
		         <?php }?>
		      </select>
		   </div>
		</div>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>City</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <select name="city_id" id="city_id" class="form-control" autocomplete="off"  >
		         <option value="">Select</option>
		         <?php $this->Query("SELECT * FROM `city` WHERE `state_id`='".$result['state_id']."' AND `country_id`='".$result['country']."'"); 
		            $citys = $this->fetchArray();
		            foreach($citys as $city) {?>
		         <option value="<?php echo $city['id']; ?>" <?php if($result['city_id']==$city['id']) {?> selected="selected" <?php } ?>><?php echo $city['city_name']; ?></option>
		         <?php }?>
		      </select>
		      <span class="msgValid" id="msgvehicle_no"></span>
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Address</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <textarea name="address" id="address" class="form-control" cols="3" style="height: 70px;"><?php echo $result['address']; ?></textarea>
		      <span class="msgValid" id="msgres_address"></span> 
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
		      <label>Pin Code</label>
		      <!-- <b style="color:red">*</b> --> 
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
		      <input type="text"  value="<?php echo $result['postalcode']; ?>" id="postalcode" name="postalcode" class="form-control" autocomplete="off">
		      <span class="msgValid" id="msgres_pincode"></span>
		   </div>
		</div>
		<div class="clearfix"></div>
		<br>
		<label><strong>Office Address 2 :</strong></label>
		<span data-toggle="tooltip" data-placement="right" title="Keep your residential address uptodate"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Country</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <select name="country_office" id="country_office"  class="form-control" autocomplete="off">
		         <option value="">Select</option>
		         <?php $this->Query("SELECT * FROM `country` WHERE 1"); 
		            $contrys = $this->fetchArray();
		            foreach($contrys as $contry) {?>
		         <option value="<?php echo $contry['id']; ?>" <?php if($company['country_office']==$contry['id']) {?> selected="selected" <?php } ?>><?php echo $contry['country_name']; ?></option>
		         <?php }?>
		      </select>
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>State</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <select name="state_office" id="state_office" class="form-control" autocomplete="off" >
		         <option value="">Select</option>
		         <?php $this->Query("SELECT * FROM `state` WHERE `country_id`='".$company['country_office']."'"); 
		            $states = $this->fetchArray();
		            foreach($states as $state) {
		            	?>
		         <option value="<?php echo $state['id']; ?>" <?php if($company['state_office']==$state['id']) {?> selected="selected" <?php } ?>><?php echo $state['state_name']; ?></option>
		         <?php }?>
		      </select>
		   </div>
		</div>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>City</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <select name="city_office" id="city_office" class="form-control" autocomplete="off"  >
		         <option value="">Select</option>
		         <?php $this->Query("SELECT * FROM `city` WHERE `state_id`='".$company['state_office']."' AND `country_id`='".$company['country_office']."'"); 
		            $citys = $this->fetchArray();
		            foreach($citys as $city) {?>
		         <option value="<?php echo $city['id']; ?>" <?php if($company['city_office']==$city['id']) {?> selected="selected" <?php } ?>><?php echo $city['city_name']; ?></option>
		         <?php }?>
		      </select>
		      <span class="msgValid" id="msgvehicle_no"></span>
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Address</label>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12">
		      <textarea name="office_add" id="office_add" class="form-control" cols="3" style="height: 70px;"><?php echo $company['office_add']; ?></textarea> 
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12" style="margin-top: -25px;">
		      <label>Pin Code</label>
		      <!-- <b style="color:red">*</b> --> 
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12" style="margin-top: -25px;">
		      <input type="text"  value="<?php echo $company['pin_office']; ?>" id="pin_office" name="pin_office" class="form-control" autocomplete="off">
		   </div>
		</div>
		<div class="clearfix"></div>
		<br>
		<!-- <label><strong>Documents :</strong></label>
		<span data-toggle="tooltip" data-placement="right" title="Here you upload all your ID proof and biometeric information"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Signature</label>
		      <span data-toggle="tooltip" data-placement="right" title="You can store your signature"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12" >
		      <input type="file" class="form-control-file" value="<?php echo $result['signature']; ?>" id="signature" name="signature" class="form-control" autocomplete="off" >
		      <div style="position: relative;">
		         <a target="<?php echo $result['signature']?'_blank':''; ?>" href="<?php echo $result['signature']?('media/user_profile/'.$result['signature']):'javascript:;';?>" id="signature_a">
		         <img id="signature_img" src="<?php echo $result['signature']?$img_path.$result['signature']:$noimg; ?>" alt="&nbsp;" height="50"  /></a>
		         <i class="fa fa-times-circle fa-lg delete_btn" id="delete_sign" onclick="delete_images('sign','<?php echo $result['signature']; ?>')"  style="display: <?php echo $result['signature']?'block':'none'; ?>;"></i>
		         <input type="hidden" name="signature" id="old_sign" value="<?php echo $result['signature']; ?>">
		      </div>
		   </div>
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Passport</label>
		      <span data-toggle="tooltip" data-placement="right" title="Upload only valid documents"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12" >
		      <input type="file" class="form-control-file" value="<?php echo $result['passport']; ?>" id="passport" name="passport" class="form-control" autocomplete="off" >
		      <div style="position: relative;">
		         <a target="<?php echo $result['passport']?'_blank':''; ?>" href="<?php echo $result['passport']?('media/user_profile/'.$result['passport']):'javascript:;';?>" id="passport_a">
		         <img id="passport_img" src="<?php echo $result['passport']?$img_path.$result['passport']:$noimg; ?>" alt="&nbsp;" height="50"  /></a>
		         <i class="fa fa-times-circle fa-lg delete_btn" id="delete_passport" onclick="delete_images('passport','<?php echo $result['passport']; ?>')"  style="display: <?php echo $result['passport']?'block':'none'; ?>;"></i>
		         <input type="hidden" name="passport" id="old_passport" value="<?php echo $result['passport']; ?>">
		      </div>
		   </div>
		</div> -->

		<div class="clearfix"></div>
		<label><strong>About the Company :</strong></label>
		<span data-toggle="tooltip" data-placement="right" title="Express yourself, share what is interesting and unique about you"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Sub Title / Tag Line</label>
		      <span data-toggle="tooltip" data-placement="right" title="Share an alias name which describes you best"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		   </div>
		   <div class="col-md-4 col-sm-4 col-xs-12" >
		      <input type="text"  value="<?php echo $result['title_me']; ?>" id="title_me" name="title_me" class="form-control" autocomplete="off">
		   </div>
		</div>
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Description</label>
		      <span data-toggle="tooltip" data-placement="right" title="Share something about your personality and the way you think, your attitude"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		   </div>
		   <div class="col-md-10 col-sm-10 col-xs-12">
		      <textarea class="form-control" style="height: 139px;" name="about_me" maxlength="500" id="about_me" cols="8"><?php echo $result['about_me']; ?></textarea>
		   </div>
		</div>
		<!-- <div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>My Goal</label>
		      <span data-toggle="tooltip" data-placement="right" title="What is ultimate goal / dream in life?"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
		   </div>
		   <div class="col-md-10 col-sm-10 col-xs-12" >
		      <input type="text"  value="<?php echo $result['goal']; ?>" id="goal" name="goal" class="form-control" autocomplete="off">
		   </div>
		</div> -->
		<div class="clearfix"></div>
		<!-- <label><strong>Company  Slogan:</strong></label> -->
		
		<div class="form-group oneline">
		   <div class="col-md-2 col-sm-2 col-xs-12">
		      <label>Company Slogan</label>
		   </div>
		   <div class="col-md-10 col-sm-10 col-xs-12" >
		       <textarea class="form-control" style="height: 139px;" name="quote1" maxlength="500" id="quote1" cols="8"><?php echo $result['quote1']; ?></textarea>
		   </div>
		</div>
		</fieldset>
		<!--=================Session Value=====================--> 
		<!-- ====================================================== -->
		<br>
		<br>
		<div class="row" style="text-align: center;">
		   <input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">
		</div>
		<input type="hidden" name="control" value="user"/>
		<input type="hidden" name="task" value="recruiter_profile"/>
		<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
		<!--account details ends here-->
		</div>
		</form>
		</div>
		</div>
		<?php } ?>
		</section>

		<!-- <link rel="stylesheet" href="template/myvcv/plugins/select2/select2.min.css"> -->
		<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
		<script src="assets/date_picker/jquery.js"></script> 
		<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
		<!-- <script type="text/javascript" src="template/myvcv/plugins/select2/select2.full.min.js"></script> -->
		<script>
		   $('#dob').datetimepicker({
		   	yearOffset:0,
		   	lang:'ch',
		   	timepicker:false,
		   	format:'d-m-Y',
		   	formatDate:'Y-m-d',
		   	scrollMonth : false,
		   //minDate:'-1970/01/02', // yesterday is minimum date
		   maxDate:'+1970/01/01' // and tommorow is maximum date calendar
		   });
		   

			   function readimage(input) {
				   if (input.files && input.files[0]) {
				   		var reader = new FileReader();
				   	reader.onload = function (e) {
				   		$('#dirimage_img').attr('src', e.target.result);
				   		$('#dirimage_a').attr('href', e.target.result);
				   		}
				   		reader.readAsDataURL(input.files[0]);
				   	}
			   	}

			   $("#dirimage").change(function(){
			   		readimage(this);
			   });

		   /*============Auto hide alert box================*/
		   $(".alert").delay(2000).slideUp(200, function() {
		   		$(this).alert('close');
		   });
		   
	
		   function select_color(val){
		   		$('#profile_color').val(val);
		   }
		   
		   $('#no_tips').click(function(){
		    if($(this).is(':checked')){
		      var column = 'profile_tips';
		       $.ajax({url: "script/tips_page.php?column="+column+"&status=0", success: function(result){
		   
		       }
		   }) }
		   });

		    /*=========================================*/
$("#country_office").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/state.php?id="+str, success: function(result){
      $("#state_office").html(result);
      
    }});
});
  /*=========================================*/
$("#state_office").change(function(){
	var str = $(this).val();
    $.ajax({url: "script/city.php?id="+str, success: function(result){
        $("#city_office").html(result);
    }});
});
 
		</script>

