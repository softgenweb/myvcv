<?php foreach($results as $result) { }  //echo "<pre>".print_r($_SESSION);    ?>
<style type="text/css">
	.tabs, .tabs .col-md-2 {
		padding: unset !important;
	}

	/*.fix_width{
    width: 20% !important;
  } */
input.file_width {
	width: 80px !important;
}
</style>
<!-- <script>
  function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
</script> -->
<div class="panel panel-default" >
	<div class="box-header">        
		<h3 class="box-title">Add Your Education & Awards</h3>
    <a  target="_blank" class="btn btn-primary bulu pull-right" href="../preview/?page=preview">View Profile</a>
	</div> 
<!-- 	<ol class="breadcrumb">
		<li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li> 
		<li><a href="index.php?control=plot_booking&task=show"><i class="fa fa-list" aria-hidden="true"></i> Plot Booking List</a></li>
		<?php if($result!='') {?>

		<li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Profile </li>
		<?php } else { ?>
		<li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Education / Awards </li>
		<?php } ?>
	</ol> -->

	<?php if(isset($_SESSION['alertmessage'])){?>
	<div class="box-body">
		<div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
		</div>
	</div>

	<?php 
	unset($_SESSION['alertmessage']);
	unset($_SESSION['errorclass']); 

			//header( "refresh:1;url=index.php?control=plot_booking&task=show" );
}	if($_REQUEST['id']==''){   ?>

<!-- <div class="col-md-10 col-md-offset-1"> <em style="color: red;">** Step-1 and Step-2 is Mandatory, Please Complete form till Step-4.</em> -->
<!-- </div>  -->
<?php } ?>
<div class="panel-body">
	<form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();">
		<div class="box-body">				

			<fieldset>


         <!-- ==================================Tabs on Mobile ==================================== -->
    <div class="col-md-12 tabs small-screen" style="margin-bottom: 15px;">
            <ul class="list-group">
            <a data-toggle="collapse" href="#collapse1"><li class="list-group-item" >Update Profile<i class="fa fa-caret-down pull-right"></i></li></a>
          </ul>
        <div id="collapse1" class="panel-collapse collapse">
          <ul class="list-group">
            <a href="index.php?control=user&task=personal_detail"><li class="list-group-item">Personal Details</li></a>
            <a href="index.php?control=user&task=video_detail"><li class="list-group-item"> Video CV</li></a>
            <a href="index.php?control=user&task=my_experience"><li class="list-group-item"> My Experience</li></a>
            <a href="index.php?control=user&task=my_skill"><li class="list-group-item"> My Skills</li></a>
            <a href="index.php?control=user&task=my_education"><li class="list-group-item"> My Awards / Education</li></a>
            <a href="index.php?control=user&task=my_portfolio"><li class="list-group-item"> My Portfolio</li></a>
          </ul>
        </div>
      
    </div>
    <!-- ====================================================================================== -->
            	<div class="col-md-12 tabs large-screen">
<?php include 'script/updateprofile_header.php'; ?>
            	</div>
            	<div class="col-sm-12 second_header ons" >
					<label><strong>My Awards / Education :</strong></label>
          <span data-toggle="tooltip" data-placement="right" title="you can share your report cards and certificates you may have achieved in your academics or other"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Title </label>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12" >
							<input type="text"  value="<?php echo $result['education_title']; ?>" id="education_title" name="education_title" class="form-control" autocomplete="off">
						</div>
					</div>
					<div class="form-group oneline">
						<div class="col-md-2 col-sm-2 col-xs-12">
							<label>Description</label>
							<!-- <b style="color:red">*</b> --> 
						</div>

						<div class="col-md-10 col-sm-10 col-xs-12">
							<textarea class="form-control" style="height: 139px;" name="education_description" maxlength="500" id="education_description" cols="8"><?php echo $result['education_description']; ?></textarea>
						</div>

					</div> 
					<div class="clearfix"></div><br>
					<label><strong>My Awards / Education:</strong></label>
					<div class="form-group oneline">
						<!-- <div class="col-md-2 col-sm-2 col-xs-12">
							<label>Country</label>
							</div> -->
							<!-- <b style="color:red">*</b> --> 
<!-- ====================JS Boxes=============== -->
 <table class="table table-bordered table-striped">
               <thead>
                  <tr>
                     <th width="50">Sl No</th>
                     <th>Institution Name</th>
                     <th>Location</th>
                     <th>Course/Award Title</th>
                     <th width="100">Worked Year</th>
                     <th>Upload Certificate</th>
                     <th>Description</th>
                     
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody id="add_items">
      <?php if($this->check_edu($_SESSION['adminid'])!=''){

$this->Query("SELECT * FROM `jobseeker_education` WHERE `user_id`='".$_SESSION['adminid']."' ORDER BY `year` DESC");
 $edus = $this->fetchArray(); 
        $j = 0;
        foreach($edus as $edu){
       $j++;    
	$ext = explode('.',$edu['certificate']);
	$name = explode('.',explode('/',$edu['certificate'])[2])[0];
	$certificate = ($ext[1]=='pdf'?'images/pdf-icon.png':'media/user_profile/'.$edu['certificate']);       ?>            
                  <tr>
                     <td><strong class="slno"><?php echo $j; ?></strong></td>
                     <td><input type="text" value="<?php echo $edu['institute_name']; ?>"   name="institute_name[]" id="institute_name<?php echo $j; ?>" class="form-control institute_name" required=""></td>
                     <td><input type="text" value="<?php echo $edu['location']; ?>" name="location[]" id="location<?php echo $j; ?>" class="form-control location" required=""></td>
                     <td><input type="text" value="<?php echo $edu['title']; ?>" name="title[]" id="title<?php echo $j; ?>" class="form-control title" required=""></td>
                     <td><select class="form-control year" name="year[]" id="year<?php echo $j; ?>" required="">
                     	<option value="" style="width: 75px;">Select</option>
                    
                     <?php  foreach (range(date('Y'), date('Y')-60) as $year) { ?>
             			<option value="<?php echo $year; ?>" <?php echo $year==$edu['year']?'selected':'' ?>><?php echo $year; ?></option>
                    <?php }  ?> </select>
                    <td width="100"><input type="file"  onchange="pdf_upload(this,<?php echo $j; ?>);" name="certificate[]" id="certificate<?php echo $j; ?>" class="file_width certificate" >
                      <div style="position: relative;">
                    <img class="certificate_img" id="certificate_img<?php echo $j; ?>" src="<?php echo $certificate; ?>" alt='&nbsp;' height="50">
                    <span class="certificate_text" id="certificate_text<?php echo $j; ?>"><?php echo $name; ?></span>

                    <i class="fa fa-times-circle fa-lg delete_btn" id="delete_certificate<?php echo $j; ?>" onclick="delete_images('certificate','<?php echo $edu['certificate']; ?>','<?php echo $j; ?>')"  style="display: <?php echo $edu['certificate']?'block':'none'; ?>;"></i>

                     <input type="hidden" name="certificate[]" id="old_certificate<?php echo $j; ?>" value="<?php echo $edu['certificate']; ?>">  
                      </div>
                    </td>
					         <td><textarea class="form-control description" name="description[]" id="description<?php echo $j; ?>"><?php echo $edu['description']; ?></textarea>   </td>
                     <td class="action_btn"><a href="javascript:;" class="remove" id="remove<?php echo $j; ?>"><i class="fa fa-minus-circle fa-2x"></i></a>&nbsp;&nbsp;
                                 <strong><input type="checkbox" onclick="status_check(<?php echo $j; ?>);"  id="status<?php echo $j; ?>" name="status[]" value="1" <?php echo $edu['status']=='0'?'checked':''; ?>> Hide
                                    <input type="hidden" name="check_status[]"   value="<?php echo $edu['status']=='0'?'1':'0'; ?>" id="check_status<?php echo $j; ?>">
                                 </strong>
                              </td>
                  </tr>  
                  <?php } }else{ ?>               
                  <tr>
                     <td><strong class="slno">1</strong></td>
                     <td><input type="text" value=""   name="institute_name[]" id="institute_name1" class="form-control institute_name" required=""></td>
                     <td><input type="text" value="" name="location[]" id="location1" class="form-control location" required=""></td>
                     <td><input type="text" value="" name="title[]" id="title1" class="form-control title" required=""></td>
                     <td><select class="form-control year" name="year[]" id="year1" required="">
                     	<option value="" style="width: 75px;">Select</option>
                    
                     <?php  foreach (range(date('Y'), date('Y')-60) as $year) { ?>
             			<option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                    <?php }  ?> </select>
                    <td width="100"><input type="file" value=""  onchange="pdf_upload(this,1);" name="certificate[]" id="certificate1" class="file_width certificate" > <img class="certificate_img" id="certificate_img1" src="" alt='&nbsp;' height="50"> 
                    	 <span class="certificate_text" id="certificate_text1"></span>
                     </td>
					<td><textarea class="form-control description" name="description[]" id="description1"></textarea>   </td>
                      <td class="action_btn"><a href="javascript:;" class="remove" id="remove1" style="display: none;"><i class="fa fa-minus-circle fa-2x"></i></a>&nbsp;&nbsp;
                                 <strong><input type="checkbox"  onclick="status_check(1);" class="minimal-red" id="status1" name="status[]" value="1"> Hide
                           <input type="hidden" name="check_status[]" value="0" id="check_status1">
                                 </strong>
                              </td>
                  </tr>
             <?php } ?>
               </tbody>
                              <tfoot>

                  <tr valign="middle" align="center" >
                     <th colspan="3"></th>
                     <th>
                     <center><a href="javascript:;" class="add_more"><i class="fa fa-plus-circle fa-2x" style="cursor: pointer;"> </i></a></center></th>
                      <th colspan="3"></th>
                    
                  </tr>

               </tfoot>
               </table>
<!-- ============================================ -->
					</div>

					<div class="clearfix"></div>

				</div>
			</fieldset>
			<!--=================Session Value=====================--> 

			<!-- ====================================================== -->

			<br>
			<br>

			
		<div class="row" style="text-align: center;">

		<input type="submit" name="save" value="<?php if($_REQUEST['id']!='') { echo 'Update';}else{ echo 'Submit';}?>" class="btn btn-primary bulu" id="myBtn" style="position:relative; right:36px;">

		</div>
		<input type="hidden" name="control" value="user"/>
		<input type="hidden" name="task" value="my_education"/>
		<input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
			<!--account details ends here-->
		</div>
	</form>
</div>
</div>



</section>

<script>
	function goBack() {
		window.history.back();
	}
</script>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>

<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
	$('#accident_date').datetimepicker({
		yearOffset:0,
		lang:'ch',
		timepicker:false,
		format:'d/m/Y',
		formatDate:'Y/m/d',
		scrollMonth : false
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});


	   $('.add_more').click(function(){
   n = $('tbody#add_items tr').length +1;
   // alert(n);
   if(n>50){
   show_error();
   // alert("You Can not add Itmes more than 50");
   return 0;
   }else{
   var html_box = '<tr><td><strong class="slno">'+n+'</strong></td><td><input type="text" value=""   name="institute_name[]" id="institute_name'+n+'" class="form-control institute_name" required=""></td> <td><input type="text" value="" name="location[]" id="location'+n+'" class="form-control location" required=""></td> <td><input type="text" value="" name="title[]" id="title'+n+'" class="form-control title" required=""></td><td><select class="form-control year" name="year[]" id="year'+n+'" required=""><option value="" style="width: 75px;">Select</option><?php  foreach (range(date('Y'), date('Y')-60) as $year) { ?><option value="<?php echo $year; ?>"><?php echo $year; ?></option><?php }  ?> </select> <td width="100"><input type="file" onchange="pdf_upload(this,'+n+');" name="certificate[]" id="certificate'+n+'" class="file_width certificate" ><img class="certificate_img" id="certificate_img'+n+'" src="" alt="&nbsp;" height="50"><span class="certificate_text" id="certificate_text'+n+'"></span> </td><td><textarea class="form-control desc" name="desc[]" id="desc'+n+'"></textarea> </td><td  class="action_btn"><a href="javascript:;" class="remove" id="remove'+n+'"><i class="fa fa-minus-circle fa-2x"></i></a>&nbsp;&nbsp; <strong><input type="checkbox"  class="minimal-red" id="status'+n+'" onclick="status_check('+n+');" name="status[]" value="1"> Hide<input type="hidden" name="check_status[]" value="0" id="check_status'+n+'"></strong></td></tr>';
   $('#add_items').append(html_box);
   $('#total_item').val(n);



   } 


$('.remove').click(function(){
   $(this).closest('tr').remove();

   $('td strong.slno').text(function (i) { return i + 1; });

     $('td input.institute_name').each(function(b){
    $(this).attr('id','institute_name'+parseInt(b+1));
   });
     $('td input.location').each(function(c){
    $(this).attr('id','location'+parseInt(c+1));
   });
     $('td input.title').each(function(d){
    $(this).attr('id','title'+parseInt(d+1));
   });
     $('td input.year').each(function(d){
    $(this).attr('id','year'+parseInt(d+1));
   });
     $('td textarea.disc').each(function(e){
    $(this).attr('id','disc'+parseInt(e+1));
   });

     $('td input.certificate').each(function(f){
    $(this).attr('id','certificate'+parseInt(f+1));
   });
/*if((($('tbody#add_items tr').length)-1) < 1){
	$('.remove').hide();
	}*/

});

});

 function pdf_upload(input,id) {
   
	certificate = '#certificate'+id;
	img = '#certificate_img'+id;
	text = '#certificate_text'+id; 
	value = $(certificate).val();
	name = value.replace(/\\/g, '/').split("/")[2].split('.')[0];
   if (input.files && input.files[0]) {
   var reader = new FileReader();
   reader.onload = function (e) {
   		if(e.target.result.split(":")[1].split(';')[0] == 'application/pdf'){
   $(img).attr('src', 'images/pdf-icon.png');
		}else if(e.target.result.split(":")[1].split('/')[0] == 'image'){
 	$(img).attr('src', e.target.result);
		}
		$(text).html(name);
   }
   reader.readAsDataURL(input.files[0]);
   }
   }
 
/*============Auto hide alert box================*/
$(".alert").delay(2000).slideUp(200, function() {
	$(this).alert('close');
});
<?php if($this->check_edu($_SESSION['adminid'])!=''){ ?>
$('.remove').click(function(){
   $(this).closest('tr').remove();

   $('td strong.slno').text(function (i) { return i + 1; });

     $('td input.institute_name').each(function(b){
    $(this).attr('id','institute_name'+parseInt(b+1));
   });
     $('td input.location').each(function(c){
    $(this).attr('id','location'+parseInt(c+1));
   });
     $('td input.title').each(function(d){
    $(this).attr('id','title'+parseInt(d+1));
   });
     $('td input.year').each(function(d){
    $(this).attr('id','year'+parseInt(d+1));
   });
     $('td textarea.disc').each(function(e){
    $(this).attr('id','disc'+parseInt(e+1));
   });

     $('td input.certificate').each(function(f){
    $(this).attr('id','certificate'+parseInt(f+1));
   });
/*if((($('tbody#add_items tr').length)-1) < 1){
	$('.remove').hide();
	}*/
     
});
<?php } ?>


function status_check(id){
   check_status = '#check_status'+id;
   console.log(check_status);
  
    if($('#status'+id).is(":checked")){
       console.log('#status'+id);
      $(check_status).val('1');
    }else{
       $(check_status).val('0');
    }
}

function delete_images(str,val,id){
//str = thumb, 
var r = confirm("Are you sure?");
if (r == true) {
  $.ajax({url: "script/delete/delete_imgs.php?image_type="+str+"&img_name="+val+"&id="+id, success: function(result){

   
   if(result == 'certificate_deleted'){
       $("#certificate_img"+id).attr('src','');
       $("#certificate_text"+id).html('');
       $("#old_certificate"+id).val('');
       $('#delete_certificate'+id).hide();
       alert('Certificate Succesfully Removed');
   }

    }});
} 
return 0;
}
</script>
