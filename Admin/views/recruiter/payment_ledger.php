<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style>
.tabs .col-md-6.active legend {
   background-color: #a43232;
}
.fix_width{
   padding: unset !important;
}
.fix_width legend{
   font-size: 16px;
   font-weight: 400;
}
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <style type="text/css">

            </style>
            <h3 class="box-title">View Recruiters Payment Ledger</h3>
            <?php foreach($results as $result) { }  ?>
            
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="index.php?control=recruiter&task=recruiter_list"><i class="fa fa-list"></i> Recruiter List</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Payment Ledger</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    
         unset($_SESSION['alertmessage']);
         unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                   <div class="col-md-12 tabs large-screen">
                     <div class="col-md-6 col-xs-12  fix_width"><a href="index.php?control=recruiter&task=commission_list&id=<?php echo $_REQUEST['id']; ?>"><legend> All Commission </legend></a></div>
                     <div class="col-md-6 col-xs-12 active fix_width "><a href="index.php?control=recruiter&task=payment_ledger&id=<?php echo $_REQUEST['id']; ?>"><legend> Released Payment </legend></a></div>
                  </div>
                  <div class="clearfix"></div>

                  <table id="example1-3" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No </div></th>                       
                           <!-- <th><div align="center">Generated On</div></th>          -->    
                           <th><div align="center">Paid on</div></th> 
                           <th><div align="center">Total Commission Amount</div></th>                
                           <th><div align="center">Total Paid Amount</div></th>       
                           <th><div align="center">Remain Amount</div></th>       
                           <th><div align="center">Payment Mode</div></th>       
                           <th><div align="center">Txn No</div></th>         
                           <th><div align="center">Status</div></th>       
                           <!-- <th><div align="center">Action</div></th>       -->
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($uresults) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($uresults as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           $Status = ($result['status']=="2")?("<span style='color:orange;'>Remaining</span>"):("<span style='color:green;'>Completed</span>");

                           $Age = (date('Y') - date('Y',strtotime($result['dob'])));
                           $created = explode(' ',$result['date_created']);
                           $payment_date = explode(' ', $this->lastPayment($result['id'],'payment_date'));
                           ?>
                         
                        <tr>
                           <td align="center"><strong><?php echo $countno; ?>.</strong></td>                
                           <td align="center"><?php echo $result['paid_on'];?></td>          
                           <!-- <td align="center"><?php echo $created[0].'<br>'.$created[1];?></td> -->
                           <td align="center"><i class="fa fa-inr"></i> <?php echo $result['commission_amt'];?></td>           
                           <td align="center"><i class="fa fa-inr"></i> <?php echo $result['paid_amount'];?></td>          
                           <td align="center"><i class="fa fa-inr"></i> <?php echo $result['remain_amount'];?></td>          
                           <td align="center"><?php echo $result['pay_mode'];?></td>          
                           <td align="center"><?php echo $result['txn_no'];?></td>                     
                           <td align="center"><strong><?php echo $Status;?></strong></td>            

                        </tr>
                        <?php 
                         $total_comm += $result['commission_amt'];
                         $total_paid += $result['paid_amount'];
                         $total_remain += $result['remain_amount'];
                     }  } ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <td></td>
                           <td align="center"><strong>Total :</strong></td>
                           <td align="center"><strong><i class="fa fa-inr"></i> <?php echo $total_comm; ?></strong></td>
                           <td align="center"><strong><i class="fa fa-inr"></i> <?php echo $total_paid; ?></strong></td>
                           <td align="center"><strong><i class="fa fa-inr"></i> <?php echo $total_remain; ?></strong></td>
                           <td colspan="2"></td>
                        </tr>
                     </tfoot>                     
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   $('.arrival').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });    
   $('.check_time').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });   
   
   $(".arrival").on("change",function(){
        var selected = $(this).val();
         $('.arrival').val(selected);
    });
   
   $('.dob').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // maxDate:'+1970/01/01',
      scrollMonth : false
   });


$(document).ready(function(){
   $('.popover-box').popover();
});
</script>

