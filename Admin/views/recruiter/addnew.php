<?php foreach($results as $result) { }  ?>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Recruiter</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=recruiter&task=recruiter_list"><i class="fa fa-list" aria-hidden="true"></i> Recruiter List</a></li>
      <?php if($result!='') {?>           
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Recruiter</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Recruiter</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Recruiter ID (Aliasname):
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" maxlength="15" value="<?php echo $result['username']; ?>" id="username" name="username" class="form-control" <?php echo $result['username']?"disabled":"required"; ?>>
                        <span class="msgValid" id="msgpUname"></span>
                  </div>
               </div>
               <!-- <div class="clearfix"></div>                            
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Title:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="title" id="title" class="form-control" autocomplete="off" required="">
                        <option value="">Select</option>
                        <option value="Mr" <?php if($result['title']=='Mr') {?> selected="selected" <?php } ?>>Mr</option>
                        <option value="Ms" <?php if($result['title']=='Ms') {?> selected="selected" <?php } ?>>Ms</option>
                        <option value="Mrs" <?php if($result['title']=='Mrs') {?> selected="selected" <?php } ?>>Mrs</option>
                     </select>
                     <span class="msgValid" id="msgtitle"></span>
                  </div>
               </div> -->
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Full Name:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text"  value="<?php echo $result['name']; ?>" id="name" name="name" class="form-control" required="">
                  </div>
               </div>
              <!--  <div class="clearfix"></div>
              <div class="col-md-4">
                 <div class="form-group center_text">
                    Gender:
                 </div>
              </div>
              <div class="col-md-6">
                 <div class="form-group">
                    <select name="gender" id="gender" class="form-control" autocomplete="off" required="">
                       <option value="">Select</option>
                       <option value="Male" <?php if($result['gender']=='Male') {?> selected="selected" <?php } ?>>Male</option>
                       <option value="Female" <?php if($result['gender']=='Female') {?> selected="selected" <?php } ?>>Female</option>
                    </select>
                 </div>
              </div> -->
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Email-Id: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                
                     <input type="text"  value="<?php echo $result['email']; ?>" id="email" name="email" class="form-control" <?php echo $result['email']?"disabled":"required"; ?>>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Contact (Optional): 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                
                     <input type="text"  value="<?php echo $result['mobile']; ?>" maxlength="12" id="mobile" name="mobile" class="form-control">
                  </div>
               </div>
               <div class="clearfix"></div> 
               <div class="col-md-4">
                  <div class="form-group center_text">
                    Company starting date: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                              
                     <input type="text"  value="<?php echo $result['dob']; ?>" id="dob" name="dob"  class="form-control" readonly>
                  </div>
               </div>
               <div class="clearfix"></div>
               <!-- <div class="col-md-4">
                  <div class="form-group center_text">
                     Country Name:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="country_id" id="country_id" class="form-control" onchange='state_change(this.value);' >
                        <option value="">Select Country</option>
                        <?php /*$sel=mysql_query("select * from country where 1");
                           while($select=mysql_fetch_array($sel))
                           {
                           if($select['status'] == '1') { ?>
                        <option value="<?php echo $select['id'];?>"<?php if($result['country_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['country_name'];?></option>
                        <?php } else { ?>  
                        <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['country_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['country_name'];?></option>
                        <?php } }*/ ?>
                     </select>
                     <span id="msgcountry_id" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     State Name:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="state_id" id="state_id" class="form-control" onchange='city_change(this.value);' >
                        <option value="">Select State</option>
                        <?php /*$sel=mysql_query("select * from state where 1");
                           while($select=mysql_fetch_array($sel))
                           if($result)  {
                           if($select['status'] == '1') { ?>
                        <option value="<?php echo $select['id'];?>"<?php if($result['state_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>
                        <?php } else { ?>  
                        <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['state_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['state_name'];?></option>
                        <?php } }*/ ?>
                     </select>
                     <span id="msgstate_id" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     City Name:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="city_id" id="city_id" class="form-control" >
                        <option value="">Select City</option>
                        <?php /*$sel=mysql_query("select * from city where 1");
                           while($select=mysql_fetch_array($sel))
                           if($result)  {
                           if($select['status'] == '1') { ?>
                        <option value="<?php echo $select['id'];?>"<?php if($result['city_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['city_name'];?></option>
                        <?php } else { ?>  
                        <option disabled="disabled" value="<?php echo $select['id'];?>"<?php if($result['city_id']==$select['id']) { ?> selected="selected" <?php } ?>><?php echo $select['city_name'];?></option>
                        <?php } }*/ ?>
                     </select>
                     <span id="msgcity_id" style="color:red;"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Address:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <textarea id="address" name="address" class="form-control" ><?php echo $result['address']; ?></textarea> 
                     <span class="msgValid" id="msgaddress"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Pincode: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">                              
                     <input type="text"  value="<?php echo $result['pincode']; ?>" id="pincode" name="pincode" class="form-control" >
                     <span class="msgValid" id="msgpincode"></span>
                  </div>
               </div>
               <div class="clearfix"></div> -->
               <div class="clearfix"></div>
               <div id="dealer_comm">
                  <div class="col-md-4">
                     <div class="form-group center_text">
                        Commission (%): 
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">                              
                        <input type="text" maxlength="2" value="<?php echo $result['commision']; ?>" id="commision" name="commision" class="form-control" >
                        <span class="msgValid" id="msgpincode"></span>
                     </div>
                  </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Password:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="password" value="<?php echo $result['password']; ?>" id="password" name="password" class="form-control" required="">
                  </div>
               </div>
               <div class="clearfix"></div> 
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Confirm Password:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="password"  value="<?php echo $result['password']; ?>" id="c_password" name="c_password" class="form-control" required="">
                     <span class="msgValid" id="msgPass"></span>
                  </div>
               </div>
                 <!--  <div class="clearfix"></div>
                 <div class="col-md-4">
                    <div class="form-group center_text">
                       Commission Amount: 
                    </div>
                 </div>
                 <div class="col-md-6">
                    <div class="form-group">                              
                       <input type="text"  value="<?php echo $result['comm_amount']; ?>" id="comm_amount" name="comm_amount" class="form-control" >
                       
                    </div>
                 </div>
                                </div> -->
               <div class="clearfix"></div>
               <!-- <div class="col-md-4">
                  <div class="form-group center_text">
                     Photo: 
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">  
                     <?php if($result['photo']!='') { ?>  
                     <a href="media/farmer/<?php echo $result['photo'];?>" target="_blank" >
                     <img onerror="this.onerror=null;this.src='images/pdf.png'" src="<?php if($result['photo']) { ?>media/farmer/<?php echo $result['photo'];  } else { echo "images/profile.jpg";}?>" width="70" /></a>            
                     <input type="file" value="<?php echo $result['photo']; ?>" id="photo" name="photo">
                     <?php } else { ?>
                     <input type="file"  value="<?php echo $result['photo']; ?>" id="photo" name="photo" >
                     <a target="_blank" href="media/farmer/<?php echo $result['photo']; ?>" id="photo_a"><img id="photo_img" src="media/farmer/<?php echo $result['photo']; ?>" alt="" height="50" /></a>
                     <?php } ?>
                     <span class="msgValid" id="msgphoto"></span>
                  </div>
               </div>
               <div class="clearfix"></div> -->
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" id="submit-btn" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
               </div>
               <input type="hidden" name="control" value="recruiter"/>
               <input type="hidden" name="edit" value="1"/>
               <input type="hidden" name="task" value="save"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script>
   $('#dob').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d-m-Y',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });
   
   
   /*=============Gsingh Jquery==============*/
   function id_proof(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
               $('#id_proof_img').attr('src', e.target.result);
               $('#id_proof_a').attr('href', e.target.result);
           }
           reader.readAsDataURL(input.files[0]);
           }
       }
   $("#id_proof").change(function(){
       id_proof(this);
   });
   
   
   function photo(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
               $('#photo_img').attr('src', e.target.result);
               $('#photo_a').attr('href', e.target.result);
           }
           reader.readAsDataURL(input.files[0]);
           }
       }
   $("#photo").change(function(){
       photo(this);
   });
   function isNumber(evt) {
       evt = (evt) ? evt : window.event;
           var charCode = (evt.which) ? evt.which : event.keyCode;
       if (charCode != 46 && charCode > 31
       && (charCode < 48 || charCode > 57))
           return false;
   
       return true;
   }
   
   
   /*==========Commission==============*/
   
   function get_commission(){
   
       var amount = $('#amount').val();
       var percent = $('#comm_per').val();
       var getcomm =  parseInt((amount*percent)/100);
   
       $('#comm_amount').val(getcomm);
   }
   $('#comm_per').keyup(function(){
       get_commission();
   });
   
    /*============Auto hide alert box================*/
    $(".alert").delay(2000).slideUp(200, function() {
      $(this).alert('close');
    });
   
   
/*==Ckh Password==*/
   $('#c_password').keyup(function(){
      password = $('#password').val();
      c_password = $(this).val();
      if(username.length<1){
          $('#msgPass').text('');
      }else{
          if(password != c_password){
            txt = "Password Didn't match, Please try again!!!";
            $("#msgPass").text(txt).css('color','red');
            $("#submit-btn").prop('disabled',true);
          }else{
            txt = "Password matched, Go ahead!!!";
            $("#msgPass").text(txt).css('color','green');
            $("#submit-btn").prop('disabled',false);
          }
    }
   });


   $('#username').blur(function(){
      username = $(this).val();
      utype = "recruiter";
      if(username.length<1){

        $('#msgpUname').text('');

      }else{

        $.ajax({url: "script/checking.php?name="+username+"&utype="+utype, success: function(result){
          if(result>0){
            txt = "Aliasname Not Available";
              $('#msgpUname').text(txt).css('color','red');
              $("#submit-btn").prop('disabled',true);
          }else{
            txt = "Aliasname Available, Go ahead!!!";
              $('#msgpUname').text(txt).css('color','green');
              $("#submit-btn").prop('disabled',false);
          }

     }
});
}
   });
   $('#email').blur(function(){
      username = $(this).val();
      utype = "recruiter";
      if(username.length<1){
        
        $('#msgpEmail').text('');

      }else{

        $.ajax({url: "script/checking.php?name="+username+"&utype="+utype, success: function(result){
          if(result>0){
            txt = "Email-Id Not Available";
              $('#msgpEmail').text(txt).css('color','red');
              $("#submit-btn").prop('disabled',true);
          }else{
            txt = "Email-Id Available, Go ahead!!!";
              $('#msgpEmail').text(txt).css('color','green');
              $("#submit-btn").prop('disabled',false);
          }

     }
});
}
   });
</script>

