<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style>
.tabs .col-md-6.active legend {
   background-color: #a43232;
}
.fix_width{
   padding: unset !important;
}
.fix_width legend{
   font-size: 16px;
   font-weight: 400;
}
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <style type="text/css">

            </style>
            <h3 class="box-title">View Recruiters</h3>
            <?php foreach($results as $result) { }  ?>
             <a href="index.php?control=recruiter&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Recruiter</a> 
             <!-- <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total JobSeeker : <?php echo $no_of_row; ?>
            </p>  -->
            <!-- <a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_paidUsers.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;height: 100%;" /></a>                -->
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Recruiter List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    
         unset($_SESSION['alertmessage']);
         unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <!-- <div class="col-md-12 tabs large-screen">
                     <div class="col-md-6 col-xs-12 active fix_width"><a href="index.php?control=jobseeker&task=paid_list"><legend> Paid Job Seeker</legend></a></div>
                     <div class="col-md-6 col-xs-12 fix_width "><a href="index.php?control=jobseeker&task=unpaid_list"><legend> Un-Paid Job Seeker</legend></a></div>
                  </div> -->
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No </div></th>        
                           <th><div align="center">Alias Name</div></th>                        
                           <th><div align="center">Joining Date</div></th>                           
                           <th><div align="center">Email / Contact</div></th>                              
                           <th><div align="center">City</div></th>                              
                           <th><div align="center">Paid User</div></th>                              
                           <th><div align="center">Commission</div></th>                              
                           <th><div align="center">Status</div></th>       
                           <th><div align="center">Action</div></th>      
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           $Status = ($result['status']=="1")?("<span style='color:green;'>Active</span>"):("<span style='color:red;'>In-Active</span>");

                           $Age = (date('Y') - date('Y',strtotime($result['dob'])));
                           $join_date = explode(' ',$result['date_time']);
                           $payment_date = explode(' ', $this->lastPayment($result['id'],'payment_date'));
                         
                           ?>
                        <tr>
                           <td align="center"><strong><?php echo $countno; ?>.</strong></td>
                           <td align="center">
                           <span class="popover-box" data-content="
                           <table class='table table-striped table-bordered'>
                              <tr><td>Full Name</td><td>:</td><td><?php echo $result['name']; ?></td></tr>
                              <tr><td>Company Age</td><td>:</td><td><?php echo $Age; ?></td></tr>
                           </table>
                           " data-placement="right" data-html="true" data-original-title="<center style='color:#000;'>Recruiter Detail</center>" data-trigger="hover">                 
                           <strong><?php echo $result['username'];?></strong>
                           </span>
                           </td>   
                           <td align="center"><?php echo $join_date[0].'<br>'.$join_date[1];?></td>           
                           <td align="center">
                               <!-- <span class="popover-box" data-content="<?php echo $result['mobile']; ?>" data-placement="top" data-html="true" data-original-title="<center style='color:#000;'><i class='fa fa-phone'></i> Contact </center>" data-trigger="hover"></span> -->
                              <?php echo $result['email']."<br>".($result['mobile']?$result['mobile']:"N/A");?></td> 
                           <td align="center"><span class="popover-box" data-content="<?php echo $result['postalcode']?$result['postalcode']:'N/A'; ?>" data-placement="top" data-html="true" data-original-title="<center style='color:#000;'><i class='fa fa-map-marker'></i> Pin Code </center>" data-trigger="hover"><?php echo $result['city_id']?($this->city_name($result['city_id'])):"N/A";?></span></td> 
                           <td align="center"><?php echo $this->total_user($result['id'],'1');?></td> 
                           <td align="center"><span class="popover-box" data-content="<table class='table table-striped table-bordered'>
                              <tr><td>Total Commission</td><td>:</td><td><i class='fa fa-inr'></i> <?php echo $this->payment_detail($result['id'],'commission_amt','recruiter_commission'); ?></td></tr>
                              <tr><td>Total Paid</td><td>:</td><td><i class='fa fa-inr'></i> <?php echo $this->payment_detail($result['id'],'paid_amount','recruiter_ledger'); ?></td></tr></tr>
                           </table>" data-placement="top" data-html="true" data-original-title="<center style='color:#000;'><i class='fa fa-money'></i> Payment Detail </center>" data-trigger="hover"><?php echo $result['commision']?$result['commision']." %":"N/A";?></span></td> 
                            
                           <td align="center"><strong><?php echo $Status;?></strong></td>            
                           <td align="center">
                              <div class="btn-group-horizontal" style="width: 130px;">
                                 <a class="btn btn-primary" href="javascript:;" title="View Detail" data-toggle="modal" data-target="#view_detail<?php echo $i; ?>" id="calender_popup"><b style="color: #fff;"><i class="fa fa-eye" aria-hidden="true"></i></b></a> 
                              <?php
                                 if($result['status']==1){  ?>
                              <a class="btn btn-danger" href="index.php?control=recruiter&task=status&status=0&id=<?php echo $result['id']; ?>" onclick="return confirm('Are you sure you want to Inactivate ?')" style="cursor:pointer;" title="Click to Inactive"><b style="color: #fff;"><i class="fa fa-ban" aria-hidden="true"></i></a>
                              <?php } else { ?>
                              <a class="btn btn-success" href="index.php?control=recruiter&task=status&status=1&id=<?php echo $result['id']; ?>" onclick="return confirm('Are you sure you want to Activate ?')" style="cursor:pointer;" title="Click to Active"><b style="color: #fff;"><i class="fa fa-check-square-o" aria-hidden="true"></i></b></a>
                              <?php } ?> 
                              <a class="btn btn-info" href="index.php?control=recruiter&task=addnew&id=<?php echo $result['id']; ?>" title="Edit"><b style="color: #fff;"><i class="fa fa-pencil-square" aria-hidden="true"></i></b></a> 
                              
                           </div>
<!-- ======Model For Action======== -->
<div id="view_detail<?php echo $i; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h4 class="modal-title"><?php echo $result['name']; ?></h4></center>
      </div>
        <div class="modal-body text-left" id="">
         <div class="col-lg-6 col-md-6 col-xs-12">
           <!-- small box -->
           <a href="index.php?control=recruiter&task=user_list&id=<?php echo $result['id']; ?>">
           <div class="small-box bg-blue">
            <div class="inner">
                <h3><?php echo $this->total_user($result['id'],''); ?></h3>
                <p>Total Registered Users</p>
              </div>
              <div class="icon">
                <i class="fa fa-users"></i>
              </div>
              <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
            </div>
         </a>
         </div> 
         <div class="col-lg-6 col-md-6 col-xs-12">
           <!-- small box -->
           <a href="index.php?control=recruiter&task=commission_list&id=<?php echo $result['id']; ?>">
           <div class="small-box bg-green">
            <div class="inner">
             <h3><i class="fa fa-inr"></i> <?php echo $this->payment_detail($result['id'],'commission_amt','recruiter_commission'); ?></h3>
             <p>Total Commissions</p>
           </div>
           <div class="icon">
             <i class="fa fa-money"></i>
           </div>
           <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
         </div>
      </a>
       </div>   
    <div class="clearfix"></div>    
        </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- ============================== -->

                        </td>

                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   $('.arrival').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });    
   $('.check_time').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });   
   
   $(".arrival").on("change",function(){
        var selected = $(this).val();
         $('.arrival').val(selected);
    });
   
   $('.dob').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // maxDate:'+1970/01/01',
      scrollMonth : false
   });


$(document).ready(function(){
   $('.popover-box').popover();
});
</script>

