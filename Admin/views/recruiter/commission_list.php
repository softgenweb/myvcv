<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style>
.tabs .col-md-6.active legend {
   background-color: #a43232;
}
.fix_width{
   padding: unset !important;
}
.fix_width legend{
   font-size: 16px;
   font-weight: 400;
}
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <style type="text/css">

            </style>
            <h3 class="box-title">View Recruiters Payment Ledger</h3>
            <?php foreach($results as $result) { }  ?>
            <a class="btn btn-primary bulu pull-right" href="javascript:;" title="View Detail" data-toggle="modal" data-target="#view_detail" ><b style="color: #fff;"><i class="fa fa-money"></i> Add Payment</b></a> 
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="index.php?control=recruiter&task=recruiter_list"><i class="fa fa-list"></i> Recruiter List</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Payment Ledger</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    
         unset($_SESSION['alertmessage']);
         unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                   <div class="col-md-12 tabs large-screen">
                     <div class="col-md-6 col-xs-12 active fix_width"><a href="index.php?control=recruiter&task=commission_list&id=<?php echo $_REQUEST['id']; ?>"><legend> All Commission </legend></a></div>
                     <div class="col-md-6 col-xs-12 fix_width "><a href="index.php?control=recruiter&task=payment_ledger&id=<?php echo $_REQUEST['id']; ?>"><legend> Released Payment </legend></a></div>
                  </div>
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No </div></th>                       
                           <th><div align="center">Generated On</div></th>        
                           <th><div align="center">Commission Amount</div></th>
                           <th><div align="center">Status</div></th>       
                           <th><div align="center">Action</div></th>      
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";

                              $paid_commission = explode(',',$this->comm_ids($result['user_id']));
                              $Status = (in_array($result['id'],$paid_commission) == false)?("<span style='color:red;'>Not-Paid</span>"):("<span style='color:green;'>Paid</span>");

                              $Age = (date('Y') - date('Y',strtotime($result['dob'])));
                              $created = explode(' ',$result['date_created']);
                              $payment_date = explode(' ', $this->lastPayment($result['id'],'payment_date'));

                              $old_remain_amount = $this->payment_detail($result['user_id'],'remain_amount','recruiter_ledger'); 
                                                    
                           ?>
                        <tr>
                           <td align="center"><strong><?php echo $countno; ?>.</strong></td>        
                           <td align="center"><?php echo $created[0].'<br>'.$created[1];?></td>
                           <td align="center"><i class="fa fa-inr"></i> <?php echo $result['commission_amt'];?></td>            
                           <td align="center"><strong><?php echo $Status;?></strong></td>            
                           <td align="center">
                              <div class="btn-group-horizontal" style="width: 130px;">
                              <?php if(!in_array($result['id'],$paid_commission)){ ?>                                 
                              <label class="checkbox-container">
                                 <input type="checkbox" id="chk_amout<?php echo $result['id']; ?>" name="chk_amout" onclick="selectChkbox(<?php echo $result['id']; ?>,<?php echo $result['commission_amt'];?>);">
                                 <span class="checkmark"></span>
                              </label>
                              <?php }else{
                                  echo $Status;
                              } ?>
                              <!-- <a class="btn btn-info" href="index.php?control=recruiter&task=addnew&id=<?php echo $result['id']; ?>" title="Edit"><b style="color: #fff;"><i class="fa fa-pencil-square" aria-hidden="true"></i></b></a>  -->
                              
                           </div>


                        </td>

                        </tr>
                        <?php  $total_comm += $result['commission_amt']; } } ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <td></td>
                           <td align="center"><strong>Total :</strong></td>
                           <td align="center"><strong><i class="fa fa-inr"></i> <?php echo $total_comm; ?></strong></td>
                           <td colspan="2"></td>
                        </tr>
                     </tfoot>
                  </table>
<div id="view_detail" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <center><h4 class="modal-title">Add Payment Detail</h4></center>
      </div>
        <div class="modal-body text-left" id="">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-sm-8 col-xs-12 col-sm-offset-2">
               <div class="col-md-5">
                  <div class="form-group center_text">
                     Total Commission Amount:
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">
                     <input type="text" maxlength="15" value="<?php echo $total_comm; ?>" id="total_comm" name="total_comm" class="form-control" readonly="">
                  </div>
               </div>
               <div class="clearfix"></div>                            
               <div class="col-md-5">
                  <div class="form-group center_text">
                     Total Selected Amount:
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">
                    <input type="text" maxlength="15" value="" id="selected_amount" name="commission_amt" class="form-control" readonly="">
                     <span class="msgValid" id="msgtitle"></span>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-5">
                  <div class="form-group center_text">
                     Amount to be Paid:
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">
                     <input type="text"  value="" id="paid_amount" name="paid_amount" class="form-control" required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-5">
                  <div class="form-group center_text">
                     Remain Amount:
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">
                     <input type="text"  value="" id="remain_amount" name="remain_amount" class="form-control" readonly="">
                     <input type="hidden"  value="" id="old_remain_amount" name="old_remain_amount" >
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-5">
                  <div class="form-group center_text">
                     Paid on:
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">                
                     <input type="text"  value="" id="paid_on" name="paid_on" class="form-control" readonly="">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-5">
                  <div class="form-group center_text">
                     Bank Name:
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">
                      <input type="text"  value="" id="bank_name" name="bank_name" class="form-control" required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-5">
                  <div class="form-group center_text">
                     Txn No. : 
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">                
                      <input type="text"  value="" id="txn_no" name="txn_no" class="form-control" required="">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-5">
                  <div class="form-group center_text">
                     Payment Mode: 
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">                
                      <select name="pay_mode" id="pay_mode" class="form-control" autocomplete="off" required="">
                        <option value="">Select</option>
                        <option value="RTGS" <?php if($result['pay_mode']=='RTGS') {?> selected="selected" <?php } ?>>RTGS</option>
                        <option value="NEFT" <?php if($result['pay_mode']=='NEFT') {?> selected="selected" <?php } ?>>NEFT</option>
                        <!-- <option value="Mrs" <?php if($result['pay_mode']=='Mrs') {?> selected="selected" <?php } ?>>Mrs</option> -->
                     </select>                    
                  </div>
               </div>
               <div class="clearfix"></div>
                 <div id="ids" style="display: none;"></div>
             </div>
        </div>
         <!--account details ends here-->
    
         <div class="clearfix"></div>    
        </div>
       <div class="modal-footer">
                       <div class="col-md-12 col-xs-12">
                  <center><input type="submit" id="submit-btn" name="submit" class="btn btn-primary butoon_brow" value="Submit"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button></center>
                
               </div>
               <input type="hidden" name="control" value="recruiter"/>
               <input type="hidden" name="user_id" value="<?php echo  $results[0]['user_id'] ?>"/>
               <input type="hidden" name="task" value="pay_commission"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />

        
          </form>
      </div>
    </div>

  </div>
</div>                  
                  <div class="clearfix"></div>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->

</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   $('#paid_on').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });    
   $('.check_time').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });   
   
   $(".arrival").on("change",function(){
        var selected = $(this).val();
         $('.arrival').val(selected);
    });
   
   $('.dob').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // maxDate:'+1970/01/01',
      scrollMonth : false
   });


$(document).ready(function(){
   $('.popover-box').popover();
});

function selectChkbox(id,amount){

   if($('#chk_amout'+id).is(':checked') == true){
      $("#ids").append('<input type="text" id="chk-'+id+'" value="'+id+'" name="comm_ids[]">');     
   }else if($('#chk_amout'+id).is(':checked') == false){
      $('#chk-'+id).remove();
   }

   total_comm = 0;
   $('input:checkbox:checked').each(function(){
      total_comm += amount;
   }); 
   $("#selected_amount").val(total_comm); 

}

$('#paid_amount').keyup(function(){
   var paid_amount = $(this).val()?$(this).val():0;
   var total_comm = $("#selected_amount").val();
   remain_amount = parseInt(total_comm) - parseInt(paid_amount);
   $("#remain_amount").val(remain_amount);
});


</script>

