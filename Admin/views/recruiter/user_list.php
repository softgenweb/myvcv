<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style>
.tabs .col-md-6.active legend {
   background-color: #a43232;
}
.fix_width{
   padding: unset !important;
}
.fix_width legend{
   font-size: 16px;
   font-weight: 400;
}
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <style type="text/css">

            </style>
            <h3 class="box-title">View Paid JobSeekers</h3>
            <?php foreach($results as $result) { }  ?>
             <!-- <a href="index.php?control=room_master&task=addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Room</a>  -->
             <!-- <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total JobSeeker : <?php echo $no_of_row; ?>
            </p>  -->
            <!-- <a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_paidUsers.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;height: 100%;" /></a>                -->
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <?php if($_SESSION['utype']=="Administrator"){ ?>
            <li><a href="index.php?control=recruiter&task=recruiter_list"><i class="fa fa-list"></i> Recruiter List</a></li>
         <?php } ?>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> JobSeeker List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    
         unset($_SESSION['alertmessage']);
         unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <!-- <div class="col-md-12 tabs large-screen">
                     <div class="col-md-6 col-xs-12 active fix_width"><a href="index.php?control=jobseeker&task=paid_list"><legend> Paid Job Seeker</legend></a></div>
                     <div class="col-md-6 col-xs-12 fix_width "><a href="index.php?control=jobseeker&task=unpaid_list"><legend> Un-Paid Job Seeker</legend></a></div>
                  </div> -->
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No </div></th>        
                           <th><div align="center">Alias Name</div></th>                        
                           <th><div align="center">Joining Date</div></th>                           
                           <th><div align="center">Conatct</div></th>                              
                           <th><div align="center">Status</div></th>    
                           <th><div align="center">Promocode</div></th>    
                           <!-- <th><div align="center">Recruiter</div></th>      -->
                           <th><div align="center">Payment Date</div></th>     
                           <th><div align="center">Action</div></th>      
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           $Status = ($result['status']=="1")?("<span style='color:green;'>Active</span>"):("<span style='color:red;'>In-Active</span>");

                           $Age = (date('Y') - date('Y',strtotime($result['dob'])));
                           $join_date = explode(' ',$result['date_time']);
                           $payment_date = explode(' ', $this->lastPayment($result['id'],'payment_date'));
                         
                           ?>
                        <tr>
                           <td align="center"><strong><?php echo $countno; ?>.</strong></td>
                           <td align="center">
                           <span class="popover-box" data-content="
                           <table class='table table-striped table-bordered'>
                              <tr><td>Full Name</td><td>:</td><td><?php echo $result['name']; ?></td></tr>
                              <tr><td>Gender</td><td>:</td><td><?php echo $result['gender']?$result['gender']:'N/A'; ?></td></tr>
                              <tr><td>Age</td><td>:</td><td><?php echo $Age; ?></td></tr>
                           </table>
                           " data-placement="right" data-html="true" data-original-title="<center style='color:#000;'>Job Seeker Detail</center>" data-trigger="hover">                 
                           <strong><?php echo $result['username'];?></strong>
                           </span>
                           </td>   
                           <td align="center"><?php echo $join_date[0].'<br>'.$join_date[1];?></td>           
                           <td align="center"><?php echo $result['mobile'];?></td>   
                           <td align="center"><strong><?php echo $Status;?></strong></td>            
                           <td align="center"><?php echo $result['coupon']?$result['coupon']:'N/A';?></td>     
                           <!-- <td align="center"><?php //echo ($result['recruiter_id']>0)?($this->user_name($result['recruiter_id'])):'N/A';?></td>      -->
                           <td align="center">                
                              <strong><?php echo $this->lastPayment($result['id'],'amount');?></strong>
                             </td>

                           <td align="center">
                              <div class="btn-group-horizontal" style="width: auto;">
                              <a class="btn btn-info" href="http://www.<?php echo $result['username']; ?>.myvcv.asia/" title="View Profile" target="_blank"><b style="color: #fff;"><i class="fa fa-user" aria-hidden="true"></i></b></a> 
                              <?php if($_SESSION['utype']=="Administrator"){ ?>
                              <a class="btn btn-success" href="index.php?control=recruiter&task=edit_details&user_id=<?php echo $result['id']; ?>" title="Edit"><b style="color: #fff;"><i class="fa fa-pencil-square" aria-hidden="true"></i></b></a> 
                                <?php
                                 if($result['status']==1){  ?>
                              <a class="btn btn-danger" href="index.php?control=recruiter&task=jobseeker_status&status=0&id=<?php echo $result['id']; ?>&recr_id=<?php echo $result['recruiter_id']; ?>" onclick="return confirm('Are you sure you want to Inactivate ?')" style="cursor:pointer;" title="Click to Inactive"><b style="color: #fff;"><i class="fa fa-ban" aria-hidden="true"></i></a>
                              <?php } else { ?>
                              <a class="btn btn-success" href="index.php?control=recruiter&task=jobseeker_status&status=1&id=<?php echo $result['id']; ?>&recr_id=<?php echo $result['recruiter_id']; ?>" onclick="return confirm('Are you sure you want to Activate ?')" style="cursor:pointer;" title="Click to Active"><b style="color: #fff;"><i class="fa fa-check-square-o" aria-hidden="true"></i></b></a>
                              <?php } }?>
                              <!-- <a class="btn btn-danger" href="javascript:;" title="Delete"><b style="color: #fff;"><i class="fa fa-trash" aria-hidden="true"></i></b></a>  -->
                           </div>


                        </td>

                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   $('.arrival').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });    
   $('.check_time').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });   
   
   $(".arrival").on("change",function(){
        var selected = $(this).val();
         $('.arrival').val(selected);
    });
   
   $('.dob').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // maxDate:'+1970/01/01',
      scrollMonth : false
   });


$(document).ready(function(){
   $('.popover-box').popover();
});
</script>

