<?php $_REQUEST['tpages'] = $_REQUEST['tpages']?$_REQUEST['tpages']:PERPAGE;?>
<style>
.tabs .col-md-6.active legend {
   background-color: #a43232;
}
.fix_width{
   padding: unset !important;
}
.fix_width legend{
   font-size: 16px;
   font-weight: 400;
}
</style>
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="box-header">
            <style type="text/css">

            </style>
            <h3 class="box-title">Coupon List</h3>
            <?php foreach($results as $result) { }  ?>
             <a href="index.php?control=master&task=promocode_addnew" class="btn btn-primary bulu" style="float:right; margin-left:5px;"><i class="fa fa-plus-circle"></i> Add Coupon Code</a> 
             <!-- <p class="btn btn-primary bulu" style="float:right;font-size:14px;">
               Total JobSeeker : <?php echo $no_of_row; ?>
            </p>  -->
            <a href="javascript:void(0);" onclick="window.open('excel/exportToexcel_paidUsers.php');" ><img src="images/excel.jpg" alt="Export To Excel" title="Export To Excel"  style="float: right;height: 100%;" /></a>               
         </div>
         <!-- /.box-header -->
         <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Coupon Code List</li>
         </ol>
         <?php if(isset($_SESSION['alertmessage'])){?>
         <div class="box-body">
            <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
            </div>
         </div>
         <?php    
         unset($_SESSION['alertmessage']);
         unset($_SESSION['errorclass']);    
            }?>
         <div class="box-body">
            <div>
               <div class="divoverflow">
                  <table id="example1-1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="15"><div align="center">S.No </div></th>        
                           <th><div align="center">Coupon Code</div></th>                        
                           <th><div align="center">Recruiter</div></th>                        
                           <th><div align="center">Discount (%)</div></th>                           
                           <th><div align="center">Discounted Amount</div></th>                              
                           <th><div align="center">Status</div></th>       
                           <th><div align="center">Action</div></th>      
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           if($results) {
                               $countno = ($page-1)*$tpages;
                               $i=0;
                               foreach($results as $result){ 
                               $i++;
                               $countno++;
                           
                           ($i%2==0)? $class="tr_line2 grd_pad" : $class="tr_line1 grd_pad";
                           $Status = ($result['status']=="1")?("<span style='color:green;'>Active</span>"):("<span style='color:red;'>In-Active</span>");

                           $join_date = explode(' ',$result['date_time']);
                         
                         
                           ?>
                        <tr>
                           <td align="center"><strong><?php echo $countno; ?>.</strong></td>
                           <td align="center"><strong><?php echo $result['promocode'];?></strong></td>   
                           <td align="center"><strong><?php echo $this->userDetail($result['recruiter_id'],'name').'('.$this->userDetail($result['recruiter_id'],'username').')';?></strong></td>   
                           <td align="center"><?php echo $result['discount'];?> %</td>           
                           <td align="center"><i class="fa fa-inr"></i> <?php echo $result['amount'];?></td> 
                            
                           <td align="center"><strong><?php echo $Status;?></strong></td>            
                           <td align="center">
                              <div class="btn-group-horizontal" style="width: 120px;">
                              <?php
                                 if($result['status']==1){  ?>
                              <a class="btn btn-danger" href="index.php?control=master&task=promocode_status&status=0&id=<?php echo $result['id']; ?>" onclick="return confirm('Are you sure you want to Inactivate ?')" style="cursor:pointer;" title="Click to Inactive"><b style="color: #fff;"><i class="fa fa-ban" aria-hidden="true"></i></a>
                              <?php } else { ?>
                              <a class="btn btn-success" href="index.php?control=master&task=promocode_status&status=1&id=<?php echo $result['id']; ?>" onclick="return confirm('Are you sure you want to Activate ?')" style="cursor:pointer;" title="Click to Active"><b style="color: #fff;"><i class="fa fa-check-square-o" aria-hidden="true"></i></b></a>
                              <?php } ?> 
                              <a class="btn btn-info" href="index.php?control=master&task=promocode_addnew&id=<?php echo $result['id']; ?>" title="Edit"><b style="color: #fff;"><i class="fa fa-pencil-square" aria-hidden="true"></i></b></a> 
                              <!-- <a class="btn btn-danger" href="javascript:;" title="Delete"><b style="color: #fff;"><i class="fa fa-trash" aria-hidden="true"></i></b></a>  -->
                           </div>


                        </td>

                        </tr>
                        <?php }  }else{?>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
            <!-- table-responsive -->
         </div>
         <!-- /.box-body -->
      </div>
      <!-- /.box -->
   </div>
   <!-- /.col -->
   <!--================ Second Table ================-->
</div>
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script>
<script>
   /*============Auto hide alert box================*/
   $(".alert").delay(2000).slideUp(200, function() {
    $(this).alert('close');
   });
   
   $('.arrival').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });    
   $('.check_time').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      minuteStep: 30,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // minDate:'+1970/01/01',
      scrollMonth : false
   });   
   
   $(".arrival").on("change",function(){
        var selected = $(this).val();
         $('.arrival').val(selected);
    });
   
   $('.dob').datetimepicker({
      yearOffset:0,
      lang:'ch',
      timepicker:false,
      format:'Y-m-d',
      formatDate:'Y-m-d',
      // maxDate:'+1970/01/01',
      scrollMonth : false
   });


$(document).ready(function(){
   $('.popover-box').popover();
});
</script>

