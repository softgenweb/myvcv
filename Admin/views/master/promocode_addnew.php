<?php foreach($results as $result) { }  ?>
<style>
   #select2-recruiter_id-container {
    padding: unset !important;
  }
</style>
<div class="panel panel-default" >
   <div class="box-header">
      <h3 class="box-title">Add Coupon Code</h3>
   </div>
   <ol class="breadcrumb">
      <li><a href="index.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li><a href="index.php?control=master&task=promocode_show"><i class="fa fa-list" aria-hidden="true"></i> Coupon Code List</a></li>
      <?php if($result!='') {?>           
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Edit Coupon Code</li>
      <?php } else { ?>
      <li class="active"><i class="fa fa-list" aria-hidden="true"></i> Add Coupon Code</li>
      <?php } ?>
   </ol>
   <?php if(isset($_SESSION['alertmessage'])){?>
   <div class="box-body">
      <div class="alert alert-<?php echo $_SESSION['errorclass'];?> alert-dismissable">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-<?php if($_SESSION['errorclass']=='success'){ echo 'check'; }else{ echo 'ban';}?>"></i> Alert!  <?php echo $_SESSION['alertmessage']; ?></h4>
      </div>
   </div>
   <?php    unset($_SESSION['alertmessage']);
      unset($_SESSION['errorclass']);    
      }?>
   <div class="panel-body">
      <form name="form" method="post" enctype="multipart/form-data" onsubmit="return validation();" autocomplete="off" >
         <div class="row col-md-12">
            <div class="col-md-6 col-sm-9 col-xs-12 col-md-offset-3">
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Coupon Code Name:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text"  value="<?php echo $result['promocode']; ?>" id="promocode" name="promocode" class="form-control" <?php echo $result['promocode']?"disabled":"required"; ?>>
                        <span class="msgValid" id="msgpUname"></span>
                  </div>
               </div>
               <div class="clearfix"></div>                            
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Discount %:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" maxlength="2" onkeyup="percent();" value="<?php echo $result['discount']; ?>" id="discount" name="discount" class="form-control" required="">
                  </div>
               </div>
               <div class="clearfix"></div>                         
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Discount Price:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <input type="text"  value="<?php echo $result['amount']; ?>" id="amount" name="amount" class="form-control" readonly="">
                     <input type="hidden" value="1000" name="vcv_reg_change" id="vcv_reg_change">
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-4">
                  <div class="form-group center_text">
                     Recruiter IDs:
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <select name="recruiter_id[]" id="recruiter_id" placeholder="Select Recruiters" class="select2 form-control" autocomplete="off" required="">
                        <!-- <option value="">Select</option> -->
                       <?php $recr_id = explode(',', $result['recruiter_id']); 
                       $sql = "SELECT * FROM `users` WHERE `utype`='recruiter' AND `status`=1 ;";
                       // echo $sql = "SELECT * FROM `users` WHERE `utype`='recruiter' AND `status`=1 AND `id` NOT IN (SELECT `recruiter_id` FROM `coupon_code` WHERE `id`='".$result['id']."');";
                       $this->Query($sql);
                       $recruiters = $this->fetchArray();
                       foreach ($recruiters as $recruiter){ ?>
                           <option value="<?php echo $recruiter['id']; ?>" <?php echo in_array($recruiter['id'], $recr_id)?'selected':''; ?>><?php echo $recruiter['name']." (".$recruiter['username'].")"; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-9 col-sm-8 col-xs-12">
                  <center><input type="submit" id="submit-btn" name="submit" class="btn btn-primary butoon_brow" value="<?php if($_REQUEST['id']!=''){ echo 'Update';}else{echo 'Submit';}?>"></center>
               </div>
               <input type="hidden" name="control" value="master"/>
               <input type="hidden" name="edit" value="1"/>
               <input type="hidden" name="task" value="promocode_save"/>
               <input type="hidden" name="id" id="idd" value="<?php echo $results[0]['id']; ?>"  />
            </div>
         </div>
         <!--account details ends here-->
      </form>
   </div>
</div>
</div><!-- table-responsive -->
</div>
</div>
<link rel="stylesheet" href="template/myvcv/plugins/select2/select2.min.css">
<link rel="stylesheet" type="text/css" href="assets/date_picker/jquery.datetimepicker.css"/>
<script src="assets/date_picker/jquery.js"></script> 
<script src="assets/date_picker/build/jquery.datetimepicker.full.js"></script> 
<script type="text/javascript" src="template/myvcv/plugins/select2/select2.full.min.js"></script>
<script>
   $('#dob').datetimepicker({
    yearOffset:0,
    lang:'ch',
    timepicker:false,
    format:'d-m-Y',
    formatDate:'Y/m/d',
    //minDate:'-1970/01/02', // yesterday is minimum date
    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
   });

    /*============Auto hide alert box================*/
    $(".alert").delay(2000).slideUp(200, function() {
      $(this).alert('close');
    });
   
   
   $('#promocode').blur(function(){
      promocode = $(this).val();

      if(promocode.length<1){
        
        $('#msgpUname').text('');

      }else{

        $.ajax({url: "script/check_coupon.php?name="+promocode, success: function(result){
          if(result>0){
            txt = "Coupon Code Already Created";
              $('#msgpUname').text(txt).css('color','red');
              $("#submit-btn").prop('disabled',true);
          }else{
            txt = "Coupon Code Available, Go ahead!!!";
              $('#msgpUname').text(txt).css('color','green');
              $("#submit-btn").prop('disabled',false);
          }

     }
});
}
   });

function percent(){
   var main_amt = parseInt(($('#vcv_reg_change').val()?$('#vcv_reg_change').val():0));
   var disc = parseInt(($('#discount').val()?$('#discount').val():0));
   // var amt = parseInt($('#amount').val());

   amt = (main_amt*disc)/100;

   $('#amount').val(amt);


}



$('.select2').select2();
</script>

