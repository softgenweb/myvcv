<?php include_once('configuration.php'); 

global $conn;
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

$system_ip = get_client_ip();

$chk = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM `visitors` WHERE `date_created` LIKE '".date('Y-m-d')."%' AND `system_ip`='".$system_ip."'"));

if(!$chk['id']){
/*==========Insert data in table========*/
$insert = mysqli_query($conn, "INSERT INTO `visitors`(`system_ip`, `country`, `counter`, `date_created`) VALUES('".$system_ip."', '', '1', '".date('Y-m-d H:i:s')."')");
}else{
	$update = mysqli_query($conn, "UPDATE `visitors` SET `counter`='".($chk['counter']+1)."' WHERE `date_created` LIKE '".date('Y-m-d')."%' AND `system_ip`='".$system_ip."'");
}
?>