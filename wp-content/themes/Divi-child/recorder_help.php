<?php
/**********
Template Name: Recoder Help
**********/

get_header();
session_start();
?>



<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"> <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<link rel="pingback" href="https://www.myvcv.in/xmlrpc.php" />

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
	<script>var et_site_url='https://www.myvcv.in';var et_post_id='31';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script><title>How to Record | MyVCV.Asia</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="MyVCV.Asia &raquo; Feed" href="https://www.myvcv.in/feed/" />
<link rel="alternate" type="application/rss+xml" title="MyVCV.Asia &raquo; Comments Feed" href="https://www.myvcv.in/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.myvcv.in\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.2"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<meta content="Divi Child Theme by SelecONE Solutions v.1.1.1559137332" name="generator"/><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='dashicons-css'  href='https://www.myvcv.in/wp-includes/css/dashicons.min.css?ver=5.2.2' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='https://www.myvcv.in/wp-includes/css/admin-bar.min.css?ver=5.2.2' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='https://www.myvcv.in/wp-includes/css/dist/block-library/style.min.css?ver=5.2.2' type='text/css' media='all' />
<link rel='stylesheet' id='chld_thm_cfg_parent-css'  href='https://www.myvcv.in/wp-content/themes/Divi/style.css?ver=5.2.2' type='text/css' media='all' />
<link rel='stylesheet' id='divi-fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&#038;subset=latin,latin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='divi-style-css'  href='https://www.myvcv.in/wp-content/themes/Divi-child/style.css?ver=3.23.3' type='text/css' media='all' />
<link rel='stylesheet' id='et-builder-googlefonts-cached-css'  href='https://fonts.googleapis.com/css?family=Didact+Gothic%3Aregular%7CBad+Script%3Aregular&#038;ver=5.2.2#038;subset=latin,latin-ext' type='text/css' media='all' />
<script type='text/javascript' src='https://www.myvcv.in/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='https://www.myvcv.in/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='https://www.myvcv.in/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.myvcv.in/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.myvcv.in/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.2.2" />
<link rel="canonical" href="https://www.myvcv.in/how-to-record/" />
<link rel='shortlink' href='https://www.myvcv.in/?p=31' />
<link rel="alternate" type="application/json+oembed" href="https://www.myvcv.in/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.myvcv.in%2Fhow-to-record%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.myvcv.in/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.myvcv.in%2Fhow-to-record%2F&#038;format=xml" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" /><style type="text/css" media="print">#wpadminbar { display:none; }</style>
	<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
/* 	.et_pb_section {
padding: 54px 0 !important;
} */
</style>
	<link rel="icon" href="https://www.myvcv.in/wp-content/uploads/2019/05/cropped-myvcv-logo-e1558949385565-32x32.png" sizes="32x32" />
<link rel="icon" href="https://www.myvcv.in/wp-content/uploads/2019/05/cropped-myvcv-logo-e1558949385565-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://www.myvcv.in/wp-content/uploads/2019/05/cropped-myvcv-logo-e1558949385565-180x180.png" />
<meta name="msapplication-TileImage" content="https://www.myvcv.in/wp-content/uploads/2019/05/cropped-myvcv-logo-e1558949385565-270x270.png" />
<style id="et-core-unified-cached-inline-styles">.woocommerce #respond input#submit,.woocommerce-page #respond input#submit,.woocommerce #content input.button,.woocommerce-page #content input.button,.woocommerce-message,.woocommerce-error,.woocommerce-info{background:rgba(239,14,51,0.83)!important}#et_search_icon:hover,.mobile_menu_bar:before,.mobile_menu_bar:after,.et_toggle_slide_menu:after,.et-social-icon a:hover,.et_pb_sum,.et_pb_pricing li a,.et_pb_pricing_table_button,.et_overlay:before,.entry-summary p.price ins,.woocommerce div.product span.price,.woocommerce-page div.product span.price,.woocommerce #content div.product span.price,.woocommerce-page #content div.product span.price,.woocommerce div.product p.price,.woocommerce-page div.product p.price,.woocommerce #content div.product p.price,.woocommerce-page #content div.product p.price,.et_pb_member_social_links a:hover,.woocommerce .star-rating span:before,.woocommerce-page .star-rating span:before,.et_pb_widget li a:hover,.et_pb_filterable_portfolio .et_pb_portfolio_filters li a.active,.et_pb_filterable_portfolio .et_pb_portofolio_pagination ul li a.active,.et_pb_gallery .et_pb_gallery_pagination ul li a.active,.wp-pagenavi span.current,.wp-pagenavi a:hover,.nav-single a,.posted_in a{color:rgba(239,14,51,0.83)}.et_pb_contact_submit,.et_password_protected_form .et_submit_button,.et_pb_bg_layout_light .et_pb_newsletter_button,.comment-reply-link,.form-submit .et_pb_button,.et_pb_bg_layout_light .et_pb_promo_button,.et_pb_bg_layout_light .et_pb_more_button,.woocommerce a.button.alt,.woocommerce-page a.button.alt,.woocommerce button.button.alt,.woocommerce-page button.button.alt,.woocommerce input.button.alt,.woocommerce-page input.button.alt,.woocommerce #respond input#submit.alt,.woocommerce-page #respond input#submit.alt,.woocommerce #content input.button.alt,.woocommerce-page #content input.button.alt,.woocommerce a.button,.woocommerce-page a.button,.woocommerce button.button,.woocommerce-page button.button,.woocommerce input.button,.woocommerce-page input.button,.et_pb_contact p input[type="checkbox"]:checked+label i:before,.et_pb_bg_layout_light.et_pb_module.et_pb_button{color:rgba(239,14,51,0.83)}.footer-widget h4{color:rgba(239,14,51,0.83)}.et-search-form,.nav li ul,.et_mobile_menu,.footer-widget li:before,.et_pb_pricing li:before,blockquote{border-color:rgba(239,14,51,0.83)}.et_pb_counter_amount,.et_pb_featured_table .et_pb_pricing_heading,.et_quote_content,.et_link_content,.et_audio_content,.et_pb_post_slider.et_pb_bg_layout_dark,.et_slide_in_menu_container,.et_pb_contact p input[type="radio"]:checked+label i:before{background-color:rgba(239,14,51,0.83)}a{color:#ff2323}#main-header,#main-header .nav li ul,.et-search-form,#main-header .et_mobile_menu{background-color:rgba(255,255,255,0.87)}#main-header .nav li ul{background-color:rgba(255,255,255,0.17)}.nav li ul{border-color:#ff2323}#top-header,#et-secondary-nav li ul{background-color:rgba(224,15,4,0.87)}#main-header .nav li ul a{color:rgba(0,0,0,0.7)}#top-header,#top-header a,#et-secondary-nav li li a,#top-header .et-social-icon a:before{font-size:14px}#top-menu li.current-menu-ancestor>a,#top-menu li.current-menu-item>a,.et_color_scheme_red #top-menu li.current-menu-ancestor>a,.et_color_scheme_red #top-menu li.current-menu-item>a,.et_color_scheme_pink #top-menu li.current-menu-ancestor>a,.et_color_scheme_pink #top-menu li.current-menu-item>a,.et_color_scheme_orange #top-menu li.current-menu-ancestor>a,.et_color_scheme_orange #top-menu li.current-menu-item>a,.et_color_scheme_green #top-menu li.current-menu-ancestor>a,.et_color_scheme_green #top-menu li.current-menu-item>a{color:#ff2323}#footer-widgets .footer-widget a,#footer-widgets .footer-widget li a,#footer-widgets .footer-widget li a:hover{color:#ffffff}.footer-widget{color:#ffffff}#main-footer .footer-widget h4{color:#ff2323}.footer-widget li:before{border-color:#ff2323}.footer-widget .et_pb_widget div,.footer-widget .et_pb_widget ul,.footer-widget .et_pb_widget ol,.footer-widget .et_pb_widget label{line-height:1.7em}#et-footer-nav .bottom-nav li.current-menu-item a{color:#ff2323}#footer-bottom .et-social-icon a{font-size:20px}#main-header{box-shadow:none}.et-fixed-header#main-header{box-shadow:none!important}body .et_pb_button,.woocommerce a.button.alt,.woocommerce-page a.button.alt,.woocommerce button.button.alt,.woocommerce-page button.button.alt,.woocommerce input.button.alt,.woocommerce-page input.button.alt,.woocommerce #respond input#submit.alt,.woocommerce-page #respond input#submit.alt,.woocommerce #content input.button.alt,.woocommerce-page #content input.button.alt,.woocommerce a.button,.woocommerce-page a.button,.woocommerce button.button,.woocommerce-page button.button,.woocommerce input.button,.woocommerce-page input.button,.woocommerce #respond input#submit,.woocommerce-page #respond input#submit,.woocommerce #content input.button,.woocommerce-page #content input.button,.woocommerce-message a.button.wc-forward{border-color:#0c71c3}body.et_pb_button_helper_class .et_pb_button,body.et_pb_button_helper_class .et_pb_module.et_pb_button,.woocommerce.et_pb_button_helper_class a.button.alt,.woocommerce-page.et_pb_button_helper_class a.button.alt,.woocommerce.et_pb_button_helper_class button.button.alt,.woocommerce-page.et_pb_button_helper_class button.button.alt,.woocommerce.et_pb_button_helper_class input.button.alt,.woocommerce-page.et_pb_button_helper_class input.button.alt,.woocommerce.et_pb_button_helper_class #respond input#submit.alt,.woocommerce-page.et_pb_button_helper_class #respond input#submit.alt,.woocommerce.et_pb_button_helper_class #content input.button.alt,.woocommerce-page.et_pb_button_helper_class #content input.button.alt,.woocommerce.et_pb_button_helper_class a.button,.woocommerce-page.et_pb_button_helper_class a.button,.woocommerce.et_pb_button_helper_class button.button,.woocommerce-page.et_pb_button_helper_class button.button,.woocommerce.et_pb_button_helper_class input.button,.woocommerce-page.et_pb_button_helper_class input.button,.woocommerce.et_pb_button_helper_class #respond input#submit,.woocommerce-page.et_pb_button_helper_class #respond input#submit,.woocommerce.et_pb_button_helper_class #content input.button,.woocommerce-page.et_pb_button_helper_class #content input.button{color:#0c71c3}body .et_pb_button:hover,.woocommerce a.button.alt:hover,.woocommerce-page a.button.alt:hover,.woocommerce button.button.alt:hover,.woocommerce-page button.button.alt:hover,.woocommerce input.button.alt:hover,.woocommerce-page input.button.alt:hover,.woocommerce #respond input#submit.alt:hover,.woocommerce-page #respond input#submit.alt:hover,.woocommerce #content input.button.alt:hover,.woocommerce-page #content input.button.alt:hover,.woocommerce a.button:hover,.woocommerce-page a.button:hover,.woocommerce button.button:hover,.woocommerce-page button.button:hover,.woocommerce input.button:hover,.woocommerce-page input.button:hover,.woocommerce #respond input#submit:hover,.woocommerce-page #respond input#submit:hover,.woocommerce #content input.button:hover,.woocommerce-page #content input.button:hover{border-radius:0px}body #page-container .et_slide_in_menu_container{background:#ff2323}@media only screen and (min-width:981px){.et_header_style_left #et-top-navigation,.et_header_style_split #et-top-navigation{padding:22px 0 0 0}.et_header_style_left #et-top-navigation nav>ul>li>a,.et_header_style_split #et-top-navigation nav>ul>li>a{padding-bottom:22px}.et_header_style_split .centered-inline-logo-wrap{width:44px;margin:-44px 0}.et_header_style_split .centered-inline-logo-wrap #logo{max-height:44px}.et_pb_svg_logo.et_header_style_split .centered-inline-logo-wrap #logo{height:44px}.et_header_style_centered #top-menu>li>a{padding-bottom:8px}.et_header_style_slide #et-top-navigation,.et_header_style_fullscreen #et-top-navigation{padding:13px 0 13px 0!important}.et_header_style_centered #main-header .logo_container{height:44px}#logo{max-height:100%}.et_pb_svg_logo #logo{height:100%}.et_header_style_centered.et_hide_primary_logo #main-header:not(.et-fixed-header) .logo_container,.et_header_style_centered.et_hide_fixed_logo #main-header.et-fixed-header .logo_container{height:7.92px}.et-fixed-header#top-header,.et-fixed-header#top-header #et-secondary-nav li ul{background-color:rgba(224,43,32,0.72)}.et-fixed-header #top-menu li.current-menu-ancestor>a,.et-fixed-header #top-menu li.current-menu-item>a{color:#ff2323!important}}@media only screen and (min-width:1350px){.et_pb_row{padding:27px 0}.et_pb_section{padding:54px 0}.single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper{padding-top:81px}.et_pb_fullwidth_section{padding:0}}@media only screen and (max-width:980px){.et_header_style_centered .mobile_nav .select_page,.et_header_style_split .mobile_nav .select_page,.et_mobile_menu li a,.mobile_menu_bar:before,.et_nav_text_color_light #top-menu>li>a,.et_nav_text_color_dark #top-menu>li>a,#top-menu a,.et_mobile_menu li a,#et_search_icon:before,#et_top_search .et-search-form input,.et_search_form_container input,#et-top-navigation .et-cart-info{color:rgba(239,47,33,0.75)}.et_close_search_field:after{color:rgba(239,47,33,0.75)!important}.et_search_form_container input::-moz-placeholder{color:rgba(239,47,33,0.75)}.et_search_form_container input::-webkit-input-placeholder{color:rgba(239,47,33,0.75)}.et_search_form_container input:-ms-input-placeholder{color:rgba(239,47,33,0.75)}h1{font-size:22px}h2,.product .related h2,.et_pb_column_1_2 .et_quote_content blockquote p{font-size:18px}h3{font-size:16px}h4,.et_pb_circle_counter h3,.et_pb_number_counter h3,.et_pb_column_1_3 .et_pb_post h2,.et_pb_column_1_4 .et_pb_post h2,.et_pb_blog_grid h2,.et_pb_column_1_3 .et_quote_content blockquote p,.et_pb_column_3_8 .et_quote_content blockquote p,.et_pb_column_1_4 .et_quote_content blockquote p,.et_pb_blog_grid .et_quote_content blockquote p,.et_pb_column_1_3 .et_link_content h2,.et_pb_column_3_8 .et_link_content h2,.et_pb_column_1_4 .et_link_content h2,.et_pb_blog_grid .et_link_content h2,.et_pb_column_1_3 .et_audio_content h2,.et_pb_column_3_8 .et_audio_content h2,.et_pb_column_1_4 .et_audio_content h2,.et_pb_blog_grid .et_audio_content h2,.et_pb_column_3_8 .et_pb_audio_module_content h2,.et_pb_column_1_3 .et_pb_audio_module_content h2,.et_pb_gallery_grid .et_pb_gallery_item h3,.et_pb_portfolio_grid .et_pb_portfolio_item h2,.et_pb_filterable_portfolio_grid .et_pb_portfolio_item h2{font-size:13px}.et_pb_slider.et_pb_module .et_pb_slides .et_pb_slide_description .et_pb_slide_title{font-size:33px}.woocommerce ul.products li.product h3,.woocommerce-page ul.products li.product h3,.et_pb_gallery_grid .et_pb_gallery_item h3,.et_pb_portfolio_grid .et_pb_portfolio_item h2,.et_pb_filterable_portfolio_grid .et_pb_portfolio_item h2,.et_pb_column_1_4 .et_pb_audio_module_content h2{font-size:11px}h5{font-size:11px}h6{font-size:10px}.et_pb_row,.et_pb_column .et_pb_row_inner{padding:22px 0}}@media only screen and (max-width:767px){body,.et_pb_column_1_2 .et_quote_content blockquote cite,.et_pb_column_1_2 .et_link_content a.et_link_main_url,.et_pb_column_1_3 .et_quote_content blockquote cite,.et_pb_column_3_8 .et_quote_content blockquote cite,.et_pb_column_1_4 .et_quote_content blockquote cite,.et_pb_blog_grid .et_quote_content blockquote cite,.et_pb_column_1_3 .et_link_content a.et_link_main_url,.et_pb_column_3_8 .et_link_content a.et_link_main_url,.et_pb_column_1_4 .et_link_content a.et_link_main_url,.et_pb_blog_grid .et_link_content a.et_link_main_url{font-size:12px}.et_pb_slider.et_pb_module .et_pb_slides .et_pb_slide_content,.et_pb_best_value{font-size:13px}.et_pb_row,.et_pb_column .et_pb_row_inner{padding:17px 0}}.et_pb_blurb h4{font-size:21px}.et_pb_accordion .et_pb_toggle_title{font-size:16px}.et_pb_accordion .et_pb_toggle_title:before{font-size:16px}.et_pb_accordion .et_pb_toggle_open,.et_pb_accordion .et_pb_toggle_close{padding:20px}.et_pb_toggle.et_pb_toggle_item.et_pb_toggle_open h5{font-weight:normal;font-style:normal;text-transform:none;text-decoration:none}.et_pb_contact_form_container .et_pb_contact p input,.et_pb_contact_form_container .et_pb_contact p textarea{font-size:16px}.et_pb_contact_captcha_question{font-size:16px}.et_pb_contact p input,.et_pb_contact p textarea{padding:16px}.et_pb_social_media_follow li a.icon{margin-right:7.98px;width:28px;height:28px}.et_pb_social_media_follow li a.icon::before{width:28px;height:28px;font-size:14px;line-height:28px}.et_pb_social_media_follow li a.follow_button{font-size:14px}.et_pb_fullwidth_section .et_pb_slide_description{padding-top:5%;padding-bottom:5%}.home #x-section-1 a .x-btn.x-btn-pill.x-btn-global{background:blue!important}.home #x-section-1 x-btn.x-btn-pill.x-btn-global:hover{background:green!important}.et_pb_tabs_0.et_pb_tabs{display:flex;flex-direction:column;justify-content:center;position:fixed;top:200px;left:0px;z-index:99998}.menu-cta{border:2px solid black}</style></head>

	<div id="page-container">

					

			<div id="et-main-area">
	
<div id="main-content">


			
				<article id="post-31" class="post-31 page type-page status-publish hentry">

				
					<div class="entry-content">
					<div id="et-boc" class="et-boc">
			
			<div class="et_builder_inner_content et_pb_gutters3">
				<div class="et_pb_section et_pb_section_0 et_section_regular">
				
				
				
				
					<div class="et_pb_row et_pb_row_0">
				<div class="et_pb_column et_pb_column_3_5 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h1 style="text-align: left;"><span style="color: #e02b20;"><strong>Tips to Make An Effective Video CV</strong></span></h1>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_2_5 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough">
				
				<?php if($_SESSION['adminid']!=''){ ?>
				<div class="et_pb_button_module_wrapper et_pb_button_0_wrapper  et_pb_module ">
				<a class="et_pb_button et_pb_button_0 et_animated et_pb_bg_layout_light" href="/Admin/index.php?control=user&task=video_detail">Record Now</a>
			</div>
				<?php }else{?>
				<div class="et_pb_button_module_wrapper et_pb_button_0_wrapper  et_pb_module ">
				<a class="et_pb_button et_pb_button_0 et_animated et_pb_bg_layout_light" href="record-a-vcv/">Record Now</a>
				</div>
			<?php	} ?>
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div> <!-- .et_pb_section --><div class="et_pb_section et_pb_section_1 et_pb_with_background et_section_regular">
				
				
				
				
					<div class="et_pb_row et_pb_row_1">
				<div class="et_pb_column et_pb_column_4_4 et_pb_column_2    et_pb_css_mix_blend_mode_passthrough et-last-child">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h1>SIMPLY FOLLOW ALL THE STEPS BELOW</h1>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div> <!-- .et_pb_section --><div class="et_pb_section et_pb_section_2 et_animated et_section_regular">
				
				
				
				
					<div class="et_pb_row et_pb_row_2">
				<div class="et_pb_column et_pb_column_1_3 et_pb_column_3    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_0 et_animated et_pb_bg_layout_light  et_pb_text_align_justified  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_left">&#xe0f5;</span></span></div>
					<div class="et_pb_blurb_container">
						<h4 class="et_pb_module_header">        1. Make an Intention </h4>
						<div class="et_pb_blurb_description">
							<p>Before you start recording and to make your profile, make a clear intention of what you need from this Virtual CV. The intention is a very simple idea and goal you want to achieve from this profile.</p>
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3 et_pb_column_4    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_1 et_pb_bg_layout_light  et_pb_text_align_justified  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#x6c;</span></span></div>
					<div class="et_pb_blurb_container">
						<h4 class="et_pb_module_header">        2. Make a Storyboard</h4>
						<div class="et_pb_blurb_description">
							<p>Based on your intention, develop a storyboard for your video. The storyboard is a simple visual narration of your idea of a video resume. On a blank paper just draw our how the screen would appear to desired audience. your clothes, your hair, your background etc.</p>
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3 et_pb_column_5    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_2 et_pb_bg_layout_light  et_pb_text_align_justified  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe601;</span></span></div>
					<div class="et_pb_blurb_container">
						<h4 class="et_pb_module_header">            3. Make a Script</h4>
						<div class="et_pb_blurb_description">
							<p>Once you have visualized how best the video would look, you can start writing a simple script. Even though the script needs to be impressive but also keep it real and authentic. Think from your heart and mind, Write a script which helps showcase all your skills and is easy for you to narrate.</p>
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_row et_pb_row_3">
				<div class="et_pb_column et_pb_column_1_3 et_pb_column_6    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_3 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe00f;</span></span></div>
					<div class="et_pb_blurb_container">
						<h4 class="et_pb_module_header">            4. Make a VCV</h4>
						<div class="et_pb_blurb_description">
							<p style="text-align: left;">By the time you have finished with your storyboard and script you would automatically have the confidence to speak in front of the camera. Now get ready and record your impressive virtual CV. Just stick to the plan you made and hit that record button, believe in yourself and see the Magik,</p>
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3 et_pb_column_7    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_4 et_pb_bg_layout_light  et_pb_text_align_justified  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe08a;</span></span></div>
					<div class="et_pb_blurb_container">
						<h4 class="et_pb_module_header">         5. Make your Profile</h4>
						<div class="et_pb_blurb_description">
							<p>After making a video you are satisfied with, the website will ask you to save the video and create a profile. Remember your audeince, in this case the Human Resource Team. Before they hire you for your dream job they would like to know everything about you. You can make/upload multiple videos, upload your certificates and marksheets, job experience letters, portfolio etc. The more you express the more you can impress.</p>
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3 et_pb_column_8    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_5 et_pb_bg_layout_light  et_pb_text_align_justified  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe021;</span></span></div>
					<div class="et_pb_blurb_container">
						<h4 class="et_pb_module_header">       6. Make an Impression</h4>
						<div class="et_pb_blurb_description">
							<p>MyVCV is a platform for you to achieve your dreams and enable you to represent yourself in in a much more appealing way. By sending a Sub-Domain Link with your Virtual CV as part of your job interview, you will instantly stand you out of the crowd and make an impression about your skills and attitude, They can easily get a feel for your personality which is an essential step before hiring you. Once you have thier attention, you definitely have the edge.</p>
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb --><div class="et_pb_button_module_wrapper et_pb_button_1_wrapper  et_pb_module ">
				<a class="et_pb_button et_pb_button_1 et_animated et_pb_bg_layout_light" href="https://myvcv.asia/record-a-vcv/">RECORD YOUR VCV</a>
			</div>
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div> <!-- .et_pb_section --><div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_regular">
				
				
				
				
					<div class="et_pb_row et_pb_row_4">
				<div class="et_pb_column et_pb_column_4_4 et_pb_column_9    et_pb_css_mix_blend_mode_passthrough et-last-child">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h1>GET READY, AS YOU WOULD, WHEN YOU GO FOR AN ACTUAL INTERVIEW</h1>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div> <!-- .et_pb_section --><div class="et_pb_section et_pb_section_4 et_section_regular">
				
				
				
				
					<div class="et_pb_with_border et_pb_row et_pb_row_5">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_10    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_6 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#x7a;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_11    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#1 Sound Check</strong></h2>
<p style="text-align: center;">Check to sound meter to check if you mic is working &amp; make sure you dont have any loud background sounds. </p>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_with_border et_pb_row et_pb_row_6">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_12    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_7 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe00e;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_13    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_4 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#2 cover 80% of screen</strong></h2>
<p style="text-align: center;">Mostly take upper body close up on face to give a feeling of interview unless your script suggest otherwise..</p>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_with_border et_pb_row et_pb_row_7">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_14    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_8 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe007;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_15    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_5 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#3 Lights</strong></h2>
<p style="text-align: center;">Make sure you <span style="font-family: Verdana;">have</span> light on your face &amp; not on your side or back.</p>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_with_border et_pb_row et_pb_row_8">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_16    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_9 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe00a;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_17    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_6 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#4 eye contact</strong></h2>
<p style="text-align: center;">Look into the camera lens and maintain an eye &#8211; contact.</p>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_with_border et_pb_row et_pb_row_9">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_18    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_10 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe08a;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_19    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_7 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#5 good clothes</strong></h2>
<p style="text-align: center;">Pick out the clothes that you would like to wear it to an interview.This is that chance to express yourself. Be You. </p>
<p style="text-align: center;">
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_with_border et_pb_row et_pb_row_10">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_20    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_11 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe106;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_21    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_8 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#6 presentable &amp; attractive</strong></h2>
<p style="text-align: center;">A first impression can have lasting effects, get maximum marks for being presentable by look and reasonable by mind. </p>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_with_border et_pb_row et_pb_row_11">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_22    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_12 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#x68;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_23    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_9 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#7 rehearse script with prompter</strong></h2>
<p style="text-align: center;">Use our promper to help deliver your choice of words while looking in the lens. Practice once before final recording for better results. </p>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_with_border et_pb_row et_pb_row_12">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_24    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_13 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe602;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_25    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_10 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#8 check internet connection</strong></h2>
<p style="text-align: center;">Make sure your best performed shot is recorded and registered at the same time. </p>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_with_border et_pb_row et_pb_row_13">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_26    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_14 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#xe075;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_27    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_11 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#9 take reference</strong></h2>
<p style="text-align: center;">Check in the recruiters section and try the search feature.<br />Take reference from other people&#8217;s videos.<br />you can also find blogs and articles links in the footer area.</p>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row --><div class="et_pb_with_border et_pb_row et_pb_row_14">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_28    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_blurb et_pb_blurb_15 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">
				
				
				<div class="et_pb_blurb_content">
					<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-pb-icon et-waypoint et_pb_animation_top">&#x6c;</span></span></div>
					<div class="et_pb_blurb_container">
						
						<div class="et_pb_blurb_description">
							
						</div><!-- .et_pb_blurb_description -->
					</div>
				</div> <!-- .et_pb_blurb_content -->
			</div> <!-- .et_pb_blurb -->
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_29    et_pb_css_mix_blend_mode_passthrough">
				
				
				<div class="et_pb_module et_pb_text et_pb_text_12 et_pb_bg_layout_light  et_pb_text_align_left">
				
				
				<div class="et_pb_text_inner">
					<h2 style="text-align: center;"><strong>#10 fill complete form</strong></h2>
<p style="text-align: center;">If someone is looking forward to hiring you, they would need as much bio-data and bachgruond check as possible. <br />More information provided by you helps them decide quicler </p>
				</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div> <!-- .et_pb_section --><div class="et_pb_section et_pb_section_5 et_section_regular">
				
				
				
				
					<div class="et_pb_row et_pb_row_15">
				<div class="et_pb_column et_pb_column_1_2 et_pb_column_30    et_pb_css_mix_blend_mode_passthrough et_pb_column_empty">
				
				
				
			</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2 et_pb_column_31    et_pb_css_mix_blend_mode_passthrough">
				
				
				<?php if($_SESSION['adminid']!=''){ ?>
				<div class="et_pb_button_module_wrapper et_pb_button_0_wrapper  et_pb_module ">
				<a class="et_pb_button et_pb_button_0 et_animated et_pb_bg_layout_light" href="/Admin/index.php?control=user&task=video_detail">Record Now</a>
			</div>
				<?php }else{?>
				<div class="et_pb_button_module_wrapper et_pb_button_0_wrapper  et_pb_module ">
				<a class="et_pb_button et_pb_button_0 et_animated et_pb_bg_layout_light" href="record-a-vcv/">Record Now</a>
				</div>
			<?php	} ?>
			</div> <!-- .et_pb_column -->
				
				
			</div> <!-- .et_pb_row -->
				
				
			</div> <!-- .et_pb_section -->			</div>
			
		</div>					</div> <!-- .entry-content -->

				
				</article> <!-- .et_pb_post -->

			

</div> <!-- #main-content -->


	<span class="et_pb_scroll_top et-pb-icon"></span>

		</div> <!-- #et-main-area -->


	</div> <!-- #page-container -->

		<script type="text/javascript">
				var et_animation_data = [{"class":"et_pb_button_0","style":"transformAnim","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_section_2","style":"fade","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_blurb_0","style":"zoom","repeat":"once","duration":"1300ms","delay":"50ms","intensity":"61%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_button_1","style":"transformAnim","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"},{"class":"et_pb_button_2","style":"transformAnim","repeat":"once","duration":"1000ms","delay":"0ms","intensity":"50%","starting_opacity":"0%","speed_curve":"ease-in-out"}];
			</script>
	<script type='text/javascript' src='https://www.myvcv.in/wp-includes/js/admin-bar.min.js?ver=5.2.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var DIVI = {"item_count":"%d Item","items_count":"%d Items"};
var et_shortcodes_strings = {"previous":"Previous","next":"Next"};
var et_pb_custom = {"ajaxurl":"https:\/\/www.myvcv.in\/wp-admin\/admin-ajax.php","images_uri":"https:\/\/www.myvcv.in\/wp-content\/themes\/Divi\/images","builder_images_uri":"https:\/\/www.myvcv.in\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"d204562557","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"3a379d9740","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"31","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"no","is_shortcode_tracking":"","tinymce_uri":""};
var et_pb_box_shadow_elements = [];
/* ]]> */
</script>
<script type='text/javascript' src='https://www.myvcv.in/wp-content/themes/Divi/js/custom.min.js?ver=3.23.3'></script>
<script type='text/javascript' src='https://www.myvcv.in/wp-content/themes/Divi/core/admin/js/common.js?ver=3.23.3'></script>
<script type='text/javascript' src='https://www.myvcv.in/wp-includes/js/wp-embed.min.js?ver=5.2.2'></script>
<style id="et-core-unified-cached-inline-styles-2">.et_pb_section_0.et_pb_section{padding-top:17px;padding-right:0px;padding-bottom:1px;padding-left:0px}.et_pb_blurb_9 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_text_8{font-size:17px}.et_pb_blurb_11 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_row_10.et_pb_row{padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_row_10{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px}.et_pb_text_7 h2{text-transform:uppercase}.et_pb_text_7{font-size:17px}.et_pb_blurb_10 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_row_9.et_pb_row{margin-bottom:0px!important;padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_row_9{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px;border-color:rgba(224,15,4,0.87)}.et_pb_text_6 h2{text-transform:uppercase}.et_pb_text_6{font-size:17px}.et_pb_row_8.et_pb_row{padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_row_11{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px;border-color:rgba(224,15,4,0.87)}.et_pb_row_8{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px}.et_pb_text_5 h2{text-transform:uppercase}.et_pb_text_5{font-size:17px}.et_pb_blurb_8 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_row_7.et_pb_row{margin-bottom:0px!important;padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_row_7{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px;border-color:rgba(224,15,4,0.87)}.et_pb_text_4 h2{text-transform:uppercase}.et_pb_text_4{font-size:17px}.et_pb_blurb_7 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_blurb_7.et_pb_blurb{padding-top:27px!important;padding-right:10px!important;padding-bottom:10px!important;padding-left:10px!important}.et_pb_row_6.et_pb_row{padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_row_6{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px}.et_pb_text_8 h2{text-transform:uppercase}.et_pb_row_11.et_pb_row{margin-bottom:0px!important;padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_text_3{font-size:17px}.et_pb_row_14.et_pb_row{padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_button_2{transform:scaleX(0.9) scaleY(0.9) skewX(1deg) skewY(1deg);transform-origin:}.et_pb_button_2.et_animated.transformAnim{animation-name:et_pb_slide_left_et_pb_button_2}@keyframes et_pb_slide_left_et_pb_button_2{0%{transform:scaleX(0.9) scaleY(0.9) translateX(calc(-100% + 0%)) translateY(0%) skewX(1deg) skewY(1deg)}100%{opacity:1;transform:scaleX(0.9) scaleY(0.9) skewX(1deg) skewY(1deg)}}body #page-container .et_pb_button_2:hover:after{color:}body #page-container .et_pb_button_2{color:#e02b20!important;border-width:4px!important;border-color:#ede900;border-radius:15px;letter-spacing:3px;font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif!important;font-weight:700!important}.et_pb_button_2_wrapper{margin-top:0px!important;margin-bottom:6px!important;margin-left:49px!important}.et_pb_button_2_wrapper .et_pb_button_2,.et_pb_button_2_wrapper .et_pb_button_2:hover{padding-left:21px!important}.et_pb_row_15.et_pb_row{padding-top:25px;padding-right:0px;padding-bottom:6px;padding-left:0px}.et_pb_section_5.et_pb_section{padding-top:0px;padding-right:0px;padding-bottom:35px;padding-left:0px}.et_pb_text_12 h2{text-transform:uppercase}.et_pb_text_12{font-size:17px}.et_pb_blurb_15 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_row_14{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px}.et_pb_blurb_12 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_text_11 h2{text-transform:uppercase}.et_pb_text_11{font-size:17px}.et_pb_blurb_14 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_row_13.et_pb_row{margin-bottom:0px!important;padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_row_13{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px;border-color:rgba(224,15,4,0.87)}.et_pb_text_10 h2{text-transform:uppercase}.et_pb_text_10{font-size:17px}.et_pb_blurb_13 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_row_12.et_pb_row{padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_row_12{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px}.et_pb_text_9 h2{text-transform:uppercase}.et_pb_text_9{font-size:17px}.et_pb_text_3 h2{text-transform:uppercase}.et_pb_blurb_6 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_row_0.et_pb_row{padding-top:43px;padding-right:0px;padding-bottom:27px;padding-left:0px}.et_pb_button_0,.et_pb_button_0:after{transition:all 300ms ease 0ms}.et_pb_blurb_1.et_pb_blurb h4,.et_pb_blurb_1.et_pb_blurb h4 a,.et_pb_blurb_1.et_pb_blurb h1.et_pb_module_header,.et_pb_blurb_1.et_pb_blurb h1.et_pb_module_header a,.et_pb_blurb_1.et_pb_blurb h2.et_pb_module_header,.et_pb_blurb_1.et_pb_blurb h2.et_pb_module_header a,.et_pb_blurb_1.et_pb_blurb h3.et_pb_module_header,.et_pb_blurb_1.et_pb_blurb h3.et_pb_module_header a,.et_pb_blurb_1.et_pb_blurb h5.et_pb_module_header,.et_pb_blurb_1.et_pb_blurb h5.et_pb_module_header a,.et_pb_blurb_1.et_pb_blurb h6.et_pb_module_header,.et_pb_blurb_1.et_pb_blurb h6.et_pb_module_header a{font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif;font-weight:700;font-size:29px;line-height:0.8em}.et_pb_column_4{padding-top:30px;padding-bottom:30px}.et_pb_blurb_0 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_blurb_0.et_pb_blurb h4,.et_pb_blurb_0.et_pb_blurb h4 a,.et_pb_blurb_0.et_pb_blurb h1.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h1.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h2.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h2.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h3.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h3.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h5.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h5.et_pb_module_header a,.et_pb_blurb_0.et_pb_blurb h6.et_pb_module_header,.et_pb_blurb_0.et_pb_blurb h6.et_pb_module_header a{font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif;font-weight:700;font-size:29px;line-height:0.8em}.et_pb_column_3{padding-top:30px;padding-bottom:30px}.et_pb_row_2{max-width:80%}.et_pb_section_2.et_pb_section{padding-top:9px;padding-right:0px;padding-bottom:2px;padding-left:0px}.et_pb_text_1 h1{font-family:'Bad Script',handwriting;font-weight:700;letter-spacing:1px;text-align:center}.et_pb_text_1{font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif;background-color:rgba(0,0,0,0)}.et_pb_row_1.et_pb_row{padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_section_1.et_pb_section{padding-top:0px;padding-bottom:0px;margin-bottom:0px;background-color:rgba(224,124,118,0.43)!important}.et_pb_button_0{transform:scaleX(0.9) scaleY(0.9) skewX(1deg) skewY(1deg);transform-origin:}.et_pb_row_5.et_pb_row{margin-bottom:0px!important;padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_button_0.et_animated.transformAnim{animation-name:et_pb_slide_left_et_pb_button_0}@keyframes et_pb_slide_left_et_pb_button_0{0%{transform:scaleX(0.9) scaleY(0.9) translateX(calc(-100% + 0%)) translateY(0%) skewX(1deg) skewY(1deg)}100%{opacity:1;transform:scaleX(0.9) scaleY(0.9) skewX(1deg) skewY(1deg)}}body #page-container .et_pb_button_0:hover:after{color:}body #page-container .et_pb_button_0{color:#e02b20!important;border-width:4px!important;border-color:#ede900;border-radius:15px;letter-spacing:3px;font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif!important;font-weight:700!important}.et_pb_button_0_wrapper{margin-top:0px!important;margin-bottom:6px!important;margin-left:49px!important}.et_pb_button_0_wrapper .et_pb_button_0,.et_pb_button_0_wrapper .et_pb_button_0:hover{padding-left:21px!important}.et_pb_text_0 h6{font-weight:600;font-style:italic;text-transform:uppercase;font-size:36px;color:#e02b20!important;letter-spacing:3px;line-height:1.4em;text-shadow:0.08em 0.08em 0.08em rgba(0,0,0,0.4)}.et_pb_text_0 h5{font-weight:600;font-style:italic;text-transform:uppercase;font-size:36px;color:#e02b20!important;letter-spacing:3px;line-height:1.4em;text-shadow:0.08em 0.08em 0.08em rgba(0,0,0,0.4)}.et_pb_text_0 h4{font-weight:600;font-style:italic;text-transform:uppercase;font-size:36px;color:#e02b20!important;letter-spacing:3px;line-height:1.4em;text-shadow:0.08em 0.08em 0.08em rgba(0,0,0,0.4)}.et_pb_text_0 h3{font-weight:600;font-style:italic;text-transform:uppercase;font-size:36px;color:#e02b20!important;letter-spacing:3px;line-height:1.4em;text-shadow:0.08em 0.08em 0.08em rgba(0,0,0,0.4)}.et_pb_text_0 h2{font-weight:600;font-style:italic;text-transform:uppercase;font-size:36px;color:#e02b20!important;letter-spacing:3px;line-height:1.4em;text-shadow:0.08em 0.08em 0.08em rgba(0,0,0,0.4)}.et_pb_text_0 h1{font-weight:600;font-style:italic;text-transform:uppercase;font-size:36px;color:#e02b20!important;letter-spacing:3px;line-height:1.4em;text-shadow:0.08em 0.08em 0.08em rgba(0,0,0,0.4)}.et_pb_blurb_1 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_column_5{padding-top:30px;padding-bottom:30px}.et_pb_blurb_2.et_pb_blurb h4,.et_pb_blurb_2.et_pb_blurb h4 a,.et_pb_blurb_2.et_pb_blurb h1.et_pb_module_header,.et_pb_blurb_2.et_pb_blurb h1.et_pb_module_header a,.et_pb_blurb_2.et_pb_blurb h2.et_pb_module_header,.et_pb_blurb_2.et_pb_blurb h2.et_pb_module_header a,.et_pb_blurb_2.et_pb_blurb h3.et_pb_module_header,.et_pb_blurb_2.et_pb_blurb h3.et_pb_module_header a,.et_pb_blurb_2.et_pb_blurb h5.et_pb_module_header,.et_pb_blurb_2.et_pb_blurb h5.et_pb_module_header a,.et_pb_blurb_2.et_pb_blurb h6.et_pb_module_header,.et_pb_blurb_2.et_pb_blurb h6.et_pb_module_header a{font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif;font-weight:700;font-size:29px;line-height:0.8em}.et_pb_button_1_wrapper{margin-top:0px!important;margin-bottom:6px!important;margin-left:49px!important}.et_pb_row_5{border-radius:15px 15px 15px 15px;overflow:hidden;border-width:4px;border-color:rgba(224,15,4,0.87)}.et_pb_section_4.et_pb_section{padding-top:38px;padding-right:5px;padding-bottom:2px;padding-left:0px}.et_pb_text_2 h1{font-family:'Bad Script',handwriting;font-weight:700;letter-spacing:1px;text-align:center}.et_pb_text_2{font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif;background-color:rgba(0,0,0,0)}.et_pb_row_4.et_pb_row{padding-top:27px;padding-right:0px;padding-bottom:10px;padding-left:0px}.et_pb_section_3.et_pb_section{padding-top:0px;padding-bottom:0px;margin-bottom:0px;background-color:rgba(224,124,118,0.43)!important}.et_pb_button_1,.et_pb_button_1:after{transition:all 300ms ease 0ms}.et_pb_button_1{transform:scaleX(0.9) scaleY(0.9) skewX(1deg) skewY(1deg);transform-origin:}.et_pb_button_1.et_animated.transformAnim{animation-name:et_pb_slide_left_et_pb_button_1}@keyframes et_pb_slide_left_et_pb_button_1{0%{transform:scaleX(0.9) scaleY(0.9) translateX(calc(-100% + 0%)) translateY(0%) skewX(1deg) skewY(1deg)}100%{opacity:1;transform:scaleX(0.9) scaleY(0.9) skewX(1deg) skewY(1deg)}}body #page-container .et_pb_button_1:hover:after{color:}body #page-container .et_pb_button_1{color:#e02b20!important;border-width:4px!important;border-color:#ede900;border-radius:15px;letter-spacing:3px;font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif!important;font-weight:700!important}.et_pb_button_1_wrapper .et_pb_button_1,.et_pb_button_1_wrapper .et_pb_button_1:hover{padding-left:21px!important}.et_pb_blurb_2 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_blurb_5 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_blurb_5.et_pb_blurb h4,.et_pb_blurb_5.et_pb_blurb h4 a,.et_pb_blurb_5.et_pb_blurb h1.et_pb_module_header,.et_pb_blurb_5.et_pb_blurb h1.et_pb_module_header a,.et_pb_blurb_5.et_pb_blurb h2.et_pb_module_header,.et_pb_blurb_5.et_pb_blurb h2.et_pb_module_header a,.et_pb_blurb_5.et_pb_blurb h3.et_pb_module_header,.et_pb_blurb_5.et_pb_blurb h3.et_pb_module_header a,.et_pb_blurb_5.et_pb_blurb h5.et_pb_module_header,.et_pb_blurb_5.et_pb_blurb h5.et_pb_module_header a,.et_pb_blurb_5.et_pb_blurb h6.et_pb_module_header,.et_pb_blurb_5.et_pb_blurb h6.et_pb_module_header a{font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif;font-weight:700;font-size:29px;line-height:0.8em}.et_pb_column_8{padding-top:30px;padding-bottom:30px}.et_pb_blurb_4 .et-pb-icon{color:rgba(239,14,51,0.83)}.et_pb_blurb_4.et_pb_blurb h4,.et_pb_blurb_4.et_pb_blurb h4 a,.et_pb_blurb_4.et_pb_blurb h1.et_pb_module_header,.et_pb_blurb_4.et_pb_blurb h1.et_pb_module_header a,.et_pb_blurb_4.et_pb_blurb h2.et_pb_module_header,.et_pb_blurb_4.et_pb_blurb h2.et_pb_module_header a,.et_pb_blurb_4.et_pb_blurb h3.et_pb_module_header,.et_pb_blurb_4.et_pb_blurb h3.et_pb_module_header a,.et_pb_blurb_4.et_pb_blurb h5.et_pb_module_header,.et_pb_blurb_4.et_pb_blurb h5.et_pb_module_header a,.et_pb_blurb_4.et_pb_blurb h6.et_pb_module_header,.et_pb_blurb_4.et_pb_blurb h6.et_pb_module_header a{font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif;font-weight:700;font-size:29px;line-height:0.8em}.et_pb_column_7{padding-top:30px;padding-bottom:30px}.et_pb_blurb_3 .et-pb-icon{font-size:108px;color:rgba(239,14,51,0.83)}.et_pb_blurb_3.et_pb_blurb h4,.et_pb_blurb_3.et_pb_blurb h4 a,.et_pb_blurb_3.et_pb_blurb h1.et_pb_module_header,.et_pb_blurb_3.et_pb_blurb h1.et_pb_module_header a,.et_pb_blurb_3.et_pb_blurb h2.et_pb_module_header,.et_pb_blurb_3.et_pb_blurb h2.et_pb_module_header a,.et_pb_blurb_3.et_pb_blurb h3.et_pb_module_header,.et_pb_blurb_3.et_pb_blurb h3.et_pb_module_header a,.et_pb_blurb_3.et_pb_blurb h5.et_pb_module_header,.et_pb_blurb_3.et_pb_blurb h5.et_pb_module_header a,.et_pb_blurb_3.et_pb_blurb h6.et_pb_module_header,.et_pb_blurb_3.et_pb_blurb h6.et_pb_module_header a{font-family:'Didact Gothic',Helvetica,Arial,Lucida,sans-serif;font-weight:700;font-size:29px;line-height:0.8em}.et_pb_column_6{padding-top:30px;padding-bottom:30px}.et_pb_row_3{max-width:80%}.et_pb_button_2,.et_pb_button_2:after{transition:all 300ms ease 0ms}.et_pb_row_2.et_pb_row{margin-left:auto!important;margin-right:auto!important;padding-top:0;padding-right:0px;padding-bottom:0;padding-left:0px}.et_pb_row_3.et_pb_row{margin-left:auto!important;margin-right:auto!important;padding-top:0;padding-right:0px;padding-bottom:0;padding-left:0px}@media only screen and (max-width:980px){.et_pb_button_0_wrapper{margin-top:0px!important;margin-left:212px!important}.et_pb_text_2 h1{font-size:20px;line-height:1.3em;letter-spacing:2px}body #page-container .et_pb_button_2:after{display:inline-block;opacity:0}body #page-container .et_pb_button_2:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_button_2{padding-left:1em;padding-right:1em}.et_pb_button_2_wrapper{margin-top:0px!important;margin-left:212px!important}.et_pb_row_14.et_pb_row{padding-right:10px!important;padding-left:10px!important}.et_pb_row_13.et_pb_row{padding-right:10px!important;padding-left:10px!important}.et_pb_row_12.et_pb_row{padding-right:10px!important;padding-left:10px!important}.et_pb_row_11.et_pb_row{padding-right:10px!important;padding-left:10px!important}.et_pb_row_10.et_pb_row{padding-right:10px!important;padding-left:10px!important}.et_pb_row_9.et_pb_row{padding-right:10px!important;padding-left:10px!important}.et_pb_row_8.et_pb_row{padding-right:10px!important;padding-left:10px!important}.et_pb_row_7.et_pb_row{padding-right:10px!important;padding-left:10px!important}.et_pb_row_6.et_pb_row{padding-right:10px!important;padding-left:10px!important}.et_pb_row_5.et_pb_row{padding-right:10px!important;padding-left:10px!important}body #page-container .et_pb_button_1:hover:after{opacity:1}body #page-container .et_pb_button_0{padding-left:1em;padding-right:1em}body #page-container .et_pb_button_1:after{display:inline-block;opacity:0}body #page-container .et_pb_button_1:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_button_1{padding-left:1em;padding-right:1em}.et_pb_button_1_wrapper{margin-top:0px!important;margin-left:212px!important}.et_pb_blurb_5 .et_pb_main_blurb_image .et_pb_image_wrap{max-width:60%}.et_pb_blurb_4 .et_pb_main_blurb_image .et_pb_image_wrap{max-width:60%}.et_pb_blurb_3 .et_pb_main_blurb_image .et_pb_image_wrap{max-width:60%}.et_pb_blurb_2 .et_pb_main_blurb_image .et_pb_image_wrap{max-width:60%}.et_pb_blurb_1 .et_pb_main_blurb_image .et_pb_image_wrap{max-width:60%}.et_pb_blurb_0 .et_pb_main_blurb_image .et_pb_image_wrap{max-width:60%}.et_pb_text_1 h1{font-size:20px;line-height:1.3em;letter-spacing:2px}body #page-container .et_pb_button_0:hover:after{opacity:1}body #page-container .et_pb_button_0:after{display:inline-block;opacity:0}body #page-container .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_button_2:hover:after{opacity:1}}@media only screen and (max-width:767px){.et_pb_button_0_wrapper .et_pb_button_0,.et_pb_button_0_wrapper .et_pb_button_0:hover{padding-left:21px!important}.et_pb_text_2 h1{font-size:20px;line-height:1.5em;letter-spacing:0px}body #page-container .et_pb_button_2:after{display:inline-block;opacity:0}body #page-container .et_pb_button_2:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_button_2{padding-left:1em;padding-right:1em}.et_pb_button_2_wrapper{margin-left:68px!important}.et_pb_button_2_wrapper .et_pb_button_2,.et_pb_button_2_wrapper .et_pb_button_2:hover{padding-left:21px!important}.et_pb_row_13.et_pb_row{margin-bottom:0px!important}.et_pb_row_11.et_pb_row{margin-bottom:0px!important}.et_pb_row_9.et_pb_row{margin-bottom:0px!important}.et_pb_row_7.et_pb_row{margin-bottom:0px!important}.et_pb_row_5.et_pb_row{margin-bottom:0px!important}body #page-container .et_pb_button_1:hover:after{opacity:1}.et_pb_button_0_wrapper{margin-left:68px!important}body #page-container .et_pb_button_1:after{display:inline-block;opacity:0}body #page-container .et_pb_button_1:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_button_1{padding-left:1em;padding-right:1em}.et_pb_button_1_wrapper{margin-left:68px!important}.et_pb_button_1_wrapper .et_pb_button_1,.et_pb_button_1_wrapper .et_pb_button_1:hover{padding-left:21px!important}.et_pb_text_1 h1{font-size:20px;line-height:1.5em;letter-spacing:0px}body #page-container .et_pb_button_0:hover:after{opacity:1}body #page-container .et_pb_button_0:after{display:inline-block;opacity:0}body #page-container .et_pb_button_0:hover{padding-left:0.7em;padding-right:2em}body #page-container .et_pb_button_0{padding-left:1em;padding-right:1em}body #page-container .et_pb_button_2:hover:after{opacity:1}}</style>	<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
	

<?php
get_footer();
?>
