<?php
   /**********
   	Template Name: Sign-Up
   	**********/
   	error_reporting(0);
   	get_header();

     session_start();
     ?>
     <style type="text/css">
      #ui-datepicker-div {
        z-index: 999 !important;
        background: #cd5c5c;
      }
    @media (max-width: 767px){
      form p.et_pb_contact_field_half{
        width: 100% !important;
      }
      input{
        border-radius: 10px !important;
      }
    }
    </style>

    <div id="main-content">
     <div class="container">
      <div id="content-area" class="clearfix">
       <div id="left-area">
        <div class="entry-content">
            <div class="et_pb_row et_pb_row_0">
                <h1 class="et_pb_contact_main_title" style="text-align: center; color: #e02b20 !important; font-weight: 600; font-size: 36px; letter-spacing: 3px; line-height: 1.4em; text-shadow: 0.08em 0.08em 0.08em rgba(0,0,0,0.4);">SIGN UP</h1> 
                <h1 class="et_pb_contact_main_title" id="js_message" style="display: none;background-color: #cd5c5c;text-align: center; margin-bottom: 3%;padding: 10px;color: #fff;"></h1>
                <?php  if($_SESSION['success_msg']!=''){ ?>
                  <h1 class="et_pb_contact_main_title" id="message" style="background-color: #cd5c5c;text-align: center; margin-bottom: 3%;padding: 10px;color: #fff;">
                    <!-- <span id="js_message"></span> -->
                    <?php
                    echo $_SESSION['success_msg']; 
                    echo "<script>
                              localStorage.setItem('final_msg', 'Registered');
                              localStorage.removeItem('js_message'); 
                              localStorage.removeItem('temp_user');
                              localStorage.removeItem('last_upload_video');
                          </script>"; 


                    unset($_SESSION['success_msg']);
                    ?></h1><?php } ?>
                    <div class="et-pb-contact-message"></div>
                    <div class="et_pb_contact">
                       <form class="et_pb_contact_form clearfix" method="post" action="<?php echo get_template_directory_uri(); ?>/queries.php">
                        <p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_half" >
                         <label for="et_pb_contact_full_name_0" class="et_pb_contact_form_label">Full Name</label>
                         <input type="text" id="et_pb_contact_full_name_0" class="input" name="name"  placeholder="Full Name" value="" required> 
                       </p>
                       <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half et_pb_contact_field_last">
                         <label for="et_pb_contact_alias_name_0" class="et_pb_contact_form_label">Alias Name</label>
                         <input type="text" id="et_pb_contact_alias_name_0" pattern="[a-zA-Z0-9]+" maxlength="15" class="input" name="username" placeholder="Alias Name" value=""  required> <span id="uname_response"></span>
                       </p>
                       <p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half" data-id="email" data-type="email">
                         <label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Email Address</label>
                         <input type="text" id="et_pb_contact_email_0" class="input" name="email" data-required_mark="required"  placeholder="Email Address" value="" required><span id="email_response"></span>
                       </p>
                       <p class="et_pb_contact_field et_pb_contact_field_3 et_pb_contact_field_half et_pb_contact_field_last" data-id="d.o.b." data-type="date">
                         <label for="et_pb_contact_d.o.b._0" class="et_pb_contact_form_label">D.O.B.</label>
                         <input type="text" id="dob" class="input" name="dob"  placeholder="D.O.B." value="" required>
                       </p>
                       <p class="et_pb_contact_field et_pb_contact_field_4 et_pb_contact_field_half" data-id="password" data-type="email">

                       </p>
                       <p class="et_pb_contact_field et_pb_contact_field_5 et_pb_contact_field_half et_pb_contact_field_last" data-id="coupon_code" data-type="text">
                         <label for="et_pb_contact_coupon_code_0" class="et_pb_contact_form_label">Coupon Code</label>
                         <input type="text" id="et_pb_contact_coupon_code_0" class="input" value="" name="coupon" placeholder="Coupon Code    (optional)"> 
                       </p>
                       <div class="et_contact_bottom_container" style="width: 100%;">
                         <input id="sighup-btn" type="submit" class="et_pb_contact_submit et_pb_button" name="signup" style="color: #e02b20 !important;
                         border-color: #e02b20;
                         border-radius: 15px;
                         background-color: initial;" value="Sign Up">
                         <input type="hidden" name="temp_user" id="temp_user" value="">
                         <input type="hidden" name="u_name" id="u_name" value="1">
                         <input type="hidden" name="e_name" id="e_name" value="1">
                         <input type="hidden" name="last_upload_video" id="last_upload_video" value="">
                       </div>
                     </form>
                   </div>
                 </div>

                 <div class="et_pb_row et_pb_row_2">
                  <div class="et_pb_column et_pb_column_4_4 et_pb_column_2    et_pb_css_mix_blend_mode_passthrough et-last-child">


                    <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">


                      <div class="et_pb_text_inner">
                        <p style="text-align: center;"><a href="/sign-in/">Already a Member?  Sign In&nbsp;Here</a><br>
                          <a href="#">Are you a Recruiter?</a></p>
                        </div>
                      </div> <!-- .et_pb_text -->
                    </div> <!-- .et_pb_column -->


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>/Admin/assets/date_picker/jquery.datetimepicker.css"> -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>  
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
      var d = new Date();
      var year = d.getFullYear() - 18;
      d.setFullYear(year);
      $('#dob').datepicker({
        dateFormat: 'dd-mm-yy' ,
        changeYear: true, 
        changeMonth: true, 
        yearRange: '<?php echo date('Y')-150; ?>:' + year + '',
        defaultDate: d
      });
     /*$('#dob').datepicker({
        changeMonth: true,
          format:'d/m/Y',
        changeYear: true,
        yearRange: '<?php echo date('Y')-150; ?>:<?php echo date('Y')-18; ?>',
        scrollMonth : false
      });*/
    </script>
    <!-- <script type="text/javascript" src="<?php echo site_url(); ?>/Admin/assets/date_picker/build/jquery.datetimepicker.full.js"></script> -->
    <script type="text/javascript">

      $("#et_pb_contact_email_0").blur(function(){
        var uname = $(this).val().trim();
        if(uname != ''){
         $.ajax({
          url: '../../Admin/script/checking.php',
          type: 'post',
          data: {name:uname},
          success: function(response){
            if(response > 0){
              $("#email_response").text("Email Already in use.").css("color", "red");
              $('#e_name').attr('value', "Exist");
              disable_btn();
            }else{
              $("#email_response").text("Email Available").css("color", "green");
              $('#e_name').attr('value', "1");
              disable_btn();
            }

          }
        });
       }
     });
      $("#et_pb_contact_alias_name_0").blur(function(){
	   	// alert();
      var uname = $(this).val().trim();

      if(uname != ''){

         // $("#uname_response").show();

         $.ajax({
          url: '../../Admin/script/checking.php',
          type: 'post',
          data: {name:uname},
          success: function(response){

            if(response > 0){
              $("#uname_response").text("Username Already in use.").css("color", "red");
              $('#u_name').attr('value', "Exist");

              disable_btn();                                               // $('#submit').prop('disabled', true);

                   }else{
                    $("#uname_response").text("Username Available").css("color", "green");
                    $('#u_name').attr('value', "1");


                 }
disable_btn();
               }

             });
       }
     });


      document.getElementById("js_message").innerHTML = localStorage.getItem("js_message");

      document.getElementById("temp_user").value = localStorage.getItem("temp_user");
      document.getElementById("last_upload_video").value = localStorage.getItem("last_upload_video");


      $(document).ready(function(){
        if(localStorage.getItem("js_message")!=null){
          $('#js_message').show();
        }
       /* if($('#js_message').html()!=''){
          
          localStorage.removeItem('js_message'); 
          localStorage.removeItem('temp_user'); 
          localStorage.removeItem('last_upload_video');
        }*/
     final_msg = localStorage.getItem("final_msg");
       if(final_msg=='Registered'){
        <?php //$_SESSION['success_msg']=''; ?>
        $('#message').hide();
       }
      });


function disable_btn(){
    u_name = $('#u_name').attr('value');
  e_name = $('#e_name').attr('value');
  console.log(u_name);
  console.log(e_name);
  if(u_name!='1' || e_name!='1'){
    $('#sighup-btn').prop('disabled',true);
  }else if(u_name=='1' && e_name=="1"){
    $('#sighup-btn').prop('disabled',false);
  }
}
    </script>
    <?php


    get_footer();

