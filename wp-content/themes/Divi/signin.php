<?php
   /**********
   	Template Name: Sign-In
   	**********/

   	get_header();
session_start();
   	?>

<style>
   @media (max-width: 767px){

   form p.et_pb_contact_field_half{
      width: 100% !important;
   }
    input{
        border-radius: 10px !important;
      }
}
</style>
   	<div id="main-content">
   		<div class="container">
   			<div id="content-area" class="clearfix">
   				<div id="left-area">
                  <div class="sign_in">
   					<div class="entry-content">
   						<div class="et_pb_row et_pb_row_1">
   									<h1 class="et_pb_contact_main_title" style="text-align: center; color: #e02b20 !important; font-weight: 600; font-size: 36px; letter-spacing: 3px; line-height: 1.4em; text-shadow: 0.08em 0.08em 0.08em rgba(0,0,0,0.4);">SIGN IN</h1>
   									<h1 class="et_pb_contact_main_title"><?php if(isset($_SESSION['msg']) || isset($_SESSION['success_msg'])){ echo $_SESSION['msg'].$_SESSION['success_msg'];  }
                             unset($_SESSION['msg']);
                              unset($_SESSION['success_msg']);
                              ?></h1>
   									<div class="et-pb-contact-message"></div>

   									<div class="et_pb_contact">
   										<form class="et_pb_contact_form clearfix" method="post" action="<?php echo get_template_directory_uri(); ?>/../../../Admin/authen.php">
   											<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half" data-id="email" data-type="email">


   												<label for="et_pb_contact_email_1" class="et_pb_contact_form_label">Email Address</label>
   												<input type="text" id="et_pb_contact_email_1" class="input" value="" name="uname" data-required_mark="required" data-field_type="email" data-original_id="email" placeholder="Email Address">
   											</p><p class="et_pb_contact_field et_pb_contact_field_3 et_pb_contact_field_half et_pb_contact_field_last" data-id="password" data-type="input">


   											<label for="et_pb_contact_password_1" class="et_pb_contact_form_label">Password</label>
   											<input type="password" id="et_pb_contact_password_1" class="input" value="" name="password" data-required_mark="required" data-field_type="input" data-original_id="password" placeholder="Password">
   										</p>
   										<input type="hidden" value="et_contact_proccess" name="et_pb_contactform_submit_1">
   										<input type="text" value="" name="et_pb_contactform_validate_1" class="et_pb_contactform_validate_field">
   										<div class="et_contact_bottom_container" style="width: 100%;">
   											
   											<button type="submit" class="et_pb_contact_submit et_pb_button" style="color: #e02b20 !important;
                                             border-color: #e02b20;
                                             border-radius: 15px;
                                             background-color: initial;">Sign In</button>
   										</div>
   									
   									</form>
   								</div> <!-- .et_pb_contact -->
   							</div> <!-- .et_pb_contact_form_container -->



   						</div> <!-- .et_pb_column -->
   						<div class="et_pb_row et_pb_row_2">
   							<div class="et_pb_column et_pb_column_4_4 et_pb_column_2    et_pb_css_mix_blend_mode_passthrough et-last-child">


   								<div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">


   									<div class="et_pb_text_inner">
   										<p style="text-align: center;"><a href="/sign-up/">New to MyVCV.Asia?  Sign Up&nbsp;Here</a>
                                    <a class="display:block" href="javascript:;" id="forgot_password_show"><br>
   											Forgot Password?</a><br>
   											<a href="#">Are you a Recruiter?</a></p>
   										</div>
   									</div> <!-- .et_pb_text -->
   								</div> <!-- .et_pb_column -->


   							</div>
                     </div>
                     <div class="forgot_password" style="display: none;">
                        <div class="entry-content">
                            <div class="et_pb_row et_pb_row_1">
                              <h1 class="et_pb_contact_main_title" style="text-align: center; color: #e02b20 !important; font-weight: 600; font-size: 36px; letter-spacing: 3px; line-height: 1.4em; text-shadow: 0.08em 0.08em 0.08em rgba(0,0,0,0.4);">Forgot Password</h1>
                              <h1 class="et_pb_contact_main_title"><?php// echo $_SESSION['msg']; 
                                  //  unset($_SESSION['msg']);
                              ?></h1>
                              <div class="et-pb-contact-message"></div>

                              <div class="et_pb_contact">
                                 <form class="et_pb_contact_form clearfix" method="post" action="<?php echo get_template_directory_uri(); ?>/queries.php" autocomplete="off">
                                    <p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_full" data-id="email" data-type="email" style="text-align: center;">


                                       <label for="et_pb_contact_email_1" class="et_pb_contact_form_label">Email Address</label>
                                       <input type="text" style="width:50%;"  id="et_pb_contact_email_1" class="input" value="" name="uname" data-required_mark="required" data-field_type="email" data-original_id="email" placeholder="Email Address">
                                       <br>
                                       <br>
                                        <button type="submit" name="forgot_password" class="et_pb_contact_submit et_pb_button" style="color: #e02b20 !important;
                                             border-color: #e02b20;
                                             border-radius: 15px;
                                             background-color: initial;">Submit</button>
                                    </p>
                                    
                              </form>
                           </div>
                           </div> <!-- .et_pb_contact -->
                        </div> <!-- .et_pb_contact_form_container -->
                     <div class="et_pb_row et_pb_row_2">
                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_2    et_pb_css_mix_blend_mode_passthrough et-last-child">


                           <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">


                              <div class="et_pb_text_inner">
                                 <p style="text-align: center;"><a href="https://www.myvcv.in/sign-up/">New to MyVCV.Asia?  Sign Up&nbsp;Here</a>
                                 </div>
                              </div> <!-- .et_pb_text -->
                           </div> <!-- .et_pb_column -->


                        </div>
                     </div>
   						</div>
   					</div>
   				</div>
   			</div>
   		</div>
   	</div>

   	<script type="text/javascript">
       $('#forgot_password_show').click(function(){
         $('.forgot_password').show();
         $('.sign_in').hide();
       });
      </script>
   </body>

   <?php
    unset($_SESSION['msg']);
   get_footer();
   unset($_SESSION['msg']);
   ?>
   </html>