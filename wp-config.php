<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_myvcv' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'FORCE_SSL_ADMIN' , true);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ayW2ykflghA2i7PkUIfzPOpzCu9XFhmIzNXxCFecsU9bweF3kfoWFxhyeWLrup8J');
define('SECURE_AUTH_KEY',  '2h0Y5niDjsQP8FPO2U2oDSL7QmkPaNL82lGhU9gNwOK5iRxs23j4nAciyIWegx0f');
define('LOGGED_IN_KEY',    'CPhh35MpAij7mA8jFUMXl984CpwNogybbMhgSnbfeFI8HkbCeyLMZKpuU2nJ5WvJ');
define('NONCE_KEY',        'zKOatsiGpr8vro0Frznji6hdGbdBlxSeYRiPx5nLVLETcj68T7x6mwz6ENYQmQkl');
define('AUTH_SALT',        'v4Svf8esW6Ih6yobQNklfbZzftQlD4tzIUbTdZzUKDSDUhx8BhG5AUX0PIcPnzMn');
define('SECURE_AUTH_SALT', 'JlaJEUzhVU7ebdF6oMaJWjFC9emLmlj5uqs61KrC3bd8FgJ05MEFJqRJMrneO8rb');
define('LOGGED_IN_SALT',   'bf4Pi7dPDYZSkh7XQgh4gqRf3o8ej0AtyiWbP0AoSG1AJ7Cp3cv3wJ9d4nmTCnRQ');
define('NONCE_SALT',       'UR4Ufm8TpYPeQtXCMDdt24Mwzp82WuK83C7YP18aNLboSbzlnC0ZijqBbPOWjtq9');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

/*================*/

include('Admin/visitors.php');
/*================*/
